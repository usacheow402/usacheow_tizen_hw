var backEventListener = null;

var unregister = function() {
    if ( backEventListener !== null ) {
        document.removeEventListener( 'tizenhwkey', backEventListener );
        backEventListener = null;
        window.tizen.application.getCurrentApplication().exit();
    }
}

//Initialize function
var init = function () {
    // register once
    if ( backEventListener !== null ) {
        return;
    }
    
    // TODO:: Do your initialization job
    console.log("init() called");
    
    var backEvent = function(e) {
        if ( e.keyName == "back" ) {
            try {
                if ( $.mobile.urlHistory.activeIndex <= 0 ) {
                    // if first page, terminate app
                    unregister();
                } else {
                    // move previous page
                    $.mobile.urlHistory.activeIndex -= 1;
                    $.mobile.urlHistory.clearForward();
                    window.history.back();
                }
            } catch( ex ) {
                unregister();
            }
        }
    }

    $("#drama").bind('click', function(event, ui){
    	getFilms("drama");
    });
    $("#comedy").bind('click', function(event, ui){
    	getFilms("comedy");
    });
    $("#actions").bind('click', function(event, ui){
    	getFilms("action");
    });
    
    $("#add_drama").bind('click', function(event, ui){
    	drama.id = $("#year").val();
    	drama.title = $("#title").val();
    	sendDrama(drama);
    });

    // add eventListener for tizenhwkey (Back Button)
    document.addEventListener( 'tizenhwkey', backEvent );
    backEventListener = backEvent;
};

function getFilms(url){
	$.ajax({
		url: "http://10.17.10.116:3000/" + url,
		type: "GET",
		dataType: "json", 
		success: function(data){
			showFilms(data)
		},
		error: function(data){
			alert("Error")
		}
	});
}

function sendDrama(drama){
	$.ajax({
		url: "http://10.17.10.116:3000/drama",
		type: "POST",
		data: drama, 
		success: function(){
			getDrama()
		},
		error: function(data){
			alert("Error")
		}
	});
}

function showFilms(posts){
	$('#posts').empty();
	posts.forEach(function(item, i, arr){
		$('#posts').append('<li>' + item.title + '</li>');
	});
	$('#posts').listview('refresh');
}


$(document).bind( 'pageinit', init );
$(document).unload( unregister );
