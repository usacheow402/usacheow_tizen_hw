S/W Version Information
Model: SM-Z300H
Tizen-Version: 2.4.0.3
Build-Number: Z300HDDU0BPI2
Build-Date: 2016.09.30 15:37:12

Crash Information
Process Name: uibuildersingleview
PID: 3290
Date: 2016-12-07 20:19:01+0530
Executable File Path: /opt/usr/apps/org.example.uibuildersingleview/bin/uibuildersingleview
Signal: 11
      (SIGSEGV)
      si_code: -6
      signal sent by tkill (sent by pid 3290, uid 5000)

Register Information
r0   = 0xb8c8e208, r1   = 0x0000001c
r2   = 0x0000004c, r3   = 0x0000001c
r4   = 0xb8c170a0, r5   = 0xb8c891e0
r6   = 0x80012493, r7   = 0xb6e2c488
r8   = 0xb8c8b1d8, r9   = 0xb8c8e208
r10  = 0x80012493, fp   = 0xb8c804f0
ip   = 0xb6a3e4d4, sp   = 0xbe99af98
lr   = 0xb6a1560d, pc   = 0xb67516f2
cpsr = 0x200e0030

Memory Information
MemTotal:   987012 KB
MemFree:    190712 KB
Buffers:     41348 KB
Cached:     317256 KB
VmPeak:     130704 KB
VmSize:     129280 KB
VmLck:           0 KB
VmPin:           0 KB
VmHWM:       28604 KB
VmRSS:       25884 KB
VmData:      46040 KB
VmStk:         136 KB
VmExe:          20 KB
VmLib:       35600 KB
VmPTE:          96 KB
VmSwap:          0 KB

Threads Information
Threads: 5
PID = 3290 TID = 3290
3290 3291 3704 3707 3715 

Maps Information
af5e2000 afde1000 rwxp [stack:3715]
b1535000 b153d000 r-xp /usr/lib/ecore_evas/engines/extn/v-1.13/module.so
b154f000 b1d4e000 rwxp [stack:3707]
b1d4e000 b1d4f000 r-xp /usr/lib/edje/modules/feedback/v-1.13/module.so
b1d5f000 b1d73000 r-xp /usr/lib/edje/modules/elm/v-1.13/module.so
b1d87000 b1d88000 r-xp /usr/lib/libX11-xcb.so.1.0.0
b1d98000 b1d9b000 r-xp /usr/lib/libxcb-sync.so.1.0.0
b1dac000 b1dad000 r-xp /usr/lib/libxshmfence.so.1.0.0
b1dbd000 b1dbf000 r-xp /usr/lib/libxcb-present.so.0.0.0
b1dcf000 b1dd1000 r-xp /usr/lib/libxcb-dri3.so.0.0.0
b1de1000 b1df1000 r-xp /usr/lib/evas/modules/engines/software_x11/v-1.13/module.so
b1e01000 b1e0d000 r-xp /usr/lib/ecore_evas/engines/x/v-1.13/module.so
b1e1f000 b261e000 rwxp [stack:3704]
b281e000 b2825000 r-xp /usr/lib/libefl-extension.so.0.1.0
b2838000 b283e000 r-xp /usr/lib/bufmgr/libtbm_sprd7727.so.0.0.0
b284e000 b2853000 r-xp /opt/usr/apps/org.example.uibuildersingleview/bin/uibuildersingleview
b29a4000 b2a87000 r-xp /usr/lib/libscim-1.0.so.8.2.3
b2abe000 b2ae6000 r-xp /usr/lib/ecore_imf/modules/isf/v-1.13/module.so
b2af8000 b32f7000 rwxp [stack:3291]
b32f7000 b32f9000 r-xp /usr/lib/ecore/system/systemd/v-1.13/module.so
b3309000 b3313000 r-xp /lib/libnss_files-2.20-2014.11.so
b3324000 b332d000 r-xp /lib/libnss_nis-2.20-2014.11.so
b333e000 b334f000 r-xp /lib/libnsl-2.20-2014.11.so
b3362000 b3368000 r-xp /lib/libnss_compat-2.20-2014.11.so
b3379000 b337a000 r-xp /usr/lib/osp/libappinfo.so.1.2.2.1
b33a2000 b33a9000 r-xp /usr/lib/libminizip.so.1.0.0
b33b9000 b33be000 r-xp /usr/lib/libstorage.so.0.1
b33ce000 b342d000 r-xp /usr/lib/libmmfcamcorder.so.0.0.0
b3443000 b3457000 r-xp /usr/lib/libcapi-media-camera.so.0.1.90
b3467000 b34ab000 r-xp /usr/lib/libgstbase-1.0.so.0.405.0
b34bb000 b34c3000 r-xp /usr/lib/libgstapp-1.0.so.0.405.0
b34d3000 b3503000 r-xp /usr/lib/libgstvideo-1.0.so.0.405.0
b3516000 b35cf000 r-xp /usr/lib/libgstreamer-1.0.so.0.405.0
b35e3000 b3636000 r-xp /usr/lib/libmmfplayer.so.0.0.0
b3647000 b3662000 r-xp /usr/lib/libcapi-media-player.so.0.2.16
b3672000 b3733000 r-xp /usr/lib/libprotobuf.so.9.0.1
b3746000 b3756000 r-xp /usr/lib/libefl-assist.so.0.1.0
b3766000 b3773000 r-xp /usr/lib/libmdm-common.so.1.0.98
b3784000 b378b000 r-xp /usr/lib/libcapi-media-tool.so.0.2.2
b379b000 b37dc000 r-xp /usr/lib/libmdm.so.1.2.12
b37ec000 b37f4000 r-xp /usr/lib/lib_DNSe_NRSS_ver225.so
b3803000 b3813000 r-xp /usr/lib/lib_SamsungRec_TizenV04014.so
b3834000 b3894000 r-xp /usr/lib/libasound.so.2.0.0
b38a6000 b38a9000 r-xp /usr/lib/libpulse-simple.so.0.0.4
b38b9000 b38bc000 r-xp /usr/lib/libsyspopup_caller.so.0.1.0
b38cc000 b38d1000 r-xp /usr/lib/libmmfsoundcommon.so.0.0.0
b38e1000 b38e2000 r-xp /usr/lib/libgthread-2.0.so.0.4301.0
b38f2000 b38fd000 r-xp /usr/lib/libaudio-session-mgr.so.0.0.1
b3911000 b3918000 r-xp /usr/lib/libmmutil_imgp.so.0.0.0
b3928000 b392e000 r-xp /usr/lib/libmmutil_jpeg.so.0.0.0
b393e000 b3943000 r-xp /usr/lib/libmmfsession.so.0.0.1
b3953000 b396e000 r-xp /usr/lib/libmmfsound.so.0.1.0
b397e000 b3985000 r-xp /usr/lib/libmmfcommon.so.0.0.0
b3995000 b3998000 r-xp /usr/lib/libcapi-media-image-util.so.0.1.10
b39a9000 b39d7000 r-xp /usr/lib/libidn.so.11.5.44
b39e7000 b39fd000 r-xp /usr/lib/libnghttp2.so.5.4.0
b3a0e000 b3a18000 r-xp /usr/lib/libcares.so.2.1.0
b3a28000 b3a32000 r-xp /usr/lib/libcapi-media-sound-manager.so.0.3.32
b3a42000 b3a44000 r-xp /usr/lib/libcapi-media-wav-player.so.0.1.18
b3a54000 b3a55000 r-xp /usr/lib/libmmfkeysound.so.0.0.0
b3a65000 b3a69000 r-xp /usr/lib/libecore_ipc.so.1.13.0
b3a7a000 b3aa2000 r-xp /usr/lib/libui-extension.so.0.1.0
b3ab3000 b3adc000 r-xp /usr/lib/libturbojpeg.so
b3afc000 b3b02000 r-xp /usr/lib/libgif.so.4.1.6
b3b12000 b3b58000 r-xp /usr/lib/libcurl.so.4.3.0
b3b69000 b3b8a000 r-xp /usr/lib/libexif.so.12.3.3
b3ba5000 b3bba000 r-xp /usr/lib/libtts.so
b3bcb000 b3bd3000 r-xp /usr/lib/libfeedback.so.0.1.4
b3be3000 b3ca9000 r-xp /usr/lib/libdali-core.so.0.0.0
b3cc9000 b3dc1000 r-xp /usr/lib/libdali-adaptor.so.0.0.0
b3de0000 b3eae000 r-xp /usr/lib/libdali-toolkit.so.0.0.0
b3ec5000 b3ec7000 r-xp /usr/lib/libboost_system.so.1.51.0
b3ed7000 b3edd000 r-xp /usr/lib/libboost_chrono.so.1.51.0
b3eed000 b3f10000 r-xp /usr/lib/libboost_thread.so.1.51.0
b3f21000 b3f23000 r-xp /usr/lib/libappsvc.so.0.1.0
b3f33000 b3f35000 r-xp /usr/lib/libcapi-appfw-app-common.so.0.3.1.0
b3f46000 b3f4b000 r-xp /usr/lib/libcapi-appfw-app-control.so.0.3.1.0
b3f62000 b3f64000 r-xp /usr/lib/libosp-env-config.so.1.2.2.1
b3f74000 b3f7b000 r-xp /usr/lib/libsensord-share.so
b3f8b000 b3fa3000 r-xp /usr/lib/libsensor.so.1.1.0
b3fb4000 b3fb7000 r-xp /usr/lib/libXv.so.1.0.0
b3fc7000 b3fcc000 r-xp /usr/lib/libutilX.so.1.1.0
b3fdc000 b3fe1000 r-xp /usr/lib/libappcore-common.so.1.1
b3ff1000 b3ff8000 r-xp /usr/lib/libcapi-ui-efl-util.so.0.2.11
b400b000 b400f000 r-xp /usr/lib/libcapi-appfw-application.so.0.3.1.0
b4020000 b40fe000 r-xp /usr/lib/libCOREGL.so.4.0
b411e000 b4121000 r-xp /usr/lib/libuuid.so.1.3.0
b4131000 b4148000 r-xp /usr/lib/libblkid.so.1.1.0
b4159000 b415b000 r-xp /usr/lib/libXau.so.6.0.0
b416b000 b41b2000 r-xp /usr/lib/pulseaudio/libpulsecommon-4.0.so
b41c4000 b41ca000 r-xp /usr/lib/libjson-c.so.2.0.1
b41db000 b41df000 r-xp /usr/lib/libogg.so.0.7.1
b41ef000 b4211000 r-xp /usr/lib/libvorbis.so.0.4.3
b4221000 b4305000 r-xp /usr/lib/libvorbisenc.so.2.0.6
b4321000 b4324000 r-xp /usr/lib/libEGL.so.1.4
b4335000 b433b000 r-xp /usr/lib/libxcb-render.so.0.0.0
b434b000 b434d000 r-xp /usr/lib/libxcb-shm.so.0.0.0
b435d000 b436a000 r-xp /usr/lib/libGLESv2.so.2.0
b437b000 b43dd000 r-xp /usr/lib/libpixman-1.so.0.28.2
b43f2000 b440a000 r-xp /usr/lib/libmount.so.1.1.0
b441c000 b4430000 r-xp /usr/lib/libxcb.so.1.1.0
b4440000 b4447000 r-xp /lib/libcrypt-2.20-2014.11.so
b447f000 b4481000 r-xp /usr/lib/libiri.so
b4491000 b449c000 r-xp /usr/lib/libgpg-error.so.0.15.0
b44ad000 b44e3000 r-xp /usr/lib/libpulse.so.0.16.2
b44f4000 b4537000 r-xp /usr/lib/libsndfile.so.1.0.25
b454c000 b4561000 r-xp /lib/libexpat.so.1.5.2
b4573000 b4631000 r-xp /usr/lib/libcairo.so.2.11200.14
b4645000 b464d000 r-xp /usr/lib/libdrm.so.2.4.0
b465d000 b4660000 r-xp /usr/lib/libdri2.so.0.0.0
b4670000 b46be000 r-xp /usr/lib/libssl.so.1.0.0
b46d3000 b46df000 r-xp /usr/lib/libeeze.so.1.13.0
b46f0000 b46f9000 r-xp /usr/lib/libethumb.so.1.13.0
b4709000 b470c000 r-xp /usr/lib/libecore_input_evas.so.1.13.0
b471c000 b48d3000 r-xp /usr/lib/libcrypto.so.1.0.0
b56be000 b56c7000 r-xp /usr/lib/libXi.so.6.1.0
b56d7000 b56d9000 r-xp /usr/lib/libXgesture.so.7.0.0
b56e9000 b56ed000 r-xp /usr/lib/libXtst.so.6.1.0
b56fd000 b5703000 r-xp /usr/lib/libXrender.so.1.3.0
b5713000 b5719000 r-xp /usr/lib/libXrandr.so.2.2.0
b5729000 b572b000 r-xp /usr/lib/libXinerama.so.1.0.0
b573c000 b573f000 r-xp /usr/lib/libXfixes.so.3.1.0
b574f000 b575a000 r-xp /usr/lib/libXext.so.6.4.0
b576a000 b576c000 r-xp /usr/lib/libXdamage.so.1.1.0
b577c000 b577e000 r-xp /usr/lib/libXcomposite.so.1.0.0
b578e000 b5870000 r-xp /usr/lib/libX11.so.6.3.0
b5884000 b588b000 r-xp /usr/lib/libXcursor.so.1.0.2
b589b000 b58b3000 r-xp /usr/lib/libudev.so.1.6.0
b58b5000 b58b8000 r-xp /lib/libattr.so.1.1.0
b58c8000 b58e8000 r-xp /usr/lib/libsecurity-server-commons.so.1.0.0
b58e9000 b58ee000 r-xp /usr/lib/libffi.so.6.0.2
b58ff000 b5917000 r-xp /lib/libz.so.1.2.8
b5927000 b5929000 r-xp /usr/lib/libgmodule-2.0.so.0.4301.0
b5939000 b5a0e000 r-xp /usr/lib/libxml2.so.2.9.2
b5a23000 b5abe000 r-xp /usr/lib/libstdc++.so.6.0.20
b5ada000 b5add000 r-xp /usr/lib/libSLP-db-util.so.0.1.0
b5aed000 b5b06000 r-xp /usr/lib/libpkgmgr_parser.so.0.1.0
b5b17000 b5b28000 r-xp /lib/libresolv-2.20-2014.11.so
b5b3c000 b5bb6000 r-xp /usr/lib/libgcrypt.so.20.0.3
b5bcb000 b5bcd000 r-xp /usr/lib/libecore_imf_evas.so.1.13.0
b5bdd000 b5be4000 r-xp /usr/lib/libembryo.so.1.13.0
b5bf4000 b5bfe000 r-xp /usr/lib/libecore_audio.so.1.13.0
b5c0f000 b5c27000 r-xp /usr/lib/libpng12.so.0.50.0
b5c38000 b5c5b000 r-xp /usr/lib/libjpeg.so.8.0.2
b5c7c000 b5c90000 r-xp /usr/lib/libector.so.1.13.0
b5ca1000 b5cb9000 r-xp /usr/lib/liblua-5.1.so
b5cca000 b5d21000 r-xp /usr/lib/libfreetype.so.6.11.3
b5d35000 b5d5d000 r-xp /usr/lib/libfontconfig.so.1.8.0
b5d6e000 b5d81000 r-xp /usr/lib/libfribidi.so.0.3.1
b5d92000 b5dcc000 r-xp /usr/lib/libharfbuzz.so.0.940.0
b5ddd000 b5deb000 r-xp /usr/lib/libgraphics-extension.so.0.1.0
b5dfb000 b5e03000 r-xp /usr/lib/libtbm.so.1.0.0
b5e13000 b5e20000 r-xp /usr/lib/libeio.so.1.13.0
b5e30000 b5e32000 r-xp /usr/lib/libefreet_trash.so.1.13.0
b5e42000 b5e47000 r-xp /usr/lib/libefreet_mime.so.1.13.0
b5e57000 b5e6e000 r-xp /usr/lib/libefreet.so.1.13.0
b5e80000 b5ea0000 r-xp /usr/lib/libeldbus.so.1.13.0
b5eb0000 b5ed0000 r-xp /usr/lib/libecore_con.so.1.13.0
b5ed2000 b5ed8000 r-xp /usr/lib/libecore_imf.so.1.13.0
b5ee8000 b5ef9000 r-xp /usr/lib/libemotion.so.1.13.0
b5f0a000 b5f11000 r-xp /usr/lib/libethumb_client.so.1.13.0
b5f21000 b5f30000 r-xp /usr/lib/libeo.so.1.13.0
b5f41000 b5f53000 r-xp /usr/lib/libecore_input.so.1.13.0
b5f64000 b5f69000 r-xp /usr/lib/libecore_file.so.1.13.0
b5f79000 b5f92000 r-xp /usr/lib/libecore_evas.so.1.13.0
b5fa2000 b5fbf000 r-xp /usr/lib/libeet.so.1.13.0
b5fd8000 b6020000 r-xp /usr/lib/libeina.so.1.13.0
b6031000 b6041000 r-xp /usr/lib/libefl.so.1.13.0
b6052000 b6137000 r-xp /usr/lib/libicuuc.so.51.1
b6154000 b6294000 r-xp /usr/lib/libicui18n.so.51.1
b62ab000 b62e3000 r-xp /usr/lib/libecore_x.so.1.13.0
b62f5000 b62f8000 r-xp /lib/libcap.so.2.21
b6308000 b6331000 r-xp /usr/lib/libsecurity-server-client.so.1.0.1
b6342000 b6349000 r-xp /usr/lib/libcapi-base-common.so.0.2.2
b635b000 b6392000 r-xp /usr/lib/libgobject-2.0.so.0.4301.0
b63a3000 b648e000 r-xp /usr/lib/libgio-2.0.so.0.4301.0
b64a1000 b651a000 r-xp /usr/lib/libsqlite3.so.0.8.6
b652c000 b6531000 r-xp /usr/lib/libcapi-system-info.so.0.2.1
b6541000 b654b000 r-xp /usr/lib/libvconf.so.0.2.45
b655b000 b655d000 r-xp /usr/lib/libvasum.so.0.3.1
b656d000 b656f000 r-xp /usr/lib/libttrace.so.1.1
b657f000 b6582000 r-xp /usr/lib/libiniparser.so.0
b6592000 b65b8000 r-xp /usr/lib/libpkgmgr-info.so.0.0.17
b65c8000 b65cd000 r-xp /usr/lib/libxdgmime.so.1.1.0
b65de000 b65f5000 r-xp /usr/lib/libdbus-glib-1.so.2.2.2
b6606000 b6671000 r-xp /lib/libm-2.20-2014.11.so
b6682000 b6688000 r-xp /lib/librt-2.20-2014.11.so
b6699000 b66a6000 r-xp /usr/lib/libunwind.so.8.0.1
b66dc000 b6800000 r-xp /lib/libc-2.20-2014.11.so
b6815000 b682e000 r-xp /lib/libgcc_s-4.9.so.1
b683e000 b6920000 r-xp /usr/lib/libglib-2.0.so.0.4301.0
b6931000 b695b000 r-xp /usr/lib/libdbus-1.so.3.8.12
b696c000 b69a8000 r-xp /usr/lib/libsystemd.so.0.4.0
b69aa000 b6a2d000 r-xp /usr/lib/libedje.so.1.13.0
b6a40000 b6a5e000 r-xp /usr/lib/libecore.so.1.13.0
b6a7e000 b6c05000 r-xp /usr/lib/libevas.so.1.13.0
b6c3a000 b6c4e000 r-xp /lib/libpthread-2.20-2014.11.so
b6c62000 b6e96000 r-xp /usr/lib/libelementary.so.1.13.0
b6ec5000 b6ec9000 r-xp /usr/lib/libsmack.so.1.0.0
b6ed9000 b6ee0000 r-xp /usr/lib/libprivilege-control.so.0.0.2
b6ef0000 b6ef2000 r-xp /usr/lib/libdlog.so.0.0.0
b6f02000 b6f05000 r-xp /usr/lib/libbundle.so.0.1.22
b6f15000 b6f17000 r-xp /lib/libdl-2.20-2014.11.so
b6f28000 b6f40000 r-xp /usr/lib/libaul.so.0.1.0
b6f54000 b6f59000 r-xp /usr/lib/libappcore-efl.so.1.1
b6f6a000 b6f77000 r-xp /usr/lib/liblptcp.so
b6f89000 b6f8d000 r-xp /usr/lib/libsys-assert.so
b6f9e000 b6fbe000 r-xp /lib/ld-2.20-2014.11.so
b6fcf000 b6fd4000 r-xp /usr/bin/launchpad-loader
b8a35000 b8d5d000 rw-p [heap]
be97b000 be99c000 rwxp [stack]
b6fcf000 b6fd4000 r-xp /usr/bin/launchpad-loader
b8a35000 b8d5d000 rw-p [heap]
be97b000 be99c000 rwxp [stack]
End of Maps Information

Callstack Information (PID:3290)
Call Stack Count: 1
 0: strcmp + 0x1 (0xb67516f2) [/lib/libc.so.6] + 0x756f2
End of Call Stack

Package Information
Package Name: org.example.uibuildersingleview
Package ID : org.example.uibuildersingleview
Version: 1.0.0
Package Type: tpk
App Name: uibuildersingleview
App ID: org.example.uibuildersingleview
Type: capp
Categories: 

Latest Debug Message Information
--------- beginning of /dev/log_main
_status_handler(3328) > pid(895) status(0)
12-07 20:18:27.481+0530 I/APP_CORE(  895): appcore-efl.c: __do_app(514) > [APP 895] Event: RESUME State: PAUSED
12-07 20:18:27.481+0530 I/CAPI_APPFW_APPLICATION(  895): app_main.c: _ui_app_appcore_resume(706) > app_appcore_resume
12-07 20:18:27.481+0530 E/cluster-home(  895): homescreen.cpp: OnResume(233) >  app resume
12-07 20:18:27.481+0530 E/EFL     (  330): eo<330> lib/eo/eo.c:676 _eo_call_resolve() in lib/edje/edje_object.eo.c:316: func 'edje_obj_signal_emit' (415) could not be resolved for class 'Evas_Object_Smart'.
12-07 20:18:27.511+0530 I/Tizen::System( 1290): (259) > Active app [com.samsun], current [org.exampl] 
12-07 20:18:27.511+0530 I/Tizen::Io( 1290): (729) > Entry not found
12-07 20:18:27.521+0530 I/Tizen::System( 1290): (157) > change brightness system value.
12-07 20:18:27.561+0530 I/TIZEN_N_SOUND_MANAGER(  961): sound_manager.c: sound_manager_get_volume(84) > returns : type=0, volume=8, ret=0x0
12-07 20:18:27.561+0530 E/TIZEN_N_SOUND_MANAGER(  961): sound_manager_private.c: __convert_sound_manager_error_code(142) > [sound_manager_get_volume] ERROR_NONE(0x00000000) : core frameworks error code(0x00000000)
12-07 20:18:27.561+0530 I/TIZEN_N_SOUND_MANAGER(  961): sound_manager.c: sound_manager_get_volume(84) > returns : type=4, volume=0, ret=0x0
12-07 20:18:27.561+0530 E/TIZEN_N_SOUND_MANAGER(  961): sound_manager_private.c: __convert_sound_manager_error_code(142) > [sound_manager_get_volume] ERROR_NONE(0x00000000) : core frameworks error code(0x00000000)
12-07 20:18:27.561+0530 W/AUL_AMD (  664): amd_request.c: __request_handler(906) > __request_handler: 15
12-07 20:18:27.721+0530 I/AUL_PAD ( 1521): sigchild.h: __launchpad_process_sigchld(162) > dead_pid = 2856 pgid = 2856
12-07 20:18:27.721+0530 I/AUL_PAD ( 1521): sigchild.h: __sigchild_action(143) > dead_pid(2856)
12-07 20:18:27.751+0530 I/AUL_PAD ( 1521): sigchild.h: __sigchild_action(149) > __send_app_dead_signal(0)
12-07 20:18:27.751+0530 I/AUL_PAD ( 1521): sigchild.h: __launchpad_process_sigchld(170) > after __sigchild_action
12-07 20:18:27.751+0530 E/AUL_PAD ( 1521): launchpad.c: main(698) > error reading sigchld info
12-07 20:18:27.751+0530 I/ESD     (  941): esd_main.c: __esd_app_dead_handler(1773) > pid: 2856
12-07 20:18:27.751+0530 W/AUL_AMD (  664): amd_main.c: __app_dead_handler(327) > __app_dead_handler, pid: 2856
12-07 20:18:27.751+0530 I/Tizen::App( 1290): (243) > App[org.example.uibuildersingleview] pid[2856] terminate event is forwarded
12-07 20:18:27.751+0530 I/Tizen::System( 1290): (256) > osp.accessorymanager.service provider is found.
12-07 20:18:27.751+0530 I/Tizen::System( 1290): (196) > Accessory Owner is removed [org.example.uibuildersingleview, 2856, ]
12-07 20:18:27.751+0530 I/Tizen::System( 1290): (256) > osp.system.service provider is found.
12-07 20:18:27.751+0530 I/Tizen::App( 1290): (506) > TerminatedApp(org.example.uibuildersingleview)
12-07 20:18:27.751+0530 I/Tizen::App( 1290): (512) > Not registered pid(2856)
12-07 20:18:27.751+0530 I/Tizen::System( 1290): (246) > Terminated app [org.example.uibuildersingleview]
12-07 20:18:27.751+0530 I/Tizen::Io( 1290): (729) > Entry not found
12-07 20:18:27.751+0530 E/RESOURCED(  779): resourced-dbus.c: resourced_dbus_system_hash_drop_busname(324) > Does not exist in busname hash: :1.317
12-07 20:18:27.771+0530 I/Tizen::System( 1290): (157) > change brightness system value.
12-07 20:18:27.771+0530 I/Tizen::App( 1290): (782) > Finished invoking application event listener for org.example.uibuildersingleview, 2856.
12-07 20:18:29.513+0530 E/PKGMGR_SERVER( 3487): pkgmgr-server.c: exit_server(1381) > exit_server Start [backend_status=1, queue_status=1, drm_status=1] 
12-07 20:18:29.513+0530 E/PKGMGR_SERVER( 3487): pkgmgr-server.c: main(2471) > package manager server terminated.
12-07 20:18:29.823+0530 E/EFL     (  895): ecore_x<895> lib/ecore_x/xlib/ecore_x_events.c:603 _ecore_x_event_handle_button_press() ButtonEvent:press time=8462317
12-07 20:18:30.334+0530 E/util-view(  895): util-text-image-provider.cpp: GetPixelsFromText(240) >  DEFAULT='font=SamsungOneUI:style=normal:weight=light:width=condensed style= shadow_color=#000000 font_size=32 align=center valign=center color=#f9f9f9f9 color_class=tizen text_class=tizen ellipsis=1 wrap=mixed linegap=0'
12-07 20:18:30.354+0530 E/util-view(  895): util-text-image-provider.cpp: GetPixelsFromText(240) >  DEFAULT='font=SamsungOneUI:style=normal:weight=light:width=condensed style= shadow_color=#000000 font_size=32 align=center valign=center color=#f9f9f9ff color_class=tizen text_class=tizen ellipsis=1 wrap=mixed linegap=0'
12-07 20:18:30.364+0530 E/util-view(  895): util-text-image-provider.cpp: GetPixelsFromText(240) >  DEFAULT='font=SamsungOneUI:style=normal:weight=light:width=condensed style= shadow_color=#000000 font_size=32 align=center valign=center color=#f9f9f9ff color_class=tizen text_class=tizen ellipsis=1 wrap=mixed linegap=0'
12-07 20:18:30.374+0530 E/util-view(  895): util-text-image-provider.cpp: GetPixelsFromText(240) >  DEFAULT='font=SamsungOneUI:style=normal:weight=light:width=condensed style= shadow_color=#000000 font_size=32 align=center valign=center color=#f9f9f9ff color_class=tizen text_class=tizen ellipsis=1 wrap=mixed linegap=0'
12-07 20:18:30.384+0530 E/util-view(  895): util-text-image-provider.cpp: GetPixelsFromText(240) >  DEFAULT='font=SamsungOneUI:style=normal:weight=light:width=condensed style= shadow_color=#000000 font_size=32 align=center valign=center color=#f9f9f9ff color_class=tizen text_class=tizen ellipsis=1 wrap=mixed linegap=0'
12-07 20:18:30.394+0530 E/VCONF   (  895): vconf.c: _vconf_check_install_key_list(1135) > key : memory/cluster-home/edit-state
12-07 20:18:30.394+0530 E/VCONF   (  895): vconf.c: _vconf_check_retry_err(1368) > memory/cluster-home/edit-state : check retry err (-21/2).
12-07 20:18:30.394+0530 E/VCONF   (  895): vconf.c: _vconf_set_key_filesys(1573) > _vconf_set_key_filesys(41-memory/cluster-home/edit-state) step(-21) failed(2 / No such file or directory)
12-07 20:18:30.394+0530 E/VCONF   (  895): vconf.c: _vconf_set_key(1621) > _vconf_set_key(memory/cluster-home/edit-state) step(-21) failed(2 / No such file or directory)
12-07 20:18:30.394+0530 E/VCONF   (  895): vconf.c: vconf_set_int(1717) > vconf_set_int(895) : memory/cluster-home/edit-state(1) error
12-07 20:18:30.694+0530 E/util-view(  895): util-text-image-provider.cpp: GetPixelsFromText(240) >  DEFAULT='font=SamsungOneUI:style=normal:weight=light:width=condensed style= shadow_color=#000000 font_size=34 align=center valign=center color=#f9f9f9ff color_class=tizen text_class=tizen ellipsis=1 wrap=mixed linegap=0'
12-07 20:18:30.704+0530 E/util-view(  895): util-text-image-provider.cpp: GetPixelsFromText(240) >  DEFAULT='font=SamsungOneUI:style=normal:weight=light:width=condensed style= shadow_color=#000000 font_size=34 align=center valign=center color=#f9f9f964 color_class=tizen text_class=tizen ellipsis=1 wrap=mixed linegap=0'
12-07 20:18:30.704+0530 E/util-view(  895): util-text-image-provider.cpp: GetPixelsFromText(240) >  DEFAULT='font=SamsungOneUI:style=normal:weight=light:width=condensed style= shadow_color=#000000 font_size=34 align=center valign=center color=#f9f9f9ff color_class=tizen text_class=tizen ellipsis=1 wrap=mixed linegap=0'
12-07 20:18:30.714+0530 E/util-view(  895): util-text-image-provider.cpp: GetPixelsFromText(240) >  DEFAULT='font=SamsungOneUI:style=normal:weight=light:width=condensed style= shadow_color=#000000 font_size=34 align=center valign=center color=#f9f9f964 color_class=tizen text_class=tizen ellipsis=1 wrap=mixed linegap=0'
12-07 20:18:30.714+0530 E/util-view(  895): util-text-image-provider.cpp: GetPixelsFromText(240) >  DEFAULT='font=SamsungOneUI:style=normal:weight=light:width=condensed style= shadow_color=#000000 font_size=34 align=center valign=center color=#f9f9f9ff color_class=tizen text_class=tizen ellipsis=1 wrap=mixed linegap=0'
12-07 20:18:30.724+0530 E/util-view(  895): util-text-image-provider.cpp: GetPixelsFromText(240) >  DEFAULT='font=SamsungOneUI:style=normal:weight=light:width=condensed style= shadow_color=#000000 font_size=34 align=center valign=center color=#f9f9f964 color_class=tizen text_class=tizen ellipsis=1 wrap=mixed linegap=0'
12-07 20:18:30.734+0530 E/util-view(  895): util-text-image-provider.cpp: GetPixelsFromText(240) >  DEFAULT='font=SamsungOneUI:style=normal:weight=light:width=condensed style= shadow_color=#000000 font_size=34 align=center valign=center color=#f9f9f9ff color_class=tizen text_class=tizen ellipsis=1 wrap=mixed linegap=0'
12-07 20:18:30.734+0530 E/util-view(  895): util-text-image-provider.cpp: GetPixelsFromText(240) >  DEFAULT='font=SamsungOneUI:style=normal:weight=light:width=condensed style= shadow_color=#000000 font_size=34 align=center valign=center color=#f9f9f964 color_class=tizen text_class=tizen ellipsis=1 wrap=mixed linegap=0'
12-07 20:18:32.406+0530 W/AUL_AMD (  664): amd_status.c: __app_terminate_timer_cb(421) > send SIGKILL: No such process
12-07 20:18:32.406+0530 W/AUL_AMD (  664): amd_status.c: __app_terminate_timer_cb(421) > send SIGKILL: No such process
12-07 20:18:33.857+0530 E/PKGMGR  ( 3581): pkgmgr.c: pkgmgr_client_reinstall(2020) > reinstall pkg start.
12-07 20:18:33.958+0530 E/PKGMGR_SERVER( 3583): pkgmgr-server.c: main(2414) > package manager server start
12-07 20:18:34.028+0530 E/PKGMGR_SERVER( 3583): pkgmgr-server-internal.c: _zone_set_type_and_backend(180) > tep_filepath [org.example.uibuildersingleview]
12-07 20:18:34.038+0530 E/PKGMGR_SERVER( 3583): pm-mdm.c: _get_package_info_from_file(116) > [0;31m[_get_package_info_from_file(): 116](ret < 0) access() failed. path: org.example.uibuildersingleview errno: 2 (No such file or directory)[0;m
12-07 20:18:34.038+0530 E/PKGMGR_SERVER( 3583): pkgmgr-server.c: __set_recovery_mode(234) > rev_file[/opt/share/packages/.recovery/pkgmgr/org.example.uibuildersingleview] is null
12-07 20:18:34.048+0530 E/PKGMGR  ( 3581): pkgmgr.c: pkgmgr_client_reinstall(2133) > reinstall pkg finish, ret=[35810002]
12-07 20:18:34.218+0530 I/Tizen::App( 1290): (1894) > PackageEventHandler - req: 12900002, pkg_type: tpk, pkg_name: org.example.uibuildersingleview, key: start, val: update
12-07 20:18:34.218+0530 I/Tizen::App( 1290): (1584) > Package = [org.example.uibuildersingleview], Key = [start], Value = [update], install = [1]
12-07 20:18:34.228+0530 E/PKGMGR  ( 1495): pkgmgr.c: __find_op_cbinfo(312) > tmp is NULL
12-07 20:18:34.228+0530 E/PKGMGR  ( 1495): pkgmgr.c: __find_op_cbinfo(312) > tmp is NULL
12-07 20:18:34.228+0530 W/AUL_AMD (  664): amd_appinfo.c: __amd_pkgmgrinfo_status_cb(1068) > __amd_pkgmgrinfo_start_handler
12-07 20:18:34.228+0530 I/Tizen::App( 1290): (1894) > PackageEventHandler - req: 12900002, pkg_type: tpk, pkg_name: org.example.uibuildersingleview, key: install_percent, val: 30
12-07 20:18:34.228+0530 I/Tizen::App( 1290): (119) > InstallationInProgress [30]
12-07 20:18:34.238+0530 I/Tizen::App( 1290): (1584) > Package = [org.example.uibuildersingleview], Key = [install_percent], Value = [30], install = [1]
12-07 20:18:34.238+0530 E/PKGMGR  ( 1495): pkgmgr.c: __find_op_cbinfo(312) > tmp is NULL
12-07 20:18:34.238+0530 E/PKGMGR  ( 1495): pkgmgr.c: __find_op_cbinfo(312) > tmp is NULL
12-07 20:18:34.348+0530 W/CERT_SVC_VCORE( 3586): DUID.cpp: CheckCertifiedDevice(668) > No device id on distributor certificate
12-07 20:18:34.398+0530 E/rpm-installer( 3586): coretpk-parser.c: _coretpk_parser_get_manifest_info(1183) > (doc == NULL) xmlParseFile() failed.
12-07 20:18:34.398+0530 E/rpm-installer( 3586): coretpk-installer.c: _coretpk_installer_verify_privilege_list(1594) > (pkg_file_info == NULL) pkg_file_info is NULL.
12-07 20:18:34.458+0530 E/PKGMGR_PARSER( 3586): pkgmgr_parser.c: __check_theme(154) > theme for uninstallation.
12-07 20:18:34.478+0530 E/PKGMGR_CERT( 3586): pkgmgrinfo_certinfo.c: pkgmgrinfo_delete_certinfo(617) > Transaction Begin
12-07 20:18:34.488+0530 E/PKGMGR_CERT( 3586): pkgmgrinfo_certinfo.c: pkgmgrinfo_delete_certinfo(622) > Certificate Deletion Success
12-07 20:18:34.488+0530 E/PKGMGR_CERT( 3586): pkgmgrinfo_certinfo.c: pkgmgrinfo_delete_certinfo(635) > Transaction Commit and End
12-07 20:18:34.528+0530 E/EFL     (  895): ecore_x<895> lib/ecore_x/xlib/ecore_x_events.c:756 _ecore_x_event_handle_button_release() ButtonEvent:release time=8467010
12-07 20:18:34.528+0530 E/rpm-installer( 3586): coretpk-installer.c: _coretpk_installer_package_reinstall(6347) > _coretpk_installer_package_reinstall(org.example.uibuildersingleview) failed.
12-07 20:18:34.538+0530 I/Tizen::App( 1290): (1894) > PackageEventHandler - req: 12900002, pkg_type: tpk, pkg_name: org.example.uibuildersingleview, key: end, val: fail
12-07 20:18:34.538+0530 I/Tizen::App( 1290): (1584) > Package = [org.example.uibuildersingleview], Key = [end], Value = [fail], install = [96]
12-07 20:18:34.538+0530 E/PKGMGR  ( 1495): pkgmgr.c: __find_op_cbinfo(312) > tmp is NULL
12-07 20:18:34.538+0530 E/PKGMGR  ( 1495): pkgmgr.c: __find_op_cbinfo(312) > tmp is NULL
12-07 20:18:34.538+0530 E/ESD     (  941): esd_main.c: __esd_pkgmgr_event_callback(1757) > pkg_event(3) falied
12-07 20:18:34.548+0530 E/EFL     (  895): ecore_x<895> lib/ecore_x/xlib/ecore_x_events.c:603 _ecore_x_event_handle_button_press() ButtonEvent:press time=8467032
12-07 20:18:34.548+0530 W/ISF_PANEL_EFL(  823): isf_panel_efl.cpp: _isf_insert_ime_info_by_pkgid(1168) > pkgmgrinfo_pkginfo_get_pkginfo("org.example.uibuildersingleview",~) returned -1
12-07 20:18:34.548+0530 W/ISF_PANEL_EFL(  823): isf_panel_efl.cpp: _package_manager_event_cb(1381) > isf_db_select_appids_by_pkgid returned 0.
12-07 20:18:34.548+0530 W/AUL_AMD (  664): amd_appinfo.c: __amd_pkgmgrinfo_status_cb(1111) > __amd_pkgmgrinfo_fail_handler
12-07 20:18:34.688+0530 E/EFL     (  895): ecore_x<895> lib/ecore_x/xlib/ecore_x_events.c:756 _ecore_x_event_handle_button_release() ButtonEvent:release time=8467175
12-07 20:18:34.708+0530 E/EFL     (  895): ecore_x<895> lib/ecore_x/xlib/ecore_x_events.c:603 _ecore_x_event_handle_button_press() ButtonEvent:press time=8467198
12-07 20:18:35.249+0530 E/EFL     (  895): ecore_x<895> lib/ecore_x/xlib/ecore_x_events.c:756 _ecore_x_event_handle_button_release() ButtonEvent:release time=8467737
12-07 20:18:36.370+0530 E/PKGMGR_SERVER( 3583): pkgmgr-server.c: __unset_recovery_mode(282) > remove recovery_file[/opt/share/packages/.recovery/pkgmgr/org.example.uibuildersingleview] fail
12-07 20:18:36.370+0530 E/PKGMGR_SERVER( 3583): pkgmgr-server.c: sighandler(417) > child NORMAL exit [3586]
12-07 20:18:36.520+0530 E/PKGMGR_SERVER( 3583): pkgmgr-server.c: exit_server(1381) > exit_server Start [backend_status=1, queue_status=1, drm_status=1] 
12-07 20:18:36.520+0530 E/PKGMGR_SERVER( 3583): pkgmgr-server.c: main(2471) > package manager server terminated.
12-07 20:18:37.701+0530 E/EFL     (  895): ecore_x<895> lib/ecore_x/xlib/ecore_x_events.c:603 _ecore_x_event_handle_button_press() ButtonEvent:press time=8470184
12-07 20:18:37.891+0530 E/EFL     (  895): ecore_x<895> lib/ecore_x/xlib/ecore_x_events.c:372 _ecore_key_press() Ecore KeyEvent:press time=8470376
12-07 20:18:37.891+0530 E/cluster-view(  895): homescreen-view-manager.cpp: _OnKeyEvent(1048) >  receive[XF86Home]Key [Down]Event!! 
12-07 20:18:37.891+0530 W/STARTER (  814): hw_key.c: _key_press_cb(758) > [_key_press_cb:758] Home Key is pressed
12-07 20:18:37.891+0530 E/EFL     (  882): ecore_x<882> lib/ecore_x/xlib/ecore_x_events.c:372 _ecore_key_press() Ecore KeyEvent:press time=8470376
12-07 20:18:37.891+0530 W/STARTER (  814): hw_key.c: _key_press_cb(776) > [_key_press_cb:776] homekey count : 1
12-07 20:18:38.021+0530 E/EFL     (  895): ecore_x<895> lib/ecore_x/xlib/ecore_x_events.c:756 _ecore_x_event_handle_button_release() ButtonEvent:release time=8470503
12-07 20:18:38.122+0530 W/STARTER (  814): hw_key.c: _key_release_cb(578) > [_key_release_cb:578] Home Key is released
12-07 20:18:38.122+0530 E/EFL     (  882): ecore_x<882> lib/ecore_x/xlib/ecore_x_events.c:377 _ecore_key_press() Ecore KeyEvent:release time=8470604
12-07 20:18:38.122+0530 E/EFL     (  895): ecore_x<895> lib/ecore_x/xlib/ecore_x_events.c:377 _ecore_key_press() Ecore KeyEvent:release time=8470604
12-07 20:18:38.122+0530 E/cluster-view(  895): homescreen-view-manager.cpp: _OnKeyEvent(1048) >  receive[XF86Home]Key [Up]Event!! 
12-07 20:18:38.132+0530 E/STARTER (  814): lock-daemon-lite.c: _lockd_invoke_dbus_method_sync(355) > [STARTER/home/abuild/rpmbuild/BUILD/starter-0.5.54/src/lock-daemon-lite.c:355:E] dbus_connection_send error(No reply)
12-07 20:18:38.132+0530 E/STARTER (  814): lock-daemon-lite.c: _lockd_invoke_dbus_method_sync(359) > [STARTER/home/abuild/rpmbuild/BUILD/starter-0.5.54/src/lock-daemon-lite.c:359:E] dbus_connection_send error(org.freedesktop.DBus.Error.UnknownMethod:Method "getstatus" with signature "" on interface "org.tizen.system.deviced.hall" doesn't exist
12-07 20:18:38.132+0530 E/STARTER (  814): )
12-07 20:18:38.132+0530 I/SYSPOPUP(  893): syspopup.c: __X_syspopup_term_handler(98) > enter syspopup term handler
12-07 20:18:38.132+0530 I/SYSPOPUP(  893): syspopup.c: __X_syspopup_term_handler(108) > term action 1 - volume
12-07 20:18:38.132+0530 E/VOLUME  (  893): volume_x_event.c: volume_x_input_event_unregister(354) > [volume_x_input_event_unregister:354] (s_info.event_outer_touch_handler == NULL) -> volume_x_input_event_unregister() return
12-07 20:18:38.132+0530 E/VOLUME  (  893): volume_control.c: volume_control_close(841) > [volume_control_close:841] Failed to unregister x input event handler
12-07 20:18:38.172+0530 E/VCONF   (  895): vconf.c: _vconf_check_install_key_list(1135) > key : memory/cluster-home/edit-state
12-07 20:18:38.172+0530 E/VCONF   (  895): vconf.c: _vconf_check_retry_err(1368) > memory/cluster-home/edit-state : check retry err (-21/2).
12-07 20:18:38.172+0530 E/VCONF   (  895): vconf.c: _vconf_set_key_filesys(1573) > _vconf_set_key_filesys(41-memory/cluster-home/edit-state) step(-21) failed(2 / No such file or directory)
12-07 20:18:38.172+0530 E/VCONF   (  895): vconf.c: _vconf_set_key(1621) > _vconf_set_key(memory/cluster-home/edit-state) step(-21) failed(2 / No such file or directory)
12-07 20:18:38.172+0530 E/VCONF   (  895): vconf.c: vconf_set_int(1717) > vconf_set_int(895) : memory/cluster-home/edit-state(0) error
12-07 20:18:38.342+0530 W/STARTER (  814): hw_key.c: _homekey_timer_cb(479) > [_homekey_timer_cb:479] _homekey_timer_cb, homekey count[1], lock state[0]
12-07 20:18:38.342+0530 E/STARTER (  814): lock-daemon-lite.c: _lockd_invoke_dbus_method_sync(355) > [STARTER/home/abuild/rpmbuild/BUILD/starter-0.5.54/src/lock-daemon-lite.c:355:E] dbus_connection_send error(No reply)
12-07 20:18:38.342+0530 E/STARTER (  814): lock-daemon-lite.c: _lockd_invoke_dbus_method_sync(359) > [STARTER/home/abuild/rpmbuild/BUILD/starter-0.5.54/src/lock-daemon-lite.c:359:E] dbus_connection_send error(org.freedesktop.DBus.Error.UnknownMethod:Method "getstatus" with signature "" on interface "org.tizen.system.deviced.hall" doesn't exist
12-07 20:18:38.342+0530 E/STARTER (  814): )
12-07 20:18:38.342+0530 W/AUL_AMD (  664): amd_request.c: __request_handler(906) > __request_handler: 14
12-07 20:18:38.342+0530 W/AUL_AMD (  664): amd_request.c: __send_result_to_client(158) > __send_result_to_client, pid: 895
12-07 20:18:38.372+0530 W/AUL     (  814): launch.c: app_request_to_launchpad(396) > request cmd(0) to(com.samsung.homescreen)
12-07 20:18:38.372+0530 W/AUL_AMD (  664): amd_request.c: __request_handler(906) > __request_handler: 0
12-07 20:18:38.382+0530 I/AUL     (  664): menu_db_util.h: _get_app_info_from_db_by_apppath(242) > path : /usr/bin/starter, ret : 0
12-07 20:18:38.382+0530 E/AUL_AMD (  664): amd_launch.c: _start_app(2499) > no caller appid info, ret: -1
12-07 20:18:38.382+0530 W/AUL_AMD (  664): amd_launch.c: _start_app(2508) > caller pid : 814
12-07 20:18:38.382+0530 E/AUL_AMD (  664): amd_appinfo.c: appinfo_get_value(1296) > appinfo get value: Invalid argument, 19
12-07 20:18:38.392+0530 W/AUL     (  664): app_signal.c: aul_send_app_resume_request_signal(468) > send_app_resume_signal, pid: 895, appid: com.samsung.homescreen
12-07 20:18:38.392+0530 W/AUL_AMD (  664): amd_launch.c: __nofork_processing(1412) > __nofork_processing, cmd: 0, pid: 895
12-07 20:18:38.392+0530 W/AUL_AMD (  664): amd_request.c: __send_home_launch_signal(577) > send a home launch signal
12-07 20:18:38.392+0530 I/APP_CORE(  895): appcore-efl.c: __do_app(514) > [APP 895] Event: RESET State: RUNNING
12-07 20:18:38.392+0530 I/CAPI_APPFW_APPLICATION(  895): app_main.c: _ui_app_appcore_reset(723) > app_appcore_reset
12-07 20:18:38.402+0530 W/CAPI_APPFW_APP_CONTROL(  895): app_control.c: app_control_error(152) > [app_control_get_extra_data] KEY_NOT_FOUND(0xffffff82)
12-07 20:18:38.412+0530 W/AUL_AMD (  664): amd_launch.c: __reply_handler(1083) > listen fd(32) , send fd(31), pid(895), cmd(0)
12-07 20:18:38.412+0530 I/APP_CORE(  895): appcore-efl.c: __do_app(564) > Legacy lifecycle: 0
12-07 20:18:38.412+0530 I/APP_CORE(  895): appcore-efl.c: __do_app(566) > [APP 895] App already running, raise the window
12-07 20:18:38.412+0530 W/AUL     (  814): launch.c: app_request_to_launchpad(425) > request cmd(0) result(895)
12-07 20:18:38.412+0530 E/STARTER (  814): dbus-util.c: starter_dbus_home_raise_signal_send(219) > [starter_dbus_home_raise_signal_send:219] Sending HOME RAISE signal, result:0
12-07 20:18:39.393+0530 W/AUL_AMD (  664): amd_key.c: _key_ungrab(269) > fail(-1) to ungrab key(XF86Back)
12-07 20:18:39.393+0530 W/AUL_AMD (  664): amd_launch.c: __grab_timeout_handler(1623) > back key ungrab error
12-07 20:18:39.743+0530 E/PKGMGR  ( 3636): pkgmgr.c: pkgmgr_client_install(1605) > install pkg start.
12-07 20:18:39.813+0530 E/PKGMGR_SERVER( 3638): pkgmgr-server.c: main(2414) > package manager server start
12-07 20:18:39.853+0530 E/PKGMGR_SERVER( 3638): pkgmgr-server-internal.c: _zone_set_type_and_backend(180) > tep_filepath [/opt/usr/apps/tmp/org.example.uibuildersingleview-1.0.0-arm.tpk]
12-07 20:18:39.863+0530 E/PKGMGR_INFO( 3638): pkgmgrinfo_pkginfo.c: pkgmgrinfo_pkginfo_get_unmounted_pkginfo(778) > (exist == 0) pkgid[/opt/usr/apps/tmp/org.example.uibuildersingleview-1.0.0-arm.tpk] not found in DB
12-07 20:18:39.873+0530 E/rpm-installer( 3638): librpm.c: __installer_util_delete_dir(171) > opendir(/tmp/coretpk-unzip) failed. [2][No such file or directory]
12-07 20:18:39.883+0530 E/PKGMGR_SERVER( 3638): pm-mdm.c: _pm_check_mdm_policy(75) > [0;31m[_pm_check_mdm_policy(): 75](ret != MDM_RESULT_SUCCESS) can not connect mdm server[0;m
12-07 20:18:39.883+0530 E/PKGMGR_SERVER( 3638): pkgmgr-server.c: __set_recovery_mode(234) > rev_file[/opt/share/packages/.recovery/pkgmgr/org.example.uibuildersingleview-1.0.0-arm.tpk] is null
12-07 20:18:39.883+0530 E/PKGMGR  ( 3636): pkgmgr.c: pkgmgr_client_install(1723) > install pkg finish, ret=[36360002]
12-07 20:18:39.993+0530 E/PKGMGR_INSTALLER( 3641): pkgmgr_installer.c: pkgmgr_installer_receive_request(225) > option is [i]
12-07 20:18:39.993+0530 E/rpm-installer( 3641): rpm-appcore-intf.c: main(186) > [/opt/usr/apps/tmp/org.example.uibuildersingleview-1.0.0-arm.tpk] is tpk package.
12-07 20:18:40.003+0530 E/rpm-installer( 3641): coretpk-parser.c: _coretpk_parser_is_svc_app(1031) > (ret == 1) NodeSet is empty. (//*[name() = 'manifest']/*[name()='service-application'])
12-07 20:18:40.003+0530 E/rpm-installer( 3641): coretpk-parser.c: __coretpk_parser_get_value(1147) > (result_value == NULL) [install-location] is empty.
12-07 20:18:40.003+0530 E/rpm-installer( 3641): coretpk-parser.c: __coretpk_parser_get_value_list(1104) > (ret == 1) [//*[name() ='privileges']/*[name()='privilege']] is empty.
12-07 20:18:40.003+0530 E/rpm-installer( 3641): coretpk-parser.c: __coretpk_parser_is_theme(31) > (ret == 1) metadata(watchface) is empty.
12-07 20:18:40.003+0530 E/rpm-installer( 3641): coretpk-parser.c: _coretpk_parser_is_widget(997) > (ret == 1) NodeSet is empty. (//*[name() = 'manifest']/*[name()='widget-application'])
12-07 20:18:40.003+0530 E/rpm-installer( 3641): installer-util.c: _installer_util_get_configuration_value(565) > [signature]=[on]
12-07 20:18:40.053+0530 W/CERT_SVC_VCORE( 3641): DUID.cpp: CheckCertifiedDevice(668) > No device id on distributor certificate
12-07 20:18:40.104+0530 I/Tizen::App( 1290): (1894) > PackageEventHandler - req: 12900002, pkg_type: tpk, pkg_name: org.example.uibuildersingleview, key: start, val: install
12-07 20:18:40.104+0530 I/Tizen::App( 1290): (1584) > Package = [org.example.uibuildersingleview], Key = [start], Value = [install], install = [96]
12-07 20:18:40.104+0530 E/PKGMGR  ( 1495): pkgmgr.c: __find_op_cbinfo(312) > tmp is NULL
12-07 20:18:40.104+0530 E/PKGMGR  ( 1495): pkgmgr.c: __find_op_cbinfo(312) > tmp is NULL
12-07 20:18:40.114+0530 I/Tizen::App( 1290): (1894) > PackageEventHandler - req: 12900002, pkg_type: tpk, pkg_name: org.example.uibuildersingleview, key: install_percent, val: 30
12-07 20:18:40.114+0530 I/Tizen::App( 1290): (119) > InstallationInProgress [30]
12-07 20:18:40.114+0530 I/Tizen::App( 1290): (1584) > Package = [org.example.uibuildersingleview], Key = [install_percent], Value = [30], install = [96]
12-07 20:18:40.124+0530 E/PKGMGR  ( 1495): pkgmgr.c: __find_op_cbinfo(312) > tmp is NULL
12-07 20:18:40.124+0530 E/PKGMGR  ( 1495): pkgmgr.c: __find_op_cbinfo(312) > tmp is NULL
12-07 20:18:40.224+0530 E/rpm-installer( 3641): coretpk-parser.c: __coretpk_parser_check_vip_tag(396) > (ret == 1) metadata(watchface) is empty.
12-07 20:18:40.224+0530 E/rpm-installer( 3641): coretpk-parser.c: __coretpk_parser_get_value(1147) > (result_value == NULL) [ui-gadget] is empty.
12-07 20:18:40.224+0530 E/rpm-installer( 3641): coretpk-parser.c: __coretpk_parser_append_path(335) > (ret == 1) NodeSet is empty. (//@portrait-effectimage)
12-07 20:18:40.224+0530 E/rpm-installer( 3641): coretpk-parser.c: __coretpk_parser_is_theme(31) > (ret == 1) metadata(watchface) is empty.
12-07 20:18:40.234+0530 E/PKGMGR_PARSER( 3641): pkgmgr_parser.c: pkgmgr_parser_check_manifest_validation(2678) > Manifest is Valid
12-07 20:18:40.234+0530 E/PKGMGR_PARSER( 3641): pkgmgr_parser_signature.c: __ps_check_mdm_policy(979) > (ret != MDM_RESULT_SUCCESS) can not connect mdm server
12-07 20:18:40.464+0530 E/PKGMGR_PARSER( 3641): pkgmgr_parser.c: __check_theme(142) > theme for installation.
12-07 20:18:40.484+0530 E/PKGMGR_CERT( 3641): pkgmgrinfo_certinfo.c: pkgmgrinfo_save_certinfo(427) > Transaction Begin
12-07 20:18:40.484+0530 E/PKGMGR_CERT( 3641): pkgmgrinfo_certinfo.c: pkgmgrinfo_save_certinfo(496) > Id:Count = 1 112
12-07 20:18:40.484+0530 E/PKGMGR_CERT( 3641): pkgmgrinfo_certinfo.c: pkgmgrinfo_save_certinfo(496) > Id:Count = 2 112
12-07 20:18:40.484+0530 E/PKGMGR_CERT( 3641): pkgmgrinfo_certinfo.c: pkgmgrinfo_save_certinfo(496) > Id:Count = 31 7
12-07 20:18:40.484+0530 E/PKGMGR_CERT( 3641): pkgmgrinfo_certinfo.c: pkgmgrinfo_save_certinfo(496) > Id:Count = 32 7
12-07 20:18:40.484+0530 E/PKGMGR_CERT( 3641): pkgmgrinfo_certinfo.c: pkgmgrinfo_save_certinfo(496) > Id:Count = 33 7
12-07 20:18:40.484+0530 E/PKGMGR_CERT( 3641): pkgmgrinfo_certinfo.c: pkgmgrinfo_save_certinfo(496) > Id:Count = 34 7
12-07 20:18:40.494+0530 E/PKGMGR_CERT( 3641): pkgmgrinfo_certinfo.c: pkgmgrinfo_save_certinfo(576) > Transaction Commit and End
12-07 20:18:40.504+0530 E/PKGMGR  ( 1495): pkgmgr.c: __find_op_cbinfo(312) > tmp is NULL
12-07 20:18:40.504+0530 I/Tizen::App( 1290): (1894) > PackageEventHandler - req: 12900002, pkg_type: tpk, pkg_name: org.example.uibuildersingleview, key: install_percent, val: 60
12-07 20:18:40.504+0530 E/PKGMGR  ( 1495): pkgmgr.c: __find_op_cbinfo(312) > tmp is NULL
12-07 20:18:40.504+0530 I/Tizen::App( 1290): (119) > InstallationInProgress [60]
12-07 20:18:40.504+0530 I/Tizen::App( 1290): (1584) > Package = [org.example.uibuildersingleview], Key = [install_percent], Value = [60], install = [96]
12-07 20:18:40.514+0530 E/rpm-installer( 3641): installer-util.c: _installer_util_get_configuration_value(565) > [signature]=[on]
12-07 20:18:40.544+0530 E/rpm-installer( 3641): coretpk-installer.c: __post_install_for_mmc(652) > (handle == NULL) handle is NULL.
12-07 20:18:40.544+0530 E/PKGMGR  ( 1495): pkgmgr.c: __find_op_cbinfo(312) > tmp is NULL
12-07 20:18:40.544+0530 I/Tizen::App( 1290): (1894) > PackageEventHandler - req: 12900002, pkg_type: tpk, pkg_name: org.example.uibuildersingleview, key: install_percent, val: 100
12-07 20:18:40.544+0530 E/PKGMGR  ( 1495): pkgmgr.c: __find_op_cbinfo(312) > tmp is NULL
12-07 20:18:40.544+0530 I/Tizen::App( 1290): (119) > InstallationInProgress [100]
12-07 20:18:40.544+0530 I/Tizen::App( 1290): (1584) > Package = [org.example.uibuildersingleview], Key = [install_percent], Value = [100], install = [96]
12-07 20:18:42.516+0530 E/PKGMGR_SERVER( 3638): pkgmgr-server.c: exit_server(1381) > exit_server Start [backend_status=0, queue_status=1, drm_status=1] 
12-07 20:18:42.526+0530 E/PKGMGR  ( 1495): pkgmgr.c: __find_op_cbinfo(312) > tmp is NULL
12-07 20:18:42.526+0530 E/PKGMGR  ( 1495): pkgmgr.c: __find_op_cbinfo(312) > tmp is NULL
12-07 20:18:42.526+0530 I/Tizen::App( 1290): (1894) > PackageEventHandler - req: 12900002, pkg_type: tpk, pkg_name: org.example.uibuildersingleview, key: end, val: ok
12-07 20:18:42.526+0530 I/Tizen::App( 1290): (78) > Installation is Completed. [Package = org.example.uibuildersingleview]
12-07 20:18:42.526+0530 I/Tizen::App( 1290): (671) > Enter. package(org.example.uibuildersingleview), installationResult(0)
12-07 20:18:42.526+0530 E/cluster-home(  895): mainmenu-package-manager.cpp: OnClientListenCb(571) >  #Step 1
12-07 20:18:42.526+0530 E/cluster-home(  895): mainmenu-package-manager.cpp: OnClientListenCb(575) >  #Step 2
12-07 20:18:42.526+0530 E/cluster-home(  895): mainmenu-package-manager.cpp: _GetAppIds(350) >  BEGIN
12-07 20:18:42.536+0530 I/Tizen::App( 1290): (1360) > package(org.example.uibuildersingleview), version(1.0.0), type(tpk), displayName(uibuildersingleview), uninstallable(1), downloaded(1), updated(0), preloaded(0)movable(1), externalStorage(0), mainApp(org.example.uibuildersingleview), storeClient(), appRootPath(/opt/usr/apps/org.example.uibuildersingleview)
12-07 20:18:42.556+0530 E/PKGMGR_SERVER( 3638): pkgmgr-server.c: __unset_recovery_mode(282) > remove recovery_file[/opt/share/packages/.recovery/pkgmgr/org.example.uibuildersingleview-1.0.0-arm.tpk] fail
12-07 20:18:42.556+0530 E/PKGMGR_SERVER( 3638): pkgmgr-server.c: sighandler(417) > child NORMAL exit [3641]
12-07 20:18:42.556+0530 I/Tizen::App( 1290): (483) > pkgmgrinfo_appinfo_get_appid(): app = [org.example.uibuildersingleview]
12-07 20:18:42.566+0530 I/Tizen::App( 1290): (416) > appName = [uibuildersingleview]
12-07 20:18:42.566+0530 I/Tizen::App( 1290): (509) > exe = [/opt/usr/apps/org.example.uibuildersingleview/bin/uibuildersingleview], displayName = [uibuildersingleview], mainApp = [1], menuIconVisible = [0], serviceApp = [0]
12-07 20:18:42.566+0530 E/PKGMGR_INFO( 1290): pkgmgrinfo_appinfo.c: pkgmgrinfo_appinfo_get_list(887) > (component == PMINFO_SVC_APP) PMINFO_SVC_APP is done
12-07 20:18:42.566+0530 I/Tizen::App( 1290): (683) > Application count(1) in this package
12-07 20:18:42.566+0530 I/Tizen::App( 1290): (840) > Enter.
12-07 20:18:42.576+0530 W/ISF_PANEL_EFL(  823): isf_panel_efl.cpp: _package_manager_event_cb(1513) > _isf_insert_ime_info_by_pkgid returned 0.
12-07 20:18:42.586+0530 I/Tizen::App( 1290): (703) > Exit.
12-07 20:18:42.586+0530 I/Tizen::App( 1290): (1584) > Package = [org.example.uibuildersingleview], Key = [end], Value = [ok], install = [96]
12-07 20:18:42.586+0530 I/Tizen::App( 1290): (416) > appName = [uibuildersingleview]
12-07 20:18:42.586+0530 I/Tizen::App( 1290): (509) > exe = [/opt/usr/apps/org.example.uibuildersingleview/bin/uibuildersingleview], displayName = [uibuildersingleview], mainApp = [1], menuIconVisible = [0], serviceApp = [0]
12-07 20:18:42.586+0530 I/Tizen::App( 1290): (2343) > info file is not existed. [/opt/usr/apps/org.exampl/info/org.example.uibuildersingleview.info]
12-07 20:18:42.586+0530 I/Tizen::App( 1290): (131) > Enter
12-07 20:18:42.586+0530 I/Tizen::App( 1290): (137) > org.example.uibuildersingleview does not have launch condition
12-07 20:18:42.586+0530 I/Tizen::App( 1290): (883) > Exit.
12-07 20:18:42.606+0530 E/cluster-home(  895): mainmenu-package-manager.cpp: _PkgMgrAppInfoGetListCb(253) >  ##### [org.example.uibuildersingleview]
12-07 20:18:42.606+0530 E/cluster-home(  895): mainmenu-package-manager.cpp: _GetAppIds(375) >  ##### [org.example.uibuildersingleview]
12-07 20:18:42.606+0530 E/cluster-home(  895): mainmenu-package-manager.cpp: _GetAppIds(379) >  END
12-07 20:18:42.606+0530 E/cluster-home(  895): mainmenu-package-manager.cpp: _DoPkgJob(458) >  #Step 3 size[1]
12-07 20:18:42.606+0530 E/cluster-home(  895): mainmenu-package-manager.cpp: _DoPkgJob(462) >  appId[org.example.uibuildersingleview]
12-07 20:18:42.616+0530 E/cluster-home(  895): mainmenu-package-manager.cpp: _GetAppInfo(990) >  Name[uibuildersingleview] enable[1] system[0]
12-07 20:18:42.616+0530 E/HOME_APPS(  895): mainmenu-package-manager.cpp: _DoPkgJob(484) >  appId[org.example.uibuildersingleview] mdm is not enabled
12-07 20:18:42.616+0530 E/cluster-home(  895): mainmenu-package-manager.cpp: GetAppInfo(630) >  Find a App Info Name[uibuildersingleview] enable[1] system[0]
12-07 20:18:42.616+0530 W/HOME_APPS(  895): mainmenu-box-impl.cpp: UpdateBoxData(1326) >  update box data!!!!! old icon path[/opt/usr/apps/org.example.uibuildersingleview/shared/res/uibuildersingleview.png], New icon path[/opt/usr/apps/org.example.uibuildersingleview/shared/res/uibuildersingleview.png]!!!!!
12-07 20:18:42.636+0530 E/util-view(  895): util-text-image-provider.cpp: GetPixelsFromText(240) >  DEFAULT='font=SamsungOneUI:style=Regular style=shadow,bottom shadow_color=# font_size=28 align=center valign=top color=#FFFFFF color_class=ATO001 text_class=ATO001 ellipsis=1 wrap=mixed linegap=-3'
12-07 20:18:42.656+0530 W/HOME_APPS(  895): mainmenu-box-impl.cpp: _OnImageLoadFinishedHandler(2152) >  finished path[/opt/usr/apps/org.example.uibuildersingleview/shared/res/uibuildersingleview.png], Loading state:[1]
12-07 20:18:44.518+0530 E/PKGMGR_SERVER( 3638): pkgmgr-server.c: exit_server(1381) > exit_server Start [backend_status=1, queue_status=1, drm_status=1] 
12-07 20:18:44.518+0530 E/PKGMGR_SERVER( 3638): pkgmgr-server.c: main(2471) > package manager server terminated.
12-07 20:18:48.342+0530 W/AUL     ( 3697): launch.c: app_request_to_launchpad(396) > request cmd(0) to(org.example.uibuildersingleview)
12-07 20:18:48.342+0530 W/AUL_AMD (  664): amd_request.c: __request_handler(906) > __request_handler: 0
12-07 20:18:48.352+0530 I/AUL     (  664): menu_db_util.h: _get_app_info_from_db_by_apppath(242) > path : /usr/bin/launch_app, ret : 0
12-07 20:18:48.362+0530 I/AUL     (  664): menu_db_util.h: _get_app_info_from_db_by_apppath(242) > path : /bin/bash, ret : 0
12-07 20:18:48.362+0530 E/AUL_AMD (  664): amd_launch.c: _start_app(2499) > no caller appid info, ret: -1
12-07 20:18:48.362+0530 W/AUL_AMD (  664): amd_launch.c: _start_app(2508) > caller pid : 3697
12-07 20:18:48.362+0530 E/AUL_AMD (  664): amd_appinfo.c: appinfo_get_value(1296) > appinfo get value: Invalid argument, 19
12-07 20:18:48.372+0530 E/RESOURCED(  779): block.c: block_prelaunch_state(134) > insert data org.example.uibuildersingleview, table num : 3
12-07 20:18:48.372+0530 E/RESOURCED(  779): heart-memory.c: heart_memory_get_data(601) > hashtable heart_memory_app_list is NULL
12-07 20:18:48.372+0530 W/AUL_AMD (  664): amd_launch.c: _start_app(3052) > pad pid(-5)
12-07 20:18:48.372+0530 W/AUL_PAD ( 1521): launchpad.c: __launchpad_main_loop(520) > Launch on type-based process-pool
12-07 20:18:48.372+0530 W/AUL_PAD ( 1521): launchpad.c: __send_result_to_caller(267) > Check app launching
12-07 20:18:48.382+0530 I/CAPI_APPFW_APPLICATION( 3290): app_main.c: ui_app_main(789) > app_efl_main
12-07 20:18:48.382+0530 I/CAPI_APPFW_APPLICATION( 3290): app_main.c: _ui_app_appcore_create(641) > app_appcore_create
12-07 20:18:48.432+0530 E/TBM     ( 3290): tbm_bufmgr.c: _tbm_bufmgr_init_state(691) > USE TGL LOCK!
12-07 20:18:48.472+0530 W/AUL     (  664): app_signal.c: aul_send_app_launch_request_signal(423) > send_app_launch_signal, pid: 3290, appid: org.example.uibuildersingleview
12-07 20:18:48.472+0530 E/AUL     (  664): amd_app_group.c: app_group_start_app(1037) > app_group_start_app
12-07 20:18:48.472+0530 E/RESOURCED(  779): proc-main.c: proc_add_program_list(237) > not found ppi : org.example.uibuildersingleview
12-07 20:18:48.482+0530 W/AUL     ( 3697): launch.c: app_request_to_launchpad(425) > request cmd(0) result(3290)
12-07 20:18:48.682+0530 I/APP_CORE( 3290): appcore-efl.c: __do_app(514) > [APP 3290] Event: RESET State: CREATED
12-07 20:18:48.682+0530 I/CAPI_APPFW_APPLICATION( 3290): app_main.c: _ui_app_appcore_reset(723) > app_appcore_reset
12-07 20:18:48.692+0530 E/EFL     ( 3290): edje<3290> lib/edje/edje_embryo.c:4134 _edje_embryo_test_run() You are running Embryo->EDC->Embryo with script program '_p26';
12-07 20:18:48.692+0530 E/EFL     ( 3290): By the power of Grayskull, your previous Embryo stack is now broken!
12-07 20:18:48.702+0530 E/EFL     ( 3290): edje<3290> lib/edje/edje_embryo.c:4134 _edje_embryo_test_run() You are running Embryo->EDC->Embryo with script program '_p26';
12-07 20:18:48.702+0530 E/EFL     ( 3290): By the power of Grayskull, your previous Embryo stack is now broken!
12-07 20:18:48.702+0530 E/EFL     ( 3290): edje<3290> lib/edje/edje_embryo.c:4134 _edje_embryo_test_run() You are running Embryo->EDC->Embryo with script program '_p26';
12-07 20:18:48.702+0530 E/EFL     ( 3290): By the power of Grayskull, your previous Embryo stack is now broken!
12-07 20:18:48.712+0530 W/APP_CORE( 3290): appcore-efl.c: __show_cb(920) > [EVENT_TEST][EVENT] GET SHOW EVENT!!!. WIN:5400002
12-07 20:18:48.712+0530 W/AUL_AMD (  664): amd_request.c: __request_handler(906) > __request_handler: 35
12-07 20:18:48.712+0530 E/EFL     (  330): eo<330> lib/eo/eo.c:676 _eo_call_resolve() in lib/edje/edje_object.eo.c:316: func 'edje_obj_signal_emit' (415) could not be resolved for class 'Evas_Object_Smart'.
12-07 20:18:48.712+0530 E/EFL     (  330): eo<330> lib/eo/eo.c:676 _eo_call_resolve() in lib/edje/edje_object.eo.c:316: func 'edje_obj_signal_emit' (415) could not be resolved for class 'Evas_Object_Smart'.
12-07 20:18:48.732+0530 I/Tizen::System( 1290): (259) > Active app [org.exampl], current [com.samsun] 
12-07 20:18:48.732+0530 I/Tizen::Io( 1290): (729) > Entry not found
12-07 20:18:48.742+0530 I/Tizen::System( 1290): (157) > change brightness system value.
12-07 20:18:48.742+0530 W/AUL_AMD (  664): amd_request.c: __request_handler(906) > __request_handler: 15
12-07 20:18:48.792+0530 I/APP_CORE(  895): appcore-efl.c: __do_app(514) > [APP 895] Event: PAUSE State: RUNNING
12-07 20:18:48.792+0530 I/CAPI_APPFW_APPLICATION(  895): app_main.c: _ui_app_appcore_pause(689) > app_appcore_pause
12-07 20:18:48.792+0530 E/cluster-home(  895): homescreen.cpp: OnPause(260) >  app pause
12-07 20:18:48.792+0530 W/AUL_AMD (  664): amd_launch.c: __e17_status_handler(3328) > pid(895) status(4)
12-07 20:18:48.792+0530 W/AUL     (  664): amd_app_group.c: __set_fg_flag(190) > send_signal BG com.samsung.homescreen(895)
12-07 20:18:48.792+0530 W/AUL     (  664): app_signal.c: aul_send_app_status_change_signal(581) > send_app_status_change_signal, pid: 895, appid: com.samsung.homescreen, status: bg
12-07 20:18:48.802+0530 W/AUL_AMD (  664): amd_launch.c: __e17_status_handler(3328) > pid(3290) status(3)
12-07 20:18:48.802+0530 W/AUL_AMD (  664): amd_key.c: _key_ungrab(269) > fail(-1) to ungrab key(XF86Back)
12-07 20:18:48.802+0530 W/AUL_AMD (  664): amd_launch.c: __e17_status_handler(3349) > back key ungrab error
12-07 20:18:48.802+0530 W/AUL     (  664): amd_app_group.c: __set_fg_flag(180) > send_signal FG org.example.uibuildersingleview(3290)
12-07 20:18:48.802+0530 W/AUL     (  664): app_signal.c: aul_send_app_status_change_signal(581) > send_app_status_change_signal, pid: 3290, appid: org.example.uibuildersingleview, status: fg
12-07 20:18:48.802+0530 I/APP_CORE( 3290): appcore-efl.c: __do_app(514) > [APP 3290] Event: RESUME State: CREATED
12-07 20:18:48.822+0530 I/APP_CORE( 3290): appcore-efl.c: __do_app(623) > Legacy lifecycle: 0
12-07 20:18:48.822+0530 I/APP_CORE( 3290): appcore-efl.c: __do_app(625) > [APP 3290] Initial Launching, call the resume_cb
12-07 20:18:48.822+0530 I/CAPI_APPFW_APPLICATION( 3290): app_main.c: _ui_app_appcore_resume(706) > app_appcore_resume
12-07 20:18:49.172+0530 W/AUL_AMD (  664): amd_launch.c: __e17_status_handler(3328) > pid(3290) status(0)
12-07 20:18:49.513+0530 I/Tizen::App( 1290): (499) > LaunchedApp(org.example.uibuildersingleview)
12-07 20:18:49.513+0530 I/Tizen::App( 1290): (733) > Finished invoking application event listener for org.example.uibuildersingleview, 3290.
12-07 20:18:49.513+0530 E/RESOURCED(  779): resourced-dbus.c: resourced_dbus_system_hash_drop_busname(324) > Does not exist in busname hash: :1.327
12-07 20:18:50.243+0530 I/UXT     ( 3710): Uxt_ObjectManager.cpp: OnInitialized(737) > Initialized.
12-07 20:18:53.787+0530 I/APP_CORE(  895): appcore-efl.c: __do_app(514) > [APP 895] Event: MEM_FLUSH State: PAUSED
12-07 20:18:59.002+0530 E/EFL     ( 3290): ecore_x<3290> lib/ecore_x/xlib/ecore_x_events.c:603 _ecore_x_event_handle_button_press() ButtonEvent:press time=8491483
12-07 20:18:59.282+0530 E/EFL     ( 3290): ecore_x<3290> lib/ecore_x/xlib/ecore_x_events.c:756 _ecore_x_event_handle_button_release() ButtonEvent:release time=8491758
12-07 20:19:00.413+0530 I/CAPI_APPFW_APPLICATION( 3290): app_main.c: _ui_app_appcore_rotation_event(485) > _ui_app_appcore_rotation_event
12-07 20:19:00.413+0530 I/CAPI_APPFW_APPLICATION(  895): app_main.c: _ui_app_appcore_rotation_event(485) > _ui_app_appcore_rotation_event
12-07 20:19:00.483+0530 I/INDICATOR(  776): clock.c: indicator_clock_changed_cb(195) > ""
12-07 20:19:00.493+0530 E/EFL     ( 3290): edje<3290> lib/edje/edje_embryo.c:4134 _edje_embryo_test_run() You are running Embryo->EDC->Embryo with script program '_p26';
12-07 20:19:00.493+0530 E/EFL     ( 3290): By the power of Grayskull, your previous Embryo stack is now broken!
12-07 20:19:00.533+0530 E/UXT     (  776): uxt_util.c: uxt_util_get_date_time_text_by_locale(389) > text : 8:19 p.m.
12-07 20:19:00.533+0530 I/INDICATOR(  776): clock.c: getTimeFormatted(176) > "time format : 8:19 p.m."
12-07 20:19:00.533+0530 I/INDICATOR(  776): clock.c: indicator_clock_changed_cb(244) > "time format : 8:19 p.m."
12-07 20:19:00.533+0530 W/INDICATOR(  776): clock.c: indicator_clock_changed_cb(272) > 
12-07 20:19:00.533+0530 I/INDICATOR(  776): clock.c: indicator_clock_changed_cb(317) > "[CLOCK MODULE] Timer Status : -2146845639 Time: <font_size=31> </font_size> <font_size=31> 8:19 p.m.</font_size></font>"
12-07 20:19:00.674+0530 E/EFL     (  894): eo<894> lib/eo/eo.c:676 _eo_call_resolve() in lib/edje/edje_object.eo.c:164: func 'edje_obj_part_geometry_get' (762) could not be resolved for class 'Elm_Layout'.
12-07 20:19:00.884+0530 E/EFL     (  330): elementary<330> elm_transit.c:648 elm_transit_del_cb_set() Elm_Transit transit has already been deleted!
12-07 20:19:00.914+0530 E/EFL     ( 3290): ecore_x<3290> lib/ecore_x/xlib/ecore_x_events.c:603 _ecore_x_event_handle_button_press() ButtonEvent:press time=8493400
12-07 20:19:01.394+0530 E/EFL     ( 3290): ecore_x<3290> lib/ecore_x/xlib/ecore_x_events.c:756 _ecore_x_event_handle_button_release() ButtonEvent:release time=8493829
12-07 20:19:01.394+0530 E/EFL     ( 3290): edje<3290> lib/edje/edje_embryo.c:4134 _edje_embryo_test_run() You are running Embryo->EDC->Embryo with script program '_p19';
12-07 20:19:01.394+0530 E/EFL     ( 3290): By the power of Grayskull, your previous Embryo stack is now broken!
12-07 20:19:01.394+0530 E/VCONF   ( 3290): vconf.c: _vconf_check_retry_err(1368) > db/ise/keysound : check retry err (-21/13).
12-07 20:19:01.394+0530 E/VCONF   ( 3290): vconf.c: _vconf_get_key_filesys(2371) > _vconf_get_key_filesys(db/ise/keysound) step(-21) failed(13 / Permission denied) retry(0) 
12-07 20:19:01.394+0530 E/VCONF   ( 3290): vconf.c: _vconf_get_key(2407) > _vconf_get_key(db/ise/keysound) step(-21) failed(13 / Permission denied)
12-07 20:19:01.394+0530 E/VCONF   ( 3290): vconf.c: vconf_get_bool(2729) > vconf_get_bool(3290) : db/ise/keysound error
12-07 20:19:01.394+0530 E/VCONF   ( 3290): vconf-kdb.c: _vconf_kdb_add_notify(318) > _vconf_kdb_add_notify : add noti(Permission denied)
12-07 20:19:01.394+0530 E/VCONF   ( 3290): vconf.c: vconf_notify_key_changed(3168) > vconf_notify_key_changed : key(db/ise/keysound) add notify fail
12-07 20:19:01.434+0530 E/EFL     ( 3290): ecore_x<3290> lib/ecore_x/xlib/ecore_x_events.c:603 _ecore_x_event_handle_button_press() ButtonEvent:press time=8493896
12-07 20:19:01.604+0530 I/CAPI_APPFW_APPLICATION(  895): app_main.c: _ui_app_appcore_rotation_event(485) > _ui_app_appcore_rotation_event
12-07 20:19:01.675+0530 I/CAPI_APPFW_APPLICATION( 3290): app_main.c: _ui_app_appcore_rotation_event(485) > _ui_app_appcore_rotation_event
12-07 20:19:01.785+0530 I/CAPI_APPFW_APPLICATION(  895): app_main.c: _ui_app_appcore_rotation_event(485) > _ui_app_appcore_rotation_event
12-07 20:19:01.795+0530 I/AUL_PAD ( 1521): sigchild.h: __launchpad_process_sigchld(162) > dead_pid = 3290 pgid = 3290
12-07 20:19:01.795+0530 I/AUL_PAD ( 1521): sigchild.h: __sigchild_action(143) > dead_pid(3290)
12-07 20:19:01.795+0530 E/EFL     (  330): eo<330> lib/eo/eo.c:676 _eo_call_resolve() in lib/edje/edje_object.eo.c:316: func 'edje_obj_signal_emit' (415) could not be resolved for class 'Evas_Object_Smart'.
12-07 20:19:01.815+0530 W/AUL_AMD (  664): amd_launch.c: __e17_status_handler(3328) > pid(895) status(3)
12-07 20:19:01.815+0530 W/AUL_AMD (  664): amd_key.c: _key_ungrab(269) > fail(-1) to ungrab key(XF86Back)
12-07 20:19:01.815+0530 W/AUL_AMD (  664): amd_launch.c: __e17_status_handler(3349) > back key ungrab error
12-07 20:19:01.815+0530 W/AUL     (  664): amd_app_group.c: __set_fg_flag(180) > send_signal FG com.samsung.homescreen(895)
12-07 20:19:01.815+0530 W/AUL     (  664): app_signal.c: aul_send_app_status_change_signal(581) > send_app_status_change_signal, pid: 895, appid: com.samsung.homescreen, status: fg
12-07 20:19:01.835+0530 E/EFL     (  894): eo<894> lib/eo/eo.c:676 _eo_call_resolve() in lib/edje/edje_object.eo.c:164: func 'edje_obj_part_geometry_get' (762) could not be resolved for class 'Elm_Layout'.
12-07 20:19:01.855+0530 I/AUL_PAD ( 1521): sigchild.h: __sigchild_action(149) > __send_app_dead_signal(0)
12-07 20:19:01.855+0530 I/AUL_PAD ( 1521): sigchild.h: __launchpad_process_sigchld(170) > after __sigchild_action
12-07 20:19:01.855+0530 E/AUL_PAD ( 1521): launchpad.c: main(698) > error reading sigchld info
12-07 20:19:01.855+0530 I/Tizen::App( 1290): (243) > App[org.example.uibuildersingleview] pid[3290] terminate event is forwarded
12-07 20:19:01.855+0530 I/Tizen::System( 1290): (256) > osp.accessorymanager.service provider is found.
12-07 20:19:01.855+0530 I/Tizen::System( 1290): (196) > Accessory Owner is removed [org.example.uibuildersingleview, 3290, ]
12-07 20:19:01.855+0530 I/Tizen::System( 1290): (256) > osp.system.service provider is found.
12-07 20:19:01.855+0530 I/Tizen::App( 1290): (506) > TerminatedApp(org.example.uibuildersingleview)
12-07 20:19:01.855+0530 I/Tizen::App( 1290): (512) > Not registered pid(3290)
12-07 20:19:01.855+0530 I/Tizen::System( 1290): (246) > Terminated app [org.example.uibuildersingleview]
12-07 20:19:01.855+0530 I/Tizen::Io( 1290): (729) > Entry not found
12-07 20:19:01.865+0530 I/ESD     (  941): esd_main.c: __esd_app_dead_handler(1773) > pid: 3290
12-07 20:19:01.865+0530 W/AUL_AMD (  664): amd_main.c: __app_dead_handler(327) > __app_dead_handler, pid: 3290
12-07 20:19:01.865+0530 W/AUL_AMD (  664): amd_main.c: __app_dead_handler(333) > app_group_leader_app, pid: 3290
12-07 20:19:01.865+0530 E/RESOURCED(  779): resourced-dbus.c: resourced_dbus_system_hash_drop_busname(324) > Does not exist in busname hash: :1.331
12-07 20:19:01.885+0530 I/APP_CORE(  895): appcore-efl.c: __do_app(514) > [APP 895] Event: RESUME State: PAUSED
12-07 20:19:01.885+0530 I/CAPI_APPFW_APPLICATION(  895): app_main.c: _ui_app_appcore_resume(706) > app_appcore_resume
12-07 20:19:01.885+0530 E/cluster-home(  895): homescreen.cpp: OnResume(233) >  app resume
12-07 20:19:01.885+0530 E/EFL     (  330): eo<330> lib/eo/eo.c:676 _eo_call_resolve() in lib/edje/edje_object.eo.c:316: func 'edje_obj_signal_emit' (415) could not be resolved for class 'Evas_Object_Smart'.
12-07 20:19:01.915+0530 W/AUL_AMD (  664): amd_launch.c: __e17_status_handler(3328) > pid(895) status(0)
12-07 20:19:01.915+0530 I/Tizen::System( 1290): (157) > change brightness system value.
12-07 20:19:01.915+0530 I/Tizen::App( 1290): (782) > Finished invoking application event listener for org.example.uibuildersingleview, 3290.
12-07 20:19:01.925+0530 W/CRASH_MANAGER( 3717): worker.c: worker_job(1199) > 1103290756962148112214
