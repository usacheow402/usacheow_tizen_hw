S/W Version Information
Model: SM-Z300H
Tizen-Version: 2.4.0.3
Build-Number: Z300HDDU0BPI2
Build-Date: 2016.09.30 15:37:12

Crash Information
Process Name: uibuildersingleview
PID: 3710
Date: 2016-12-07 20:20:25+0530
Executable File Path: /opt/usr/apps/org.example.uibuildersingleview/bin/uibuildersingleview
Signal: 11
      (SIGSEGV)
      si_code: -6
      signal sent by tkill (sent by pid 3710, uid 5000)

Register Information
r0   = 0xb91a6630, r1   = 0x0000000c
r2   = 0x0000003c, r3   = 0x0000000c
r4   = 0xb91160a0, r5   = 0xb9188010
r6   = 0x80012493, r7   = 0xb6d85488
r8   = 0xb918a660, r9   = 0xb91a6630
r10  = 0x80012493, fp   = 0xb917f320
ip   = 0xb69974d4, sp   = 0xbe9fef98
lr   = 0xb696e60d, pc   = 0xb66aa6f2
cpsr = 0x200e0030

Memory Information
MemTotal:   987012 KB
MemFree:    193380 KB
Buffers:     41868 KB
Cached:     314576 KB
VmPeak:     127784 KB
VmSize:     127780 KB
VmLck:           0 KB
VmPin:           0 KB
VmHWM:       28576 KB
VmRSS:       25964 KB
VmData:      44876 KB
VmStk:         136 KB
VmExe:          20 KB
VmLib:       35600 KB
VmPTE:          94 KB
VmSwap:          0 KB

Threads Information
Threads: 5
PID = 3710 TID = 3710
3710 3711 4014 4015 4024 

Maps Information
af10a000 af909000 rwxp [stack:4024]
b148e000 b1496000 r-xp /usr/lib/ecore_evas/engines/extn/v-1.13/module.so
b14a8000 b1ca7000 rwxp [stack:4015]
b1ca7000 b1ca8000 r-xp /usr/lib/edje/modules/feedback/v-1.13/module.so
b1cb8000 b1ccc000 r-xp /usr/lib/edje/modules/elm/v-1.13/module.so
b1ce0000 b1ce1000 r-xp /usr/lib/libX11-xcb.so.1.0.0
b1cf1000 b1cf4000 r-xp /usr/lib/libxcb-sync.so.1.0.0
b1d05000 b1d06000 r-xp /usr/lib/libxshmfence.so.1.0.0
b1d16000 b1d18000 r-xp /usr/lib/libxcb-present.so.0.0.0
b1d28000 b1d2a000 r-xp /usr/lib/libxcb-dri3.so.0.0.0
b1d3a000 b1d4a000 r-xp /usr/lib/evas/modules/engines/software_x11/v-1.13/module.so
b1d5a000 b1d66000 r-xp /usr/lib/ecore_evas/engines/x/v-1.13/module.so
b1d78000 b2577000 rwxp [stack:4014]
b2777000 b277e000 r-xp /usr/lib/libefl-extension.so.0.1.0
b2791000 b2797000 r-xp /usr/lib/bufmgr/libtbm_sprd7727.so.0.0.0
b27a7000 b27ac000 r-xp /opt/usr/apps/org.example.uibuildersingleview/bin/uibuildersingleview
b28fd000 b29e0000 r-xp /usr/lib/libscim-1.0.so.8.2.3
b2a17000 b2a3f000 r-xp /usr/lib/ecore_imf/modules/isf/v-1.13/module.so
b2a51000 b3250000 rwxp [stack:3711]
b3250000 b3252000 r-xp /usr/lib/ecore/system/systemd/v-1.13/module.so
b3262000 b326c000 r-xp /lib/libnss_files-2.20-2014.11.so
b327d000 b3286000 r-xp /lib/libnss_nis-2.20-2014.11.so
b3297000 b32a8000 r-xp /lib/libnsl-2.20-2014.11.so
b32bb000 b32c1000 r-xp /lib/libnss_compat-2.20-2014.11.so
b32d2000 b32d3000 r-xp /usr/lib/osp/libappinfo.so.1.2.2.1
b32fb000 b3302000 r-xp /usr/lib/libminizip.so.1.0.0
b3312000 b3317000 r-xp /usr/lib/libstorage.so.0.1
b3327000 b3386000 r-xp /usr/lib/libmmfcamcorder.so.0.0.0
b339c000 b33b0000 r-xp /usr/lib/libcapi-media-camera.so.0.1.90
b33c0000 b3404000 r-xp /usr/lib/libgstbase-1.0.so.0.405.0
b3414000 b341c000 r-xp /usr/lib/libgstapp-1.0.so.0.405.0
b342c000 b345c000 r-xp /usr/lib/libgstvideo-1.0.so.0.405.0
b346f000 b3528000 r-xp /usr/lib/libgstreamer-1.0.so.0.405.0
b353c000 b358f000 r-xp /usr/lib/libmmfplayer.so.0.0.0
b35a0000 b35bb000 r-xp /usr/lib/libcapi-media-player.so.0.2.16
b35cb000 b368c000 r-xp /usr/lib/libprotobuf.so.9.0.1
b369f000 b36af000 r-xp /usr/lib/libefl-assist.so.0.1.0
b36bf000 b36cc000 r-xp /usr/lib/libmdm-common.so.1.0.98
b36dd000 b36e4000 r-xp /usr/lib/libcapi-media-tool.so.0.2.2
b36f4000 b3735000 r-xp /usr/lib/libmdm.so.1.2.12
b3745000 b374d000 r-xp /usr/lib/lib_DNSe_NRSS_ver225.so
b375c000 b376c000 r-xp /usr/lib/lib_SamsungRec_TizenV04014.so
b378d000 b37ed000 r-xp /usr/lib/libasound.so.2.0.0
b37ff000 b3802000 r-xp /usr/lib/libpulse-simple.so.0.0.4
b3812000 b3815000 r-xp /usr/lib/libsyspopup_caller.so.0.1.0
b3825000 b382a000 r-xp /usr/lib/libmmfsoundcommon.so.0.0.0
b383a000 b383b000 r-xp /usr/lib/libgthread-2.0.so.0.4301.0
b384b000 b3856000 r-xp /usr/lib/libaudio-session-mgr.so.0.0.1
b386a000 b3871000 r-xp /usr/lib/libmmutil_imgp.so.0.0.0
b3881000 b3887000 r-xp /usr/lib/libmmutil_jpeg.so.0.0.0
b3897000 b389c000 r-xp /usr/lib/libmmfsession.so.0.0.1
b38ac000 b38c7000 r-xp /usr/lib/libmmfsound.so.0.1.0
b38d7000 b38de000 r-xp /usr/lib/libmmfcommon.so.0.0.0
b38ee000 b38f1000 r-xp /usr/lib/libcapi-media-image-util.so.0.1.10
b3902000 b3930000 r-xp /usr/lib/libidn.so.11.5.44
b3940000 b3956000 r-xp /usr/lib/libnghttp2.so.5.4.0
b3967000 b3971000 r-xp /usr/lib/libcares.so.2.1.0
b3981000 b398b000 r-xp /usr/lib/libcapi-media-sound-manager.so.0.3.32
b399b000 b399d000 r-xp /usr/lib/libcapi-media-wav-player.so.0.1.18
b39ad000 b39ae000 r-xp /usr/lib/libmmfkeysound.so.0.0.0
b39be000 b39c2000 r-xp /usr/lib/libecore_ipc.so.1.13.0
b39d3000 b39fb000 r-xp /usr/lib/libui-extension.so.0.1.0
b3a0c000 b3a35000 r-xp /usr/lib/libturbojpeg.so
b3a55000 b3a5b000 r-xp /usr/lib/libgif.so.4.1.6
b3a6b000 b3ab1000 r-xp /usr/lib/libcurl.so.4.3.0
b3ac2000 b3ae3000 r-xp /usr/lib/libexif.so.12.3.3
b3afe000 b3b13000 r-xp /usr/lib/libtts.so
b3b24000 b3b2c000 r-xp /usr/lib/libfeedback.so.0.1.4
b3b3c000 b3c02000 r-xp /usr/lib/libdali-core.so.0.0.0
b3c22000 b3d1a000 r-xp /usr/lib/libdali-adaptor.so.0.0.0
b3d39000 b3e07000 r-xp /usr/lib/libdali-toolkit.so.0.0.0
b3e1e000 b3e20000 r-xp /usr/lib/libboost_system.so.1.51.0
b3e30000 b3e36000 r-xp /usr/lib/libboost_chrono.so.1.51.0
b3e46000 b3e69000 r-xp /usr/lib/libboost_thread.so.1.51.0
b3e7a000 b3e7c000 r-xp /usr/lib/libappsvc.so.0.1.0
b3e8c000 b3e8e000 r-xp /usr/lib/libcapi-appfw-app-common.so.0.3.1.0
b3e9f000 b3ea4000 r-xp /usr/lib/libcapi-appfw-app-control.so.0.3.1.0
b3ebb000 b3ebd000 r-xp /usr/lib/libosp-env-config.so.1.2.2.1
b3ecd000 b3ed4000 r-xp /usr/lib/libsensord-share.so
b3ee4000 b3efc000 r-xp /usr/lib/libsensor.so.1.1.0
b3f0d000 b3f10000 r-xp /usr/lib/libXv.so.1.0.0
b3f20000 b3f25000 r-xp /usr/lib/libutilX.so.1.1.0
b3f35000 b3f3a000 r-xp /usr/lib/libappcore-common.so.1.1
b3f4a000 b3f51000 r-xp /usr/lib/libcapi-ui-efl-util.so.0.2.11
b3f64000 b3f68000 r-xp /usr/lib/libcapi-appfw-application.so.0.3.1.0
b3f79000 b4057000 r-xp /usr/lib/libCOREGL.so.4.0
b4077000 b407a000 r-xp /usr/lib/libuuid.so.1.3.0
b408a000 b40a1000 r-xp /usr/lib/libblkid.so.1.1.0
b40b2000 b40b4000 r-xp /usr/lib/libXau.so.6.0.0
b40c4000 b410b000 r-xp /usr/lib/pulseaudio/libpulsecommon-4.0.so
b411d000 b4123000 r-xp /usr/lib/libjson-c.so.2.0.1
b4134000 b4138000 r-xp /usr/lib/libogg.so.0.7.1
b4148000 b416a000 r-xp /usr/lib/libvorbis.so.0.4.3
b417a000 b425e000 r-xp /usr/lib/libvorbisenc.so.2.0.6
b427a000 b427d000 r-xp /usr/lib/libEGL.so.1.4
b428e000 b4294000 r-xp /usr/lib/libxcb-render.so.0.0.0
b42a4000 b42a6000 r-xp /usr/lib/libxcb-shm.so.0.0.0
b42b6000 b42c3000 r-xp /usr/lib/libGLESv2.so.2.0
b42d4000 b4336000 r-xp /usr/lib/libpixman-1.so.0.28.2
b434b000 b4363000 r-xp /usr/lib/libmount.so.1.1.0
b4375000 b4389000 r-xp /usr/lib/libxcb.so.1.1.0
b4399000 b43a0000 r-xp /lib/libcrypt-2.20-2014.11.so
b43d8000 b43da000 r-xp /usr/lib/libiri.so
b43ea000 b43f5000 r-xp /usr/lib/libgpg-error.so.0.15.0
b4406000 b443c000 r-xp /usr/lib/libpulse.so.0.16.2
b444d000 b4490000 r-xp /usr/lib/libsndfile.so.1.0.25
b44a5000 b44ba000 r-xp /lib/libexpat.so.1.5.2
b44cc000 b458a000 r-xp /usr/lib/libcairo.so.2.11200.14
b459e000 b45a6000 r-xp /usr/lib/libdrm.so.2.4.0
b45b6000 b45b9000 r-xp /usr/lib/libdri2.so.0.0.0
b45c9000 b4617000 r-xp /usr/lib/libssl.so.1.0.0
b462c000 b4638000 r-xp /usr/lib/libeeze.so.1.13.0
b4649000 b4652000 r-xp /usr/lib/libethumb.so.1.13.0
b4662000 b4665000 r-xp /usr/lib/libecore_input_evas.so.1.13.0
b4675000 b482c000 r-xp /usr/lib/libcrypto.so.1.0.0
b5617000 b5620000 r-xp /usr/lib/libXi.so.6.1.0
b5630000 b5632000 r-xp /usr/lib/libXgesture.so.7.0.0
b5642000 b5646000 r-xp /usr/lib/libXtst.so.6.1.0
b5656000 b565c000 r-xp /usr/lib/libXrender.so.1.3.0
b566c000 b5672000 r-xp /usr/lib/libXrandr.so.2.2.0
b5682000 b5684000 r-xp /usr/lib/libXinerama.so.1.0.0
b5695000 b5698000 r-xp /usr/lib/libXfixes.so.3.1.0
b56a8000 b56b3000 r-xp /usr/lib/libXext.so.6.4.0
b56c3000 b56c5000 r-xp /usr/lib/libXdamage.so.1.1.0
b56d5000 b56d7000 r-xp /usr/lib/libXcomposite.so.1.0.0
b56e7000 b57c9000 r-xp /usr/lib/libX11.so.6.3.0
b57dd000 b57e4000 r-xp /usr/lib/libXcursor.so.1.0.2
b57f4000 b580c000 r-xp /usr/lib/libudev.so.1.6.0
b580e000 b5811000 r-xp /lib/libattr.so.1.1.0
b5821000 b5841000 r-xp /usr/lib/libsecurity-server-commons.so.1.0.0
b5842000 b5847000 r-xp /usr/lib/libffi.so.6.0.2
b5858000 b5870000 r-xp /lib/libz.so.1.2.8
b5880000 b5882000 r-xp /usr/lib/libgmodule-2.0.so.0.4301.0
b5892000 b5967000 r-xp /usr/lib/libxml2.so.2.9.2
b597c000 b5a17000 r-xp /usr/lib/libstdc++.so.6.0.20
b5a33000 b5a36000 r-xp /usr/lib/libSLP-db-util.so.0.1.0
b5a46000 b5a5f000 r-xp /usr/lib/libpkgmgr_parser.so.0.1.0
b5a70000 b5a81000 r-xp /lib/libresolv-2.20-2014.11.so
b5a95000 b5b0f000 r-xp /usr/lib/libgcrypt.so.20.0.3
b5b24000 b5b26000 r-xp /usr/lib/libecore_imf_evas.so.1.13.0
b5b36000 b5b3d000 r-xp /usr/lib/libembryo.so.1.13.0
b5b4d000 b5b57000 r-xp /usr/lib/libecore_audio.so.1.13.0
b5b68000 b5b80000 r-xp /usr/lib/libpng12.so.0.50.0
b5b91000 b5bb4000 r-xp /usr/lib/libjpeg.so.8.0.2
b5bd5000 b5be9000 r-xp /usr/lib/libector.so.1.13.0
b5bfa000 b5c12000 r-xp /usr/lib/liblua-5.1.so
b5c23000 b5c7a000 r-xp /usr/lib/libfreetype.so.6.11.3
b5c8e000 b5cb6000 r-xp /usr/lib/libfontconfig.so.1.8.0
b5cc7000 b5cda000 r-xp /usr/lib/libfribidi.so.0.3.1
b5ceb000 b5d25000 r-xp /usr/lib/libharfbuzz.so.0.940.0
b5d36000 b5d44000 r-xp /usr/lib/libgraphics-extension.so.0.1.0
b5d54000 b5d5c000 r-xp /usr/lib/libtbm.so.1.0.0
b5d6c000 b5d79000 r-xp /usr/lib/libeio.so.1.13.0
b5d89000 b5d8b000 r-xp /usr/lib/libefreet_trash.so.1.13.0
b5d9b000 b5da0000 r-xp /usr/lib/libefreet_mime.so.1.13.0
b5db0000 b5dc7000 r-xp /usr/lib/libefreet.so.1.13.0
b5dd9000 b5df9000 r-xp /usr/lib/libeldbus.so.1.13.0
b5e09000 b5e29000 r-xp /usr/lib/libecore_con.so.1.13.0
b5e2b000 b5e31000 r-xp /usr/lib/libecore_imf.so.1.13.0
b5e41000 b5e52000 r-xp /usr/lib/libemotion.so.1.13.0
b5e63000 b5e6a000 r-xp /usr/lib/libethumb_client.so.1.13.0
b5e7a000 b5e89000 r-xp /usr/lib/libeo.so.1.13.0
b5e9a000 b5eac000 r-xp /usr/lib/libecore_input.so.1.13.0
b5ebd000 b5ec2000 r-xp /usr/lib/libecore_file.so.1.13.0
b5ed2000 b5eeb000 r-xp /usr/lib/libecore_evas.so.1.13.0
b5efb000 b5f18000 r-xp /usr/lib/libeet.so.1.13.0
b5f31000 b5f79000 r-xp /usr/lib/libeina.so.1.13.0
b5f8a000 b5f9a000 r-xp /usr/lib/libefl.so.1.13.0
b5fab000 b6090000 r-xp /usr/lib/libicuuc.so.51.1
b60ad000 b61ed000 r-xp /usr/lib/libicui18n.so.51.1
b6204000 b623c000 r-xp /usr/lib/libecore_x.so.1.13.0
b624e000 b6251000 r-xp /lib/libcap.so.2.21
b6261000 b628a000 r-xp /usr/lib/libsecurity-server-client.so.1.0.1
b629b000 b62a2000 r-xp /usr/lib/libcapi-base-common.so.0.2.2
b62b4000 b62eb000 r-xp /usr/lib/libgobject-2.0.so.0.4301.0
b62fc000 b63e7000 r-xp /usr/lib/libgio-2.0.so.0.4301.0
b63fa000 b6473000 r-xp /usr/lib/libsqlite3.so.0.8.6
b6485000 b648a000 r-xp /usr/lib/libcapi-system-info.so.0.2.1
b649a000 b64a4000 r-xp /usr/lib/libvconf.so.0.2.45
b64b4000 b64b6000 r-xp /usr/lib/libvasum.so.0.3.1
b64c6000 b64c8000 r-xp /usr/lib/libttrace.so.1.1
b64d8000 b64db000 r-xp /usr/lib/libiniparser.so.0
b64eb000 b6511000 r-xp /usr/lib/libpkgmgr-info.so.0.0.17
b6521000 b6526000 r-xp /usr/lib/libxdgmime.so.1.1.0
b6537000 b654e000 r-xp /usr/lib/libdbus-glib-1.so.2.2.2
b655f000 b65ca000 r-xp /lib/libm-2.20-2014.11.so
b65db000 b65e1000 r-xp /lib/librt-2.20-2014.11.so
b65f2000 b65ff000 r-xp /usr/lib/libunwind.so.8.0.1
b6635000 b6759000 r-xp /lib/libc-2.20-2014.11.so
b676e000 b6787000 r-xp /lib/libgcc_s-4.9.so.1
b6797000 b6879000 r-xp /usr/lib/libglib-2.0.so.0.4301.0
b688a000 b68b4000 r-xp /usr/lib/libdbus-1.so.3.8.12
b68c5000 b6901000 r-xp /usr/lib/libsystemd.so.0.4.0
b6903000 b6986000 r-xp /usr/lib/libedje.so.1.13.0
b6999000 b69b7000 r-xp /usr/lib/libecore.so.1.13.0
b69d7000 b6b5e000 r-xp /usr/lib/libevas.so.1.13.0
b6b93000 b6ba7000 r-xp /lib/libpthread-2.20-2014.11.so
b6bbb000 b6def000 r-xp /usr/lib/libelementary.so.1.13.0
b6e1e000 b6e22000 r-xp /usr/lib/libsmack.so.1.0.0
b6e32000 b6e39000 r-xp /usr/lib/libprivilege-control.so.0.0.2
b6e49000 b6e4b000 r-xp /usr/lib/libdlog.so.0.0.0
b6e5b000 b6e5e000 r-xp /usr/lib/libbundle.so.0.1.22
b6e6e000 b6e70000 r-xp /lib/libdl-2.20-2014.11.so
b6e81000 b6e99000 r-xp /usr/lib/libaul.so.0.1.0
b6ead000 b6eb2000 r-xp /usr/lib/libappcore-efl.so.1.1
b6ec3000 b6ed0000 r-xp /usr/lib/liblptcp.so
b6ee2000 b6ee6000 r-xp /usr/lib/libsys-assert.so
b6ef7000 b6f17000 r-xp /lib/ld-2.20-2014.11.so
b6f28000 b6f2d000 r-xp /usr/bin/launchpad-loader
b8f34000 b9259000 rw-p [heap]
be9df000 bea00000 rwxp [stack]
b8f34000 b9259000 rw-p [heap]
be9df000 bea00000 rwxp [stack]
End of Maps Information

Callstack Information (PID:3710)
Call Stack Count: 1
 0: strcmp + 0x1 (0xb66aa6f2) [/lib/libc.so.6] + 0x756f2
End of Call Stack

Package Information
Package Name: org.example.uibuildersingleview
Package ID : org.example.uibuildersingleview
Version: 1.0.0
Package Type: tpk
App Name: uibuildersingleview
App ID: org.example.uibuildersingleview
Type: capp
Categories: 

Latest Debug Message Information
--------- beginning of /dev/log_main
9:01.935+0530 I/Tizen::System( 1290): (259) > Active app [com.samsun], current [org.exampl] 
12-07 20:19:01.935+0530 I/Tizen::Io( 1290): (729) > Entry not found
12-07 20:19:01.945+0530 I/Tizen::System( 1290): (157) > change brightness system value.
12-07 20:19:01.945+0530 I/TIZEN_N_SOUND_MANAGER(  961): sound_manager.c: sound_manager_get_volume(84) > returns : type=0, volume=8, ret=0x0
12-07 20:19:01.945+0530 E/TIZEN_N_SOUND_MANAGER(  961): sound_manager_private.c: __convert_sound_manager_error_code(142) > [sound_manager_get_volume] ERROR_NONE(0x00000000) : core frameworks error code(0x00000000)
12-07 20:19:01.945+0530 I/TIZEN_N_SOUND_MANAGER(  961): sound_manager.c: sound_manager_get_volume(84) > returns : type=4, volume=0, ret=0x0
12-07 20:19:01.945+0530 E/TIZEN_N_SOUND_MANAGER(  961): sound_manager_private.c: __convert_sound_manager_error_code(142) > [sound_manager_get_volume] ERROR_NONE(0x00000000) : core frameworks error code(0x00000000)
12-07 20:19:01.945+0530 W/AUL_AMD (  664): amd_request.c: __request_handler(906) > __request_handler: 15
12-07 20:19:02.085+0530 E/EFL     (  330): elementary<330> elm_transit.c:648 elm_transit_del_cb_set() Elm_Transit transit has already been deleted!
12-07 20:19:06.750+0530 E/ALARM_MANAGER(  709): alarm-manager.c: __get_zone_name(506) > zone name [/]
12-07 20:19:06.750+0530 W/ALARM_MANAGER(  709): alarm-manager.c: __check_privilege_by_cookie(2084) > Get proc/595/status successfully
12-07 20:19:06.750+0530 W/ALARM_MANAGER(  709): alarm-manager.c: __check_privilege_by_cookie(2093) > uid : 0
12-07 20:19:06.750+0530 E/ALARM_MANAGER(  709): alarm-manager.c: __alarm_remove_from_list(639) > [alarm-server]:Remove alarm id(907658645) zone(/)
12-07 20:19:06.750+0530 E/ALARM_MANAGER(  709): alarm-manager.c: __rtc_set(237) > [alarm-server]ALARM_CLEAR ioctl is successfully done.
12-07 20:19:06.750+0530 E/ALARM_MANAGER(  709): alarm-manager.c: __rtc_set(244) > Setted RTC Alarm date/time is 7-12-2016, 15:05:10 (UTC).
12-07 20:19:06.750+0530 E/ALARM_MANAGER(  709): alarm-manager.c: __rtc_set(259) > [alarm-server]RTC ALARM_SET ioctl is successfully done.
12-07 20:19:06.750+0530 E/ALARM_MANAGER(  709): alarm-manager.c: alarm_manager_alarm_delete(2954) > alarm_id[907658645] is removed.
12-07 20:19:06.750+0530 E/ALARM_MANAGER(  709): alarm-manager.c: __get_zone_name(506) > zone name [/]
12-07 20:19:06.750+0530 W/ALARM_MANAGER(  709): alarm-manager.c: __check_privilege_by_cookie(2084) > Get proc/595/status successfully
12-07 20:19:06.750+0530 W/ALARM_MANAGER(  709): alarm-manager.c: __check_privilege_by_cookie(2093) > uid : 0
12-07 20:19:06.750+0530 W/AUL_AMD (  664): amd_request.c: __request_handler(906) > __request_handler: 15
12-07 20:19:06.760+0530 I/AUL_AMD (  664): menu_db_util.h: _get_app_info_from_db_by_apppath(242) > path : /usr/sbin/connmand, ret : 0
12-07 20:19:06.770+0530 I/AUL_AMD (  664): menu_db_util.h: _get_app_info_from_db_by_apppath(242) > path : /usr/sbin/connmand, ret : 0
12-07 20:19:06.770+0530 W/AUL_AMD (  664): amd_request.c: __request_handler(906) > __request_handler: 15
12-07 20:19:06.780+0530 I/AUL_AMD (  664): menu_db_util.h: _get_app_info_from_db_by_apppath(242) > path : /usr/sbin/connmand, ret : 0
12-07 20:19:06.780+0530 I/AUL_AMD (  664): menu_db_util.h: _get_app_info_from_db_by_apppath(242) > path : /usr/sbin/connmand, ret : 0
12-07 20:19:06.790+0530 W/AUL_AMD (  664): amd_request.c: __request_handler(906) > __request_handler: 38
12-07 20:19:06.790+0530 E/AUL_AMD (  664): amd_status.c: _status_get_cmdline(1204) > cmdline : /usr/sbin/connmand
12-07 20:19:06.790+0530 E/AUL_AMD (  664): amd_status.c: _status_get_cmdline(1207) > pkt->data : /usr/sbin/connmand
12-07 20:19:06.790+0530 E/ALARM_MANAGER(  709): alarm-manager-schedule.c: __alarm_next_duetime_once(152) > Final due_time = 1481122150, Wed Dec  7 20:19:10 2016
12-07 20:19:06.790+0530 E/ALARM_MANAGER(  709): alarm-manager-schedule.c: _alarm_next_duetime(502) > alarm_id: 2037912670, next duetime: 1481122150
12-07 20:19:06.790+0530 E/ALARM_MANAGER(  709): alarm-manager.c: __alarm_add_to_list(562) > [alarm-server]: After add alarm_id(2037912670)
12-07 20:19:06.790+0530 E/ALARM_MANAGER(  709): alarm-manager.c: __alarm_create(1183) > [alarm-server]:alarm_context.c_due_time(1481123110), due_time(1481122150)
12-07 20:19:06.790+0530 E/ALARM_MANAGER(  709): alarm-manager.c: __rtc_set(237) > [alarm-server]ALARM_CLEAR ioctl is successfully done.
12-07 20:19:06.790+0530 E/ALARM_MANAGER(  709): alarm-manager.c: __rtc_set(244) > Setted RTC Alarm date/time is 7-12-2016, 14:49:10 (UTC).
12-07 20:19:06.790+0530 E/ALARM_MANAGER(  709): alarm-manager.c: __rtc_set(259) > [alarm-server]RTC ALARM_SET ioctl is successfully done.
12-07 20:19:06.790+0530 E/ALARM_MANAGER(  709): alarm-manager.c: __get_zone_name(506) > zone name [/]
12-07 20:19:06.790+0530 W/ALARM_MANAGER(  709): alarm-manager.c: __check_privilege_by_cookie(2084) > Get proc/595/status successfully
12-07 20:19:06.790+0530 W/ALARM_MANAGER(  709): alarm-manager.c: __check_privilege_by_cookie(2093) > uid : 0
12-07 20:19:06.790+0530 E/ALARM_MANAGER(  709): alarm-manager.c: __alarm_remove_from_list(639) > [alarm-server]:Remove alarm id(2037912670) zone(/)
12-07 20:19:06.790+0530 E/ALARM_MANAGER(  709): alarm-manager.c: __rtc_set(237) > [alarm-server]ALARM_CLEAR ioctl is successfully done.
12-07 20:19:06.790+0530 E/ALARM_MANAGER(  709): alarm-manager.c: __rtc_set(244) > Setted RTC Alarm date/time is 7-12-2016, 15:05:10 (UTC).
12-07 20:19:06.790+0530 E/ALARM_MANAGER(  709): alarm-manager.c: __rtc_set(259) > [alarm-server]RTC ALARM_SET ioctl is successfully done.
12-07 20:19:06.790+0530 E/ALARM_MANAGER(  709): alarm-manager.c: alarm_manager_alarm_delete(2954) > alarm_id[2037912670] is removed.
12-07 20:19:06.800+0530 E/ALARM_MANAGER(  709): alarm-manager.c: __get_zone_name(506) > zone name [/]
12-07 20:19:06.800+0530 W/ALARM_MANAGER(  709): alarm-manager.c: __check_privilege_by_cookie(2084) > Get proc/595/status successfully
12-07 20:19:06.800+0530 W/ALARM_MANAGER(  709): alarm-manager.c: __check_privilege_by_cookie(2093) > uid : 0
12-07 20:19:06.800+0530 W/AUL_AMD (  664): amd_request.c: __request_handler(906) > __request_handler: 15
12-07 20:19:06.800+0530 I/AUL_AMD (  664): menu_db_util.h: _get_app_info_from_db_by_apppath(242) > path : /usr/sbin/connmand, ret : 0
12-07 20:19:06.810+0530 I/AUL_AMD (  664): menu_db_util.h: _get_app_info_from_db_by_apppath(242) > path : /usr/sbin/connmand, ret : 0
12-07 20:19:06.810+0530 W/AUL_AMD (  664): amd_request.c: __request_handler(906) > __request_handler: 15
12-07 20:19:06.820+0530 I/AUL_AMD (  664): menu_db_util.h: _get_app_info_from_db_by_apppath(242) > path : /usr/sbin/connmand, ret : 0
12-07 20:19:06.830+0530 I/AUL_AMD (  664): menu_db_util.h: _get_app_info_from_db_by_apppath(242) > path : /usr/sbin/connmand, ret : 0
12-07 20:19:06.830+0530 W/AUL_AMD (  664): amd_request.c: __request_handler(906) > __request_handler: 38
12-07 20:19:06.830+0530 E/AUL_AMD (  664): amd_status.c: _status_get_cmdline(1204) > cmdline : /usr/sbin/connmand
12-07 20:19:06.830+0530 E/AUL_AMD (  664): amd_status.c: _status_get_cmdline(1207) > pkt->data : /usr/sbin/connmand
12-07 20:19:06.830+0530 E/ALARM_MANAGER(  709): alarm-manager-schedule.c: __alarm_next_duetime_once(152) > Final due_time = 1481123946, Wed Dec  7 20:49:06 2016
12-07 20:19:06.830+0530 E/ALARM_MANAGER(  709): alarm-manager-schedule.c: _alarm_next_duetime(502) > alarm_id: 2037912670, next duetime: 1481123946
12-07 20:19:06.830+0530 E/ALARM_MANAGER(  709): alarm-manager.c: __alarm_add_to_list(562) > [alarm-server]: After add alarm_id(2037912670)
12-07 20:19:06.830+0530 E/ALARM_MANAGER(  709): alarm-manager.c: __alarm_create(1183) > [alarm-server]:alarm_context.c_due_time(1481123110), due_time(1481123946)
12-07 20:19:06.830+0530 E/ALARM_MANAGER(  709): alarm-manager.c: __rtc_set(237) > [alarm-server]ALARM_CLEAR ioctl is successfully done.
12-07 20:19:06.830+0530 E/ALARM_MANAGER(  709): alarm-manager.c: __rtc_set(244) > Setted RTC Alarm date/time is 7-12-2016, 15:05:10 (UTC).
12-07 20:19:06.830+0530 E/ALARM_MANAGER(  709): alarm-manager.c: __rtc_set(259) > [alarm-server]RTC ALARM_SET ioctl is successfully done.
12-07 20:19:40.372+0530 E/INDICATOR(  776): list.c: list_try_to_find_icon_to_show(299) > (!icon) -> list_try_to_find_icon_to_show() return
12-07 20:19:40.372+0530 E/INDICATOR(  776): list.c: list_try_to_find_icon_to_show(299) > (!icon) -> list_try_to_find_icon_to_show() return
12-07 20:19:40.372+0530 E/INDICATOR(  776): list.c: list_try_to_find_icon_to_show(299) > (!icon) -> list_try_to_find_icon_to_show() return
12-07 20:19:40.372+0530 E/INDICATOR(  776): list.c: list_try_to_find_icon_to_show(299) > (!icon) -> list_try_to_find_icon_to_show() return
12-07 20:19:40.372+0530 E/INDICATOR(  776): box.c: _update_icon(232) > (!list) -> _update_icon() return
12-07 20:19:40.372+0530 E/INDICATOR(  776): box.c: _update_icon(232) > (!list) -> _update_icon() return
12-07 20:19:40.372+0530 E/INDICATOR(  776): list.c: list_try_to_find_icon_to_remove(378) > (!icon) -> list_try_to_find_icon_to_remove() return
12-07 20:19:40.372+0530 E/INDICATOR(  776): list.c: list_try_to_find_icon_to_show(299) > (!icon) -> list_try_to_find_icon_to_show() return
12-07 20:19:40.372+0530 E/INDICATOR(  776): list.c: list_try_to_find_icon_to_show(299) > (!icon) -> list_try_to_find_icon_to_show() return
12-07 20:19:40.372+0530 E/INDICATOR(  776): list.c: list_try_to_find_icon_to_show(299) > (!icon) -> list_try_to_find_icon_to_show() return
12-07 20:19:40.392+0530 E/INDICATOR(  776): box.c: _update_icon(232) > (!list) -> _update_icon() return
12-07 20:19:40.392+0530 E/INDICATOR(  776): box.c: _update_icon(232) > (!list) -> _update_icon() return
12-07 20:19:49.371+0530 E/INDICATOR(  776): list.c: list_try_to_find_icon_to_show(299) > (!icon) -> list_try_to_find_icon_to_show() return
12-07 20:19:49.371+0530 E/INDICATOR(  776): list.c: list_try_to_find_icon_to_show(299) > (!icon) -> list_try_to_find_icon_to_show() return
12-07 20:19:49.371+0530 E/INDICATOR(  776): list.c: list_try_to_find_icon_to_show(299) > (!icon) -> list_try_to_find_icon_to_show() return
12-07 20:19:49.371+0530 E/INDICATOR(  776): list.c: list_try_to_find_icon_to_show(299) > (!icon) -> list_try_to_find_icon_to_show() return
12-07 20:19:49.371+0530 E/INDICATOR(  776): box.c: _update_icon(232) > (!list) -> _update_icon() return
12-07 20:19:49.371+0530 E/INDICATOR(  776): box.c: _update_icon(232) > (!list) -> _update_icon() return
12-07 20:19:49.371+0530 E/INDICATOR(  776): list.c: list_try_to_find_icon_to_remove(378) > (!icon) -> list_try_to_find_icon_to_remove() return
12-07 20:19:49.371+0530 E/INDICATOR(  776): list.c: list_try_to_find_icon_to_show(299) > (!icon) -> list_try_to_find_icon_to_show() return
12-07 20:19:49.371+0530 E/INDICATOR(  776): list.c: list_try_to_find_icon_to_show(299) > (!icon) -> list_try_to_find_icon_to_show() return
12-07 20:19:49.371+0530 E/INDICATOR(  776): list.c: list_try_to_find_icon_to_show(299) > (!icon) -> list_try_to_find_icon_to_show() return
12-07 20:19:49.381+0530 E/INDICATOR(  776): box.c: _update_icon(232) > (!list) -> _update_icon() return
12-07 20:19:49.381+0530 E/INDICATOR(  776): box.c: _update_icon(232) > (!list) -> _update_icon() return
12-07 20:19:51.353+0530 W/LOCKSCREEN(  882): property.c: _vconf_cb(228) > [_vconf_cb:228:W] property 200 is changed to 35
12-07 20:19:51.353+0530 W/LOCKSCREEN(  882): daemon.c: lockd_event(1028) > [lockd_event:1028:W] event:40000:CONF_CHANGED
12-07 20:19:51.353+0530 W/LOCKSCREEN(  882): daemon.c: _event_route(838) > [_event_route:838:W] event:40000 event_info:200
12-07 20:19:51.353+0530 W/LOCKSCREEN(  882): view-mgr.c: _event_route(130) > [_event_route:130:W] event:40000 event_info:200
12-07 20:19:51.363+0530 I/INDICATOR(  776): battery.c: show_battery_icon(376) > "Percentage:(0) / Level:(3) / batt_Full:(0) / battery_charging(1)"
12-07 20:19:51.363+0530 E/INDICATOR(  776): list.c: list_try_to_find_icon_to_remove(378) > (!icon) -> list_try_to_find_icon_to_remove() return
12-07 20:19:51.363+0530 E/INDICATOR(  776): list.c: list_try_to_find_icon_to_show(299) > (!icon) -> list_try_to_find_icon_to_show() return
12-07 20:19:51.363+0530 E/INDICATOR(  776): list.c: list_try_to_find_icon_to_show(299) > (!icon) -> list_try_to_find_icon_to_show() return
12-07 20:19:51.363+0530 E/INDICATOR(  776): list.c: list_try_to_find_icon_to_show(299) > (!icon) -> list_try_to_find_icon_to_show() return
12-07 20:19:51.363+0530 E/INDICATOR(  776): box.c: _update_icon(232) > (!list) -> _update_icon() return
12-07 20:19:51.363+0530 E/INDICATOR(  776): box.c: _update_icon(232) > (!list) -> _update_icon() return
12-07 20:19:52.084+0530 E/PKGMGR_SERVER( 3802): pkgmgr-server.c: main(2414) > package manager server start
12-07 20:19:52.124+0530 E/PKGMGR_SERVER( 3802): pkgmgr-server.c: __set_recovery_mode(234) > rev_file[/opt/share/packages/.recovery/pkgmgr/org.example.uibuildersingleview] is null
12-07 20:19:52.134+0530 E/PKGMGR  ( 3800): pkgmgr.c: __check_sync_process(883) > file is can not remove[/tmp/org.example.uibuildersingleview, -1]
12-07 20:19:52.174+0530 W/AUL_AMD (  664): amd_request.c: __request_handler(906) > __request_handler: 14
12-07 20:19:52.174+0530 W/AUL_AMD (  664): amd_request.c: __send_result_to_client(158) > __send_result_to_client, pid: -1
12-07 20:19:52.184+0530 E/PKGMGR_SERVER( 3802): pkgmgr-server.c: __unset_recovery_mode(282) > remove recovery_file[/opt/share/packages/.recovery/pkgmgr/org.example.uibuildersingleview] fail
12-07 20:19:52.184+0530 E/PKGMGR_SERVER( 3802): pkgmgr-server.c: sighandler(417) > child NORMAL exit [3805]
12-07 20:19:54.516+0530 E/PKGMGR_SERVER( 3802): pkgmgr-server.c: exit_server(1381) > exit_server Start [backend_status=1, queue_status=1, drm_status=1] 
12-07 20:19:54.516+0530 E/PKGMGR_SERVER( 3802): pkgmgr-server.c: main(2471) > package manager server terminated.
12-07 20:19:59.081+0530 E/PKGMGR  ( 3890): pkgmgr.c: pkgmgr_client_reinstall(2020) > reinstall pkg start.
12-07 20:19:59.161+0530 E/PKGMGR_SERVER( 3892): pkgmgr-server.c: main(2414) > package manager server start
12-07 20:19:59.211+0530 E/PKGMGR_SERVER( 3892): pkgmgr-server-internal.c: _zone_set_type_and_backend(180) > tep_filepath [org.example.uibuildersingleview]
12-07 20:19:59.221+0530 E/PKGMGR_SERVER( 3892): pm-mdm.c: _get_package_info_from_file(116) > [0;31m[_get_package_info_from_file(): 116](ret < 0) access() failed. path: org.example.uibuildersingleview errno: 2 (No such file or directory)[0;m
12-07 20:19:59.221+0530 E/PKGMGR_SERVER( 3892): pkgmgr-server.c: __set_recovery_mode(234) > rev_file[/opt/share/packages/.recovery/pkgmgr/org.example.uibuildersingleview] is null
12-07 20:19:59.221+0530 E/PKGMGR  ( 3890): pkgmgr.c: pkgmgr_client_reinstall(2133) > reinstall pkg finish, ret=[38900002]
12-07 20:19:59.371+0530 I/Tizen::App( 1290): (1894) > PackageEventHandler - req: 12900002, pkg_type: tpk, pkg_name: org.example.uibuildersingleview, key: start, val: update
12-07 20:19:59.371+0530 I/Tizen::App( 1290): (1584) > Package = [org.example.uibuildersingleview], Key = [start], Value = [update], install = [1]
12-07 20:19:59.371+0530 E/PKGMGR  ( 1495): pkgmgr.c: __find_op_cbinfo(312) > tmp is NULL
12-07 20:19:59.371+0530 E/PKGMGR  ( 1495): pkgmgr.c: __find_op_cbinfo(312) > tmp is NULL
12-07 20:19:59.381+0530 W/AUL_AMD (  664): amd_appinfo.c: __amd_pkgmgrinfo_status_cb(1068) > __amd_pkgmgrinfo_start_handler
12-07 20:19:59.381+0530 I/Tizen::App( 1290): (1894) > PackageEventHandler - req: 12900002, pkg_type: tpk, pkg_name: org.example.uibuildersingleview, key: install_percent, val: 30
12-07 20:19:59.381+0530 I/Tizen::App( 1290): (119) > InstallationInProgress [30]
12-07 20:19:59.381+0530 I/Tizen::App( 1290): (1584) > Package = [org.example.uibuildersingleview], Key = [install_percent], Value = [30], install = [1]
12-07 20:19:59.381+0530 E/PKGMGR  ( 1495): pkgmgr.c: __find_op_cbinfo(312) > tmp is NULL
12-07 20:19:59.381+0530 E/PKGMGR  ( 1495): pkgmgr.c: __find_op_cbinfo(312) > tmp is NULL
12-07 20:19:59.481+0530 W/CERT_SVC_VCORE( 3895): DUID.cpp: CheckCertifiedDevice(668) > No device id on distributor certificate
12-07 20:19:59.531+0530 E/rpm-installer( 3895): coretpk-parser.c: _coretpk_parser_get_manifest_info(1183) > (doc == NULL) xmlParseFile() failed.
12-07 20:19:59.531+0530 E/rpm-installer( 3895): coretpk-installer.c: _coretpk_installer_verify_privilege_list(1594) > (pkg_file_info == NULL) pkg_file_info is NULL.
12-07 20:19:59.591+0530 E/PKGMGR_PARSER( 3895): pkgmgr_parser.c: __check_theme(154) > theme for uninstallation.
12-07 20:19:59.611+0530 E/PKGMGR_CERT( 3895): pkgmgrinfo_certinfo.c: pkgmgrinfo_delete_certinfo(617) > Transaction Begin
12-07 20:19:59.611+0530 E/PKGMGR_CERT( 3895): pkgmgrinfo_certinfo.c: pkgmgrinfo_delete_certinfo(622) > Certificate Deletion Success
12-07 20:19:59.621+0530 E/PKGMGR_CERT( 3895): pkgmgrinfo_certinfo.c: pkgmgrinfo_delete_certinfo(635) > Transaction Commit and End
12-07 20:19:59.651+0530 E/rpm-installer( 3895): coretpk-installer.c: _coretpk_installer_package_reinstall(6347) > _coretpk_installer_package_reinstall(org.example.uibuildersingleview) failed.
12-07 20:19:59.651+0530 E/PKGMGR  ( 1495): pkgmgr.c: __find_op_cbinfo(312) > tmp is NULL
12-07 20:19:59.651+0530 E/PKGMGR  ( 1495): pkgmgr.c: __find_op_cbinfo(312) > tmp is NULL
12-07 20:19:59.661+0530 E/ESD     (  941): esd_main.c: __esd_pkgmgr_event_callback(1757) > pkg_event(3) falied
12-07 20:19:59.661+0530 I/Tizen::App( 1290): (1894) > PackageEventHandler - req: 12900002, pkg_type: tpk, pkg_name: org.example.uibuildersingleview, key: end, val: fail
12-07 20:19:59.661+0530 I/Tizen::App( 1290): (1584) > Package = [org.example.uibuildersingleview], Key = [end], Value = [fail], install = [96]
12-07 20:19:59.661+0530 W/AUL_AMD (  664): amd_appinfo.c: __amd_pkgmgrinfo_status_cb(1111) > __amd_pkgmgrinfo_fail_handler
12-07 20:19:59.671+0530 W/ISF_PANEL_EFL(  823): isf_panel_efl.cpp: _isf_insert_ime_info_by_pkgid(1168) > pkgmgrinfo_pkginfo_get_pkginfo("org.example.uibuildersingleview",~) returned -1
12-07 20:19:59.671+0530 W/ISF_PANEL_EFL(  823): isf_panel_efl.cpp: _package_manager_event_cb(1381) > isf_db_select_appids_by_pkgid returned 0.
12-07 20:20:00.492+0530 I/INDICATOR(  776): clock.c: indicator_clock_changed_cb(195) > ""
12-07 20:20:00.492+0530 E/UXT     (  776): uxt_util.c: uxt_util_get_date_time_text_by_locale(389) > text : 8:20 p.m.
12-07 20:20:00.492+0530 I/INDICATOR(  776): clock.c: getTimeFormatted(176) > "time format : 8:20 p.m."
12-07 20:20:00.492+0530 I/INDICATOR(  776): clock.c: indicator_clock_changed_cb(244) > "time format : 8:20 p.m."
12-07 20:20:00.492+0530 W/INDICATOR(  776): clock.c: indicator_clock_changed_cb(272) > 
12-07 20:20:00.492+0530 I/INDICATOR(  776): clock.c: indicator_clock_changed_cb(317) > "[CLOCK MODULE] Timer Status : -2146747153 Time: <font_size=31> </font_size> <font_size=31> 8:20 p.m.</font_size></font>"
12-07 20:20:01.393+0530 E/PKGMGR_SERVER( 3892): pkgmgr-server.c: __unset_recovery_mode(282) > remove recovery_file[/opt/share/packages/.recovery/pkgmgr/org.example.uibuildersingleview] fail
12-07 20:20:01.393+0530 E/PKGMGR_SERVER( 3892): pkgmgr-server.c: sighandler(417) > child NORMAL exit [3895]
12-07 20:20:01.513+0530 E/PKGMGR_SERVER( 3892): pkgmgr-server.c: exit_server(1381) > exit_server Start [backend_status=1, queue_status=1, drm_status=1] 
12-07 20:20:01.513+0530 E/PKGMGR_SERVER( 3892): pkgmgr-server.c: main(2471) > package manager server terminated.
12-07 20:20:04.145+0530 E/PKGMGR  ( 3946): pkgmgr.c: pkgmgr_client_install(1605) > install pkg start.
12-07 20:20:04.226+0530 E/PKGMGR_SERVER( 3948): pkgmgr-server.c: main(2414) > package manager server start
12-07 20:20:04.276+0530 E/PKGMGR_SERVER( 3948): pkgmgr-server-internal.c: _zone_set_type_and_backend(180) > tep_filepath [/opt/usr/apps/tmp/org.example.uibuildersingleview-1.0.0-arm.tpk]
12-07 20:20:04.286+0530 E/PKGMGR_INFO( 3948): pkgmgrinfo_pkginfo.c: pkgmgrinfo_pkginfo_get_unmounted_pkginfo(778) > (exist == 0) pkgid[/opt/usr/apps/tmp/org.example.uibuildersingleview-1.0.0-arm.tpk] not found in DB
12-07 20:20:04.296+0530 E/rpm-installer( 3948): librpm.c: __installer_util_delete_dir(171) > opendir(/tmp/coretpk-unzip) failed. [2][No such file or directory]
12-07 20:20:04.306+0530 E/PKGMGR_SERVER( 3948): pm-mdm.c: _pm_check_mdm_policy(75) > [0;31m[_pm_check_mdm_policy(): 75](ret != MDM_RESULT_SUCCESS) can not connect mdm server[0;m
12-07 20:20:04.306+0530 E/PKGMGR_SERVER( 3948): pkgmgr-server.c: __set_recovery_mode(234) > rev_file[/opt/share/packages/.recovery/pkgmgr/org.example.uibuildersingleview-1.0.0-arm.tpk] is null
12-07 20:20:04.306+0530 E/PKGMGR  ( 3946): pkgmgr.c: pkgmgr_client_install(1723) > install pkg finish, ret=[39460002]
12-07 20:20:04.426+0530 E/PKGMGR_INSTALLER( 3951): pkgmgr_installer.c: pkgmgr_installer_receive_request(225) > option is [i]
12-07 20:20:04.426+0530 E/rpm-installer( 3951): rpm-appcore-intf.c: main(186) > [/opt/usr/apps/tmp/org.example.uibuildersingleview-1.0.0-arm.tpk] is tpk package.
12-07 20:20:04.446+0530 E/rpm-installer( 3951): coretpk-parser.c: _coretpk_parser_is_svc_app(1031) > (ret == 1) NodeSet is empty. (//*[name() = 'manifest']/*[name()='service-application'])
12-07 20:20:04.446+0530 E/rpm-installer( 3951): coretpk-parser.c: __coretpk_parser_get_value(1147) > (result_value == NULL) [install-location] is empty.
12-07 20:20:04.446+0530 E/rpm-installer( 3951): coretpk-parser.c: __coretpk_parser_get_value_list(1104) > (ret == 1) [//*[name() ='privileges']/*[name()='privilege']] is empty.
12-07 20:20:04.446+0530 E/rpm-installer( 3951): coretpk-parser.c: __coretpk_parser_is_theme(31) > (ret == 1) metadata(watchface) is empty.
12-07 20:20:04.446+0530 E/rpm-installer( 3951): coretpk-parser.c: _coretpk_parser_is_widget(997) > (ret == 1) NodeSet is empty. (//*[name() = 'manifest']/*[name()='widget-application'])
12-07 20:20:04.446+0530 E/rpm-installer( 3951): installer-util.c: _installer_util_get_configuration_value(565) > [signature]=[on]
12-07 20:20:04.496+0530 W/CERT_SVC_VCORE( 3951): DUID.cpp: CheckCertifiedDevice(668) > No device id on distributor certificate
12-07 20:20:04.546+0530 I/Tizen::App( 1290): (1894) > PackageEventHandler - req: 12900002, pkg_type: tpk, pkg_name: org.example.uibuildersingleview, key: start, val: install
12-07 20:20:04.546+0530 I/Tizen::App( 1290): (1584) > Package = [org.example.uibuildersingleview], Key = [start], Value = [install], install = [96]
12-07 20:20:04.546+0530 E/PKGMGR  ( 1495): pkgmgr.c: __find_op_cbinfo(312) > tmp is NULL
12-07 20:20:04.546+0530 E/PKGMGR  ( 1495): pkgmgr.c: __find_op_cbinfo(312) > tmp is NULL
12-07 20:20:04.556+0530 I/Tizen::App( 1290): (1894) > PackageEventHandler - req: 12900002, pkg_type: tpk, pkg_name: org.example.uibuildersingleview, key: install_percent, val: 30
12-07 20:20:04.556+0530 I/Tizen::App( 1290): (119) > InstallationInProgress [30]
12-07 20:20:04.556+0530 I/Tizen::App( 1290): (1584) > Package = [org.example.uibuildersingleview], Key = [install_percent], Value = [30], install = [96]
12-07 20:20:04.556+0530 E/PKGMGR  ( 1495): pkgmgr.c: __find_op_cbinfo(312) > tmp is NULL
12-07 20:20:04.556+0530 E/PKGMGR  ( 1495): pkgmgr.c: __find_op_cbinfo(312) > tmp is NULL
12-07 20:20:04.656+0530 E/rpm-installer( 3951): coretpk-parser.c: __coretpk_parser_check_vip_tag(396) > (ret == 1) metadata(watchface) is empty.
12-07 20:20:04.656+0530 E/rpm-installer( 3951): coretpk-parser.c: __coretpk_parser_get_value(1147) > (result_value == NULL) [ui-gadget] is empty.
12-07 20:20:04.666+0530 E/rpm-installer( 3951): coretpk-parser.c: __coretpk_parser_append_path(335) > (ret == 1) NodeSet is empty. (//@portrait-effectimage)
12-07 20:20:04.666+0530 E/rpm-installer( 3951): coretpk-parser.c: __coretpk_parser_is_theme(31) > (ret == 1) metadata(watchface) is empty.
12-07 20:20:04.666+0530 E/PKGMGR_PARSER( 3951): pkgmgr_parser.c: pkgmgr_parser_check_manifest_validation(2678) > Manifest is Valid
12-07 20:20:04.676+0530 E/PKGMGR_PARSER( 3951): pkgmgr_parser_signature.c: __ps_check_mdm_policy(979) > (ret != MDM_RESULT_SUCCESS) can not connect mdm server
12-07 20:20:04.916+0530 E/PKGMGR_PARSER( 3951): pkgmgr_parser.c: __check_theme(142) > theme for installation.
12-07 20:20:04.936+0530 E/PKGMGR_CERT( 3951): pkgmgrinfo_certinfo.c: pkgmgrinfo_save_certinfo(427) > Transaction Begin
12-07 20:20:04.936+0530 E/PKGMGR_CERT( 3951): pkgmgrinfo_certinfo.c: pkgmgrinfo_save_certinfo(496) > Id:Count = 1 112
12-07 20:20:04.936+0530 E/PKGMGR_CERT( 3951): pkgmgrinfo_certinfo.c: pkgmgrinfo_save_certinfo(496) > Id:Count = 2 112
12-07 20:20:04.936+0530 E/PKGMGR_CERT( 3951): pkgmgrinfo_certinfo.c: pkgmgrinfo_save_certinfo(496) > Id:Count = 31 7
12-07 20:20:04.936+0530 E/PKGMGR_CERT( 3951): pkgmgrinfo_certinfo.c: pkgmgrinfo_save_certinfo(496) > Id:Count = 32 7
12-07 20:20:04.936+0530 E/PKGMGR_CERT( 3951): pkgmgrinfo_certinfo.c: pkgmgrinfo_save_certinfo(496) > Id:Count = 33 7
12-07 20:20:04.936+0530 E/PKGMGR_CERT( 3951): pkgmgrinfo_certinfo.c: pkgmgrinfo_save_certinfo(496) > Id:Count = 34 7
12-07 20:20:04.936+0530 E/PKGMGR_CERT( 3951): pkgmgrinfo_certinfo.c: pkgmgrinfo_save_certinfo(576) > Transaction Commit and End
12-07 20:20:04.946+0530 I/Tizen::App( 1290): (1894) > PackageEventHandler - req: 12900002, pkg_type: tpk, pkg_name: org.example.uibuildersingleview, key: install_percent, val: 60
12-07 20:20:04.946+0530 I/Tizen::App( 1290): (119) > InstallationInProgress [60]
12-07 20:20:04.946+0530 I/Tizen::App( 1290): (1584) > Package = [org.example.uibuildersingleview], Key = [install_percent], Value = [60], install = [96]
12-07 20:20:04.946+0530 E/PKGMGR  ( 1495): pkgmgr.c: __find_op_cbinfo(312) > tmp is NULL
12-07 20:20:04.946+0530 E/PKGMGR  ( 1495): pkgmgr.c: __find_op_cbinfo(312) > tmp is NULL
12-07 20:20:04.956+0530 E/rpm-installer( 3951): installer-util.c: _installer_util_get_configuration_value(565) > [signature]=[on]
12-07 20:20:04.986+0530 E/rpm-installer( 3951): coretpk-installer.c: __post_install_for_mmc(652) > (handle == NULL) handle is NULL.
12-07 20:20:04.986+0530 I/Tizen::App( 1290): (1894) > PackageEventHandler - req: 12900002, pkg_type: tpk, pkg_name: org.example.uibuildersingleview, key: install_percent, val: 100
12-07 20:20:04.986+0530 I/Tizen::App( 1290): (119) > InstallationInProgress [100]
12-07 20:20:04.986+0530 I/Tizen::App( 1290): (1584) > Package = [org.example.uibuildersingleview], Key = [install_percent], Value = [100], install = [96]
12-07 20:20:04.986+0530 E/PKGMGR  ( 1495): pkgmgr.c: __find_op_cbinfo(312) > tmp is NULL
12-07 20:20:04.986+0530 E/PKGMGR  ( 1495): pkgmgr.c: __find_op_cbinfo(312) > tmp is NULL
12-07 20:20:06.518+0530 E/PKGMGR_SERVER( 3948): pkgmgr-server.c: exit_server(1381) > exit_server Start [backend_status=0, queue_status=1, drm_status=1] 
12-07 20:20:06.948+0530 E/cluster-home(  895): mainmenu-package-manager.cpp: OnClientListenCb(571) >  #Step 1
12-07 20:20:06.948+0530 E/cluster-home(  895): mainmenu-package-manager.cpp: OnClientListenCb(575) >  #Step 2
12-07 20:20:06.958+0530 E/cluster-home(  895): mainmenu-package-manager.cpp: _GetAppIds(350) >  BEGIN
12-07 20:20:06.958+0530 I/Tizen::App( 1290): (1894) > PackageEventHandler - req: 12900002, pkg_type: tpk, pkg_name: org.example.uibuildersingleview, key: end, val: ok
12-07 20:20:06.958+0530 I/Tizen::App( 1290): (78) > Installation is Completed. [Package = org.example.uibuildersingleview]
12-07 20:20:06.958+0530 I/Tizen::App( 1290): (671) > Enter. package(org.example.uibuildersingleview), installationResult(0)
12-07 20:20:06.958+0530 E/PKGMGR  ( 1495): pkgmgr.c: __find_op_cbinfo(312) > tmp is NULL
12-07 20:20:06.958+0530 E/PKGMGR  ( 1495): pkgmgr.c: __find_op_cbinfo(312) > tmp is NULL
12-07 20:20:06.958+0530 I/Tizen::App( 1290): (1360) > package(org.example.uibuildersingleview), version(1.0.0), type(tpk), displayName(uibuildersingleview), uninstallable(1), downloaded(1), updated(0), preloaded(0)movable(1), externalStorage(0), mainApp(org.example.uibuildersingleview), storeClient(), appRootPath(/opt/usr/apps/org.example.uibuildersingleview)
12-07 20:20:06.978+0530 I/Tizen::App( 1290): (483) > pkgmgrinfo_appinfo_get_appid(): app = [org.example.uibuildersingleview]
12-07 20:20:06.988+0530 I/Tizen::App( 1290): (416) > appName = [uibuildersingleview]
12-07 20:20:06.988+0530 I/Tizen::App( 1290): (509) > exe = [/opt/usr/apps/org.example.uibuildersingleview/bin/uibuildersingleview], displayName = [uibuildersingleview], mainApp = [1], menuIconVisible = [0], serviceApp = [0]
12-07 20:20:06.988+0530 E/PKGMGR_INFO( 1290): pkgmgrinfo_appinfo.c: pkgmgrinfo_appinfo_get_list(887) > (component == PMINFO_SVC_APP) PMINFO_SVC_APP is done
12-07 20:20:06.988+0530 I/Tizen::App( 1290): (683) > Application count(1) in this package
12-07 20:20:06.988+0530 I/Tizen::App( 1290): (840) > Enter.
12-07 20:20:06.988+0530 E/cluster-home(  895): mainmenu-package-manager.cpp: _PkgMgrAppInfoGetListCb(253) >  ##### [org.example.uibuildersingleview]
12-07 20:20:06.988+0530 E/cluster-home(  895): mainmenu-package-manager.cpp: _GetAppIds(375) >  ##### [org.example.uibuildersingleview]
12-07 20:20:06.988+0530 E/cluster-home(  895): mainmenu-package-manager.cpp: _GetAppIds(379) >  END
12-07 20:20:06.988+0530 E/cluster-home(  895): mainmenu-package-manager.cpp: _DoPkgJob(458) >  #Step 3 size[1]
12-07 20:20:06.988+0530 E/cluster-home(  895): mainmenu-package-manager.cpp: _DoPkgJob(462) >  appId[org.example.uibuildersingleview]
12-07 20:20:06.998+0530 I/Tizen::App( 1290): (703) > Exit.
12-07 20:20:06.998+0530 I/Tizen::App( 1290): (1584) > Package = [org.example.uibuildersingleview], Key = [end], Value = [ok], install = [96]
12-07 20:20:06.998+0530 I/Tizen::App( 1290): (416) > appName = [uibuildersingleview]
12-07 20:20:06.998+0530 I/Tizen::App( 1290): (509) > exe = [/opt/usr/apps/org.example.uibuildersingleview/bin/uibuildersingleview], displayName = [uibuildersingleview], mainApp = [1], menuIconVisible = [0], serviceApp = [0]
12-07 20:20:06.998+0530 I/Tizen::App( 1290): (2343) > info file is not existed. [/opt/usr/apps/org.exampl/info/org.example.uibuildersingleview.info]
12-07 20:20:06.998+0530 I/Tizen::App( 1290): (131) > Enter
12-07 20:20:07.008+0530 I/Tizen::App( 1290): (137) > org.example.uibuildersingleview does not have launch condition
12-07 20:20:07.008+0530 I/Tizen::App( 1290): (883) > Exit.
12-07 20:20:07.008+0530 W/ISF_PANEL_EFL(  823): isf_panel_efl.cpp: _package_manager_event_cb(1513) > _isf_insert_ime_info_by_pkgid returned 0.
12-07 20:20:07.008+0530 E/cluster-home(  895): mainmenu-package-manager.cpp: _GetAppInfo(990) >  Name[uibuildersingleview] enable[1] system[0]
12-07 20:20:07.008+0530 E/HOME_APPS(  895): mainmenu-package-manager.cpp: _DoPkgJob(484) >  appId[org.example.uibuildersingleview] mdm is not enabled
12-07 20:20:07.008+0530 E/cluster-home(  895): mainmenu-package-manager.cpp: GetAppInfo(630) >  Find a App Info Name[uibuildersingleview] enable[1] system[0]
12-07 20:20:07.008+0530 W/HOME_APPS(  895): mainmenu-box-impl.cpp: UpdateBoxData(1326) >  update box data!!!!! old icon path[/opt/usr/apps/org.example.uibuildersingleview/shared/res/uibuildersingleview.png], New icon path[/opt/usr/apps/org.example.uibuildersingleview/shared/res/uibuildersingleview.png]!!!!!
12-07 20:20:07.028+0530 E/util-view(  895): util-text-image-provider.cpp: GetPixelsFromText(240) >  DEFAULT='font=SamsungOneUI:style=Regular style=shadow,bottom shadow_color=# font_size=28 align=center valign=top color=#FFFFFF color_class=ATO001 text_class=ATO001 ellipsis=1 wrap=mixed linegap=-3'
12-07 20:20:07.038+0530 E/PKGMGR_SERVER( 3948): pkgmgr-server.c: __unset_recovery_mode(282) > remove recovery_file[/opt/share/packages/.recovery/pkgmgr/org.example.uibuildersingleview-1.0.0-arm.tpk] fail
12-07 20:20:07.038+0530 E/PKGMGR_SERVER( 3948): pkgmgr-server.c: sighandler(417) > child NORMAL exit [3951]
12-07 20:20:07.048+0530 E/RESOURCED(  779): procfs.c: proc_get_oom_score_adj(131) > fopen /proc/3951/oom_score_adj failed
12-07 20:20:07.048+0530 E/RESOURCED(  779): proc-main.c: resourced_proc_status_change(867) > Empty pid or process not exists. 3951
12-07 20:20:07.048+0530 W/HOME_APPS(  895): mainmenu-box-impl.cpp: _OnImageLoadFinishedHandler(2152) >  finished path[/opt/usr/apps/org.example.uibuildersingleview/shared/res/uibuildersingleview.png], Loading state:[1]
12-07 20:20:08.520+0530 E/PKGMGR_SERVER( 3948): pkgmgr-server.c: exit_server(1381) > exit_server Start [backend_status=1, queue_status=1, drm_status=1] 
12-07 20:20:08.520+0530 E/PKGMGR_SERVER( 3948): pkgmgr-server.c: main(2471) > package manager server terminated.
12-07 20:20:12.764+0530 W/AUL     ( 4007): launch.c: app_request_to_launchpad(396) > request cmd(0) to(org.example.uibuildersingleview)
12-07 20:20:12.764+0530 W/AUL_AMD (  664): amd_request.c: __request_handler(906) > __request_handler: 0
12-07 20:20:12.774+0530 I/AUL     (  664): menu_db_util.h: _get_app_info_from_db_by_apppath(242) > path : /usr/bin/launch_app, ret : 0
12-07 20:20:12.784+0530 I/AUL     (  664): menu_db_util.h: _get_app_info_from_db_by_apppath(242) > path : /bin/bash, ret : 0
12-07 20:20:12.784+0530 E/AUL_AMD (  664): amd_launch.c: _start_app(2499) > no caller appid info, ret: -1
12-07 20:20:12.784+0530 W/AUL_AMD (  664): amd_launch.c: _start_app(2508) > caller pid : 4007
12-07 20:20:12.784+0530 E/AUL_AMD (  664): amd_appinfo.c: appinfo_get_value(1296) > appinfo get value: Invalid argument, 19
12-07 20:20:12.784+0530 E/RESOURCED(  779): block.c: block_prelaunch_state(134) > insert data org.example.uibuildersingleview, table num : 3
12-07 20:20:12.794+0530 E/RESOURCED(  779): heart-memory.c: heart_memory_get_data(601) > hashtable heart_memory_app_list is NULL
12-07 20:20:12.794+0530 W/AUL_AMD (  664): amd_launch.c: _start_app(3052) > pad pid(-5)
12-07 20:20:12.794+0530 W/AUL_PAD ( 1521): launchpad.c: __launchpad_main_loop(520) > Launch on type-based process-pool
12-07 20:20:12.794+0530 W/AUL_PAD ( 1521): launchpad.c: __send_result_to_caller(267) > Check app launching
12-07 20:20:12.804+0530 I/CAPI_APPFW_APPLICATION( 3710): app_main.c: ui_app_main(789) > app_efl_main
12-07 20:20:12.804+0530 I/CAPI_APPFW_APPLICATION( 3710): app_main.c: _ui_app_appcore_create(641) > app_appcore_create
12-07 20:20:12.844+0530 E/TBM     ( 3710): tbm_bufmgr.c: _tbm_bufmgr_init_state(691) > USE TGL LOCK!
12-07 20:20:12.894+0530 W/AUL     (  664): app_signal.c: aul_send_app_launch_request_signal(423) > send_app_launch_signal, pid: 3710, appid: org.example.uibuildersingleview
12-07 20:20:12.894+0530 E/AUL     (  664): amd_app_group.c: app_group_start_app(1037) > app_group_start_app
12-07 20:20:12.894+0530 E/RESOURCED(  779): proc-main.c: proc_add_program_list(237) > not found ppi : org.example.uibuildersingleview
12-07 20:20:12.904+0530 W/AUL     ( 4007): launch.c: app_request_to_launchpad(425) > request cmd(0) result(3710)
12-07 20:20:13.074+0530 I/APP_CORE( 3710): appcore-efl.c: __do_app(514) > [APP 3710] Event: RESET State: CREATED
12-07 20:20:13.074+0530 I/CAPI_APPFW_APPLICATION( 3710): app_main.c: _ui_app_appcore_reset(723) > app_appcore_reset
12-07 20:20:13.074+0530 E/EFL     ( 3710): edje<3710> lib/edje/edje_embryo.c:4134 _edje_embryo_test_run() You are running Embryo->EDC->Embryo with script program '_p26';
12-07 20:20:13.074+0530 E/EFL     ( 3710): By the power of Grayskull, your previous Embryo stack is now broken!
12-07 20:20:13.084+0530 E/EFL     ( 3710): edje<3710> lib/edje/edje_embryo.c:4134 _edje_embryo_test_run() You are running Embryo->EDC->Embryo with script program '_p26';
12-07 20:20:13.084+0530 E/EFL     ( 3710): By the power of Grayskull, your previous Embryo stack is now broken!
12-07 20:20:13.084+0530 E/EFL     ( 3710): edje<3710> lib/edje/edje_embryo.c:4134 _edje_embryo_test_run() You are running Embryo->EDC->Embryo with script program '_p26';
12-07 20:20:13.084+0530 E/EFL     ( 3710): By the power of Grayskull, your previous Embryo stack is now broken!
12-07 20:20:13.094+0530 W/APP_CORE( 3710): appcore-efl.c: __show_cb(920) > [EVENT_TEST][EVENT] GET SHOW EVENT!!!. WIN:5600002
12-07 20:20:13.094+0530 W/AUL_AMD (  664): amd_request.c: __request_handler(906) > __request_handler: 35
12-07 20:20:13.104+0530 E/EFL     (  330): eo<330> lib/eo/eo.c:676 _eo_call_resolve() in lib/edje/edje_object.eo.c:316: func 'edje_obj_signal_emit' (415) could not be resolved for class 'Evas_Object_Smart'.
12-07 20:20:13.104+0530 E/EFL     (  330): eo<330> lib/eo/eo.c:676 _eo_call_resolve() in lib/edje/edje_object.eo.c:316: func 'edje_obj_signal_emit' (415) could not be resolved for class 'Evas_Object_Smart'.
12-07 20:20:13.114+0530 I/Tizen::System( 1290): (259) > Active app [org.exampl], current [com.samsun] 
12-07 20:20:13.114+0530 I/Tizen::Io( 1290): (729) > Entry not found
12-07 20:20:13.124+0530 I/Tizen::System( 1290): (157) > change brightness system value.
12-07 20:20:13.134+0530 W/AUL_AMD (  664): amd_request.c: __request_handler(906) > __request_handler: 15
12-07 20:20:13.174+0530 I/APP_CORE(  895): appcore-efl.c: __do_app(514) > [APP 895] Event: PAUSE State: RUNNING
12-07 20:20:13.174+0530 I/CAPI_APPFW_APPLICATION(  895): app_main.c: _ui_app_appcore_pause(689) > app_appcore_pause
12-07 20:20:13.174+0530 I/APP_CORE( 3710): appcore-efl.c: __do_app(514) > [APP 3710] Event: RESUME State: CREATED
12-07 20:20:13.174+0530 E/cluster-home(  895): homescreen.cpp: OnPause(260) >  app pause
12-07 20:20:13.184+0530 W/AUL_AMD (  664): amd_launch.c: __e17_status_handler(3328) > pid(895) status(4)
12-07 20:20:13.184+0530 W/AUL     (  664): amd_app_group.c: __set_fg_flag(190) > send_signal BG com.samsung.homescreen(895)
12-07 20:20:13.184+0530 W/AUL     (  664): app_signal.c: aul_send_app_status_change_signal(581) > send_app_status_change_signal, pid: 895, appid: com.samsung.homescreen, status: bg
12-07 20:20:13.184+0530 W/AUL_AMD (  664): amd_launch.c: __e17_status_handler(3328) > pid(3710) status(3)
12-07 20:20:13.184+0530 W/AUL_AMD (  664): amd_key.c: _key_ungrab(269) > fail(-1) to ungrab key(XF86Back)
12-07 20:20:13.184+0530 W/AUL_AMD (  664): amd_launch.c: __e17_status_handler(3349) > back key ungrab error
12-07 20:20:13.184+0530 W/AUL     (  664): amd_app_group.c: __set_fg_flag(180) > send_signal FG org.example.uibuildersingleview(3710)
12-07 20:20:13.184+0530 W/AUL     (  664): app_signal.c: aul_send_app_status_change_signal(581) > send_app_status_change_signal, pid: 3710, appid: org.example.uibuildersingleview, status: fg
12-07 20:20:13.194+0530 I/APP_CORE( 3710): appcore-efl.c: __do_app(623) > Legacy lifecycle: 0
12-07 20:20:13.194+0530 I/APP_CORE( 3710): appcore-efl.c: __do_app(625) > [APP 3710] Initial Launching, call the resume_cb
12-07 20:20:13.194+0530 I/CAPI_APPFW_APPLICATION( 3710): app_main.c: _ui_app_appcore_resume(706) > app_appcore_resume
12-07 20:20:13.545+0530 W/AUL_AMD (  664): amd_launch.c: __e17_status_handler(3328) > pid(3710) status(0)
12-07 20:20:13.945+0530 E/RESOURCED(  779): resourced-dbus.c: resourced_dbus_system_hash_drop_busname(324) > Does not exist in busname hash: :1.344
12-07 20:20:13.955+0530 I/Tizen::App( 1290): (499) > LaunchedApp(org.example.uibuildersingleview)
12-07 20:20:13.955+0530 I/Tizen::App( 1290): (733) > Finished invoking application event listener for org.example.uibuildersingleview, 3710.
12-07 20:20:14.696+0530 I/UXT     ( 4020): Uxt_ObjectManager.cpp: OnInitialized(737) > Initialized.
12-07 20:20:18.189+0530 I/APP_CORE(  895): appcore-efl.c: __do_app(514) > [APP 895] Event: MEM_FLUSH State: PAUSED
12-07 20:20:19.080+0530 E/EFL     ( 3710): ecore_x<3710> lib/ecore_x/xlib/ecore_x_events.c:603 _ecore_x_event_handle_button_press() ButtonEvent:press time=8571566
12-07 20:20:19.190+0530 I/CAPI_APPFW_APPLICATION(  895): app_main.c: _ui_app_appcore_rotation_event(485) > _ui_app_appcore_rotation_event
12-07 20:20:19.200+0530 I/CAPI_APPFW_APPLICATION( 3710): app_main.c: _ui_app_appcore_rotation_event(485) > _ui_app_appcore_rotation_event
12-07 20:20:19.290+0530 E/EFL     ( 3710): edje<3710> lib/edje/edje_embryo.c:4134 _edje_embryo_test_run() You are running Embryo->EDC->Embryo with script program '_p26';
12-07 20:20:19.290+0530 E/EFL     ( 3710): By the power of Grayskull, your previous Embryo stack is now broken!
12-07 20:20:19.310+0530 I/CAPI_APPFW_APPLICATION(  895): app_main.c: _ui_app_appcore_rotation_event(485) > _ui_app_appcore_rotation_event
12-07 20:20:19.470+0530 E/EFL     (  894): eo<894> lib/eo/eo.c:676 _eo_call_resolve() in lib/edje/edje_object.eo.c:164: func 'edje_obj_part_geometry_get' (762) could not be resolved for class 'Elm_Layout'.
12-07 20:20:19.520+0530 E/EFL     (  330): <330> e_mod_processmgr.c:486 _e_mod_processmgr_anr_ping() safety check failed: bd == NULL
12-07 20:20:19.621+0530 E/EFL     ( 3710): ecore_x<3710> lib/ecore_x/xlib/ecore_x_events.c:756 _ecore_x_event_handle_button_release() ButtonEvent:release time=8571996
12-07 20:20:19.621+0530 I/CAPI_APPFW_APPLICATION( 3710): app_main.c: _ui_app_appcore_rotation_event(485) > _ui_app_appcore_rotation_event
12-07 20:20:19.681+0530 E/EFL     (  330): elementary<330> elm_transit.c:648 elm_transit_del_cb_set() Elm_Transit transit has already been deleted!
12-07 20:20:20.031+0530 I/CAPI_APPFW_APPLICATION(  895): app_main.c: _ui_app_appcore_rotation_event(485) > _ui_app_appcore_rotation_event
12-07 20:20:20.031+0530 I/CAPI_APPFW_APPLICATION( 3710): app_main.c: _ui_app_appcore_rotation_event(485) > _ui_app_appcore_rotation_event
12-07 20:20:20.081+0530 E/EFL     ( 3710): edje<3710> lib/edje/edje_embryo.c:4134 _edje_embryo_test_run() You are running Embryo->EDC->Embryo with script program '_p26';
12-07 20:20:20.081+0530 E/EFL     ( 3710): By the power of Grayskull, your previous Embryo stack is now broken!
12-07 20:20:20.121+0530 E/EFL     (  894): eo<894> lib/eo/eo.c:676 _eo_call_resolve() in lib/edje/edje_object.eo.c:164: func 'edje_obj_part_geometry_get' (762) could not be resolved for class 'Elm_Layout'.
12-07 20:20:20.331+0530 E/EFL     (  330): elementary<330> elm_transit.c:648 elm_transit_del_cb_set() Elm_Transit transit has already been deleted!
12-07 20:20:21.773+0530 E/EFL     ( 3710): ecore_x<3710> lib/ecore_x/xlib/ecore_x_events.c:603 _ecore_x_event_handle_button_press() ButtonEvent:press time=8574255
12-07 20:20:21.863+0530 E/EFL     ( 3710): ecore_x<3710> lib/ecore_x/xlib/ecore_x_events.c:756 _ecore_x_event_handle_button_release() ButtonEvent:release time=8574343
12-07 20:20:21.863+0530 E/VCONF   ( 3710): vconf.c: _vconf_check_retry_err(1368) > db/ise/keysound : check retry err (-21/13).
12-07 20:20:21.863+0530 E/VCONF   ( 3710): vconf.c: _vconf_get_key_filesys(2371) > _vconf_get_key_filesys(db/ise/keysound) step(-21) failed(13 / Permission denied) retry(0) 
12-07 20:20:21.863+0530 E/VCONF   ( 3710): vconf.c: _vconf_get_key(2407) > _vconf_get_key(db/ise/keysound) step(-21) failed(13 / Permission denied)
12-07 20:20:21.863+0530 E/VCONF   ( 3710): vconf.c: vconf_get_bool(2729) > vconf_get_bool(3710) : db/ise/keysound error
12-07 20:20:21.873+0530 E/VCONF   ( 3710): vconf-kdb.c: _vconf_kdb_add_notify(318) > _vconf_kdb_add_notify : add noti(Permission denied)
12-07 20:20:21.873+0530 E/VCONF   ( 3710): vconf.c: vconf_notify_key_changed(3168) > vconf_notify_key_changed : key(db/ise/keysound) add notify fail
12-07 20:20:22.503+0530 E/EFL     ( 3710): ecore_x<3710> lib/ecore_x/xlib/ecore_x_events.c:603 _ecore_x_event_handle_button_press() ButtonEvent:press time=8574997
12-07 20:20:22.644+0530 E/EFL     ( 3710): ecore_x<3710> lib/ecore_x/xlib/ecore_x_events.c:756 _ecore_x_event_handle_button_release() ButtonEvent:release time=8575128
12-07 20:20:23.755+0530 E/EFL     ( 3710): ecore_x<3710> lib/ecore_x/xlib/ecore_x_events.c:603 _ecore_x_event_handle_button_press() ButtonEvent:press time=8576246
12-07 20:20:23.875+0530 E/EFL     ( 3710): ecore_x<3710> lib/ecore_x/xlib/ecore_x_events.c:756 _ecore_x_event_handle_button_release() ButtonEvent:release time=8576367
12-07 20:20:25.006+0530 E/EFL     ( 3710): ecore_x<3710> lib/ecore_x/xlib/ecore_x_events.c:603 _ecore_x_event_handle_button_press() ButtonEvent:press time=8577485
12-07 20:20:25.126+0530 E/EFL     ( 3710): ecore_x<3710> lib/ecore_x/xlib/ecore_x_events.c:756 _ecore_x_event_handle_button_release() ButtonEvent:release time=8577606
12-07 20:20:25.266+0530 E/EFL     (  330): eo<330> lib/eo/eo.c:676 _eo_call_resolve() in lib/edje/edje_object.eo.c:316: func 'edje_obj_signal_emit' (415) could not be resolved for class 'Evas_Object_Smart'.
12-07 20:20:25.286+0530 W/AUL_AMD (  664): amd_launch.c: __e17_status_handler(3328) > pid(895) status(3)
12-07 20:20:25.286+0530 W/AUL_AMD (  664): amd_key.c: _key_ungrab(269) > fail(-1) to ungrab key(XF86Back)
12-07 20:20:25.286+0530 W/AUL_AMD (  664): amd_launch.c: __e17_status_handler(3349) > back key ungrab error
12-07 20:20:25.286+0530 W/AUL     (  664): amd_app_group.c: __set_fg_flag(180) > send_signal FG com.samsung.homescreen(895)
12-07 20:20:25.286+0530 W/AUL     (  664): app_signal.c: aul_send_app_status_change_signal(581) > send_app_status_change_signal, pid: 895, appid: com.samsung.homescreen, status: fg
12-07 20:20:25.296+0530 I/AUL_PAD ( 1521): sigchild.h: __launchpad_process_sigchld(162) > dead_pid = 3710 pgid = 3710
12-07 20:20:25.296+0530 I/AUL_PAD ( 1521): sigchild.h: __sigchild_action(143) > dead_pid(3710)
12-07 20:20:25.316+0530 E/EFL     (  776): <776> lib/ecore_ipc/ecore_ipc.c:804 ecore_ipc_client_send() safety check failed: !ecore_con_client_connected_get(cl->client) is true
12-07 20:20:25.316+0530 E/EFL     (  776): <776> lib/ecore_ipc/ecore_ipc.c:804 ecore_ipc_client_send() safety check failed: !ecore_con_client_connected_get(cl->client) is true
12-07 20:20:25.336+0530 I/AUL_PAD ( 1521): sigchild.h: __sigchild_action(149) > __send_app_dead_signal(0)
12-07 20:20:25.336+0530 I/AUL_PAD ( 1521): sigchild.h: __launchpad_process_sigchld(170) > after __sigchild_action
12-07 20:20:25.336+0530 E/AUL_PAD ( 1521): launchpad.c: main(698) > error reading sigchld info
12-07 20:20:25.336+0530 I/Tizen::App( 1290): (243) > App[org.example.uibuildersingleview] pid[3710] terminate event is forwarded
12-07 20:20:25.336+0530 I/Tizen::System( 1290): (256) > osp.accessorymanager.service provider is found.
12-07 20:20:25.336+0530 I/Tizen::System( 1290): (196) > Accessory Owner is removed [org.example.uibuildersingleview, 3710, ]
12-07 20:20:25.336+0530 I/Tizen::System( 1290): (256) > osp.system.service provider is found.
12-07 20:20:25.336+0530 I/Tizen::App( 1290): (506) > TerminatedApp(org.example.uibuildersingleview)
12-07 20:20:25.336+0530 I/Tizen::App( 1290): (512) > Not registered pid(3710)
12-07 20:20:25.336+0530 I/Tizen::System( 1290): (246) > Terminated app [org.example.uibuildersingleview]
12-07 20:20:25.336+0530 I/Tizen::Io( 1290): (729) > Entry not found
12-07 20:20:25.336+0530 I/APP_CORE(  895): appcore-efl.c: __do_app(514) > [APP 895] Event: RESUME State: PAUSED
12-07 20:20:25.336+0530 I/CAPI_APPFW_APPLICATION(  895): app_main.c: _ui_app_appcore_resume(706) > app_appcore_resume
12-07 20:20:25.336+0530 E/cluster-home(  895): homescreen.cpp: OnResume(233) >  app resume
12-07 20:20:25.336+0530 E/EFL     (  330): eo<330> lib/eo/eo.c:676 _eo_call_resolve() in lib/edje/edje_object.eo.c:316: func 'edje_obj_signal_emit' (415) could not be resolved for class 'Evas_Object_Smart'.
12-07 20:20:25.336+0530 I/ESD     (  941): esd_main.c: __esd_app_dead_handler(1773) > pid: 3710
12-07 20:20:25.336+0530 W/AUL_AMD (  664): amd_main.c: __app_dead_handler(327) > __app_dead_handler, pid: 3710
12-07 20:20:25.336+0530 W/AUL_AMD (  664): amd_main.c: __app_dead_handler(333) > app_group_leader_app, pid: 3710
12-07 20:20:25.346+0530 E/RESOURCED(  779): resourced-dbus.c: resourced_dbus_system_hash_drop_busname(324) > Does not exist in busname hash: :1.348
12-07 20:20:25.366+0530 W/AUL_AMD (  664): amd_launch.c: __e17_status_handler(3328) > pid(895) status(0)
12-07 20:20:25.386+0530 I/Tizen::System( 1290): (157) > change brightness system value.
12-07 20:20:25.386+0530 I/Tizen::App( 1290): (782) > Finished invoking application event listener for org.example.uibuildersingleview, 3710.
12-07 20:20:25.386+0530 I/TIZEN_N_SOUND_MANAGER(  961): sound_manager.c: sound_manager_get_volume(84) > returns : type=0, volume=8, ret=0x0
12-07 20:20:25.386+0530 E/TIZEN_N_SOUND_MANAGER(  961): sound_manager_private.c: __convert_sound_manager_error_code(142) > [sound_manager_get_volume] ERROR_NONE(0x00000000) : core frameworks error code(0x00000000)
12-07 20:20:25.396+0530 I/TIZEN_N_SOUND_MANAGER(  961): sound_manager.c: sound_manager_get_volume(84) > returns : type=4, volume=0, ret=0x0
12-07 20:20:25.396+0530 E/TIZEN_N_SOUND_MANAGER(  961): sound_manager_private.c: __convert_sound_manager_error_code(142) > [sound_manager_get_volume] ERROR_NONE(0x00000000) : core frameworks error code(0x00000000)
12-07 20:20:25.396+0530 W/AUL_AMD (  664): amd_request.c: __request_handler(906) > __request_handler: 15
12-07 20:20:25.416+0530 I/Tizen::System( 1290): (259) > Active app [com.samsun], current [org.exampl] 
12-07 20:20:25.416+0530 I/Tizen::Io( 1290): (729) > Entry not found
12-07 20:20:25.446+0530 I/Tizen::System( 1290): (157) > change brightness system value.
12-07 20:20:25.456+0530 W/CRASH_MANAGER( 4026): worker.c: worker_job(1199) > 1103710756962148112222
