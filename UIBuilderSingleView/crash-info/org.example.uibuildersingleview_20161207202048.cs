S/W Version Information
Model: SM-Z300H
Tizen-Version: 2.4.0.3
Build-Number: Z300HDDU0BPI2
Build-Date: 2016.09.30 15:37:12

Crash Information
Process Name: uibuildersingleview
PID: 4020
Date: 2016-12-07 20:20:48+0530
Executable File Path: /opt/usr/apps/org.example.uibuildersingleview/bin/uibuildersingleview
Signal: 11
      (SIGSEGV)
      si_code: -6
      signal sent by tkill (sent by pid 4020, uid 5000)

Register Information
r0   = 0xb77b40a8, r1   = 0x0000000c
r2   = 0x0000003c, r3   = 0x0000000c
r4   = 0xb77200a0, r5   = 0xb77920a0
r6   = 0x80012493, r7   = 0xb6ddd488
r8   = 0xb7794098, r9   = 0xb77b40a8
r10  = 0x80012493, fp   = 0xb77893b8
ip   = 0xb69ef4d4, sp   = 0xbe8e3f98
lr   = 0xb69c660d, pc   = 0xb67026f2
cpsr = 0x200e0030

Memory Information
MemTotal:   987012 KB
MemFree:    169728 KB
Buffers:     42184 KB
Cached:     316968 KB
VmPeak:     127552 KB
VmSize:     127548 KB
VmLck:           0 KB
VmPin:           0 KB
VmHWM:       25956 KB
VmRSS:       25956 KB
VmData:      44876 KB
VmStk:         136 KB
VmExe:          20 KB
VmLib:       35600 KB
VmPTE:          94 KB
VmSwap:          0 KB

Threads Information
Threads: 5
PID = 4020 TID = 4020
4020 4021 4151 4152 4170 

Maps Information
af6ee000 afeed000 rwxp [stack:4170]
b14e6000 b14ee000 r-xp /usr/lib/ecore_evas/engines/extn/v-1.13/module.so
b1500000 b1cff000 rwxp [stack:4152]
b1cff000 b1d00000 r-xp /usr/lib/edje/modules/feedback/v-1.13/module.so
b1d10000 b1d24000 r-xp /usr/lib/edje/modules/elm/v-1.13/module.so
b1d38000 b1d39000 r-xp /usr/lib/libX11-xcb.so.1.0.0
b1d49000 b1d4c000 r-xp /usr/lib/libxcb-sync.so.1.0.0
b1d5d000 b1d5e000 r-xp /usr/lib/libxshmfence.so.1.0.0
b1d6e000 b1d70000 r-xp /usr/lib/libxcb-present.so.0.0.0
b1d80000 b1d82000 r-xp /usr/lib/libxcb-dri3.so.0.0.0
b1d92000 b1da2000 r-xp /usr/lib/evas/modules/engines/software_x11/v-1.13/module.so
b1db2000 b1dbe000 r-xp /usr/lib/ecore_evas/engines/x/v-1.13/module.so
b1dd0000 b25cf000 rwxp [stack:4151]
b27cf000 b27d6000 r-xp /usr/lib/libefl-extension.so.0.1.0
b27e9000 b27ef000 r-xp /usr/lib/bufmgr/libtbm_sprd7727.so.0.0.0
b27ff000 b2804000 r-xp /opt/usr/apps/org.example.uibuildersingleview/bin/uibuildersingleview
b2955000 b2a38000 r-xp /usr/lib/libscim-1.0.so.8.2.3
b2a6f000 b2a97000 r-xp /usr/lib/ecore_imf/modules/isf/v-1.13/module.so
b2aa9000 b32a8000 rwxp [stack:4021]
b32a8000 b32aa000 r-xp /usr/lib/ecore/system/systemd/v-1.13/module.so
b32ba000 b32c4000 r-xp /lib/libnss_files-2.20-2014.11.so
b32d5000 b32de000 r-xp /lib/libnss_nis-2.20-2014.11.so
b32ef000 b3300000 r-xp /lib/libnsl-2.20-2014.11.so
b3313000 b3319000 r-xp /lib/libnss_compat-2.20-2014.11.so
b332a000 b332b000 r-xp /usr/lib/osp/libappinfo.so.1.2.2.1
b3353000 b335a000 r-xp /usr/lib/libminizip.so.1.0.0
b336a000 b336f000 r-xp /usr/lib/libstorage.so.0.1
b337f000 b33de000 r-xp /usr/lib/libmmfcamcorder.so.0.0.0
b33f4000 b3408000 r-xp /usr/lib/libcapi-media-camera.so.0.1.90
b3418000 b345c000 r-xp /usr/lib/libgstbase-1.0.so.0.405.0
b346c000 b3474000 r-xp /usr/lib/libgstapp-1.0.so.0.405.0
b3484000 b34b4000 r-xp /usr/lib/libgstvideo-1.0.so.0.405.0
b34c7000 b3580000 r-xp /usr/lib/libgstreamer-1.0.so.0.405.0
b3594000 b35e7000 r-xp /usr/lib/libmmfplayer.so.0.0.0
b35f8000 b3613000 r-xp /usr/lib/libcapi-media-player.so.0.2.16
b3623000 b36e4000 r-xp /usr/lib/libprotobuf.so.9.0.1
b36f7000 b3707000 r-xp /usr/lib/libefl-assist.so.0.1.0
b3717000 b3724000 r-xp /usr/lib/libmdm-common.so.1.0.98
b3735000 b373c000 r-xp /usr/lib/libcapi-media-tool.so.0.2.2
b374c000 b378d000 r-xp /usr/lib/libmdm.so.1.2.12
b379d000 b37a5000 r-xp /usr/lib/lib_DNSe_NRSS_ver225.so
b37b4000 b37c4000 r-xp /usr/lib/lib_SamsungRec_TizenV04014.so
b37e5000 b3845000 r-xp /usr/lib/libasound.so.2.0.0
b3857000 b385a000 r-xp /usr/lib/libpulse-simple.so.0.0.4
b386a000 b386d000 r-xp /usr/lib/libsyspopup_caller.so.0.1.0
b387d000 b3882000 r-xp /usr/lib/libmmfsoundcommon.so.0.0.0
b3892000 b3893000 r-xp /usr/lib/libgthread-2.0.so.0.4301.0
b38a3000 b38ae000 r-xp /usr/lib/libaudio-session-mgr.so.0.0.1
b38c2000 b38c9000 r-xp /usr/lib/libmmutil_imgp.so.0.0.0
b38d9000 b38df000 r-xp /usr/lib/libmmutil_jpeg.so.0.0.0
b38ef000 b38f4000 r-xp /usr/lib/libmmfsession.so.0.0.1
b3904000 b391f000 r-xp /usr/lib/libmmfsound.so.0.1.0
b392f000 b3936000 r-xp /usr/lib/libmmfcommon.so.0.0.0
b3946000 b3949000 r-xp /usr/lib/libcapi-media-image-util.so.0.1.10
b395a000 b3988000 r-xp /usr/lib/libidn.so.11.5.44
b3998000 b39ae000 r-xp /usr/lib/libnghttp2.so.5.4.0
b39bf000 b39c9000 r-xp /usr/lib/libcares.so.2.1.0
b39d9000 b39e3000 r-xp /usr/lib/libcapi-media-sound-manager.so.0.3.32
b39f3000 b39f5000 r-xp /usr/lib/libcapi-media-wav-player.so.0.1.18
b3a05000 b3a06000 r-xp /usr/lib/libmmfkeysound.so.0.0.0
b3a16000 b3a1a000 r-xp /usr/lib/libecore_ipc.so.1.13.0
b3a2b000 b3a53000 r-xp /usr/lib/libui-extension.so.0.1.0
b3a64000 b3a8d000 r-xp /usr/lib/libturbojpeg.so
b3aad000 b3ab3000 r-xp /usr/lib/libgif.so.4.1.6
b3ac3000 b3b09000 r-xp /usr/lib/libcurl.so.4.3.0
b3b1a000 b3b3b000 r-xp /usr/lib/libexif.so.12.3.3
b3b56000 b3b6b000 r-xp /usr/lib/libtts.so
b3b7c000 b3b84000 r-xp /usr/lib/libfeedback.so.0.1.4
b3b94000 b3c5a000 r-xp /usr/lib/libdali-core.so.0.0.0
b3c7a000 b3d72000 r-xp /usr/lib/libdali-adaptor.so.0.0.0
b3d91000 b3e5f000 r-xp /usr/lib/libdali-toolkit.so.0.0.0
b3e76000 b3e78000 r-xp /usr/lib/libboost_system.so.1.51.0
b3e88000 b3e8e000 r-xp /usr/lib/libboost_chrono.so.1.51.0
b3e9e000 b3ec1000 r-xp /usr/lib/libboost_thread.so.1.51.0
b3ed2000 b3ed4000 r-xp /usr/lib/libappsvc.so.0.1.0
b3ee4000 b3ee6000 r-xp /usr/lib/libcapi-appfw-app-common.so.0.3.1.0
b3ef7000 b3efc000 r-xp /usr/lib/libcapi-appfw-app-control.so.0.3.1.0
b3f13000 b3f15000 r-xp /usr/lib/libosp-env-config.so.1.2.2.1
b3f25000 b3f2c000 r-xp /usr/lib/libsensord-share.so
b3f3c000 b3f54000 r-xp /usr/lib/libsensor.so.1.1.0
b3f65000 b3f68000 r-xp /usr/lib/libXv.so.1.0.0
b3f78000 b3f7d000 r-xp /usr/lib/libutilX.so.1.1.0
b3f8d000 b3f92000 r-xp /usr/lib/libappcore-common.so.1.1
b3fa2000 b3fa9000 r-xp /usr/lib/libcapi-ui-efl-util.so.0.2.11
b3fbc000 b3fc0000 r-xp /usr/lib/libcapi-appfw-application.so.0.3.1.0
b3fd1000 b40af000 r-xp /usr/lib/libCOREGL.so.4.0
b40cf000 b40d2000 r-xp /usr/lib/libuuid.so.1.3.0
b40e2000 b40f9000 r-xp /usr/lib/libblkid.so.1.1.0
b410a000 b410c000 r-xp /usr/lib/libXau.so.6.0.0
b411c000 b4163000 r-xp /usr/lib/pulseaudio/libpulsecommon-4.0.so
b4175000 b417b000 r-xp /usr/lib/libjson-c.so.2.0.1
b418c000 b4190000 r-xp /usr/lib/libogg.so.0.7.1
b41a0000 b41c2000 r-xp /usr/lib/libvorbis.so.0.4.3
b41d2000 b42b6000 r-xp /usr/lib/libvorbisenc.so.2.0.6
b42d2000 b42d5000 r-xp /usr/lib/libEGL.so.1.4
b42e6000 b42ec000 r-xp /usr/lib/libxcb-render.so.0.0.0
b42fc000 b42fe000 r-xp /usr/lib/libxcb-shm.so.0.0.0
b430e000 b431b000 r-xp /usr/lib/libGLESv2.so.2.0
b432c000 b438e000 r-xp /usr/lib/libpixman-1.so.0.28.2
b43a3000 b43bb000 r-xp /usr/lib/libmount.so.1.1.0
b43cd000 b43e1000 r-xp /usr/lib/libxcb.so.1.1.0
b43f1000 b43f8000 r-xp /lib/libcrypt-2.20-2014.11.so
b4430000 b4432000 r-xp /usr/lib/libiri.so
b4442000 b444d000 r-xp /usr/lib/libgpg-error.so.0.15.0
b445e000 b4494000 r-xp /usr/lib/libpulse.so.0.16.2
b44a5000 b44e8000 r-xp /usr/lib/libsndfile.so.1.0.25
b44fd000 b4512000 r-xp /lib/libexpat.so.1.5.2
b4524000 b45e2000 r-xp /usr/lib/libcairo.so.2.11200.14
b45f6000 b45fe000 r-xp /usr/lib/libdrm.so.2.4.0
b460e000 b4611000 r-xp /usr/lib/libdri2.so.0.0.0
b4621000 b466f000 r-xp /usr/lib/libssl.so.1.0.0
b4684000 b4690000 r-xp /usr/lib/libeeze.so.1.13.0
b46a1000 b46aa000 r-xp /usr/lib/libethumb.so.1.13.0
b46ba000 b46bd000 r-xp /usr/lib/libecore_input_evas.so.1.13.0
b46cd000 b4884000 r-xp /usr/lib/libcrypto.so.1.0.0
b566f000 b5678000 r-xp /usr/lib/libXi.so.6.1.0
b5688000 b568a000 r-xp /usr/lib/libXgesture.so.7.0.0
b569a000 b569e000 r-xp /usr/lib/libXtst.so.6.1.0
b56ae000 b56b4000 r-xp /usr/lib/libXrender.so.1.3.0
b56c4000 b56ca000 r-xp /usr/lib/libXrandr.so.2.2.0
b56da000 b56dc000 r-xp /usr/lib/libXinerama.so.1.0.0
b56ed000 b56f0000 r-xp /usr/lib/libXfixes.so.3.1.0
b5700000 b570b000 r-xp /usr/lib/libXext.so.6.4.0
b571b000 b571d000 r-xp /usr/lib/libXdamage.so.1.1.0
b572d000 b572f000 r-xp /usr/lib/libXcomposite.so.1.0.0
b573f000 b5821000 r-xp /usr/lib/libX11.so.6.3.0
b5835000 b583c000 r-xp /usr/lib/libXcursor.so.1.0.2
b584c000 b5864000 r-xp /usr/lib/libudev.so.1.6.0
b5866000 b5869000 r-xp /lib/libattr.so.1.1.0
b5879000 b5899000 r-xp /usr/lib/libsecurity-server-commons.so.1.0.0
b589a000 b589f000 r-xp /usr/lib/libffi.so.6.0.2
b58b0000 b58c8000 r-xp /lib/libz.so.1.2.8
b58d8000 b58da000 r-xp /usr/lib/libgmodule-2.0.so.0.4301.0
b58ea000 b59bf000 r-xp /usr/lib/libxml2.so.2.9.2
b59d4000 b5a6f000 r-xp /usr/lib/libstdc++.so.6.0.20
b5a8b000 b5a8e000 r-xp /usr/lib/libSLP-db-util.so.0.1.0
b5a9e000 b5ab7000 r-xp /usr/lib/libpkgmgr_parser.so.0.1.0
b5ac8000 b5ad9000 r-xp /lib/libresolv-2.20-2014.11.so
b5aed000 b5b67000 r-xp /usr/lib/libgcrypt.so.20.0.3
b5b7c000 b5b7e000 r-xp /usr/lib/libecore_imf_evas.so.1.13.0
b5b8e000 b5b95000 r-xp /usr/lib/libembryo.so.1.13.0
b5ba5000 b5baf000 r-xp /usr/lib/libecore_audio.so.1.13.0
b5bc0000 b5bd8000 r-xp /usr/lib/libpng12.so.0.50.0
b5be9000 b5c0c000 r-xp /usr/lib/libjpeg.so.8.0.2
b5c2d000 b5c41000 r-xp /usr/lib/libector.so.1.13.0
b5c52000 b5c6a000 r-xp /usr/lib/liblua-5.1.so
b5c7b000 b5cd2000 r-xp /usr/lib/libfreetype.so.6.11.3
b5ce6000 b5d0e000 r-xp /usr/lib/libfontconfig.so.1.8.0
b5d1f000 b5d32000 r-xp /usr/lib/libfribidi.so.0.3.1
b5d43000 b5d7d000 r-xp /usr/lib/libharfbuzz.so.0.940.0
b5d8e000 b5d9c000 r-xp /usr/lib/libgraphics-extension.so.0.1.0
b5dac000 b5db4000 r-xp /usr/lib/libtbm.so.1.0.0
b5dc4000 b5dd1000 r-xp /usr/lib/libeio.so.1.13.0
b5de1000 b5de3000 r-xp /usr/lib/libefreet_trash.so.1.13.0
b5df3000 b5df8000 r-xp /usr/lib/libefreet_mime.so.1.13.0
b5e08000 b5e1f000 r-xp /usr/lib/libefreet.so.1.13.0
b5e31000 b5e51000 r-xp /usr/lib/libeldbus.so.1.13.0
b5e61000 b5e81000 r-xp /usr/lib/libecore_con.so.1.13.0
b5e83000 b5e89000 r-xp /usr/lib/libecore_imf.so.1.13.0
b5e99000 b5eaa000 r-xp /usr/lib/libemotion.so.1.13.0
b5ebb000 b5ec2000 r-xp /usr/lib/libethumb_client.so.1.13.0
b5ed2000 b5ee1000 r-xp /usr/lib/libeo.so.1.13.0
b5ef2000 b5f04000 r-xp /usr/lib/libecore_input.so.1.13.0
b5f15000 b5f1a000 r-xp /usr/lib/libecore_file.so.1.13.0
b5f2a000 b5f43000 r-xp /usr/lib/libecore_evas.so.1.13.0
b5f53000 b5f70000 r-xp /usr/lib/libeet.so.1.13.0
b5f89000 b5fd1000 r-xp /usr/lib/libeina.so.1.13.0
b5fe2000 b5ff2000 r-xp /usr/lib/libefl.so.1.13.0
b6003000 b60e8000 r-xp /usr/lib/libicuuc.so.51.1
b6105000 b6245000 r-xp /usr/lib/libicui18n.so.51.1
b625c000 b6294000 r-xp /usr/lib/libecore_x.so.1.13.0
b62a6000 b62a9000 r-xp /lib/libcap.so.2.21
b62b9000 b62e2000 r-xp /usr/lib/libsecurity-server-client.so.1.0.1
b62f3000 b62fa000 r-xp /usr/lib/libcapi-base-common.so.0.2.2
b630c000 b6343000 r-xp /usr/lib/libgobject-2.0.so.0.4301.0
b6354000 b643f000 r-xp /usr/lib/libgio-2.0.so.0.4301.0
b6452000 b64cb000 r-xp /usr/lib/libsqlite3.so.0.8.6
b64dd000 b64e2000 r-xp /usr/lib/libcapi-system-info.so.0.2.1
b64f2000 b64fc000 r-xp /usr/lib/libvconf.so.0.2.45
b650c000 b650e000 r-xp /usr/lib/libvasum.so.0.3.1
b651e000 b6520000 r-xp /usr/lib/libttrace.so.1.1
b6530000 b6533000 r-xp /usr/lib/libiniparser.so.0
b6543000 b6569000 r-xp /usr/lib/libpkgmgr-info.so.0.0.17
b6579000 b657e000 r-xp /usr/lib/libxdgmime.so.1.1.0
b658f000 b65a6000 r-xp /usr/lib/libdbus-glib-1.so.2.2.2
b65b7000 b6622000 r-xp /lib/libm-2.20-2014.11.so
b6633000 b6639000 r-xp /lib/librt-2.20-2014.11.so
b664a000 b6657000 r-xp /usr/lib/libunwind.so.8.0.1
b668d000 b67b1000 r-xp /lib/libc-2.20-2014.11.so
b67c6000 b67df000 r-xp /lib/libgcc_s-4.9.so.1
b67ef000 b68d1000 r-xp /usr/lib/libglib-2.0.so.0.4301.0
b68e2000 b690c000 r-xp /usr/lib/libdbus-1.so.3.8.12
b691d000 b6959000 r-xp /usr/lib/libsystemd.so.0.4.0
b695b000 b69de000 r-xp /usr/lib/libedje.so.1.13.0
b69f1000 b6a0f000 r-xp /usr/lib/libecore.so.1.13.0
b6a2f000 b6bb6000 r-xp /usr/lib/libevas.so.1.13.0
b6beb000 b6bff000 r-xp /lib/libpthread-2.20-2014.11.so
b6c13000 b6e47000 r-xp /usr/lib/libelementary.so.1.13.0
b6e76000 b6e7a000 r-xp /usr/lib/libsmack.so.1.0.0
b6e8a000 b6e91000 r-xp /usr/lib/libprivilege-control.so.0.0.2
b6ea1000 b6ea3000 r-xp /usr/lib/libdlog.so.0.0.0
b6eb3000 b6eb6000 r-xp /usr/lib/libbundle.so.0.1.22
b6ec6000 b6ec8000 r-xp /lib/libdl-2.20-2014.11.so
b6ed9000 b6ef1000 r-xp /usr/lib/libaul.so.0.1.0
b6f05000 b6f0a000 r-xp /usr/lib/libappcore-efl.so.1.1
b6f1b000 b6f28000 r-xp /usr/lib/liblptcp.so
b6f3a000 b6f3e000 r-xp /usr/lib/libsys-assert.so
b6f4f000 b6f6f000 r-xp /lib/ld-2.20-2014.11.so
b6f80000 b6f85000 r-xp /usr/bin/launchpad-loader
b753e000 b7863000 rw-p [heap]
be8c4000 be8e5000 rwxp [stack]
b753e000 b7863000 rw-p [heap]
be8c4000 be8e5000 rwxp [stack]
End of Maps Information

Callstack Information (PID:4020)
Call Stack Count: 1
 0: strcmp + 0x1 (0xb67026f2) [/lib/libc.so.6] + 0x756f2
End of Call Stack

Package Information
Package Name: org.example.uibuildersingleview
Package ID : org.example.uibuildersingleview
Version: 1.0.0
Package Type: tpk
App Name: uibuildersingleview
App ID: org.example.uibuildersingleview
Type: capp
Categories: 

Latest Debug Message Information
--------- beginning of /dev/log_main
_by_apppath(242) > path : /usr/bin/wrt_launchpad_daemon_candidate, ret : 0
12-07 20:20:38.359+0530 I/AUL     ( 4076): menu_db_util.h: _get_app_info_from_db_by_apppath(242) > path : /usr/bin/wrt_launchpad_daemon_candidate, ret : 0
12-07 20:20:38.359+0530 E/AUL     ( 4076): aul_path.c: __get_pkgid(104) > Failed to get appid. (err:-1)
12-07 20:20:38.359+0530 E/AUL     ( 4076): aul_path.c: __get_path(227) > root_path is NULL!
12-07 20:20:38.379+0530 I/AUL     ( 4076): menu_db_util.h: _get_app_info_from_db_by_apppath(242) > path : /usr/bin/wrt_launchpad_daemon_candidate, ret : 0
12-07 20:20:38.389+0530 I/AUL     ( 4076): menu_db_util.h: _get_app_info_from_db_by_apppath(242) > path : /usr/bin/wrt_launchpad_daemon_candidate, ret : 0
12-07 20:20:38.389+0530 E/AUL     ( 4076): aul_path.c: __get_pkgid(104) > Failed to get appid. (err:-1)
12-07 20:20:38.389+0530 E/AUL     ( 4076): aul_path.c: __get_path(227) > root_path is NULL!
12-07 20:20:38.399+0530 I/AUL     ( 4076): menu_db_util.h: _get_app_info_from_db_by_apppath(242) > path : /usr/bin/wrt_launchpad_daemon_candidate, ret : 0
12-07 20:20:38.409+0530 I/AUL     ( 4076): menu_db_util.h: _get_app_info_from_db_by_apppath(242) > path : /usr/bin/wrt_launchpad_daemon_candidate, ret : 0
12-07 20:20:38.409+0530 E/AUL     ( 4076): aul_path.c: __get_pkgid(104) > Failed to get appid. (err:-1)
12-07 20:20:38.409+0530 E/AUL     ( 4076): aul_path.c: __get_path(227) > root_path is NULL!
12-07 20:20:38.419+0530 I/AUL     ( 4076): menu_db_util.h: _get_app_info_from_db_by_apppath(242) > path : /usr/bin/wrt_launchpad_daemon_candidate, ret : 0
12-07 20:20:38.429+0530 I/AUL     ( 4076): menu_db_util.h: _get_app_info_from_db_by_apppath(242) > path : /usr/bin/wrt_launchpad_daemon_candidate, ret : 0
12-07 20:20:38.429+0530 E/AUL     ( 4076): aul_path.c: __get_pkgid(104) > Failed to get appid. (err:-1)
12-07 20:20:38.429+0530 E/AUL     ( 4076): aul_path.c: __get_path(227) > root_path is NULL!
12-07 20:20:38.439+0530 I/AUL     ( 4076): menu_db_util.h: _get_app_info_from_db_by_apppath(242) > path : /usr/bin/wrt_launchpad_daemon_candidate, ret : 0
12-07 20:20:38.449+0530 I/AUL     ( 4076): menu_db_util.h: _get_app_info_from_db_by_apppath(242) > path : /usr/bin/wrt_launchpad_daemon_candidate, ret : 0
12-07 20:20:38.449+0530 E/AUL     ( 4076): aul_path.c: __get_pkgid(104) > Failed to get appid. (err:-1)
12-07 20:20:38.449+0530 E/AUL     ( 4076): aul_path.c: __get_path(227) > root_path is NULL!
12-07 20:20:38.459+0530 I/AUL     ( 4076): menu_db_util.h: _get_app_info_from_db_by_apppath(242) > path : /usr/bin/wrt_launchpad_daemon_candidate, ret : 0
12-07 20:20:38.469+0530 I/AUL     ( 4076): menu_db_util.h: _get_app_info_from_db_by_apppath(242) > path : /usr/bin/wrt_launchpad_daemon_candidate, ret : 0
12-07 20:20:38.469+0530 E/AUL     ( 4076): aul_path.c: __get_pkgid(104) > Failed to get appid. (err:-1)
12-07 20:20:38.469+0530 E/AUL     ( 4076): aul_path.c: __get_path(227) > root_path is NULL!
12-07 20:20:38.479+0530 I/AUL     ( 4076): menu_db_util.h: _get_app_info_from_db_by_apppath(242) > path : /usr/bin/wrt_launchpad_daemon_candidate, ret : 0
12-07 20:20:38.479+0530 I/AUL     ( 4076): menu_db_util.h: _get_app_info_from_db_by_apppath(242) > path : /usr/bin/wrt_launchpad_daemon_candidate, ret : 0
12-07 20:20:38.479+0530 E/AUL     ( 4076): aul_path.c: __get_pkgid(104) > Failed to get appid. (err:-1)
12-07 20:20:38.479+0530 E/AUL     ( 4076): aul_path.c: __get_path(227) > root_path is NULL!
12-07 20:20:38.719+0530 I/Tizen::App( 1290): (499) > LaunchedApp(com.samsung.task-mgr)
12-07 20:20:38.719+0530 I/Tizen::App( 1290): (733) > Finished invoking application event listener for com.samsung.task-mgr, 1690.
12-07 20:20:38.719+0530 E/RESOURCED(  779): resourced-dbus.c: resourced_dbus_system_hash_drop_busname(324) > Does not exist in busname hash: :1.357
12-07 20:20:38.809+0530 I/UXT     ( 4076): Uxt_ObjectManager.cpp: OnInitialized(737) > Initialized.
12-07 20:20:38.879+0530 E/TBM     ( 4076): tbm_bufmgr.c: _tbm_bufmgr_init_state(691) > USE TGL LOCK!
12-07 20:20:38.879+0530 I/MALI    ( 4076): egl_platform_x11.c: __egl_platform_initialize(242) > [EGL-X11] PID=4076   open drm_fd=38 
12-07 20:20:38.919+0530 I/UXT     ( 4112): Uxt_ObjectManager.cpp: OnInitialized(737) > Initialized.
12-07 20:20:38.939+0530 E/UXT     ( 4112): uxt_theme.c: _set_changeable_ui_data(129) > failed to get ecore_evas list.
12-07 20:20:38.939+0530 E/UXT     ( 4112): uxt_theme.c: _set_changeable_ui_data(129) > failed to get ecore_evas list.
12-07 20:20:39.060+0530 I/GXT_SIB ( 4076): gxt_shared_image_buffer.c: gxt_shared_image_buffer_initialize(113) > [Shared Image Buffer] flink_id=5
12-07 20:20:39.090+0530 E/EFL     ( 4076): edje<4076> lib/edje/edje_embryo.c:4134 _edje_embryo_test_run() You are running Embryo->EDC->Embryo with script program '_p26';
12-07 20:20:39.090+0530 E/EFL     ( 4076): By the power of Grayskull, your previous Embryo stack is now broken!
12-07 20:20:39.090+0530 E/EFL     ( 4076): edje<4076> lib/edje/edje_embryo.c:4134 _edje_embryo_test_run() You are running Embryo->EDC->Embryo with script program '_p26';
12-07 20:20:39.090+0530 E/EFL     ( 4076): By the power of Grayskull, your previous Embryo stack is now broken!
12-07 20:20:39.090+0530 E/EFL     ( 4076): edje<4076> lib/edje/edje_embryo.c:4134 _edje_embryo_test_run() You are running Embryo->EDC->Embryo with script program '_p26';
12-07 20:20:39.090+0530 E/EFL     ( 4076): By the power of Grayskull, your previous Embryo stack is now broken!
12-07 20:20:39.110+0530 I/AUL     ( 4112): menu_db_util.h: _get_app_info_from_db_by_apppath(242) > path : /usr/bin/WebProcess, ret : 0
12-07 20:20:39.120+0530 I/AUL     ( 4112): menu_db_util.h: _get_app_info_from_db_by_apppath(242) > path : /usr/bin/wrt_launchpad_daemon_candidate, ret : 0
12-07 20:20:39.120+0530 E/AUL     ( 4112): aul_path.c: __get_pkgid(104) > Failed to get appid. (err:-1)
12-07 20:20:39.120+0530 E/AUL     ( 4112): aul_path.c: __get_path(227) > root_path is NULL!
12-07 20:20:39.560+0530 I/UXT     ( 4120): Uxt_ObjectManager.cpp: OnInitialized(737) > Initialized.
12-07 20:20:39.610+0530 E/TBM     ( 4120): tbm_bufmgr.c: _tbm_bufmgr_init_state(691) > USE TGL LOCK!
12-07 20:20:39.610+0530 I/MALI    ( 4120): egl_platform_x11.c: __egl_platform_initialize(242) > [EGL-X11] PID=4120   open drm_fd=30 
12-07 20:20:39.660+0530 E/EFL     ( 1690): ecore_x<1690> lib/ecore_x/xlib/ecore_x_events.c:603 _ecore_x_event_handle_button_press() ButtonEvent:press time=8592143
12-07 20:20:39.730+0530 E/EFL     ( 4120): edje<4120> lib/edje/edje_embryo.c:4134 _edje_embryo_test_run() You are running Embryo->EDC->Embryo with script program '_p26';
12-07 20:20:39.730+0530 E/EFL     ( 4120): By the power of Grayskull, your previous Embryo stack is now broken!
12-07 20:20:39.730+0530 E/EFL     ( 4120): edje<4120> lib/edje/edje_embryo.c:4134 _edje_embryo_test_run() You are running Embryo->EDC->Embryo with script program '_p26';
12-07 20:20:39.730+0530 E/EFL     ( 4120): By the power of Grayskull, your previous Embryo stack is now broken!
12-07 20:20:39.730+0530 E/EFL     ( 4120): edje<4120> lib/edje/edje_embryo.c:4134 _edje_embryo_test_run() You are running Embryo->EDC->Embryo with script program '_p26';
12-07 20:20:39.730+0530 E/EFL     ( 4120): By the power of Grayskull, your previous Embryo stack is now broken!
12-07 20:20:39.790+0530 E/EFL     ( 1690): ecore_x<1690> lib/ecore_x/xlib/ecore_x_events.c:756 _ecore_x_event_handle_button_release() ButtonEvent:release time=8592275
12-07 20:20:40.071+0530 E/EFL     ( 1690): ecore_x<1690> lib/ecore_x/xlib/ecore_x_events.c:603 _ecore_x_event_handle_button_press() ButtonEvent:press time=8592552
12-07 20:20:40.231+0530 E/EFL     ( 1690): ecore_x<1690> lib/ecore_x/xlib/ecore_x_events.c:756 _ecore_x_event_handle_button_release() ButtonEvent:release time=8592717
12-07 20:20:40.661+0530 E/EFL     ( 1690): ecore_x<1690> lib/ecore_x/xlib/ecore_x_events.c:603 _ecore_x_event_handle_button_press() ButtonEvent:press time=8593145
12-07 20:20:40.821+0530 E/EFL     ( 1690): ecore_x<1690> lib/ecore_x/xlib/ecore_x_events.c:756 _ecore_x_event_handle_button_release() ButtonEvent:release time=8593299
12-07 20:20:40.821+0530 W/AUL_AMD (  664): amd_request.c: __request_handler(906) > __request_handler: 14
12-07 20:20:40.821+0530 W/AUL_AMD (  664): amd_request.c: __send_result_to_client(158) > __send_result_to_client, pid: 4074
12-07 20:20:40.821+0530 W/AUL     ( 1690): launch.c: app_request_to_launchpad(396) > request cmd(4) to(4074)
12-07 20:20:40.821+0530 W/AUL_AMD (  664): amd_request.c: __request_handler(906) > __request_handler: 4
12-07 20:20:40.821+0530 W/AUL_AMD (  664): amd_launch.c: __reply_handler(1083) > listen fd(32) , send fd(31), pid(4074), cmd(4)
12-07 20:20:40.821+0530 I/APP_CORE( 4074): appcore-efl.c: __do_app(514) > [APP 4074] Event: TERMINATE State: PAUSED
12-07 20:20:40.831+0530 W/AUL_AMD (  664): amd_request.c: __request_handler(906) > __request_handler: 22
12-07 20:20:40.831+0530 W/AUL_AMD (  664): amd_request.c: __request_handler(1201) > app status : 5
12-07 20:20:40.831+0530 W/AUL_AMD (  664): amd_request.c: __request_handler(906) > __request_handler: 22
12-07 20:20:40.831+0530 W/AUL_AMD (  664): amd_request.c: __request_handler(1201) > app status : 5
12-07 20:20:40.831+0530 W/AUL     ( 1690): launch.c: app_request_to_launchpad(425) > request cmd(4) result(0)
12-07 20:20:40.851+0530 I/TIZEN_N_EFL_UTIL_WINDOW( 4074): efl_util_window_dump.c: efl_util_capture_windows_in_buffer(832) > Windump Time = 25
12-07 20:20:40.851+0530 E/APP_CORE( 4074): appcore-efl.c: _capture_and_make_file(1619) > win[5600003] widget[720] height[1280]
12-07 20:20:40.851+0530 W/AUL_AMD (  664): amd_request.c: __request_handler(906) > __request_handler: 33
12-07 20:20:40.851+0530 E/APP_CORE( 4074): appcore-efl.c: _capture_and_make_file(1622) > cannot a capture file for the package of [kNWnnj0vTH.MyGalaxy]
12-07 20:20:40.861+0530 E/TIZEN_N_SYSTEM_SETTINGS( 4074): system_settings.c: system_settings_unset_changed_cb(592) > Enter [system_settings_unset_changed_cb]
12-07 20:20:40.861+0530 E/TIZEN_N_SYSTEM_SETTINGS( 4074): system_settings.c: system_settings_get_item(379) > Enter [system_settings_get_item], key=4
12-07 20:20:40.861+0530 E/TIZEN_N_SYSTEM_SETTINGS( 4074): system_settings.c: system_settings_get_item(392) > Enter [system_settings_get_item], index = 4, key = 4, type = 0
12-07 20:20:40.971+0530 I/MALI    ( 4074): egl_platform_x11.c: __egl_platform_terminate(358) > [EGL-X11] PID=4074   close drm_fd=43 
12-07 20:20:40.971+0530 E/CoreGL  ( 4074): coregl_fastpath_egl.c: fastpath_eglReleaseThread(1059) > [40;31;1mfastpath_eglReleaseThread(1059) error. 'dpy != EGL_NO_DISPLAY'[0m
12-07 20:20:40.971+0530 W/CoreGL  ( 4074): coregl_fastpath_egl.c: fastpath_eglMakeCurrent(1149) >  Error making context [(nil)] current. (invalid EGL display [(nil)] or EGL surface [D:(nil)/R:(nil)])
12-07 20:20:40.981+0530 I/CAPI_NETWORK_CONNECTION( 4074): connection.c: __connection_set_type_changed_callback(181) > Successfully de-registered(0)
12-07 20:20:40.981+0530 I/CAPI_NETWORK_CONNECTION( 4074): connection.c: connection_destroy(427) > Destroy handle: 0xb78a2200
12-07 20:20:40.981+0530 E/VCONF   ( 4074): vconf-kdb.c: _vconf_kdb_del_notify(468) > Error: inotify_add_watch() [/var/run/memory/pm+battery_timetofull]: Permission denied
12-07 20:20:40.981+0530 E/VCONF   ( 4074): vconf.c: vconf_ignore_key_changed(3218) > vconf_ignore_key_changed() failed: key(memory/pm/battery_timetofull)
12-07 20:20:40.991+0530 E/VCONF   ( 4074): vconf-kdb.c: _vconf_kdb_del_notify(468) > Error: inotify_add_watch() [/var/run/memory/pm+battery_timetoempty]: Permission denied
12-07 20:20:40.991+0530 E/VCONF   ( 4074): vconf.c: vconf_ignore_key_changed(3218) > vconf_ignore_key_changed() failed: key(memory/pm/battery_timetoempty)
12-07 20:20:41.041+0530 I/ESD     (  941): esd_main.c: __esd_app_dead_handler(1773) > pid: 4074
12-07 20:20:41.041+0530 I/Tizen::App( 1290): (243) > App[kNWnnj0vTH.MyGalaxy] pid[4074] terminate event is forwarded
12-07 20:20:41.041+0530 I/Tizen::System( 1290): (256) > osp.accessorymanager.service provider is found.
12-07 20:20:41.041+0530 I/Tizen::System( 1290): (196) > Accessory Owner is removed [kNWnnj0vTH.MyGalaxy, 4074, ]
12-07 20:20:41.041+0530 I/Tizen::System( 1290): (256) > osp.system.service provider is found.
12-07 20:20:41.041+0530 I/Tizen::App( 1290): (506) > TerminatedApp(kNWnnj0vTH.MyGalaxy)
12-07 20:20:41.041+0530 I/Tizen::App( 1290): (512) > Not registered pid(4074)
12-07 20:20:41.041+0530 I/Tizen::System( 1290): (246) > Terminated app [kNWnnj0vTH.MyGalaxy]
12-07 20:20:41.041+0530 I/Tizen::Io( 1290): (729) > Entry not found
12-07 20:20:41.041+0530 W/AUL_AMD (  664): amd_main.c: __app_dead_handler(327) > __app_dead_handler, pid: 4074
12-07 20:20:41.062+0530 I/Tizen::System( 1290): (157) > change brightness system value.
12-07 20:20:41.062+0530 I/Tizen::App( 1290): (782) > Finished invoking application event listener for kNWnnj0vTH.MyGalaxy, 4074.
12-07 20:20:41.262+0530 E/EFL     ( 1690): ecore_x<1690> lib/ecore_x/xlib/ecore_x_events.c:603 _ecore_x_event_handle_button_press() ButtonEvent:press time=8593741
12-07 20:20:41.412+0530 E/EFL     ( 1690): ecore_x<1690> lib/ecore_x/xlib/ecore_x_events.c:756 _ecore_x_event_handle_button_release() ButtonEvent:release time=8593895
12-07 20:20:41.412+0530 W/AUL_AMD (  664): amd_request.c: __request_handler(906) > __request_handler: 14
12-07 20:20:41.412+0530 W/AUL_AMD (  664): amd_request.c: __send_result_to_client(158) > __send_result_to_client, pid: 1480
12-07 20:20:41.412+0530 W/AUL     ( 1690): launch.c: app_request_to_launchpad(396) > request cmd(4) to(1480)
12-07 20:20:41.412+0530 W/AUL_AMD (  664): amd_request.c: __request_handler(906) > __request_handler: 4
12-07 20:20:41.422+0530 W/AUL_AMD (  664): amd_launch.c: __reply_handler(1083) > listen fd(32) , send fd(31), pid(1480), cmd(4)
12-07 20:20:41.422+0530 W/AUL     ( 1690): launch.c: app_request_to_launchpad(425) > request cmd(4) result(0)
12-07 20:20:41.422+0530 I/APP_CORE( 1480): appcore-efl.c: __do_app(514) > [APP 1480] Event: TERMINATE State: PAUSED
12-07 20:20:41.422+0530 W/AUL_AMD (  664): amd_request.c: __request_handler(906) > __request_handler: 22
12-07 20:20:41.422+0530 W/AUL_AMD (  664): amd_request.c: __request_handler(1201) > app status : 5
12-07 20:20:41.432+0530 W/AUL_AMD (  664): amd_request.c: __request_handler(906) > __request_handler: 22
12-07 20:20:41.432+0530 W/AUL_AMD (  664): amd_request.c: __request_handler(1201) > app status : 5
12-07 20:20:41.452+0530 I/TIZEN_N_EFL_UTIL_WINDOW( 1480): efl_util_window_dump.c: efl_util_capture_windows_in_buffer(832) > Windump Time = 17
12-07 20:20:41.452+0530 E/APP_CORE( 1480): appcore-efl.c: _capture_and_make_file(1619) > win[3200003] widget[720] height[1280]
12-07 20:20:41.452+0530 W/AUL_AMD (  664): amd_request.c: __request_handler(906) > __request_handler: 33
12-07 20:20:41.452+0530 E/APP_CORE( 1480): appcore-efl.c: _capture_and_make_file(1622) > cannot a capture file for the package of [com.samsung.camera-app-lite]
12-07 20:20:41.462+0530 I/CAPI_APPFW_APPLICATION( 1480): app_main.c: _ui_app_appcore_terminate(663) > app_appcore_terminate
12-07 20:20:41.462+0530 W/CAM_APP ( 1480): CamApp.cpp: onTerminate(186) > [0;35mBEGIN[0;m
12-07 20:20:41.462+0530 W/CAM_APP ( 1480): CamControl.cpp: operator()(125) > [0;35mevent type: 3[0;m
12-07 20:20:41.462+0530 W/CAM_APP ( 1480): CamControl.cpp: __terminate(1365) > [0;35mBEGIN[0;m
12-07 20:20:41.462+0530 W/CAM_APP ( 1480): CamControl.cpp: destroyShot(390) > [0;35mBEGIN[0;m
12-07 20:20:41.462+0530 W/CAM_APP ( 1480): CamControl.cpp: destroyShot(399) > [0;35mEND[0;m
12-07 20:20:41.462+0530 W/CAM_APP ( 1480): CamControl.cpp: destroyRecord(444) > [0;35mBEGIN[0;m
12-07 20:20:41.462+0530 W/CAM_APP ( 1480): CamControl.cpp: destroyRecord(451) > [0;35mEND[0;m
12-07 20:20:41.462+0530 W/CAM_APP ( 1480): CamControl.cpp: __terminate(1386) > [0;35mEND[0;m
12-07 20:20:41.462+0530 W/CAM_APP ( 1480): CamStandbyView.cpp: operator()(267) > [0;35mevent type: 3[0;m
12-07 20:20:41.462+0530 W/CAM_APP ( 1480): CamControl.cpp: unsetFaceDetectedCallback(844) > [0;35munset face detected callback: 1[0;m
12-07 20:20:41.462+0530 W/CAM_APP ( 1480): CamView.cpp: stopSelfTerminateTimer(356) > [0;35mHIT[0;m
12-07 20:20:41.462+0530 W/CAM_APP ( 1480): CamSystemDeviceMgr.cpp: setMdnieMode(439) > [0;35mBEGIN[0;m
12-07 20:20:41.462+0530 W/CAM_APP ( 1480): CamSystemDeviceMgr.cpp: __invokeDbusMethodSync(714) > [0;35mBEGIN[0;m
12-07 20:20:41.462+0530 W/CAM_APP ( 1480): CamSystemDeviceMgr.cpp: __invokeDbusMethodSync(756) > [0;35mEND[0;m
12-07 20:20:41.462+0530 W/CAM_APP ( 1480): CamSystemDeviceMgr.cpp: __setEnhanceScenario(476) > [0;35mset mdnie - SCENARIO_UI [0;m
12-07 20:20:41.462+0530 W/CAM_APP ( 1480): CamSystemDeviceMgr.cpp: __invokeDbusMethodSync(714) > [0;35mBEGIN[0;m
12-07 20:20:41.472+0530 W/CAM_APP ( 1480): CamSystemDeviceMgr.cpp: __invokeDbusMethodSync(756) > [0;35mEND[0;m
12-07 20:20:41.472+0530 W/CAM_APP ( 1480): CamSystemDeviceMgr.cpp: setMdnieMode(443) > [0;35mEND[0;m
12-07 20:20:41.472+0530 W/CAM_APP ( 1480): CamLauncher.cpp: terminatePreloadImageViewer(242) > [0;35mBEGIN[0;m
12-07 20:20:41.472+0530 W/CAM_APP ( 1480): CamLauncher.cpp: terminatePreloadImageViewer(268) > [0;35mEND[0;m
12-07 20:20:41.472+0530 W/CAM_APP ( 1480): CamThreadMgr.cpp: joinThread(85) > [0;35mBEGIN[0;m
12-07 20:20:41.472+0530 W/CAM_APP ( 1480): CamFileRegisterMgr.cpp: __registerFileThreadCb(325) > [0;35mEnd of REGISTER_FILE thread loop[0;m
12-07 20:20:41.472+0530 W/CAM_APP ( 1480): CamFileRegisterMgr.cpp: __registerFileThreadCb(330) > [0;35mEND[0;m
12-07 20:20:41.472+0530 W/CAM_APP ( 1480): CamThreadMgr.cpp: joinThread(102) > [0;35mEND[0;m
12-07 20:20:41.472+0530 I/CAPI_CONTENT_MEDIA_CONTENT( 1480): media_content.c: media_content_disconnect(940) > [32m[1480]ref count changed to: 0
12-07 20:20:41.472+0530 W/CAM_APP ( 1480): CamThreadMgr.cpp: joinThread(85) > [0;35mBEGIN[0;m
12-07 20:20:41.472+0530 W/CAM_APP ( 1480): CamSubControlMgr.cpp: __threadCb(355) > [0;35mEnd of thread loop[0;m
12-07 20:20:41.472+0530 W/CAM_APP ( 1480): CamSubControlMgr.cpp: __threadCb(360) > [0;35mEND[0;m
12-07 20:20:41.472+0530 W/CAM_APP ( 1480): CamThreadMgr.cpp: joinThread(102) > [0;35mEND[0;m
12-07 20:20:41.472+0530 W/CAM_APP ( 1480): CamCoordRotationMgr.cpp: stop(83) > [0;35mBEGIN[0;m
12-07 20:20:41.472+0530 W/CAM_APP ( 1480): CamCoordRotationMgr.cpp: stop(86) > [0;35malready stopped. skip this[0;m
12-07 20:20:41.472+0530 W/CAM_APP ( 1480): CamSystemDeviceMgr.cpp: unlockDisplay(97) > [0;35mBEGIN[0;m
12-07 20:20:41.472+0530 W/CAM_APP ( 1480): CamSystemDeviceMgr.cpp: unlockDisplay(112) > [0;35mEND[0;m
12-07 20:20:41.472+0530 W/CAM_APP ( 1480): CamSystemDeviceMgr.cpp: unregisterEvent(416) > [0;35mBEGIN[0;m
12-07 20:20:41.492+0530 W/CAM_APP ( 1480): CamSystemDeviceMgr.cpp: unregisterEvent(434) > [0;35mEND[0;m
12-07 20:20:41.492+0530 W/CAM_APP ( 1480): CamAFMgr.cpp: stop(130) > [0;35mBEGIN[0;m
12-07 20:20:41.492+0530 E/CAM_APP ( 1480): CamMenuComposer.cpp: isMenuEnable(131) > [0;31m* Critical *  appData is nullptr [0;m
12-07 20:20:41.492+0530 W/CAM_APP ( 1480): CamAFMgr.cpp: stop(133) > [0;35mcurrent mode doesnt support AF[0;m
12-07 20:20:41.492+0530 W/CAM_APP ( 1480): CamControl.cpp: destroyShot(390) > [0;35mBEGIN[0;m
12-07 20:20:41.492+0530 W/CAM_APP ( 1480): CamControl.cpp: destroyShot(399) > [0;35mEND[0;m
12-07 20:20:41.492+0530 W/CAM_APP ( 1480): CamControl.cpp: destroyRecord(444) > [0;35mBEGIN[0;m
12-07 20:20:41.492+0530 W/CAM_APP ( 1480): CamControl.cpp: destroyRecord(451) > [0;35mEND[0;m
12-07 20:20:41.492+0530 E/efl-extension( 1480): efl_extension.c: eext_win_keygrab_unset(123) >   Can't unset keygrab of [XF86AudioRaiseVolume].
12-07 20:20:41.492+0530 E/efl-extension( 1480): efl_extension.c: eext_win_keygrab_unset(123) >   Can't unset keygrab of [XF86AudioLowerVolume].
12-07 20:20:41.492+0530 W/CAM_APP ( 1480): CamDevControl.cpp: destroyDevice(130) > [0;35mBEGIN[0;m
12-07 20:20:41.492+0530 W/CAM_APP ( 1480): CamSoundSessionMgr.cpp: destroySession(67) > [0;35mBEGIN[0;m
12-07 20:20:41.492+0530 I/TIZEN_N_SOUND_MANAGER( 1480): sound_manager_product.c: sound_manager_multi_session_destroy(939) > >> enter : session=0xb86cc050
12-07 20:20:41.492+0530 I/TIZEN_N_SOUND_MANAGER( 1480): sound_manager_product.c: sound_manager_multi_session_destroy(1001) > << leave : already set same option(0), skip it
12-07 20:20:41.492+0530 I/TIZEN_N_SOUND_MANAGER( 1480): sound_manager_product.c: sound_manager_multi_session_destroy(1010) > << leave : session=(nil)
12-07 20:20:41.492+0530 W/CAM_APP ( 1480): CamSoundSessionMgr.cpp: destroySession(75) > [0;35mEND[0;m
12-07 20:20:41.492+0530 E/TIZEN_N_RECORDER( 1480): recorder.c: __convert_recorder_error_code(189) > [recorder_destroy] ERROR_NONE(0x00000000) : core frameworks error code(0x00000000)
12-07 20:20:41.492+0530 W/TIZEN_N_CAMERA( 1480): camera.c: camera_destroy(984) > camera handle 0xb8944e08
12-07 20:20:41.492+0530 I/TIZEN_N_CAMERA( 1480): camera.c: _camera_remove_cb_message(91) > start
12-07 20:20:41.492+0530 W/TIZEN_N_CAMERA( 1480): camera.c: _camera_remove_cb_message(117) > There is no remained callback
12-07 20:20:41.492+0530 I/TIZEN_N_CAMERA( 1480): camera.c: _camera_remove_cb_message(122) > done
12-07 20:20:41.492+0530 W/CAM_APP ( 1480): CamDevControl.cpp: destroyDevice(152) > [0;35mEND[0;m
12-07 20:20:41.492+0530 W/CAM_APP ( 1480): CamApp.cpp: onTerminate(239) > [0;35mEND[0;m
12-07 20:20:41.502+0530 W/CAM_APP ( 1480): CamButton.cpp: ~CamButton(45) > [0;35mname = [Camera button], buttonMenu = [0][0;m
12-07 20:20:41.502+0530 W/CAM_APP ( 1480): CamButton.cpp: ~CamButton(45) > [0;35mname = [Camcorder button], buttonMenu = [2][0;m
12-07 20:20:41.502+0530 W/CAM_APP ( 1480): CamButton.cpp: ~CamButton(45) > [0;35mname = [Switch button], buttonMenu = [37][0;m
12-07 20:20:41.512+0530 W/CAM_APP ( 1480): CamButton.cpp: ~CamButton(45) > [0;35mname = [Quick setting button], buttonMenu = [7][0;m
12-07 20:20:41.512+0530 W/CAM_APP ( 1480): CamButton.cpp: ~CamButton(45) > [0;35mname = [Thumbnail button], buttonMenu = [8][0;m
12-07 20:20:41.512+0530 W/CAM_APP ( 1480): CamButton.cpp: ~CamButton(45) > [0;35mname = [Mode button], buttonMenu = [55][0;m
12-07 20:20:41.522+0530 W/CAM_APP ( 1480): CamGuideText.cpp: clear(79) > [0;35mguide text is not displayed again until CamGuideText is re-created[0;m
12-07 20:20:41.522+0530 E/EFL     ( 1480): <1480> elm_main.c:1305 elm_object_part_content_unset() safety check failed: obj == NULL
12-07 20:20:41.562+0530 I/MALI    ( 1480): egl_platform_x11.c: __egl_platform_terminate(358) > [EGL-X11] PID=1480   close drm_fd=33 
12-07 20:20:41.772+0530 W/CAM_APP ( 1480): main.cpp: main(32) > [0;35mEND[0;m
12-07 20:20:41.802+0530 W/AUL_AMD (  664): amd_request.c: __send_app_termination_signal(609) > send dead signal done
12-07 20:20:41.812+0530 I/ESD     (  941): esd_main.c: __esd_app_dead_handler(1773) > pid: 1480
12-07 20:20:41.812+0530 I/Tizen::App( 1290): (243) > App[com.samsung.camera-app-lite] pid[1480] terminate event is forwarded
12-07 20:20:41.812+0530 I/Tizen::System( 1290): (256) > osp.accessorymanager.service provider is found.
12-07 20:20:41.812+0530 I/Tizen::System( 1290): (196) > Accessory Owner is removed [com.samsung.camera-app-lite, 1480, ]
12-07 20:20:41.812+0530 I/Tizen::System( 1290): (256) > osp.system.service provider is found.
12-07 20:20:41.812+0530 I/Tizen::App( 1290): (506) > TerminatedApp(com.samsung.camera-app-lite)
12-07 20:20:41.812+0530 I/Tizen::App( 1290): (512) > Not registered pid(1480)
12-07 20:20:41.812+0530 I/Tizen::System( 1290): (246) > Terminated app [com.samsung.camera-app-lite]
12-07 20:20:41.812+0530 I/Tizen::Io( 1290): (729) > Entry not found
12-07 20:20:41.812+0530 W/AUL_AMD (  664): amd_main.c: __app_dead_handler(327) > __app_dead_handler, pid: 1480
12-07 20:20:41.822+0530 W/CAM_SERVICE( 1475): CamService.cpp: __launchCameraApp(86) > [33mBEGIN[0m
12-07 20:20:41.822+0530 W/AUL_AMD (  664): amd_request.c: __request_handler(906) > __request_handler: 14
12-07 20:20:41.822+0530 I/Tizen::System( 1290): (157) > change brightness system value.
12-07 20:20:41.822+0530 I/Tizen::App( 1290): (782) > Finished invoking application event listener for com.samsung.camera-app-lite, 1480.
12-07 20:20:41.822+0530 E/TASK_MGR_LITE( 1690): recent_app_viewer.c: on_item_mouse_move(863) > mouse hasn't been pressed!
12-07 20:20:41.822+0530 E/EFL     ( 1690): ecore_x<1690> lib/ecore_x/xlib/ecore_x_events.c:603 _ecore_x_event_handle_button_press() ButtonEvent:press time=8594308
12-07 20:20:41.832+0530 W/AUL_AMD (  664): amd_request.c: __send_result_to_client(158) > __send_result_to_client, pid: -1
12-07 20:20:41.832+0530 W/AUL     ( 1475): launch.c: app_request_to_launchpad(396) > request cmd(0) to(com.samsung.camera-app-lite)
12-07 20:20:41.832+0530 W/AUL_AMD (  664): amd_request.c: __request_handler(906) > __request_handler: 0
12-07 20:20:41.832+0530 W/AUL_AMD (  664): amd_launch.c: _start_app(2508) > caller pid : 1475
12-07 20:20:41.832+0530 I/AUL_AMD (  664): amd_launch.c: __check_app_control_privilege(1830) > Skip the privilege check in case of preloaded apps
12-07 20:20:41.842+0530 E/RESOURCED(  779): heart-memory.c: heart_memory_get_data(601) > hashtable heart_memory_app_list is NULL
12-07 20:20:41.842+0530 W/AUL_AMD (  664): amd_launch.c: _start_app(3052) > pad pid(-5)
12-07 20:20:41.842+0530 W/AUL_PAD ( 1521): launchpad.c: __launchpad_main_loop(520) > Launch on type-based process-pool
12-07 20:20:41.842+0530 W/AUL_PAD ( 1521): launchpad.c: __send_result_to_caller(267) > Check app launching
12-07 20:20:41.912+0530 E/EFL     ( 1690): ecore_x<1690> lib/ecore_x/xlib/ecore_x_events.c:756 _ecore_x_event_handle_button_release() ButtonEvent:release time=8594395
12-07 20:20:41.932+0530 W/CAM_APP ( 4120): main.cpp: main(26) > [0;35mBEGIN[0;m
12-07 20:20:41.932+0530 I/CAPI_APPFW_APPLICATION( 4120): app_main.c: ui_app_main(789) > app_efl_main
12-07 20:20:41.942+0530 I/CAPI_APPFW_APPLICATION( 4120): app_main.c: _ui_app_appcore_create(641) > app_appcore_create
12-07 20:20:41.942+0530 W/CAM_APP ( 4120): CamApp.cpp: onCreate(102) > [0;35mBEGIN[0;m
12-07 20:20:41.942+0530 W/AUL     (  664): app_signal.c: aul_send_app_launch_request_signal(423) > send_app_launch_signal, pid: 4120, appid: com.samsung.camera-app-lite
12-07 20:20:41.942+0530 E/AUL     (  664): amd_app_group.c: app_group_start_app(1037) > app_group_start_app
12-07 20:20:41.952+0530 W/AUL_AMD (  664): amd_request.c: __request_handler(906) > __request_handler: 14
12-07 20:20:41.952+0530 W/AUL     ( 1475): launch.c: app_request_to_launchpad(425) > request cmd(0) result(4120)
12-07 20:20:41.952+0530 W/CAM_SERVICE( 1475): CamService.cpp: __launchCameraApp(119) > [33mEND[0m
12-07 20:20:41.952+0530 W/AUL_AMD (  664): amd_request.c: __send_result_to_client(158) > __send_result_to_client, pid: -1
12-07 20:20:41.952+0530 W/AUL     ( 1690): launch.c: app_request_to_launchpad(396) > request cmd(0) to(org.example.uibuildersingleview)
12-07 20:20:41.952+0530 W/AUL_AMD (  664): amd_request.c: __request_handler(906) > __request_handler: 0
12-07 20:20:41.952+0530 W/AUL_AMD (  664): amd_launch.c: _start_app(2508) > caller pid : 1690
12-07 20:20:41.972+0530 E/RESOURCED(  779): block.c: block_prelaunch_state(134) > insert data org.example.uibuildersingleview, table num : 4
12-07 20:20:41.972+0530 E/RESOURCED(  779): heart-memory.c: heart_memory_get_data(601) > hashtable heart_memory_app_list is NULL
12-07 20:20:41.972+0530 W/AUL_AMD (  664): amd_launch.c: _start_app(3052) > pad pid(-5)
12-07 20:20:42.002+0530 I/CAPI_CONTENT_MEDIA_CONTENT( 4120): media_content.c: media_content_connect(856) > [32m[4120]ref count : 0
12-07 20:20:42.002+0530 I/CAPI_CONTENT_MEDIA_CONTENT( 4120): media_content.c: media_content_connect(888) > [32m[4120]ref count changed to: 1
12-07 20:20:42.002+0530 W/CAM_APP ( 4120): CamThreadMgr.cpp: createThread(47) > [0;35mBEGIN[0;m
12-07 20:20:42.002+0530 W/CAM_APP ( 4120): CamThreadMgr.cpp: createThread(67) > [0;35mEND[0;m
12-07 20:20:42.002+0530 W/CAM_APP ( 4120): CamFileRegisterMgr.cpp: __registerFileThreadCb(277) > [0;35mBEGIN[0;m
12-07 20:20:42.002+0530 W/CAM_APP ( 4120): CamDreamTemplateInfo.cpp: init(47) > [0;35mSTART[0;m
12-07 20:20:42.002+0530 W/CAM_APP ( 4120): CamDreamTemplateInfo.cpp: init(65) > [0;35mg_key_file_load_from_file failed: No such file or directory[0;m
12-07 20:20:42.002+0530 W/CAM_APP ( 4120): CamDreamTemplateInfo.cpp: init(76) > [0;35mload default data for dreamshot[0;m
12-07 20:20:42.002+0530 W/CAM_APP ( 4120): CamDreamTemplateInfo.cpp: init(90) > [0;35mEND[0;m
12-07 20:20:42.032+0530 W/CAM_APP ( 4120): CamSiopControlMgr.cpp: start(49) > [0;35mBEGIN[0;m
12-07 20:20:42.032+0530 W/CAM_APP ( 4120): CamSiopControlMgr.cpp: start(62) > [0;35mEND[0;m
12-07 20:20:42.163+0530 I/APP_CORE(  895): appcore-efl.c: __do_app(514) > [APP 895] Event: MEM_FLUSH State: PAUSED
12-07 20:20:42.963+0530 W/AUL_PAD ( 1521): launchpad.c: __launchpad_main_loop(520) > Launch on type-based process-pool
12-07 20:20:42.963+0530 W/AUL_PAD ( 1521): launchpad.c: __send_result_to_caller(267) > Check app launching
12-07 20:20:42.973+0530 I/Tizen::App( 1290): (499) > LaunchedApp(com.samsung.camera-app-lite)
12-07 20:20:42.973+0530 I/Tizen::App( 1290): (733) > Finished invoking application event listener for com.samsung.camera-app-lite, 4120.
12-07 20:20:42.973+0530 E/RESOURCED(  779): resourced-dbus.c: resourced_dbus_system_hash_drop_busname(324) > Does not exist in busname hash: :1.361
12-07 20:20:42.973+0530 I/CAPI_APPFW_APPLICATION( 4020): app_main.c: ui_app_main(789) > app_efl_main
12-07 20:20:42.983+0530 I/CAPI_APPFW_APPLICATION( 4020): app_main.c: _ui_app_appcore_create(641) > app_appcore_create
12-07 20:20:43.023+0530 E/TBM     ( 4020): tbm_bufmgr.c: _tbm_bufmgr_init_state(691) > USE TGL LOCK!
12-07 20:20:43.063+0530 W/AUL     (  664): app_signal.c: aul_send_app_launch_request_signal(423) > send_app_launch_signal, pid: 4020, appid: org.example.uibuildersingleview
12-07 20:20:43.063+0530 E/AUL     (  664): amd_app_group.c: app_group_start_app(1037) > app_group_start_app
12-07 20:20:43.063+0530 W/AUL     ( 1690): launch.c: app_request_to_launchpad(425) > request cmd(0) result(4020)
12-07 20:20:43.073+0530 E/EFL     (  664): ecore_x<664> lib/ecore_x/xlib/ecore_x_events.c:603 _ecore_x_event_handle_button_press() ButtonEvent:press time=8595254
12-07 20:20:43.073+0530 E/EFL     (  664): ecore_x<664> lib/ecore_x/xlib/ecore_x_events.c:756 _ecore_x_event_handle_button_release() ButtonEvent:release time=8595342
12-07 20:20:43.073+0530 W/AUL_AMD (  664): amd_request.c: __request_handler(906) > __request_handler: 14
12-07 20:20:43.073+0530 W/AUL_AMD (  664): amd_request.c: __send_result_to_client(158) > __send_result_to_client, pid: 4120
12-07 20:20:43.073+0530 E/RESOURCED(  779): proc-main.c: proc_add_program_list(237) > not found ppi : org.example.uibuildersingleview
12-07 20:20:43.083+0530 W/AUL_AMD (  664): amd_request.c: __request_handler(906) > __request_handler: 12
12-07 20:20:43.083+0530 W/CAM_APP ( 4120): CamApp.cpp: onCreate(180) > [0;35mEND[0;m
12-07 20:20:43.083+0530 I/APP_CORE( 4120): appcore-efl.c: __do_app(514) > [APP 4120] Event: RESET State: CREATED
12-07 20:20:43.083+0530 I/CAPI_APPFW_APPLICATION( 4120): app_main.c: _ui_app_appcore_reset(723) > app_appcore_reset
12-07 20:20:43.083+0530 W/CAM_APP ( 4120): CamApp.cpp: onAppControl(244) > [0;35mBEGIN : firstLaunch - [1][0;m
12-07 20:20:43.083+0530 W/CAM_APP ( 4120): CamRequestParser.cpp: parse(61) > [0;35mBEGIN[0;m
12-07 20:20:43.083+0530 W/CAM_APP ( 4120): CamRequestParser.cpp: __parseMainData(127) > [0;35moperation - [http://tizen.org/appcontrol/operation/prelaunch][0;m
12-07 20:20:43.083+0530 W/CAM_APP ( 4120): CamRequestParser.cpp: parse(76) > [0;35mdonot need to parse other data[0;m
12-07 20:20:43.083+0530 W/CAM_APP ( 4120): CamRequestParser.cpp: parse(83) > [0;35mEND[0;m
12-07 20:20:43.083+0530 W/CAM_APP ( 4120): CamDevControl.cpp: createDevice(91) > [0;35mBEGIN - type : [1][0;m
12-07 20:20:43.083+0530 W/CAM_APP ( 4120): CamSoundSessionMgr.cpp: createSession(54) > [0;35mBEGIN[0;m
12-07 20:20:43.083+0530 I/TIZEN_N_SOUND_MANAGER( 4120): sound_manager_product.c: sound_manager_multi_session_create(656) > >> enter : type=2, session=0xb7310308
12-07 20:20:43.083+0530 I/TIZEN_N_SOUND_MANAGER( 4120): sound_manager_product.c: sound_manager_multi_session_create(726) > << leave : type=2, session=0xb7310308
12-07 20:20:43.083+0530 W/CAM_APP ( 4120): CamSoundSessionMgr.cpp: createSession(60) > [0;35mEND[0;m
12-07 20:20:43.083+0530 W/TIZEN_N_CAMERA( 4120): camera.c: camera_create(879) > device name = [1]
12-07 20:20:43.144+0530 W/TIZEN_N_CAMERA( 4120): camera.c: camera_create(963) > camera handle 0xb7310a60
12-07 20:20:43.144+0530 W/TIZEN_N_RECORDER( 4120): recorder.c: recorder_create_videorecorder(553) > permission check done
12-07 20:20:43.144+0530 E/TIZEN_N_RECORDER( 4120): recorder.c: __convert_recorder_error_code(189) > [audio-device] ERROR_NONE(0x00000000) : core frameworks error code(0x00000000)
12-07 20:20:43.144+0530 E/TIZEN_N_RECORDER( 4120): recorder.c: __convert_recorder_error_code(189) > [recorder_attr_set_audio_tuning] ERROR_NONE(0x00000000) : core frameworks error code(0x00000000)
12-07 20:20:43.144+0530 W/CAM_APP ( 4120): CamDevControl.cpp: createDevice(124) > [0;35mEND[0;m
12-07 20:20:43.144+0530 W/CAM_APP ( 4120): CamSubControlMgr.cpp: runSubControlThread(90) > [0;35mBEGIN - controlType:[9][0;m
12-07 20:20:43.144+0530 W/CAM_APP ( 4120): CamSubControlMgr.cpp: runSubControlThread(101) > [0;35mcreate CONTROL_CAMERA thread[0;m
12-07 20:20:43.144+0530 W/CAM_APP ( 4120): CamThreadMgr.cpp: createThread(47) > [0;35mBEGIN[0;m
12-07 20:20:43.144+0530 W/CAM_APP ( 4120): CamThreadMgr.cpp: createThread(67) > [0;35mEND[0;m
12-07 20:20:43.144+0530 W/CAM_APP ( 4120): CamSubControlMgr.cpp: __threadCb(131) > [0;35mBEGIN[0;m
12-07 20:20:43.144+0530 W/CAM_APP ( 4120): CamSubControlMgr.cpp: __threadCb(152) > [0;35mstart thread control type - [9][0;m
12-07 20:20:43.144+0530 W/CAM_APP ( 4120): CamSubControlMgr.cpp: runSubControlThread(125) > [0;35mEND[0;m
12-07 20:20:43.144+0530 I/TIZEN_N_CAMERA( 4120): camera_product.c: camera_preload_framework(1142) > start load plugin
12-07 20:20:43.164+0530 W/CAM_APP ( 4120): CamView.cpp: __initialize(67) > [0;35mBEGIN[0;m
12-07 20:20:43.164+0530 W/CAM_APP ( 4120): CamView.cpp: __initialize(85) > [0;35mEND[0;m
12-07 20:20:43.214+0530 W/CAM_APP ( 4120): CamApp.cpp: onAppControl(378) > [0;35mset _E_MOVE_PANEL_SCROLLABLE_STATE as unblocked[0;m
12-07 20:20:43.214+0530 W/CAM_APP ( 4120): CamSystemDeviceMgr.cpp: isIdleLocked(353) > [0;35mVCONFKEY_IDLE_LOCK_STATE state = [0][0;m
12-07 20:20:43.214+0530 W/CAM_APP ( 4120): CamApp.cpp: onAppControl(396) > [0;35mEND[0;m
12-07 20:20:43.214+0530 E/EFL     ( 4120): edje<4120> lib/edje/edje_embryo.c:4134 _edje_embryo_test_run() You are running Embryo->EDC->Embryo with script program '_p26';
12-07 20:20:43.214+0530 E/EFL     ( 4120): By the power of Grayskull, your previous Embryo stack is now broken!
12-07 20:20:43.214+0530 E/EFL     ( 4120): edje<4120> lib/edje/edje_embryo.c:4134 _edje_embryo_test_run() You are running Embryo->EDC->Embryo with script program '_p26';
12-07 20:20:43.214+0530 E/EFL     ( 4120): By the power of Grayskull, your previous Embryo stack is now broken!
12-07 20:20:43.214+0530 E/EFL     ( 4120): edje<4120> lib/edje/edje_embryo.c:4134 _edje_embryo_test_run() You are running Embryo->EDC->Embryo with script program '_p26';
12-07 20:20:43.214+0530 E/EFL     ( 4120): By the power of Grayskull, your previous Embryo stack is now broken!
12-07 20:20:43.214+0530 E/EFL     ( 4120): edje<4120> lib/edje/edje_embryo.c:4134 _edje_embryo_test_run() You are running Embryo->EDC->Embryo with script program '_p26';
12-07 20:20:43.214+0530 E/EFL     ( 4120): By the power of Grayskull, your previous Embryo stack is now broken!
12-07 20:20:43.214+0530 E/EFL     ( 4120): edje<4120> lib/edje/edje_embryo.c:4134 _edje_embryo_test_run() You are running Embryo->EDC->Embryo with script program '_p26';
12-07 20:20:43.214+0530 E/EFL     ( 4120): By the power of Grayskull, your previous Embryo stack is now broken!
12-07 20:20:43.214+0530 E/EFL     ( 4120): edje<4120> lib/edje/edje_embryo.c:4134 _edje_embryo_test_run() You are running Embryo->EDC->Embryo with script program '_p26';
12-07 20:20:43.214+0530 E/EFL     ( 4120): By the power of Grayskull, your previous Embryo stack is now broken!
12-07 20:20:43.214+0530 E/EFL     ( 4120): edje<4120> lib/edje/edje_embryo.c:4134 _edje_embryo_test_run() You are running Embryo->EDC->Embryo with script program '_p26';
12-07 20:20:43.214+0530 E/EFL     ( 4120): By the power of Grayskull, your previous Embryo stack is now broken!
12-07 20:20:43.214+0530 E/EFL     ( 4120): edje<4120> lib/edje/edje_embryo.c:4134 _edje_embryo_test_run() You are running Embryo->EDC->Embryo with script program '_p26';
12-07 20:20:43.214+0530 E/EFL     ( 4120): By the power of Grayskull, your previous Embryo stack is now broken!
12-07 20:20:43.324+0530 I/APP_CORE( 4020): appcore-efl.c: __do_app(514) > [APP 4020] Event: RESET State: CREATED
12-07 20:20:43.324+0530 I/CAPI_APPFW_APPLICATION( 4020): app_main.c: _ui_app_appcore_reset(723) > app_appcore_reset
12-07 20:20:43.334+0530 E/EFL     ( 4020): edje<4020> lib/edje/edje_embryo.c:4134 _edje_embryo_test_run() You are running Embryo->EDC->Embryo with script program '_p26';
12-07 20:20:43.334+0530 E/EFL     ( 4020): By the power of Grayskull, your previous Embryo stack is now broken!
12-07 20:20:43.344+0530 E/EFL     ( 4020): edje<4020> lib/edje/edje_embryo.c:4134 _edje_embryo_test_run() You are running Embryo->EDC->Embryo with script program '_p26';
12-07 20:20:43.344+0530 E/EFL     ( 4020): By the power of Grayskull, your previous Embryo stack is now broken!
12-07 20:20:43.344+0530 E/EFL     ( 4020): edje<4020> lib/edje/edje_embryo.c:4134 _edje_embryo_test_run() You are running Embryo->EDC->Embryo with script program '_p26';
12-07 20:20:43.344+0530 E/EFL     ( 4020): By the power of Grayskull, your previous Embryo stack is now broken!
12-07 20:20:43.364+0530 I/TIZEN_N_CAMERA( 4120): camera_product.c: camera_preload_framework(1152) > done load plugin
12-07 20:20:43.364+0530 W/CAM_APP ( 4120): CamSubControlMgr.cpp: __threadCb(351) > [0;35mdone[0;m
12-07 20:20:43.364+0530 W/APP_CORE( 4020): appcore-efl.c: __show_cb(920) > [EVENT_TEST][EVENT] GET SHOW EVENT!!!. WIN:5400002
12-07 20:20:43.364+0530 W/AUL_AMD (  664): amd_request.c: __request_handler(906) > __request_handler: 35
12-07 20:20:43.364+0530 E/EFL     (  330): eo<330> lib/eo/eo.c:676 _eo_call_resolve() in lib/edje/edje_object.eo.c:316: func 'edje_obj_signal_emit' (415) could not be resolved for class 'Evas_Object_Smart'.
12-07 20:20:43.364+0530 E/EFL     (  330): eo<330> lib/eo/eo.c:676 _eo_call_resolve() in lib/edje/edje_object.eo.c:316: func 'edje_obj_signal_emit' (415) could not be resolved for class 'Evas_Object_Smart'.
12-07 20:20:43.394+0530 I/Tizen::System( 1290): (259) > Active app [org.exampl], current [com.samsun] 
12-07 20:20:43.394+0530 I/Tizen::Io( 1290): (729) > Entry not found
12-07 20:20:43.394+0530 I/Tizen::System( 1290): (157) > change brightness system value.
12-07 20:20:43.414+0530 W/AUL_AMD (  664): amd_request.c: __request_handler(906) > __request_handler: 15
12-07 20:20:43.464+0530 I/APP_CORE( 4020): appcore-efl.c: __do_app(514) > [APP 4020] Event: RESUME State: CREATED
12-07 20:20:43.464+0530 I/APP_CORE( 1690): appcore-efl.c: __do_app(514) > [APP 1690] Event: PAUSE State: RUNNING
12-07 20:20:43.464+0530 I/CAPI_APPFW_APPLICATION( 1690): app_main.c: _ui_app_appcore_pause(689) > app_appcore_pause
12-07 20:20:43.464+0530 W/TASK_MGR_LITE( 1690): task-mgr-lite.c: _pause_app(406) > 
12-07 20:20:43.464+0530 W/AUL_AMD (  664): amd_request.c: __request_handler(906) > __request_handler: 33
12-07 20:20:43.474+0530 W/AUL_AMD (  664): amd_launch.c: __e17_status_handler(3328) > pid(1690) status(4)
12-07 20:20:43.474+0530 W/AUL     (  664): amd_app_group.c: __set_fg_flag(190) > send_signal BG com.samsung.task-mgr(1690)
12-07 20:20:43.474+0530 W/AUL     (  664): app_signal.c: aul_send_app_status_change_signal(581) > send_app_status_change_signal, pid: 1690, appid: com.samsung.task-mgr, status: bg
12-07 20:20:43.474+0530 W/AUL_AMD (  664): amd_request.c: __request_handler(906) > __request_handler: 22
12-07 20:20:43.474+0530 W/AUL_AMD (  664): amd_request.c: __request_handler(1201) > app status : 5
12-07 20:20:43.474+0530 I/CAPI_APPFW_APPLICATION( 1690): app_main.c: _ui_app_appcore_terminate(663) > app_appcore_terminate
12-07 20:20:43.474+0530 E/TASK_MGR_LITE( 1690): recent_app_viewer.c: destroy_recent_app_viewer(1583) > destroy_recent_app_viewer
12-07 20:20:43.484+0530 W/AUL_AMD (  664): amd_launch.c: __e17_status_handler(3328) > pid(4020) status(3)
12-07 20:20:43.484+0530 W/AUL_AMD (  664): amd_key.c: _key_ungrab(269) > fail(-1) to ungrab key(XF86Back)
12-07 20:20:43.484+0530 W/AUL_AMD (  664): amd_launch.c: __e17_status_handler(3349) > back key ungrab error
12-07 20:20:43.484+0530 W/AUL     (  664): amd_app_group.c: __set_fg_flag(180) > send_signal FG org.example.uibuildersingleview(4020)
12-07 20:20:43.484+0530 W/AUL     (  664): app_signal.c: aul_send_app_status_change_signal(581) > send_app_status_change_signal, pid: 4020, appid: org.example.uibuildersingleview, status: fg
12-07 20:20:43.494+0530 I/APP_CORE( 4020): appcore-efl.c: __do_app(623) > Legacy lifecycle: 0
12-07 20:20:43.494+0530 I/APP_CORE( 4020): appcore-efl.c: __do_app(625) > [APP 4020] Initial Launching, call the resume_cb
12-07 20:20:43.494+0530 I/CAPI_APPFW_APPLICATION( 4020): app_main.c: _ui_app_appcore_resume(706) > app_appcore_resume
12-07 20:20:43.494+0530 E/APP_CORE( 1690): appcore.c: appcore_flush_memory(793) > Appcore not initialized
12-07 20:20:43.524+0530 I/MALI    ( 1690): egl_platform_x11.c: __egl_platform_terminate(358) > [EGL-X11] PID=1690   close drm_fd=30 
12-07 20:20:43.824+0530 W/AUL_AMD (  664): amd_launch.c: __e17_status_handler(3328) > pid(4020) status(0)
12-07 20:20:43.944+0530 I/UXT     ( 4144): Uxt_ObjectManager.cpp: OnInitialized(737) > Initialized.
12-07 20:20:43.994+0530 E/TBM     ( 4144): tbm_bufmgr.c: _tbm_bufmgr_init_state(691) > USE TGL LOCK!
12-07 20:20:43.994+0530 I/MALI    ( 4144): egl_platform_x11.c: __egl_platform_initialize(242) > [EGL-X11] PID=4144   open drm_fd=30 
12-07 20:20:44.084+0530 I/AUL_PAD ( 1521): sigchild.h: __launchpad_process_sigchld(162) > dead_pid = 1690 pgid = 1690
12-07 20:20:44.084+0530 I/AUL_PAD ( 1521): sigchild.h: __sigchild_action(143) > dead_pid(1690)
12-07 20:20:44.094+0530 E/RESOURCED(  779): resourced-dbus.c: resourced_dbus_system_hash_drop_busname(324) > Does not exist in busname hash: :1.364
12-07 20:20:44.094+0530 I/Tizen::App( 1290): (499) > LaunchedApp(org.example.uibuildersingleview)
12-07 20:20:44.094+0530 I/Tizen::App( 1290): (733) > Finished invoking application event listener for org.example.uibuildersingleview, 4020.
12-07 20:20:44.125+0530 I/AUL_PAD ( 1521): sigchild.h: __sigchild_action(149) > __send_app_dead_signal(0)
12-07 20:20:44.125+0530 I/AUL_PAD ( 1521): sigchild.h: __launchpad_process_sigchld(170) > after __sigchild_action
12-07 20:20:44.125+0530 E/AUL_PAD ( 1521): launchpad.c: main(698) > error reading sigchld info
12-07 20:20:44.125+0530 I/Tizen::App( 1290): (243) > App[com.samsung.task-mgr] pid[1690] terminate event is forwarded
12-07 20:20:44.125+0530 I/Tizen::System( 1290): (256) > osp.accessorymanager.service provider is found.
12-07 20:20:44.125+0530 I/Tizen::System( 1290): (196) > Accessory Owner is removed [com.samsung.task-mgr, 1690, ]
12-07 20:20:44.125+0530 I/Tizen::System( 1290): (256) > osp.system.service provider is found.
12-07 20:20:44.125+0530 I/Tizen::App( 1290): (506) > TerminatedApp(com.samsung.task-mgr)
12-07 20:20:44.125+0530 I/Tizen::App( 1290): (512) > Not registered pid(1690)
12-07 20:20:44.125+0530 I/Tizen::System( 1290): (246) > Terminated app [com.samsung.task-mgr]
12-07 20:20:44.125+0530 I/Tizen::Io( 1290): (729) > Entry not found
12-07 20:20:44.125+0530 I/ESD     (  941): esd_main.c: __esd_app_dead_handler(1773) > pid: 1690
12-07 20:20:44.135+0530 W/AUL_AMD (  664): amd_main.c: __app_dead_handler(327) > __app_dead_handler, pid: 1690
12-07 20:20:44.135+0530 W/AUL_AMD (  664): amd_main.c: __app_dead_handler(333) > app_group_leader_app, pid: 1690
12-07 20:20:44.135+0530 E/RESOURCED(  779): resourced-dbus.c: resourced_dbus_system_hash_drop_busname(324) > Does not exist in busname hash: :1.365
12-07 20:20:44.145+0530 I/Tizen::System( 1290): (157) > change brightness system value.
12-07 20:20:44.145+0530 I/Tizen::App( 1290): (782) > Finished invoking application event listener for com.samsung.task-mgr, 1690.
12-07 20:20:44.145+0530 E/EFL     ( 4144): edje<4144> lib/edje/edje_embryo.c:4134 _edje_embryo_test_run() You are running Embryo->EDC->Embryo with script program '_p26';
12-07 20:20:44.145+0530 E/EFL     ( 4144): By the power of Grayskull, your previous Embryo stack is now broken!
12-07 20:20:44.145+0530 E/EFL     ( 4144): edje<4144> lib/edje/edje_embryo.c:4134 _edje_embryo_test_run() You are running Embryo->EDC->Embryo with script program '_p26';
12-07 20:20:44.145+0530 E/EFL     ( 4144): By the power of Grayskull, your previous Embryo stack is now broken!
12-07 20:20:44.145+0530 E/EFL     ( 4144): edje<4144> lib/edje/edje_embryo.c:4134 _edje_embryo_test_run() You are running Embryo->EDC->Embryo with script program '_p26';
12-07 20:20:44.145+0530 E/EFL     ( 4144): By the power of Grayskull, your previous Embryo stack is now broken!
12-07 20:20:44.835+0530 I/UXT     ( 4165): Uxt_ObjectManager.cpp: OnInitialized(737) > Initialized.
12-07 20:20:44.865+0530 I/CAPI_APPFW_APPLICATION(  895): app_main.c: _ui_app_appcore_rotation_event(485) > _ui_app_appcore_rotation_event
12-07 20:20:44.865+0530 I/CAPI_APPFW_APPLICATION( 4020): app_main.c: _ui_app_appcore_rotation_event(485) > _ui_app_appcore_rotation_event
12-07 20:20:45.386+0530 E/EFL     ( 4020): ecore_x<4020> lib/ecore_x/xlib/ecore_x_events.c:603 _ecore_x_event_handle_button_press() ButtonEvent:press time=8597870
12-07 20:20:45.516+0530 E/EFL     ( 4020): ecore_x<4020> lib/ecore_x/xlib/ecore_x_events.c:756 _ecore_x_event_handle_button_release() ButtonEvent:release time=8598002
12-07 20:20:45.516+0530 E/VCONF   ( 4020): vconf.c: _vconf_check_retry_err(1368) > db/ise/keysound : check retry err (-21/13).
12-07 20:20:45.516+0530 E/VCONF   ( 4020): vconf.c: _vconf_get_key_filesys(2371) > _vconf_get_key_filesys(db/ise/keysound) step(-21) failed(13 / Permission denied) retry(0) 
12-07 20:20:45.516+0530 E/VCONF   ( 4020): vconf.c: _vconf_get_key(2407) > _vconf_get_key(db/ise/keysound) step(-21) failed(13 / Permission denied)
12-07 20:20:45.516+0530 E/VCONF   ( 4020): vconf.c: vconf_get_bool(2729) > vconf_get_bool(4020) : db/ise/keysound error
12-07 20:20:45.526+0530 E/VCONF   ( 4020): vconf-kdb.c: _vconf_kdb_add_notify(318) > _vconf_kdb_add_notify : add noti(Permission denied)
12-07 20:20:45.526+0530 E/VCONF   ( 4020): vconf.c: vconf_notify_key_changed(3168) > vconf_notify_key_changed : key(db/ise/keysound) add notify fail
12-07 20:20:45.826+0530 W/AUL_AMD (  664): amd_status.c: __app_terminate_timer_cb(421) > send SIGKILL: No such process
12-07 20:20:45.826+0530 W/AUL_AMD (  664): amd_status.c: __app_terminate_timer_cb(421) > send SIGKILL: No such process
12-07 20:20:46.417+0530 W/AUL_AMD (  664): amd_status.c: __app_terminate_timer_cb(421) > send SIGKILL: No such process
12-07 20:20:46.427+0530 W/AUL_AMD (  664): amd_status.c: __app_terminate_timer_cb(421) > send SIGKILL: No such process
12-07 20:20:46.517+0530 E/EFL     ( 4020): ecore_x<4020> lib/ecore_x/xlib/ecore_x_events.c:603 _ecore_x_event_handle_button_press() ButtonEvent:press time=8599000
12-07 20:20:46.627+0530 E/EFL     ( 4020): ecore_x<4020> lib/ecore_x/xlib/ecore_x_events.c:756 _ecore_x_event_handle_button_release() ButtonEvent:release time=8599110
12-07 20:20:46.947+0530 W/AUL     (  664): amd_app_group.c: __set_fg_flag(190) > send_signal BG com.samsung.camera-app-lite(4120)
12-07 20:20:46.947+0530 W/AUL     (  664): app_signal.c: aul_send_app_status_change_signal(581) > send_app_status_change_signal, pid: 4120, appid: com.samsung.camera-app-lite, status: bg
12-07 20:20:47.017+0530 E/EFL     ( 4020): ecore_x<4020> lib/ecore_x/xlib/ecore_x_events.c:603 _ecore_x_event_handle_button_press() ButtonEvent:press time=8599489
12-07 20:20:47.127+0530 E/EFL     ( 4020): ecore_x<4020> lib/ecore_x/xlib/ecore_x_events.c:756 _ecore_x_event_handle_button_release() ButtonEvent:release time=8599609
12-07 20:20:48.038+0530 E/EFL     ( 4020): ecore_x<4020> lib/ecore_x/xlib/ecore_x_events.c:603 _ecore_x_event_handle_button_press() ButtonEvent:press time=8600517
12-07 20:20:48.158+0530 E/EFL     ( 4020): ecore_x<4020> lib/ecore_x/xlib/ecore_x_events.c:756 _ecore_x_event_handle_button_release() ButtonEvent:release time=8600637
12-07 20:20:48.289+0530 W/CRASH_MANAGER( 4026): worker.c: worker_job(1199) > 1104020756962148112224
