S/W Version Information
Model: SM-Z300H
Tizen-Version: 2.4.0.3
Build-Number: Z300HDDU0BPI2
Build-Date: 2016.09.30 15:37:12

Crash Information
Process Name: uibuildersingleview
PID: 4165
Date: 2016-12-07 20:24:52+0530
Executable File Path: /opt/usr/apps/org.example.uibuildersingleview/bin/uibuildersingleview
Signal: 11
      (SIGSEGV)
      si_code: -6
      signal sent by tkill (sent by pid 4165, uid 5000)

Register Information
r0   = 0xb723d6a8, r1   = 0x0000000c
r2   = 0x0000003c, r3   = 0x0000000c
r4   = 0xb71a51c0, r5   = 0xb7215ea0
r6   = 0x80012493, r7   = 0xb6da6488
r8   = 0xb7217e80, r9   = 0xb723d6a8
r10  = 0x80012493, fp   = 0xb720d1b8
ip   = 0xb69b84d4, sp   = 0xbea89f98
lr   = 0xb698f60d, pc   = 0xb66cb6f2
cpsr = 0x200e0030

Memory Information
MemTotal:   987012 KB
MemFree:    179516 KB
Buffers:     43192 KB
Cached:     317756 KB
VmPeak:     127556 KB
VmSize:     127552 KB
VmLck:           0 KB
VmPin:           0 KB
VmHWM:       25832 KB
VmRSS:       25832 KB
VmData:      44880 KB
VmStk:         136 KB
VmExe:          20 KB
VmLib:       35600 KB
VmPTE:          94 KB
VmSwap:          0 KB

Threads Information
Threads: 5
PID = 4165 TID = 4165
4165 4168 4742 4743 4751 

Maps Information
afd2e000 b052d000 rwxp [stack:4751]
b14af000 b14b7000 r-xp /usr/lib/ecore_evas/engines/extn/v-1.13/module.so
b14c9000 b1cc8000 rwxp [stack:4743]
b1cc8000 b1cc9000 r-xp /usr/lib/edje/modules/feedback/v-1.13/module.so
b1cd9000 b1ced000 r-xp /usr/lib/edje/modules/elm/v-1.13/module.so
b1d01000 b1d02000 r-xp /usr/lib/libX11-xcb.so.1.0.0
b1d12000 b1d15000 r-xp /usr/lib/libxcb-sync.so.1.0.0
b1d26000 b1d27000 r-xp /usr/lib/libxshmfence.so.1.0.0
b1d37000 b1d39000 r-xp /usr/lib/libxcb-present.so.0.0.0
b1d49000 b1d4b000 r-xp /usr/lib/libxcb-dri3.so.0.0.0
b1d5b000 b1d6b000 r-xp /usr/lib/evas/modules/engines/software_x11/v-1.13/module.so
b1d7b000 b1d87000 r-xp /usr/lib/ecore_evas/engines/x/v-1.13/module.so
b1d99000 b2598000 rwxp [stack:4742]
b2798000 b279f000 r-xp /usr/lib/libefl-extension.so.0.1.0
b27b2000 b27b8000 r-xp /usr/lib/bufmgr/libtbm_sprd7727.so.0.0.0
b27c8000 b27cd000 r-xp /opt/usr/apps/org.example.uibuildersingleview/bin/uibuildersingleview
b291e000 b2a01000 r-xp /usr/lib/libscim-1.0.so.8.2.3
b2a38000 b2a60000 r-xp /usr/lib/ecore_imf/modules/isf/v-1.13/module.so
b2a72000 b3271000 rwxp [stack:4168]
b3271000 b3273000 r-xp /usr/lib/ecore/system/systemd/v-1.13/module.so
b3283000 b328d000 r-xp /lib/libnss_files-2.20-2014.11.so
b329e000 b32a7000 r-xp /lib/libnss_nis-2.20-2014.11.so
b32b8000 b32c9000 r-xp /lib/libnsl-2.20-2014.11.so
b32dc000 b32e2000 r-xp /lib/libnss_compat-2.20-2014.11.so
b32f3000 b32f4000 r-xp /usr/lib/osp/libappinfo.so.1.2.2.1
b331c000 b3323000 r-xp /usr/lib/libminizip.so.1.0.0
b3333000 b3338000 r-xp /usr/lib/libstorage.so.0.1
b3348000 b33a7000 r-xp /usr/lib/libmmfcamcorder.so.0.0.0
b33bd000 b33d1000 r-xp /usr/lib/libcapi-media-camera.so.0.1.90
b33e1000 b3425000 r-xp /usr/lib/libgstbase-1.0.so.0.405.0
b3435000 b343d000 r-xp /usr/lib/libgstapp-1.0.so.0.405.0
b344d000 b347d000 r-xp /usr/lib/libgstvideo-1.0.so.0.405.0
b3490000 b3549000 r-xp /usr/lib/libgstreamer-1.0.so.0.405.0
b355d000 b35b0000 r-xp /usr/lib/libmmfplayer.so.0.0.0
b35c1000 b35dc000 r-xp /usr/lib/libcapi-media-player.so.0.2.16
b35ec000 b36ad000 r-xp /usr/lib/libprotobuf.so.9.0.1
b36c0000 b36d0000 r-xp /usr/lib/libefl-assist.so.0.1.0
b36e0000 b36ed000 r-xp /usr/lib/libmdm-common.so.1.0.98
b36fe000 b3705000 r-xp /usr/lib/libcapi-media-tool.so.0.2.2
b3715000 b3756000 r-xp /usr/lib/libmdm.so.1.2.12
b3766000 b376e000 r-xp /usr/lib/lib_DNSe_NRSS_ver225.so
b377d000 b378d000 r-xp /usr/lib/lib_SamsungRec_TizenV04014.so
b37ae000 b380e000 r-xp /usr/lib/libasound.so.2.0.0
b3820000 b3823000 r-xp /usr/lib/libpulse-simple.so.0.0.4
b3833000 b3836000 r-xp /usr/lib/libsyspopup_caller.so.0.1.0
b3846000 b384b000 r-xp /usr/lib/libmmfsoundcommon.so.0.0.0
b385b000 b385c000 r-xp /usr/lib/libgthread-2.0.so.0.4301.0
b386c000 b3877000 r-xp /usr/lib/libaudio-session-mgr.so.0.0.1
b388b000 b3892000 r-xp /usr/lib/libmmutil_imgp.so.0.0.0
b38a2000 b38a8000 r-xp /usr/lib/libmmutil_jpeg.so.0.0.0
b38b8000 b38bd000 r-xp /usr/lib/libmmfsession.so.0.0.1
b38cd000 b38e8000 r-xp /usr/lib/libmmfsound.so.0.1.0
b38f8000 b38ff000 r-xp /usr/lib/libmmfcommon.so.0.0.0
b390f000 b3912000 r-xp /usr/lib/libcapi-media-image-util.so.0.1.10
b3923000 b3951000 r-xp /usr/lib/libidn.so.11.5.44
b3961000 b3977000 r-xp /usr/lib/libnghttp2.so.5.4.0
b3988000 b3992000 r-xp /usr/lib/libcares.so.2.1.0
b39a2000 b39ac000 r-xp /usr/lib/libcapi-media-sound-manager.so.0.3.32
b39bc000 b39be000 r-xp /usr/lib/libcapi-media-wav-player.so.0.1.18
b39ce000 b39cf000 r-xp /usr/lib/libmmfkeysound.so.0.0.0
b39df000 b39e3000 r-xp /usr/lib/libecore_ipc.so.1.13.0
b39f4000 b3a1c000 r-xp /usr/lib/libui-extension.so.0.1.0
b3a2d000 b3a56000 r-xp /usr/lib/libturbojpeg.so
b3a76000 b3a7c000 r-xp /usr/lib/libgif.so.4.1.6
b3a8c000 b3ad2000 r-xp /usr/lib/libcurl.so.4.3.0
b3ae3000 b3b04000 r-xp /usr/lib/libexif.so.12.3.3
b3b1f000 b3b34000 r-xp /usr/lib/libtts.so
b3b45000 b3b4d000 r-xp /usr/lib/libfeedback.so.0.1.4
b3b5d000 b3c23000 r-xp /usr/lib/libdali-core.so.0.0.0
b3c43000 b3d3b000 r-xp /usr/lib/libdali-adaptor.so.0.0.0
b3d5a000 b3e28000 r-xp /usr/lib/libdali-toolkit.so.0.0.0
b3e3f000 b3e41000 r-xp /usr/lib/libboost_system.so.1.51.0
b3e51000 b3e57000 r-xp /usr/lib/libboost_chrono.so.1.51.0
b3e67000 b3e8a000 r-xp /usr/lib/libboost_thread.so.1.51.0
b3e9b000 b3e9d000 r-xp /usr/lib/libappsvc.so.0.1.0
b3ead000 b3eaf000 r-xp /usr/lib/libcapi-appfw-app-common.so.0.3.1.0
b3ec0000 b3ec5000 r-xp /usr/lib/libcapi-appfw-app-control.so.0.3.1.0
b3edc000 b3ede000 r-xp /usr/lib/libosp-env-config.so.1.2.2.1
b3eee000 b3ef5000 r-xp /usr/lib/libsensord-share.so
b3f05000 b3f1d000 r-xp /usr/lib/libsensor.so.1.1.0
b3f2e000 b3f31000 r-xp /usr/lib/libXv.so.1.0.0
b3f41000 b3f46000 r-xp /usr/lib/libutilX.so.1.1.0
b3f56000 b3f5b000 r-xp /usr/lib/libappcore-common.so.1.1
b3f6b000 b3f72000 r-xp /usr/lib/libcapi-ui-efl-util.so.0.2.11
b3f85000 b3f89000 r-xp /usr/lib/libcapi-appfw-application.so.0.3.1.0
b3f9a000 b4078000 r-xp /usr/lib/libCOREGL.so.4.0
b4098000 b409b000 r-xp /usr/lib/libuuid.so.1.3.0
b40ab000 b40c2000 r-xp /usr/lib/libblkid.so.1.1.0
b40d3000 b40d5000 r-xp /usr/lib/libXau.so.6.0.0
b40e5000 b412c000 r-xp /usr/lib/pulseaudio/libpulsecommon-4.0.so
b413e000 b4144000 r-xp /usr/lib/libjson-c.so.2.0.1
b4155000 b4159000 r-xp /usr/lib/libogg.so.0.7.1
b4169000 b418b000 r-xp /usr/lib/libvorbis.so.0.4.3
b419b000 b427f000 r-xp /usr/lib/libvorbisenc.so.2.0.6
b429b000 b429e000 r-xp /usr/lib/libEGL.so.1.4
b42af000 b42b5000 r-xp /usr/lib/libxcb-render.so.0.0.0
b42c5000 b42c7000 r-xp /usr/lib/libxcb-shm.so.0.0.0
b42d7000 b42e4000 r-xp /usr/lib/libGLESv2.so.2.0
b42f5000 b4357000 r-xp /usr/lib/libpixman-1.so.0.28.2
b436c000 b4384000 r-xp /usr/lib/libmount.so.1.1.0
b4396000 b43aa000 r-xp /usr/lib/libxcb.so.1.1.0
b43ba000 b43c1000 r-xp /lib/libcrypt-2.20-2014.11.so
b43f9000 b43fb000 r-xp /usr/lib/libiri.so
b440b000 b4416000 r-xp /usr/lib/libgpg-error.so.0.15.0
b4427000 b445d000 r-xp /usr/lib/libpulse.so.0.16.2
b446e000 b44b1000 r-xp /usr/lib/libsndfile.so.1.0.25
b44c6000 b44db000 r-xp /lib/libexpat.so.1.5.2
b44ed000 b45ab000 r-xp /usr/lib/libcairo.so.2.11200.14
b45bf000 b45c7000 r-xp /usr/lib/libdrm.so.2.4.0
b45d7000 b45da000 r-xp /usr/lib/libdri2.so.0.0.0
b45ea000 b4638000 r-xp /usr/lib/libssl.so.1.0.0
b464d000 b4659000 r-xp /usr/lib/libeeze.so.1.13.0
b466a000 b4673000 r-xp /usr/lib/libethumb.so.1.13.0
b4683000 b4686000 r-xp /usr/lib/libecore_input_evas.so.1.13.0
b4696000 b484d000 r-xp /usr/lib/libcrypto.so.1.0.0
b5638000 b5641000 r-xp /usr/lib/libXi.so.6.1.0
b5651000 b5653000 r-xp /usr/lib/libXgesture.so.7.0.0
b5663000 b5667000 r-xp /usr/lib/libXtst.so.6.1.0
b5677000 b567d000 r-xp /usr/lib/libXrender.so.1.3.0
b568d000 b5693000 r-xp /usr/lib/libXrandr.so.2.2.0
b56a3000 b56a5000 r-xp /usr/lib/libXinerama.so.1.0.0
b56b6000 b56b9000 r-xp /usr/lib/libXfixes.so.3.1.0
b56c9000 b56d4000 r-xp /usr/lib/libXext.so.6.4.0
b56e4000 b56e6000 r-xp /usr/lib/libXdamage.so.1.1.0
b56f6000 b56f8000 r-xp /usr/lib/libXcomposite.so.1.0.0
b5708000 b57ea000 r-xp /usr/lib/libX11.so.6.3.0
b57fe000 b5805000 r-xp /usr/lib/libXcursor.so.1.0.2
b5815000 b582d000 r-xp /usr/lib/libudev.so.1.6.0
b582f000 b5832000 r-xp /lib/libattr.so.1.1.0
b5842000 b5862000 r-xp /usr/lib/libsecurity-server-commons.so.1.0.0
b5863000 b5868000 r-xp /usr/lib/libffi.so.6.0.2
b5879000 b5891000 r-xp /lib/libz.so.1.2.8
b58a1000 b58a3000 r-xp /usr/lib/libgmodule-2.0.so.0.4301.0
b58b3000 b5988000 r-xp /usr/lib/libxml2.so.2.9.2
b599d000 b5a38000 r-xp /usr/lib/libstdc++.so.6.0.20
b5a54000 b5a57000 r-xp /usr/lib/libSLP-db-util.so.0.1.0
b5a67000 b5a80000 r-xp /usr/lib/libpkgmgr_parser.so.0.1.0
b5a91000 b5aa2000 r-xp /lib/libresolv-2.20-2014.11.so
b5ab6000 b5b30000 r-xp /usr/lib/libgcrypt.so.20.0.3
b5b45000 b5b47000 r-xp /usr/lib/libecore_imf_evas.so.1.13.0
b5b57000 b5b5e000 r-xp /usr/lib/libembryo.so.1.13.0
b5b6e000 b5b78000 r-xp /usr/lib/libecore_audio.so.1.13.0
b5b89000 b5ba1000 r-xp /usr/lib/libpng12.so.0.50.0
b5bb2000 b5bd5000 r-xp /usr/lib/libjpeg.so.8.0.2
b5bf6000 b5c0a000 r-xp /usr/lib/libector.so.1.13.0
b5c1b000 b5c33000 r-xp /usr/lib/liblua-5.1.so
b5c44000 b5c9b000 r-xp /usr/lib/libfreetype.so.6.11.3
b5caf000 b5cd7000 r-xp /usr/lib/libfontconfig.so.1.8.0
b5ce8000 b5cfb000 r-xp /usr/lib/libfribidi.so.0.3.1
b5d0c000 b5d46000 r-xp /usr/lib/libharfbuzz.so.0.940.0
b5d57000 b5d65000 r-xp /usr/lib/libgraphics-extension.so.0.1.0
b5d75000 b5d7d000 r-xp /usr/lib/libtbm.so.1.0.0
b5d8d000 b5d9a000 r-xp /usr/lib/libeio.so.1.13.0
b5daa000 b5dac000 r-xp /usr/lib/libefreet_trash.so.1.13.0
b5dbc000 b5dc1000 r-xp /usr/lib/libefreet_mime.so.1.13.0
b5dd1000 b5de8000 r-xp /usr/lib/libefreet.so.1.13.0
b5dfa000 b5e1a000 r-xp /usr/lib/libeldbus.so.1.13.0
b5e2a000 b5e4a000 r-xp /usr/lib/libecore_con.so.1.13.0
b5e4c000 b5e52000 r-xp /usr/lib/libecore_imf.so.1.13.0
b5e62000 b5e73000 r-xp /usr/lib/libemotion.so.1.13.0
b5e84000 b5e8b000 r-xp /usr/lib/libethumb_client.so.1.13.0
b5e9b000 b5eaa000 r-xp /usr/lib/libeo.so.1.13.0
b5ebb000 b5ecd000 r-xp /usr/lib/libecore_input.so.1.13.0
b5ede000 b5ee3000 r-xp /usr/lib/libecore_file.so.1.13.0
b5ef3000 b5f0c000 r-xp /usr/lib/libecore_evas.so.1.13.0
b5f1c000 b5f39000 r-xp /usr/lib/libeet.so.1.13.0
b5f52000 b5f9a000 r-xp /usr/lib/libeina.so.1.13.0
b5fab000 b5fbb000 r-xp /usr/lib/libefl.so.1.13.0
b5fcc000 b60b1000 r-xp /usr/lib/libicuuc.so.51.1
b60ce000 b620e000 r-xp /usr/lib/libicui18n.so.51.1
b6225000 b625d000 r-xp /usr/lib/libecore_x.so.1.13.0
b626f000 b6272000 r-xp /lib/libcap.so.2.21
b6282000 b62ab000 r-xp /usr/lib/libsecurity-server-client.so.1.0.1
b62bc000 b62c3000 r-xp /usr/lib/libcapi-base-common.so.0.2.2
b62d5000 b630c000 r-xp /usr/lib/libgobject-2.0.so.0.4301.0
b631d000 b6408000 r-xp /usr/lib/libgio-2.0.so.0.4301.0
b641b000 b6494000 r-xp /usr/lib/libsqlite3.so.0.8.6
b64a6000 b64ab000 r-xp /usr/lib/libcapi-system-info.so.0.2.1
b64bb000 b64c5000 r-xp /usr/lib/libvconf.so.0.2.45
b64d5000 b64d7000 r-xp /usr/lib/libvasum.so.0.3.1
b64e7000 b64e9000 r-xp /usr/lib/libttrace.so.1.1
b64f9000 b64fc000 r-xp /usr/lib/libiniparser.so.0
b650c000 b6532000 r-xp /usr/lib/libpkgmgr-info.so.0.0.17
b6542000 b6547000 r-xp /usr/lib/libxdgmime.so.1.1.0
b6558000 b656f000 r-xp /usr/lib/libdbus-glib-1.so.2.2.2
b6580000 b65eb000 r-xp /lib/libm-2.20-2014.11.so
b65fc000 b6602000 r-xp /lib/librt-2.20-2014.11.so
b6613000 b6620000 r-xp /usr/lib/libunwind.so.8.0.1
b6656000 b677a000 r-xp /lib/libc-2.20-2014.11.so
b678f000 b67a8000 r-xp /lib/libgcc_s-4.9.so.1
b67b8000 b689a000 r-xp /usr/lib/libglib-2.0.so.0.4301.0
b68ab000 b68d5000 r-xp /usr/lib/libdbus-1.so.3.8.12
b68e6000 b6922000 r-xp /usr/lib/libsystemd.so.0.4.0
b6924000 b69a7000 r-xp /usr/lib/libedje.so.1.13.0
b69ba000 b69d8000 r-xp /usr/lib/libecore.so.1.13.0
b69f8000 b6b7f000 r-xp /usr/lib/libevas.so.1.13.0
b6bb4000 b6bc8000 r-xp /lib/libpthread-2.20-2014.11.so
b6bdc000 b6e10000 r-xp /usr/lib/libelementary.so.1.13.0
b6e3f000 b6e43000 r-xp /usr/lib/libsmack.so.1.0.0
b6e53000 b6e5a000 r-xp /usr/lib/libprivilege-control.so.0.0.2
b6e6a000 b6e6c000 r-xp /usr/lib/libdlog.so.0.0.0
b6e7c000 b6e7f000 r-xp /usr/lib/libbundle.so.0.1.22
b6e8f000 b6e91000 r-xp /lib/libdl-2.20-2014.11.so
b6ea2000 b6eba000 r-xp /usr/lib/libaul.so.0.1.0
b6ece000 b6ed3000 r-xp /usr/lib/libappcore-efl.so.1.1
b6ee4000 b6ef1000 r-xp /usr/lib/liblptcp.so
b6f03000 b6f07000 r-xp /usr/lib/libsys-assert.so
b6f18000 b6f38000 r-xp /lib/ld-2.20-2014.11.so
b6f49000 b6f4e000 r-xp /usr/bin/launchpad-loader
b6fc2000 b72ea000 rw-p [heap]
bea6a000 bea8b000 rwxp [stack]
bea6a000 bea8b000 rwxp [stack]
End of Maps Information

Callstack Information (PID:4165)
Call Stack Count: 1
 0: strcmp + 0x1 (0xb66cb6f2) [/lib/libc.so.6] + 0x756f2
End of Call Stack

Package Information
Package Name: org.example.uibuildersingleview
Package ID : org.example.uibuildersingleview
Version: 1.0.0
Package Type: tpk
App Name: uibuildersingleview
App ID: org.example.uibuildersingleview
Type: capp
Categories: 

Latest Debug Message Information
--------- beginning of /dev/log_main
ll = [96]
12-07 20:21:27.517+0530 E/PKGMGR_SERVER( 4398): pkgmgr-server.c: exit_server(1381) > exit_server Start [backend_status=0, queue_status=1, drm_status=1] 
12-07 20:21:28.288+0530 I/Tizen::App( 1290): (1894) > PackageEventHandler - req: 12900002, pkg_type: tpk, pkg_name: org.example.uibuildersingleview, key: end, val: ok
12-07 20:21:28.288+0530 I/Tizen::App( 1290): (78) > Installation is Completed. [Package = org.example.uibuildersingleview]
12-07 20:21:28.288+0530 I/Tizen::App( 1290): (671) > Enter. package(org.example.uibuildersingleview), installationResult(0)
12-07 20:21:28.288+0530 E/cluster-home(  895): mainmenu-package-manager.cpp: OnClientListenCb(571) >  #Step 1
12-07 20:21:28.288+0530 E/cluster-home(  895): mainmenu-package-manager.cpp: OnClientListenCb(575) >  #Step 2
12-07 20:21:28.288+0530 E/cluster-home(  895): mainmenu-package-manager.cpp: _GetAppIds(350) >  BEGIN
12-07 20:21:28.318+0530 W/ISF_PANEL_EFL(  823): isf_panel_efl.cpp: _package_manager_event_cb(1513) > _isf_insert_ime_info_by_pkgid returned 0.
12-07 20:21:28.318+0530 I/Tizen::App( 1290): (1360) > package(org.example.uibuildersingleview), version(1.0.0), type(tpk), displayName(uibuildersingleview), uninstallable(1), downloaded(1), updated(0), preloaded(0)movable(1), externalStorage(0), mainApp(org.example.uibuildersingleview), storeClient(), appRootPath(/opt/usr/apps/org.example.uibuildersingleview)
12-07 20:21:28.328+0530 I/Tizen::App( 1290): (483) > pkgmgrinfo_appinfo_get_appid(): app = [org.example.uibuildersingleview]
12-07 20:21:28.338+0530 E/PKGMGR  ( 1495): pkgmgr.c: __find_op_cbinfo(312) > tmp is NULL
12-07 20:21:28.338+0530 E/PKGMGR  ( 1495): pkgmgr.c: __find_op_cbinfo(312) > tmp is NULL
12-07 20:21:28.338+0530 E/cluster-home(  895): mainmenu-package-manager.cpp: _PkgMgrAppInfoGetListCb(253) >  ##### [org.example.uibuildersingleview]
12-07 20:21:28.338+0530 E/cluster-home(  895): mainmenu-package-manager.cpp: _GetAppIds(375) >  ##### [org.example.uibuildersingleview]
12-07 20:21:28.338+0530 E/cluster-home(  895): mainmenu-package-manager.cpp: _GetAppIds(379) >  END
12-07 20:21:28.338+0530 E/cluster-home(  895): mainmenu-package-manager.cpp: _DoPkgJob(458) >  #Step 3 size[1]
12-07 20:21:28.338+0530 E/cluster-home(  895): mainmenu-package-manager.cpp: _DoPkgJob(462) >  appId[org.example.uibuildersingleview]
12-07 20:21:28.348+0530 I/Tizen::App( 1290): (416) > appName = [uibuildersingleview]
12-07 20:21:28.348+0530 E/cluster-home(  895): mainmenu-package-manager.cpp: _GetAppInfo(990) >  Name[uibuildersingleview] enable[1] system[0]
12-07 20:21:28.348+0530 I/Tizen::App( 1290): (509) > exe = [/opt/usr/apps/org.example.uibuildersingleview/bin/uibuildersingleview], displayName = [uibuildersingleview], mainApp = [1], menuIconVisible = [0], serviceApp = [0]
12-07 20:21:28.348+0530 E/HOME_APPS(  895): mainmenu-package-manager.cpp: _DoPkgJob(484) >  appId[org.example.uibuildersingleview] mdm is not enabled
12-07 20:21:28.348+0530 E/PKGMGR_INFO( 1290): pkgmgrinfo_appinfo.c: pkgmgrinfo_appinfo_get_list(887) > (component == PMINFO_SVC_APP) PMINFO_SVC_APP is done
12-07 20:21:28.348+0530 I/Tizen::App( 1290): (683) > Application count(1) in this package
12-07 20:21:28.348+0530 I/Tizen::App( 1290): (840) > Enter.
12-07 20:21:28.348+0530 I/Tizen::App( 1290): (703) > Exit.
12-07 20:21:28.348+0530 I/Tizen::App( 1290): (1584) > Package = [org.example.uibuildersingleview], Key = [end], Value = [ok], install = [96]
12-07 20:21:28.358+0530 E/cluster-home(  895): mainmenu-package-manager.cpp: GetAppInfo(630) >  Find a App Info Name[uibuildersingleview] enable[1] system[0]
12-07 20:21:28.358+0530 W/HOME_APPS(  895): mainmenu-box-impl.cpp: UpdateBoxData(1326) >  update box data!!!!! old icon path[/opt/usr/apps/org.example.uibuildersingleview/shared/res/uibuildersingleview.png], New icon path[/opt/usr/apps/org.example.uibuildersingleview/shared/res/uibuildersingleview.png]!!!!!
12-07 20:21:28.358+0530 I/Tizen::App( 1290): (416) > appName = [uibuildersingleview]
12-07 20:21:28.368+0530 I/Tizen::App( 1290): (509) > exe = [/opt/usr/apps/org.example.uibuildersingleview/bin/uibuildersingleview], displayName = [uibuildersingleview], mainApp = [1], menuIconVisible = [0], serviceApp = [0]
12-07 20:21:28.368+0530 I/Tizen::App( 1290): (2343) > info file is not existed. [/opt/usr/apps/org.exampl/info/org.example.uibuildersingleview.info]
12-07 20:21:28.368+0530 I/Tizen::App( 1290): (131) > Enter
12-07 20:21:28.368+0530 I/Tizen::App( 1290): (137) > org.example.uibuildersingleview does not have launch condition
12-07 20:21:28.368+0530 I/Tizen::App( 1290): (883) > Exit.
12-07 20:21:28.368+0530 E/util-view(  895): util-text-image-provider.cpp: GetPixelsFromText(240) >  DEFAULT='font=SamsungOneUI:style=Regular style=shadow,bottom shadow_color=# font_size=28 align=center valign=top color=#FFFFFF color_class=ATO001 text_class=ATO001 ellipsis=1 wrap=mixed linegap=-3'
12-07 20:21:28.398+0530 E/PKGMGR_SERVER( 4398): pkgmgr-server.c: __unset_recovery_mode(282) > remove recovery_file[/opt/share/packages/.recovery/pkgmgr/org.example.uibuildersingleview-1.0.0-arm.tpk] fail
12-07 20:21:28.398+0530 E/PKGMGR_SERVER( 4398): pkgmgr-server.c: sighandler(417) > child NORMAL exit [4401]
12-07 20:21:28.398+0530 W/HOME_APPS(  895): mainmenu-box-impl.cpp: _OnImageLoadFinishedHandler(2152) >  finished path[/opt/usr/apps/org.example.uibuildersingleview/shared/res/uibuildersingleview.png], Loading state:[1]
12-07 20:21:29.519+0530 E/PKGMGR_SERVER( 4398): pkgmgr-server.c: exit_server(1381) > exit_server Start [backend_status=1, queue_status=1, drm_status=1] 
12-07 20:21:29.519+0530 E/PKGMGR_SERVER( 4398): pkgmgr-server.c: main(2471) > package manager server terminated.
12-07 20:21:34.233+0530 W/AUL     ( 4460): launch.c: app_request_to_launchpad(396) > request cmd(0) to(org.example.uibuildersingleview)
12-07 20:21:34.233+0530 W/AUL_AMD (  664): amd_request.c: __request_handler(906) > __request_handler: 0
12-07 20:21:34.243+0530 I/AUL     (  664): menu_db_util.h: _get_app_info_from_db_by_apppath(242) > path : /usr/bin/launch_app, ret : 0
12-07 20:21:34.243+0530 I/AUL     (  664): menu_db_util.h: _get_app_info_from_db_by_apppath(242) > path : /bin/bash, ret : 0
12-07 20:21:34.243+0530 E/AUL_AMD (  664): amd_launch.c: _start_app(2499) > no caller appid info, ret: -1
12-07 20:21:34.243+0530 W/AUL_AMD (  664): amd_launch.c: _start_app(2508) > caller pid : 4460
12-07 20:21:34.243+0530 E/AUL_AMD (  664): amd_appinfo.c: appinfo_get_value(1296) > appinfo get value: Invalid argument, 19
12-07 20:21:34.253+0530 E/RESOURCED(  779): block.c: block_prelaunch_state(134) > insert data org.example.uibuildersingleview, table num : 4
12-07 20:21:34.253+0530 E/RESOURCED(  779): heart-memory.c: heart_memory_get_data(601) > hashtable heart_memory_app_list is NULL
12-07 20:21:34.253+0530 W/AUL_AMD (  664): amd_launch.c: _start_app(3052) > pad pid(-4)
12-07 20:21:34.394+0530 W/AUL     (  664): app_signal.c: aul_send_app_launch_request_signal(423) > send_app_launch_signal, pid: 4462, appid: org.example.uibuildersingleview
12-07 20:21:34.394+0530 E/AUL     (  664): amd_app_group.c: app_group_start_app(1037) > app_group_start_app
12-07 20:21:34.404+0530 E/RESOURCED(  779): proc-main.c: proc_add_program_list(237) > not found ppi : org.example.uibuildersingleview
12-07 20:21:34.414+0530 W/AUL     ( 4460): launch.c: app_request_to_launchpad(425) > request cmd(0) result(4462)
12-07 20:21:35.405+0530 W/AUL_AMD (  664): amd_key.c: _key_ungrab(269) > fail(-1) to ungrab key(XF86Back)
12-07 20:21:35.405+0530 W/AUL_AMD (  664): amd_launch.c: __grab_timeout_handler(1623) > back key ungrab error
12-07 20:21:37.517+0530 E/RESOURCED(  779): file-helper.c: fread_uint(105) > Fail to open /sys/fs/cgroup/net_cls/com.samsung.camera-app.service/net_cls.classid file.
12-07 20:21:37.517+0530 E/RESOURCED(  779): file-helper.c: fread_uint(105) > errno -2, errmsg No such file or directory
12-07 20:21:37.517+0530 E/RESOURCED(  779): net-cls-cgroup.c: get_classid_from_cgroup(105) > Cant read classid from cgroup /sys/fs/cgroup/net_cls/com.samsung.camera-app.service
12-07 20:21:37.517+0530 E/RESOURCED(  779): net-cls-cgroup.c: get_classid_from_cgroup(105) > errno -2, errmsg No such file or directory
12-07 20:21:37.527+0530 E/RESOURCED(  779): datausage-common.c: turn_on_counters(1734) > Can't get iftype for remove counter
12-07 20:21:39.408+0530 W/AUL     (  664): amd_app_group.c: __set_fg_flag(190) > send_signal BG org.example.uibuildersingleview(4462)
12-07 20:21:39.408+0530 W/AUL     (  664): app_signal.c: aul_send_app_status_change_signal(581) > send_app_status_change_signal, pid: 4462, appid: org.example.uibuildersingleview, status: bg
12-07 20:21:41.931+0530 E/EFL     (  895): ecore_x<895> lib/ecore_x/xlib/ecore_x_events.c:603 _ecore_x_event_handle_button_press() ButtonEvent:press time=8654408
12-07 20:21:41.971+0530 E/EFL     (  895): ecore_x<895> lib/ecore_x/xlib/ecore_x_events.c:756 _ecore_x_event_handle_button_release() ButtonEvent:release time=8654452
12-07 20:21:42.231+0530 W/cluster-view(  895): custom-cluster-impl.cpp: OnCustomScrollComplete(8717) >  booster timer is still running on cluster-view, Stop boost timer!!!
12-07 20:21:44.153+0530 I/CAPI_APPFW_APPLICATION(  895): app_main.c: _ui_app_appcore_rotation_event(485) > _ui_app_appcore_rotation_event
12-07 20:21:44.213+0530 I/CAPI_APPFW_APPLICATION(  895): app_main.c: _ui_app_appcore_rotation_event(485) > _ui_app_appcore_rotation_event
12-07 20:21:44.744+0530 I/CAPI_APPFW_APPLICATION(  895): app_main.c: _ui_app_appcore_rotation_event(485) > _ui_app_appcore_rotation_event
12-07 20:21:45.945+0530 I/CAPI_APPFW_APPLICATION(  895): app_main.c: _ui_app_appcore_rotation_event(485) > _ui_app_appcore_rotation_event
12-07 20:21:46.485+0530 I/CAPI_APPFW_APPLICATION(  895): app_main.c: _ui_app_appcore_rotation_event(485) > _ui_app_appcore_rotation_event
12-07 20:21:55.304+0530 I/CAPI_APPFW_APPLICATION(  895): app_main.c: _ui_app_appcore_rotation_event(485) > _ui_app_appcore_rotation_event
12-07 20:21:55.604+0530 I/CAPI_APPFW_APPLICATION(  895): app_main.c: _ui_app_appcore_rotation_event(485) > _ui_app_appcore_rotation_event
12-07 20:21:56.325+0530 I/CAPI_APPFW_APPLICATION(  895): app_main.c: _ui_app_appcore_rotation_event(485) > _ui_app_appcore_rotation_event
12-07 20:21:56.926+0530 I/CAPI_APPFW_APPLICATION(  895): app_main.c: _ui_app_appcore_rotation_event(485) > _ui_app_appcore_rotation_event
12-07 20:21:57.166+0530 I/CAPI_APPFW_APPLICATION(  895): app_main.c: _ui_app_appcore_rotation_event(485) > _ui_app_appcore_rotation_event
12-07 20:21:57.226+0530 I/CAPI_APPFW_APPLICATION(  895): app_main.c: _ui_app_appcore_rotation_event(485) > _ui_app_appcore_rotation_event
12-07 20:21:58.187+0530 I/CAPI_APPFW_APPLICATION(  895): app_main.c: _ui_app_appcore_rotation_event(485) > _ui_app_appcore_rotation_event
12-07 20:21:58.247+0530 I/CAPI_APPFW_APPLICATION(  895): app_main.c: _ui_app_appcore_rotation_event(485) > _ui_app_appcore_rotation_event
12-07 20:21:59.388+0530 I/CAPI_APPFW_APPLICATION(  895): app_main.c: _ui_app_appcore_rotation_event(485) > _ui_app_appcore_rotation_event
12-07 20:21:59.808+0530 I/CAPI_APPFW_APPLICATION(  895): app_main.c: _ui_app_appcore_rotation_event(485) > _ui_app_appcore_rotation_event
12-07 20:21:59.928+0530 I/CAPI_APPFW_APPLICATION(  895): app_main.c: _ui_app_appcore_rotation_event(485) > _ui_app_appcore_rotation_event
12-07 20:22:00.109+0530 I/CAPI_APPFW_APPLICATION(  895): app_main.c: _ui_app_appcore_rotation_event(485) > _ui_app_appcore_rotation_event
12-07 20:22:00.229+0530 I/CAPI_APPFW_APPLICATION(  895): app_main.c: _ui_app_appcore_rotation_event(485) > _ui_app_appcore_rotation_event
12-07 20:22:00.499+0530 I/INDICATOR(  776): clock.c: indicator_clock_changed_cb(195) > ""
12-07 20:22:00.499+0530 E/UXT     (  776): uxt_util.c: uxt_util_get_date_time_text_by_locale(389) > text : 8:22 p.m.
12-07 20:22:00.499+0530 I/INDICATOR(  776): clock.c: getTimeFormatted(176) > "time format : 8:22 p.m."
12-07 20:22:00.499+0530 I/INDICATOR(  776): clock.c: indicator_clock_changed_cb(244) > "time format : 8:22 p.m."
12-07 20:22:00.499+0530 W/INDICATOR(  776): clock.c: indicator_clock_changed_cb(272) > 
12-07 20:22:00.499+0530 I/INDICATOR(  776): clock.c: indicator_clock_changed_cb(317) > "[CLOCK MODULE] Timer Status : -2146668665 Time: <font_size=31> </font_size> <font_size=31> 8:22 p.m.</font_size></font>"
12-07 20:22:00.829+0530 I/CAPI_APPFW_APPLICATION(  895): app_main.c: _ui_app_appcore_rotation_event(485) > _ui_app_appcore_rotation_event
12-07 20:22:00.889+0530 I/CAPI_APPFW_APPLICATION(  895): app_main.c: _ui_app_appcore_rotation_event(485) > _ui_app_appcore_rotation_event
12-07 20:22:00.949+0530 I/CAPI_APPFW_APPLICATION(  895): app_main.c: _ui_app_appcore_rotation_event(485) > _ui_app_appcore_rotation_event
12-07 20:22:27.706+0530 I/CAPI_APPFW_APPLICATION(  895): app_main.c: _ui_app_appcore_rotation_event(485) > _ui_app_appcore_rotation_event
12-07 20:22:27.766+0530 I/CAPI_APPFW_APPLICATION(  895): app_main.c: _ui_app_appcore_rotation_event(485) > _ui_app_appcore_rotation_event
12-07 20:22:38.876+0530 E/EFL     ( 4112): eldbus<4112> lib/eldbus/eldbus_proxy.c:785 _props_get_all() Error getting all properties of org.a11y.Bus /org/a11y/bus, error message: org.freedesktop.DBus.Error.TimedOut Activation of org.a11y.Bus timed out
12-07 20:22:38.876+0530 E/EFL     ( 4076): eldbus<4076> lib/eldbus/eldbus_proxy.c:785 _props_get_all() Error getting all properties of org.a11y.Bus /org/a11y/bus, error message: org.freedesktop.DBus.Error.TimedOut Activation of org.a11y.Bus timed out
12-07 20:22:43.911+0530 I/CAPI_APPFW_APPLICATION(  895): app_main.c: _ui_app_appcore_rotation_event(485) > _ui_app_appcore_rotation_event
12-07 20:22:45.653+0530 I/CAPI_APPFW_APPLICATION(  895): app_main.c: _ui_app_appcore_rotation_event(485) > _ui_app_appcore_rotation_event
12-07 20:22:46.003+0530 I/CAPI_APPFW_APPLICATION(  895): app_main.c: _ui_app_appcore_rotation_event(485) > _ui_app_appcore_rotation_event
12-07 20:23:00.518+0530 I/INDICATOR(  776): clock.c: indicator_clock_changed_cb(195) > ""
12-07 20:23:00.528+0530 E/UXT     (  776): uxt_util.c: uxt_util_get_date_time_text_by_locale(389) > text : 8:23 p.m.
12-07 20:23:00.528+0530 I/INDICATOR(  776): clock.c: getTimeFormatted(176) > "time format : 8:23 p.m."
12-07 20:23:00.528+0530 I/INDICATOR(  776): clock.c: indicator_clock_changed_cb(244) > "time format : 8:23 p.m."
12-07 20:23:00.528+0530 W/INDICATOR(  776): clock.c: indicator_clock_changed_cb(272) > 
12-07 20:23:00.528+0530 I/INDICATOR(  776): clock.c: indicator_clock_changed_cb(317) > "[CLOCK MODULE] Timer Status : -2146645580 Time: <font_size=31> </font_size> <font_size=31> 8:23 p.m.</font_size></font>"
12-07 20:24:00.566+0530 I/INDICATOR(  776): clock.c: indicator_clock_changed_cb(195) > ""
12-07 20:24:00.576+0530 E/UXT     (  776): uxt_util.c: uxt_util_get_date_time_text_by_locale(389) > text : 8:24 p.m.
12-07 20:24:00.576+0530 I/INDICATOR(  776): clock.c: getTimeFormatted(176) > "time format : 8:24 p.m."
12-07 20:24:00.576+0530 I/INDICATOR(  776): clock.c: indicator_clock_changed_cb(244) > "time format : 8:24 p.m."
12-07 20:24:00.576+0530 W/INDICATOR(  776): clock.c: indicator_clock_changed_cb(272) > 
12-07 20:24:00.576+0530 I/INDICATOR(  776): clock.c: indicator_clock_changed_cb(317) > "[CLOCK MODULE] Timer Status : -2146635320 Time: <font_size=31> </font_size> <font_size=31> 8:24 p.m.</font_size></font>"
12-07 20:24:17.923+0530 I/CAPI_APPFW_APPLICATION(  895): app_main.c: _ui_app_appcore_rotation_event(485) > _ui_app_appcore_rotation_event
12-07 20:24:27.943+0530 E/PKGMGR_SERVER( 4531): pkgmgr-server.c: main(2414) > package manager server start
12-07 20:24:27.983+0530 E/PKGMGR_SERVER( 4531): pkgmgr-server.c: __set_recovery_mode(234) > rev_file[/opt/share/packages/.recovery/pkgmgr/org.example.uibuildersingleview] is null
12-07 20:24:27.983+0530 E/PKGMGR  ( 4529): pkgmgr.c: __check_sync_process(883) > file is can not remove[/tmp/org.example.uibuildersingleview, -1]
12-07 20:24:28.023+0530 W/AUL_AMD (  664): amd_request.c: __request_handler(906) > __request_handler: 14
12-07 20:24:28.023+0530 E/AUL_AMD (  664): amd_status.c: _status_app_is_running_from_cache(827) > is_running garbage, pid: 4462
12-07 20:24:28.023+0530 W/AUL_AMD (  664): amd_request.c: __send_result_to_client(158) > __send_result_to_client, pid: -1
12-07 20:24:28.033+0530 E/PKGMGR_SERVER( 4531): pkgmgr-server.c: __unset_recovery_mode(282) > remove recovery_file[/opt/share/packages/.recovery/pkgmgr/org.example.uibuildersingleview] fail
12-07 20:24:28.033+0530 E/PKGMGR_SERVER( 4531): pkgmgr-server.c: sighandler(417) > child NORMAL exit [4534]
12-07 20:24:30.515+0530 E/PKGMGR_SERVER( 4531): pkgmgr-server.c: exit_server(1381) > exit_server Start [backend_status=1, queue_status=1, drm_status=1] 
12-07 20:24:30.515+0530 E/PKGMGR_SERVER( 4531): pkgmgr-server.c: main(2471) > package manager server terminated.
12-07 20:24:34.479+0530 E/PKGMGR  ( 4620): pkgmgr.c: pkgmgr_client_reinstall(2020) > reinstall pkg start.
12-07 20:24:34.569+0530 E/PKGMGR_SERVER( 4622): pkgmgr-server.c: main(2414) > package manager server start
12-07 20:24:34.619+0530 E/PKGMGR_SERVER( 4622): pkgmgr-server-internal.c: _zone_set_type_and_backend(180) > tep_filepath [org.example.uibuildersingleview]
12-07 20:24:34.629+0530 E/PKGMGR_SERVER( 4622): pm-mdm.c: _get_package_info_from_file(116) > [0;31m[_get_package_info_from_file(): 116](ret < 0) access() failed. path: org.example.uibuildersingleview errno: 2 (No such file or directory)[0;m
12-07 20:24:34.629+0530 E/PKGMGR_SERVER( 4622): pkgmgr-server.c: __set_recovery_mode(234) > rev_file[/opt/share/packages/.recovery/pkgmgr/org.example.uibuildersingleview] is null
12-07 20:24:34.629+0530 E/PKGMGR  ( 4620): pkgmgr.c: pkgmgr_client_reinstall(2133) > reinstall pkg finish, ret=[46200002]
12-07 20:24:34.780+0530 I/Tizen::App( 1290): (1894) > PackageEventHandler - req: 12900002, pkg_type: tpk, pkg_name: org.example.uibuildersingleview, key: start, val: update
12-07 20:24:34.780+0530 I/Tizen::App( 1290): (1584) > Package = [org.example.uibuildersingleview], Key = [start], Value = [update], install = [1]
12-07 20:24:34.780+0530 E/PKGMGR  ( 1495): pkgmgr.c: __find_op_cbinfo(312) > tmp is NULL
12-07 20:24:34.780+0530 E/PKGMGR  ( 1495): pkgmgr.c: __find_op_cbinfo(312) > tmp is NULL
12-07 20:24:34.780+0530 W/AUL_AMD (  664): amd_appinfo.c: __amd_pkgmgrinfo_status_cb(1068) > __amd_pkgmgrinfo_start_handler
12-07 20:24:34.790+0530 I/Tizen::App( 1290): (1894) > PackageEventHandler - req: 12900002, pkg_type: tpk, pkg_name: org.example.uibuildersingleview, key: install_percent, val: 30
12-07 20:24:34.790+0530 I/Tizen::App( 1290): (119) > InstallationInProgress [30]
12-07 20:24:34.790+0530 I/Tizen::App( 1290): (1584) > Package = [org.example.uibuildersingleview], Key = [install_percent], Value = [30], install = [96]
12-07 20:24:34.790+0530 E/PKGMGR  ( 1495): pkgmgr.c: __find_op_cbinfo(312) > tmp is NULL
12-07 20:24:34.790+0530 E/PKGMGR  ( 1495): pkgmgr.c: __find_op_cbinfo(312) > tmp is NULL
12-07 20:24:34.890+0530 W/CERT_SVC_VCORE( 4625): DUID.cpp: CheckCertifiedDevice(668) > No device id on distributor certificate
12-07 20:24:34.940+0530 E/rpm-installer( 4625): coretpk-parser.c: _coretpk_parser_get_manifest_info(1183) > (doc == NULL) xmlParseFile() failed.
12-07 20:24:34.940+0530 E/rpm-installer( 4625): coretpk-installer.c: _coretpk_installer_verify_privilege_list(1594) > (pkg_file_info == NULL) pkg_file_info is NULL.
12-07 20:24:34.990+0530 E/PKGMGR_PARSER( 4625): pkgmgr_parser.c: __check_theme(154) > theme for uninstallation.
12-07 20:24:35.010+0530 E/PKGMGR_CERT( 4625): pkgmgrinfo_certinfo.c: pkgmgrinfo_delete_certinfo(617) > Transaction Begin
12-07 20:24:35.010+0530 E/PKGMGR_CERT( 4625): pkgmgrinfo_certinfo.c: pkgmgrinfo_delete_certinfo(622) > Certificate Deletion Success
12-07 20:24:35.020+0530 E/PKGMGR_CERT( 4625): pkgmgrinfo_certinfo.c: pkgmgrinfo_delete_certinfo(635) > Transaction Commit and End
12-07 20:24:35.050+0530 E/rpm-installer( 4625): coretpk-installer.c: _coretpk_installer_package_reinstall(6347) > _coretpk_installer_package_reinstall(org.example.uibuildersingleview) failed.
12-07 20:24:35.050+0530 E/PKGMGR  ( 1495): pkgmgr.c: __find_op_cbinfo(312) > tmp is NULL
12-07 20:24:35.050+0530 I/Tizen::App( 1290): (1894) > PackageEventHandler - req: 12900002, pkg_type: tpk, pkg_name: org.example.uibuildersingleview, key: end, val: fail
12-07 20:24:35.050+0530 I/Tizen::App( 1290): (1584) > Package = [org.example.uibuildersingleview], Key = [end], Value = [fail], install = [96]
12-07 20:24:35.050+0530 E/PKGMGR  ( 1495): pkgmgr.c: __find_op_cbinfo(312) > tmp is NULL
12-07 20:24:35.060+0530 E/ESD     (  941): esd_main.c: __esd_pkgmgr_event_callback(1757) > pkg_event(3) falied
12-07 20:24:35.060+0530 W/AUL_AMD (  664): amd_appinfo.c: __amd_pkgmgrinfo_status_cb(1111) > __amd_pkgmgrinfo_fail_handler
12-07 20:24:35.070+0530 W/ISF_PANEL_EFL(  823): isf_panel_efl.cpp: _isf_insert_ime_info_by_pkgid(1168) > pkgmgrinfo_pkginfo_get_pkginfo("org.example.uibuildersingleview",~) returned -1
12-07 20:24:35.070+0530 W/ISF_PANEL_EFL(  823): isf_panel_efl.cpp: _package_manager_event_cb(1381) > isf_db_select_appids_by_pkgid returned 0.
12-07 20:24:36.521+0530 E/PKGMGR_SERVER( 4622): pkgmgr-server.c: exit_server(1381) > exit_server Start [backend_status=0, queue_status=1, drm_status=1] 
12-07 20:24:36.791+0530 E/PKGMGR_SERVER( 4622): pkgmgr-server.c: __unset_recovery_mode(282) > remove recovery_file[/opt/share/packages/.recovery/pkgmgr/org.example.uibuildersingleview] fail
12-07 20:24:36.791+0530 E/PKGMGR_SERVER( 4622): pkgmgr-server.c: sighandler(417) > child NORMAL exit [4625]
12-07 20:24:38.513+0530 E/PKGMGR_SERVER( 4622): pkgmgr-server.c: exit_server(1381) > exit_server Start [backend_status=1, queue_status=1, drm_status=1] 
12-07 20:24:38.513+0530 E/PKGMGR_SERVER( 4622): pkgmgr-server.c: main(2471) > package manager server terminated.
12-07 20:24:39.304+0530 E/PKGMGR  ( 4674): pkgmgr.c: pkgmgr_client_install(1605) > install pkg start.
12-07 20:24:39.384+0530 E/PKGMGR_SERVER( 4676): pkgmgr-server.c: main(2414) > package manager server start
12-07 20:24:39.434+0530 E/PKGMGR_SERVER( 4676): pkgmgr-server-internal.c: _zone_set_type_and_backend(180) > tep_filepath [/opt/usr/apps/tmp/org.example.uibuildersingleview-1.0.0-arm.tpk]
12-07 20:24:39.444+0530 E/PKGMGR_INFO( 4676): pkgmgrinfo_pkginfo.c: pkgmgrinfo_pkginfo_get_unmounted_pkginfo(778) > (exist == 0) pkgid[/opt/usr/apps/tmp/org.example.uibuildersingleview-1.0.0-arm.tpk] not found in DB
12-07 20:24:39.454+0530 E/rpm-installer( 4676): librpm.c: __installer_util_delete_dir(171) > opendir(/tmp/coretpk-unzip) failed. [2][No such file or directory]
12-07 20:24:39.464+0530 E/PKGMGR_SERVER( 4676): pm-mdm.c: _pm_check_mdm_policy(75) > [0;31m[_pm_check_mdm_policy(): 75](ret != MDM_RESULT_SUCCESS) can not connect mdm server[0;m
12-07 20:24:39.464+0530 E/PKGMGR_SERVER( 4676): pkgmgr-server.c: __set_recovery_mode(234) > rev_file[/opt/share/packages/.recovery/pkgmgr/org.example.uibuildersingleview-1.0.0-arm.tpk] is null
12-07 20:24:39.464+0530 E/PKGMGR  ( 4674): pkgmgr.c: pkgmgr_client_install(1723) > install pkg finish, ret=[46740002]
12-07 20:24:39.574+0530 E/PKGMGR_INSTALLER( 4679): pkgmgr_installer.c: pkgmgr_installer_receive_request(225) > option is [i]
12-07 20:24:39.574+0530 E/rpm-installer( 4679): rpm-appcore-intf.c: main(186) > [/opt/usr/apps/tmp/org.example.uibuildersingleview-1.0.0-arm.tpk] is tpk package.
12-07 20:24:39.594+0530 E/rpm-installer( 4679): coretpk-parser.c: _coretpk_parser_is_svc_app(1031) > (ret == 1) NodeSet is empty. (//*[name() = 'manifest']/*[name()='service-application'])
12-07 20:24:39.594+0530 E/rpm-installer( 4679): coretpk-parser.c: __coretpk_parser_get_value(1147) > (result_value == NULL) [install-location] is empty.
12-07 20:24:39.594+0530 E/rpm-installer( 4679): coretpk-parser.c: __coretpk_parser_get_value_list(1104) > (ret == 1) [//*[name() ='privileges']/*[name()='privilege']] is empty.
12-07 20:24:39.594+0530 E/rpm-installer( 4679): coretpk-parser.c: __coretpk_parser_is_theme(31) > (ret == 1) metadata(watchface) is empty.
12-07 20:24:39.594+0530 E/rpm-installer( 4679): coretpk-parser.c: _coretpk_parser_is_widget(997) > (ret == 1) NodeSet is empty. (//*[name() = 'manifest']/*[name()='widget-application'])
12-07 20:24:39.594+0530 E/rpm-installer( 4679): installer-util.c: _installer_util_get_configuration_value(565) > [signature]=[on]
12-07 20:24:39.654+0530 W/CERT_SVC_VCORE( 4679): DUID.cpp: CheckCertifiedDevice(668) > No device id on distributor certificate
12-07 20:24:39.704+0530 I/Tizen::App( 1290): (1894) > PackageEventHandler - req: 12900002, pkg_type: tpk, pkg_name: org.example.uibuildersingleview, key: start, val: install
12-07 20:24:39.704+0530 I/Tizen::App( 1290): (1584) > Package = [org.example.uibuildersingleview], Key = [start], Value = [install], install = [96]
12-07 20:24:39.704+0530 E/PKGMGR  ( 1495): pkgmgr.c: __find_op_cbinfo(312) > tmp is NULL
12-07 20:24:39.704+0530 E/PKGMGR  ( 1495): pkgmgr.c: __find_op_cbinfo(312) > tmp is NULL
12-07 20:24:39.714+0530 I/Tizen::App( 1290): (1894) > PackageEventHandler - req: 12900002, pkg_type: tpk, pkg_name: org.example.uibuildersingleview, key: install_percent, val: 30
12-07 20:24:39.714+0530 I/Tizen::App( 1290): (119) > InstallationInProgress [30]
12-07 20:24:39.714+0530 I/Tizen::App( 1290): (1584) > Package = [org.example.uibuildersingleview], Key = [install_percent], Value = [30], install = [96]
12-07 20:24:39.714+0530 E/PKGMGR  ( 1495): pkgmgr.c: __find_op_cbinfo(312) > tmp is NULL
12-07 20:24:39.714+0530 E/PKGMGR  ( 1495): pkgmgr.c: __find_op_cbinfo(312) > tmp is NULL
12-07 20:24:39.814+0530 E/rpm-installer( 4679): coretpk-parser.c: __coretpk_parser_check_vip_tag(396) > (ret == 1) metadata(watchface) is empty.
12-07 20:24:39.814+0530 E/rpm-installer( 4679): coretpk-parser.c: __coretpk_parser_get_value(1147) > (result_value == NULL) [ui-gadget] is empty.
12-07 20:24:39.824+0530 E/rpm-installer( 4679): coretpk-parser.c: __coretpk_parser_append_path(335) > (ret == 1) NodeSet is empty. (//@portrait-effectimage)
12-07 20:24:39.824+0530 E/rpm-installer( 4679): coretpk-parser.c: __coretpk_parser_is_theme(31) > (ret == 1) metadata(watchface) is empty.
12-07 20:24:39.824+0530 E/PKGMGR_PARSER( 4679): pkgmgr_parser.c: pkgmgr_parser_check_manifest_validation(2678) > Manifest is Valid
12-07 20:24:39.834+0530 E/PKGMGR_PARSER( 4679): pkgmgr_parser_signature.c: __ps_check_mdm_policy(979) > (ret != MDM_RESULT_SUCCESS) can not connect mdm server
12-07 20:24:40.065+0530 E/PKGMGR_PARSER( 4679): pkgmgr_parser.c: __check_theme(142) > theme for installation.
12-07 20:24:40.085+0530 E/PKGMGR_CERT( 4679): pkgmgrinfo_certinfo.c: pkgmgrinfo_save_certinfo(427) > Transaction Begin
12-07 20:24:40.085+0530 E/PKGMGR_CERT( 4679): pkgmgrinfo_certinfo.c: pkgmgrinfo_save_certinfo(496) > Id:Count = 1 112
12-07 20:24:40.085+0530 E/PKGMGR_CERT( 4679): pkgmgrinfo_certinfo.c: pkgmgrinfo_save_certinfo(496) > Id:Count = 2 112
12-07 20:24:40.085+0530 E/PKGMGR_CERT( 4679): pkgmgrinfo_certinfo.c: pkgmgrinfo_save_certinfo(496) > Id:Count = 31 7
12-07 20:24:40.085+0530 E/PKGMGR_CERT( 4679): pkgmgrinfo_certinfo.c: pkgmgrinfo_save_certinfo(496) > Id:Count = 32 7
12-07 20:24:40.085+0530 E/PKGMGR_CERT( 4679): pkgmgrinfo_certinfo.c: pkgmgrinfo_save_certinfo(496) > Id:Count = 33 7
12-07 20:24:40.085+0530 E/PKGMGR_CERT( 4679): pkgmgrinfo_certinfo.c: pkgmgrinfo_save_certinfo(496) > Id:Count = 34 7
12-07 20:24:40.095+0530 E/PKGMGR_CERT( 4679): pkgmgrinfo_certinfo.c: pkgmgrinfo_save_certinfo(576) > Transaction Commit and End
12-07 20:24:40.095+0530 I/Tizen::App( 1290): (1894) > PackageEventHandler - req: 12900002, pkg_type: tpk, pkg_name: org.example.uibuildersingleview, key: install_percent, val: 60
12-07 20:24:40.095+0530 I/Tizen::App( 1290): (119) > InstallationInProgress [60]
12-07 20:24:40.095+0530 I/Tizen::App( 1290): (1584) > Package = [org.example.uibuildersingleview], Key = [install_percent], Value = [60], install = [96]
12-07 20:24:40.095+0530 E/PKGMGR  ( 1495): pkgmgr.c: __find_op_cbinfo(312) > tmp is NULL
12-07 20:24:40.095+0530 E/PKGMGR  ( 1495): pkgmgr.c: __find_op_cbinfo(312) > tmp is NULL
12-07 20:24:40.115+0530 E/rpm-installer( 4679): installer-util.c: _installer_util_get_configuration_value(565) > [signature]=[on]
12-07 20:24:40.135+0530 E/rpm-installer( 4679): coretpk-installer.c: __post_install_for_mmc(652) > (handle == NULL) handle is NULL.
12-07 20:24:40.135+0530 E/PKGMGR  ( 1495): pkgmgr.c: __find_op_cbinfo(312) > tmp is NULL
12-07 20:24:40.135+0530 I/Tizen::App( 1290): (1894) > PackageEventHandler - req: 12900002, pkg_type: tpk, pkg_name: org.example.uibuildersingleview, key: install_percent, val: 100
12-07 20:24:40.135+0530 I/Tizen::App( 1290): (119) > InstallationInProgress [100]
12-07 20:24:40.135+0530 I/Tizen::App( 1290): (1584) > Package = [org.example.uibuildersingleview], Key = [install_percent], Value = [100], install = [96]
12-07 20:24:40.135+0530 E/PKGMGR  ( 1495): pkgmgr.c: __find_op_cbinfo(312) > tmp is NULL
12-07 20:24:41.486+0530 W/LOCKSCREEN(  882): property.c: _vconf_cb(228) > [_vconf_cb:228:W] property 200 is changed to 36
12-07 20:24:41.486+0530 W/LOCKSCREEN(  882): daemon.c: lockd_event(1028) > [lockd_event:1028:W] event:40000:CONF_CHANGED
12-07 20:24:41.486+0530 W/LOCKSCREEN(  882): daemon.c: _event_route(838) > [_event_route:838:W] event:40000 event_info:200
12-07 20:24:41.486+0530 W/LOCKSCREEN(  882): view-mgr.c: _event_route(130) > [_event_route:130:W] event:40000 event_info:200
12-07 20:24:41.486+0530 I/INDICATOR(  776): battery.c: show_battery_icon(376) > "Percentage:(0) / Level:(3) / batt_Full:(0) / battery_charging(1)"
12-07 20:24:41.496+0530 E/INDICATOR(  776): list.c: list_try_to_find_icon_to_remove(378) > (!icon) -> list_try_to_find_icon_to_remove() return
12-07 20:24:41.496+0530 E/INDICATOR(  776): list.c: list_try_to_find_icon_to_show(299) > (!icon) -> list_try_to_find_icon_to_show() return
12-07 20:24:41.496+0530 E/INDICATOR(  776): list.c: list_try_to_find_icon_to_show(299) > (!icon) -> list_try_to_find_icon_to_show() return
12-07 20:24:41.496+0530 E/INDICATOR(  776): list.c: list_try_to_find_icon_to_show(299) > (!icon) -> list_try_to_find_icon_to_show() return
12-07 20:24:41.496+0530 E/INDICATOR(  776): box.c: _update_icon(232) > (!list) -> _update_icon() return
12-07 20:24:41.496+0530 E/INDICATOR(  776): box.c: _update_icon(232) > (!list) -> _update_icon() return
12-07 20:24:41.516+0530 E/PKGMGR_SERVER( 4676): pkgmgr-server.c: exit_server(1381) > exit_server Start [backend_status=0, queue_status=1, drm_status=1] 
12-07 20:24:42.147+0530 I/Tizen::App( 1290): (1894) > PackageEventHandler - req: 12900002, pkg_type: tpk, pkg_name: org.example.uibuildersingleview, key: end, val: ok
12-07 20:24:42.147+0530 I/Tizen::App( 1290): (78) > Installation is Completed. [Package = org.example.uibuildersingleview]
12-07 20:24:42.147+0530 I/Tizen::App( 1290): (671) > Enter. package(org.example.uibuildersingleview), installationResult(0)
12-07 20:24:42.157+0530 E/PKGMGR  ( 1495): pkgmgr.c: __find_op_cbinfo(312) > tmp is NULL
12-07 20:24:42.157+0530 E/PKGMGR  ( 1495): pkgmgr.c: __find_op_cbinfo(312) > tmp is NULL
12-07 20:24:42.157+0530 E/cluster-home(  895): mainmenu-package-manager.cpp: OnClientListenCb(571) >  #Step 1
12-07 20:24:42.157+0530 E/cluster-home(  895): mainmenu-package-manager.cpp: OnClientListenCb(575) >  #Step 2
12-07 20:24:42.157+0530 E/cluster-home(  895): mainmenu-package-manager.cpp: _GetAppIds(350) >  BEGIN
12-07 20:24:42.177+0530 I/Tizen::App( 1290): (1360) > package(org.example.uibuildersingleview), version(1.0.0), type(tpk), displayName(uibuildersingleview), uninstallable(1), downloaded(1), updated(0), preloaded(0)movable(1), externalStorage(0), mainApp(org.example.uibuildersingleview), storeClient(), appRootPath(/opt/usr/apps/org.example.uibuildersingleview)
12-07 20:24:42.197+0530 E/PKGMGR_SERVER( 4676): pkgmgr-server.c: __unset_recovery_mode(282) > remove recovery_file[/opt/share/packages/.recovery/pkgmgr/org.example.uibuildersingleview-1.0.0-arm.tpk] fail
12-07 20:24:42.197+0530 E/PKGMGR_SERVER( 4676): pkgmgr-server.c: sighandler(417) > child NORMAL exit [4679]
12-07 20:24:42.197+0530 I/Tizen::App( 1290): (483) > pkgmgrinfo_appinfo_get_appid(): app = [org.example.uibuildersingleview]
12-07 20:24:42.207+0530 I/Tizen::App( 1290): (416) > appName = [uibuildersingleview]
12-07 20:24:42.207+0530 I/Tizen::App( 1290): (509) > exe = [/opt/usr/apps/org.example.uibuildersingleview/bin/uibuildersingleview], displayName = [uibuildersingleview], mainApp = [1], menuIconVisible = [0], serviceApp = [0]
12-07 20:24:42.217+0530 E/PKGMGR_INFO( 1290): pkgmgrinfo_appinfo.c: pkgmgrinfo_appinfo_get_list(887) > (component == PMINFO_SVC_APP) PMINFO_SVC_APP is done
12-07 20:24:42.217+0530 I/Tizen::App( 1290): (683) > Application count(1) in this package
12-07 20:24:42.217+0530 I/Tizen::App( 1290): (840) > Enter.
12-07 20:24:42.217+0530 I/Tizen::App( 1290): (416) > appName = [uibuildersingleview]
12-07 20:24:42.227+0530 I/Tizen::App( 1290): (703) > Exit.
12-07 20:24:42.227+0530 I/Tizen::App( 1290): (1584) > Package = [org.example.uibuildersingleview], Key = [end], Value = [ok], install = [96]
12-07 20:24:42.227+0530 W/ISF_PANEL_EFL(  823): isf_panel_efl.cpp: _package_manager_event_cb(1513) > _isf_insert_ime_info_by_pkgid returned 0.
12-07 20:24:42.237+0530 I/Tizen::App( 1290): (509) > exe = [/opt/usr/apps/org.example.uibuildersingleview/bin/uibuildersingleview], displayName = [uibuildersingleview], mainApp = [1], menuIconVisible = [0], serviceApp = [0]
12-07 20:24:42.237+0530 I/Tizen::App( 1290): (2343) > info file is not existed. [/opt/usr/apps/org.exampl/info/org.example.uibuildersingleview.info]
12-07 20:24:42.237+0530 I/Tizen::App( 1290): (131) > Enter
12-07 20:24:42.237+0530 I/Tizen::App( 1290): (137) > org.example.uibuildersingleview does not have launch condition
12-07 20:24:42.237+0530 I/Tizen::App( 1290): (883) > Exit.
12-07 20:24:42.257+0530 E/cluster-home(  895): mainmenu-package-manager.cpp: _PkgMgrAppInfoGetListCb(253) >  ##### [org.example.uibuildersingleview]
12-07 20:24:42.257+0530 E/cluster-home(  895): mainmenu-package-manager.cpp: _GetAppIds(375) >  ##### [org.example.uibuildersingleview]
12-07 20:24:42.257+0530 E/cluster-home(  895): mainmenu-package-manager.cpp: _GetAppIds(379) >  END
12-07 20:24:42.257+0530 E/cluster-home(  895): mainmenu-package-manager.cpp: _DoPkgJob(458) >  #Step 3 size[1]
12-07 20:24:42.257+0530 E/cluster-home(  895): mainmenu-package-manager.cpp: _DoPkgJob(462) >  appId[org.example.uibuildersingleview]
12-07 20:24:42.267+0530 E/cluster-home(  895): mainmenu-package-manager.cpp: _GetAppInfo(990) >  Name[uibuildersingleview] enable[1] system[0]
12-07 20:24:42.267+0530 E/HOME_APPS(  895): mainmenu-package-manager.cpp: _DoPkgJob(484) >  appId[org.example.uibuildersingleview] mdm is not enabled
12-07 20:24:42.267+0530 E/cluster-home(  895): mainmenu-package-manager.cpp: GetAppInfo(630) >  Find a App Info Name[uibuildersingleview] enable[1] system[0]
12-07 20:24:42.267+0530 W/HOME_APPS(  895): mainmenu-box-impl.cpp: UpdateBoxData(1326) >  update box data!!!!! old icon path[/opt/usr/apps/org.example.uibuildersingleview/shared/res/uibuildersingleview.png], New icon path[/opt/usr/apps/org.example.uibuildersingleview/shared/res/uibuildersingleview.png]!!!!!
12-07 20:24:42.287+0530 E/util-view(  895): util-text-image-provider.cpp: GetPixelsFromText(240) >  DEFAULT='font=SamsungOneUI:style=Regular style=shadow,bottom shadow_color=# font_size=28 align=center valign=top color=#FFFFFF color_class=ATO001 text_class=ATO001 ellipsis=1 wrap=mixed linegap=-3'
12-07 20:24:42.307+0530 W/HOME_APPS(  895): mainmenu-box-impl.cpp: _OnImageLoadFinishedHandler(2152) >  finished path[/opt/usr/apps/org.example.uibuildersingleview/shared/res/uibuildersingleview.png], Loading state:[1]
12-07 20:24:43.518+0530 E/PKGMGR_SERVER( 4676): pkgmgr-server.c: exit_server(1381) > exit_server Start [backend_status=1, queue_status=1, drm_status=1] 
12-07 20:24:43.518+0530 E/PKGMGR_SERVER( 4676): pkgmgr-server.c: main(2471) > package manager server terminated.
12-07 20:24:47.882+0530 W/AUL     ( 4735): launch.c: app_request_to_launchpad(396) > request cmd(0) to(org.example.uibuildersingleview)
12-07 20:24:47.882+0530 W/AUL_AMD (  664): amd_request.c: __request_handler(906) > __request_handler: 0
12-07 20:24:47.892+0530 I/AUL     (  664): menu_db_util.h: _get_app_info_from_db_by_apppath(242) > path : /usr/bin/launch_app, ret : 0
12-07 20:24:47.902+0530 I/AUL     (  664): menu_db_util.h: _get_app_info_from_db_by_apppath(242) > path : /bin/bash, ret : 0
12-07 20:24:47.902+0530 E/AUL_AMD (  664): amd_launch.c: _start_app(2499) > no caller appid info, ret: -1
12-07 20:24:47.902+0530 W/AUL_AMD (  664): amd_launch.c: _start_app(2508) > caller pid : 4735
12-07 20:24:47.902+0530 E/AUL_AMD (  664): amd_appinfo.c: appinfo_get_value(1296) > appinfo get value: Invalid argument, 19
12-07 20:24:47.912+0530 E/RESOURCED(  779): block.c: block_prelaunch_state(134) > insert data org.example.uibuildersingleview, table num : 4
12-07 20:24:47.912+0530 E/RESOURCED(  779): heart-memory.c: heart_memory_get_data(601) > hashtable heart_memory_app_list is NULL
12-07 20:24:47.912+0530 W/AUL_AMD (  664): amd_launch.c: _start_app(3052) > pad pid(-5)
12-07 20:24:47.912+0530 W/AUL_PAD ( 1521): launchpad.c: __launchpad_main_loop(520) > Launch on type-based process-pool
12-07 20:24:47.912+0530 W/AUL_PAD ( 1521): launchpad.c: __send_result_to_caller(267) > Check app launching
12-07 20:24:47.922+0530 I/CAPI_APPFW_APPLICATION( 4165): app_main.c: ui_app_main(789) > app_efl_main
12-07 20:24:47.932+0530 I/CAPI_APPFW_APPLICATION( 4165): app_main.c: _ui_app_appcore_create(641) > app_appcore_create
12-07 20:24:47.972+0530 E/TBM     ( 4165): tbm_bufmgr.c: _tbm_bufmgr_init_state(691) > USE TGL LOCK!
12-07 20:24:48.012+0530 W/AUL     (  664): app_signal.c: aul_send_app_launch_request_signal(423) > send_app_launch_signal, pid: 4165, appid: org.example.uibuildersingleview
12-07 20:24:48.012+0530 E/AUL     (  664): amd_app_group.c: app_group_start_app(1037) > app_group_start_app
12-07 20:24:48.032+0530 W/AUL     ( 4735): launch.c: app_request_to_launchpad(425) > request cmd(0) result(4165)
12-07 20:24:48.203+0530 I/APP_CORE( 4165): appcore-efl.c: __do_app(514) > [APP 4165] Event: RESET State: CREATED
12-07 20:24:48.203+0530 I/CAPI_APPFW_APPLICATION( 4165): app_main.c: _ui_app_appcore_reset(723) > app_appcore_reset
12-07 20:24:48.203+0530 E/EFL     ( 4165): edje<4165> lib/edje/edje_embryo.c:4134 _edje_embryo_test_run() You are running Embryo->EDC->Embryo with script program '_p26';
12-07 20:24:48.203+0530 E/EFL     ( 4165): By the power of Grayskull, your previous Embryo stack is now broken!
12-07 20:24:48.213+0530 E/EFL     ( 4165): edje<4165> lib/edje/edje_embryo.c:4134 _edje_embryo_test_run() You are running Embryo->EDC->Embryo with script program '_p26';
12-07 20:24:48.213+0530 E/EFL     ( 4165): By the power of Grayskull, your previous Embryo stack is now broken!
12-07 20:24:48.213+0530 E/EFL     ( 4165): edje<4165> lib/edje/edje_embryo.c:4134 _edje_embryo_test_run() You are running Embryo->EDC->Embryo with script program '_p26';
12-07 20:24:48.213+0530 E/EFL     ( 4165): By the power of Grayskull, your previous Embryo stack is now broken!
12-07 20:24:48.223+0530 W/APP_CORE( 4165): appcore-efl.c: __show_cb(920) > [EVENT_TEST][EVENT] GET SHOW EVENT!!!. WIN:5600002
12-07 20:24:48.223+0530 W/AUL_AMD (  664): amd_request.c: __request_handler(906) > __request_handler: 35
12-07 20:24:48.233+0530 E/EFL     (  330): eo<330> lib/eo/eo.c:676 _eo_call_resolve() in lib/edje/edje_object.eo.c:316: func 'edje_obj_signal_emit' (415) could not be resolved for class 'Evas_Object_Smart'.
12-07 20:24:48.233+0530 E/EFL     (  330): eo<330> lib/eo/eo.c:676 _eo_call_resolve() in lib/edje/edje_object.eo.c:316: func 'edje_obj_signal_emit' (415) could not be resolved for class 'Evas_Object_Smart'.
12-07 20:24:48.243+0530 I/Tizen::System( 1290): (259) > Active app [org.exampl], current [com.samsun] 
12-07 20:24:48.243+0530 I/Tizen::Io( 1290): (729) > Entry not found
12-07 20:24:48.253+0530 I/Tizen::System( 1290): (157) > change brightness system value.
12-07 20:24:48.253+0530 W/AUL_AMD (  664): amd_request.c: __request_handler(906) > __request_handler: 15
12-07 20:24:48.313+0530 I/APP_CORE(  895): appcore-efl.c: __do_app(514) > [APP 895] Event: PAUSE State: RUNNING
12-07 20:24:48.313+0530 I/CAPI_APPFW_APPLICATION(  895): app_main.c: _ui_app_appcore_pause(689) > app_appcore_pause
12-07 20:24:48.313+0530 I/APP_CORE( 4165): appcore-efl.c: __do_app(514) > [APP 4165] Event: RESUME State: CREATED
12-07 20:24:48.313+0530 E/cluster-home(  895): homescreen.cpp: OnPause(260) >  app pause
12-07 20:24:48.313+0530 W/AUL_AMD (  664): amd_launch.c: __e17_status_handler(3328) > pid(895) status(4)
12-07 20:24:48.313+0530 W/AUL     (  664): amd_app_group.c: __set_fg_flag(190) > send_signal BG com.samsung.homescreen(895)
12-07 20:24:48.313+0530 W/AUL     (  664): app_signal.c: aul_send_app_status_change_signal(581) > send_app_status_change_signal, pid: 895, appid: com.samsung.homescreen, status: bg
12-07 20:24:48.313+0530 W/AUL_AMD (  664): amd_launch.c: __e17_status_handler(3328) > pid(4165) status(3)
12-07 20:24:48.313+0530 W/AUL_AMD (  664): amd_key.c: _key_ungrab(269) > fail(-1) to ungrab key(XF86Back)
12-07 20:24:48.313+0530 W/AUL_AMD (  664): amd_launch.c: __e17_status_handler(3349) > back key ungrab error
12-07 20:24:48.313+0530 W/AUL     (  664): amd_app_group.c: __set_fg_flag(180) > send_signal FG org.example.uibuildersingleview(4165)
12-07 20:24:48.313+0530 W/AUL     (  664): app_signal.c: aul_send_app_status_change_signal(581) > send_app_status_change_signal, pid: 4165, appid: org.example.uibuildersingleview, status: fg
12-07 20:24:48.323+0530 I/APP_CORE( 4165): appcore-efl.c: __do_app(623) > Legacy lifecycle: 0
12-07 20:24:48.323+0530 I/APP_CORE( 4165): appcore-efl.c: __do_app(625) > [APP 4165] Initial Launching, call the resume_cb
12-07 20:24:48.323+0530 I/CAPI_APPFW_APPLICATION( 4165): app_main.c: _ui_app_appcore_resume(706) > app_appcore_resume
12-07 20:24:48.663+0530 W/AUL_AMD (  664): amd_launch.c: __e17_status_handler(3328) > pid(4165) status(0)
12-07 20:24:49.063+0530 I/Tizen::App( 1290): (499) > LaunchedApp(org.example.uibuildersingleview)
12-07 20:24:49.073+0530 E/RESOURCED(  779): resourced-dbus.c: resourced_dbus_system_hash_drop_busname(324) > Does not exist in busname hash: :1.393
12-07 20:24:49.083+0530 I/Tizen::App( 1290): (733) > Finished invoking application event listener for org.example.uibuildersingleview, 4165.
12-07 20:24:49.824+0530 I/UXT     ( 4748): Uxt_ObjectManager.cpp: OnInitialized(737) > Initialized.
12-07 20:24:50.465+0530 E/EFL     ( 4165): ecore_x<4165> lib/ecore_x/xlib/ecore_x_events.c:603 _ecore_x_event_handle_button_press() ButtonEvent:press time=8842944
12-07 20:24:50.565+0530 E/EFL     ( 4165): ecore_x<4165> lib/ecore_x/xlib/ecore_x_events.c:756 _ecore_x_event_handle_button_release() ButtonEvent:release time=8843032
12-07 20:24:50.575+0530 E/VCONF   ( 4165): vconf.c: _vconf_check_retry_err(1368) > db/ise/keysound : check retry err (-21/13).
12-07 20:24:50.575+0530 E/VCONF   ( 4165): vconf.c: _vconf_get_key_filesys(2371) > _vconf_get_key_filesys(db/ise/keysound) step(-21) failed(13 / Permission denied) retry(0) 
12-07 20:24:50.575+0530 E/VCONF   ( 4165): vconf.c: _vconf_get_key(2407) > _vconf_get_key(db/ise/keysound) step(-21) failed(13 / Permission denied)
12-07 20:24:50.575+0530 E/VCONF   ( 4165): vconf.c: vconf_get_bool(2729) > vconf_get_bool(4165) : db/ise/keysound error
12-07 20:24:50.575+0530 E/VCONF   ( 4165): vconf-kdb.c: _vconf_kdb_add_notify(318) > _vconf_kdb_add_notify : add noti(Permission denied)
12-07 20:24:50.575+0530 E/VCONF   ( 4165): vconf.c: vconf_notify_key_changed(3168) > vconf_notify_key_changed : key(db/ise/keysound) add notify fail
12-07 20:24:51.246+0530 E/EFL     ( 4165): ecore_x<4165> lib/ecore_x/xlib/ecore_x_events.c:603 _ecore_x_event_handle_button_press() ButtonEvent:press time=8843730
12-07 20:24:51.326+0530 E/EFL     ( 4165): ecore_x<4165> lib/ecore_x/xlib/ecore_x_events.c:756 _ecore_x_event_handle_button_release() ButtonEvent:release time=8843817
12-07 20:24:51.756+0530 E/EFL     ( 4165): ecore_x<4165> lib/ecore_x/xlib/ecore_x_events.c:603 _ecore_x_event_handle_button_press() ButtonEvent:press time=8844244
12-07 20:24:51.836+0530 E/EFL     ( 4165): ecore_x<4165> lib/ecore_x/xlib/ecore_x_events.c:756 _ecore_x_event_handle_button_release() ButtonEvent:release time=8844321
12-07 20:24:52.717+0530 E/EFL     ( 4165): ecore_x<4165> lib/ecore_x/xlib/ecore_x_events.c:603 _ecore_x_event_handle_button_press() ButtonEvent:press time=8845198
12-07 20:24:52.807+0530 E/EFL     ( 4165): ecore_x<4165> lib/ecore_x/xlib/ecore_x_events.c:756 _ecore_x_event_handle_button_release() ButtonEvent:release time=8845286
12-07 20:24:52.947+0530 E/EFL     (  330): eo<330> lib/eo/eo.c:676 _eo_call_resolve() in lib/edje/edje_object.eo.c:316: func 'edje_obj_signal_emit' (415) could not be resolved for class 'Evas_Object_Smart'.
12-07 20:24:52.957+0530 W/AUL_AMD (  664): amd_launch.c: __e17_status_handler(3328) > pid(895) status(3)
12-07 20:24:52.957+0530 W/AUL_AMD (  664): amd_key.c: _key_ungrab(269) > fail(-1) to ungrab key(XF86Back)
12-07 20:24:52.957+0530 W/AUL_AMD (  664): amd_launch.c: __e17_status_handler(3349) > back key ungrab error
12-07 20:24:52.957+0530 W/AUL     (  664): amd_app_group.c: __set_fg_flag(180) > send_signal FG com.samsung.homescreen(895)
12-07 20:24:52.957+0530 W/AUL     (  664): app_signal.c: aul_send_app_status_change_signal(581) > send_app_status_change_signal, pid: 895, appid: com.samsung.homescreen, status: fg
12-07 20:24:52.957+0530 I/AUL_PAD ( 1521): sigchild.h: __launchpad_process_sigchld(162) > dead_pid = 4165 pgid = 4165
12-07 20:24:52.957+0530 I/AUL_PAD ( 1521): sigchild.h: __sigchild_action(143) > dead_pid(4165)
12-07 20:24:52.987+0530 I/AUL_PAD ( 1521): sigchild.h: __sigchild_action(149) > __send_app_dead_signal(0)
12-07 20:24:52.987+0530 I/AUL_PAD ( 1521): sigchild.h: __launchpad_process_sigchld(170) > after __sigchild_action
12-07 20:24:52.987+0530 E/AUL_PAD ( 1521): launchpad.c: main(698) > error reading sigchld info
12-07 20:24:52.987+0530 I/ESD     (  941): esd_main.c: __esd_app_dead_handler(1773) > pid: 4165
12-07 20:24:52.987+0530 I/Tizen::App( 1290): (243) > App[org.example.uibuildersingleview] pid[4165] terminate event is forwarded
12-07 20:24:52.987+0530 I/Tizen::System( 1290): (256) > osp.accessorymanager.service provider is found.
12-07 20:24:52.987+0530 I/Tizen::System( 1290): (196) > Accessory Owner is removed [org.example.uibuildersingleview, 4165, ]
12-07 20:24:52.987+0530 I/Tizen::System( 1290): (256) > osp.system.service provider is found.
12-07 20:24:52.987+0530 I/Tizen::App( 1290): (506) > TerminatedApp(org.example.uibuildersingleview)
12-07 20:24:52.987+0530 I/Tizen::App( 1290): (512) > Not registered pid(4165)
12-07 20:24:52.987+0530 I/Tizen::System( 1290): (246) > Terminated app [org.example.uibuildersingleview]
12-07 20:24:52.987+0530 I/Tizen::Io( 1290): (729) > Entry not found
12-07 20:24:52.997+0530 W/AUL_AMD (  664): amd_main.c: __app_dead_handler(327) > __app_dead_handler, pid: 4165
12-07 20:24:52.997+0530 W/AUL_AMD (  664): amd_main.c: __app_dead_handler(333) > app_group_leader_app, pid: 4165
12-07 20:24:52.997+0530 E/RESOURCED(  779): resourced-dbus.c: resourced_dbus_system_hash_drop_busname(324) > Does not exist in busname hash: :1.397
12-07 20:24:53.007+0530 W/AUL_AMD (  664): amd_launch.c: __e17_status_handler(3328) > pid(895) status(0)
12-07 20:24:53.017+0530 I/Tizen::System( 1290): (157) > change brightness system value.
12-07 20:24:53.017+0530 I/Tizen::App( 1290): (782) > Finished invoking application event listener for org.example.uibuildersingleview, 4165.
12-07 20:24:53.027+0530 I/APP_CORE(  895): appcore-efl.c: __do_app(514) > [APP 895] Event: RESUME State: PAUSED
12-07 20:24:53.027+0530 I/CAPI_APPFW_APPLICATION(  895): app_main.c: _ui_app_appcore_resume(706) > app_appcore_resume
12-07 20:24:53.027+0530 E/cluster-home(  895): homescreen.cpp: OnResume(233) >  app resume
12-07 20:24:53.037+0530 E/EFL     (  330): eo<330> lib/eo/eo.c:676 _eo_call_resolve() in lib/edje/edje_object.eo.c:316: func 'edje_obj_signal_emit' (415) could not be resolved for class 'Evas_Object_Smart'.
12-07 20:24:53.067+0530 I/Tizen::System( 1290): (259) > Active app [com.samsun], current [org.exampl] 
12-07 20:24:53.067+0530 I/Tizen::Io( 1290): (729) > Entry not found
12-07 20:24:53.087+0530 I/Tizen::System( 1290): (157) > change brightness system value.
12-07 20:24:53.097+0530 I/TIZEN_N_SOUND_MANAGER(  961): sound_manager.c: sound_manager_get_volume(84) > returns : type=0, volume=8, ret=0x0
12-07 20:24:53.097+0530 E/TIZEN_N_SOUND_MANAGER(  961): sound_manager_private.c: __convert_sound_manager_error_code(142) > [sound_manager_get_volume] ERROR_NONE(0x00000000) : core frameworks error code(0x00000000)
12-07 20:24:53.097+0530 W/CRASH_MANAGER( 4756): worker.c: worker_job(1199) > 1104165756962148112249
