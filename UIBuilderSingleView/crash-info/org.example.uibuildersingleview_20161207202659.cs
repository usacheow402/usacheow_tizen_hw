S/W Version Information
Model: SM-Z300H
Tizen-Version: 2.4.0.3
Build-Number: Z300HDDU0BPI2
Build-Date: 2016.09.30 15:37:12

Crash Information
Process Name: uibuildersingleview
PID: 4748
Date: 2016-12-07 20:26:59+0530
Executable File Path: /opt/usr/apps/org.example.uibuildersingleview/bin/uibuildersingleview
Signal: 11
      (SIGSEGV)
      si_code: -6
      signal sent by tkill (sent by pid 4748, uid 5000)

Register Information
r0   = 0xb7c64678, r1   = 0x0000000c
r2   = 0x0000003c, r3   = 0x0000000c
r4   = 0xb7bcbe00, r5   = 0xb7c3ce70
r6   = 0x80012493, r7   = 0xb6e1a488
r8   = 0xb7c3f2f8, r9   = 0xb7c64678
r10  = 0x80012493, fp   = 0xb7c34188
ip   = 0xb6a2c4d4, sp   = 0xbe9cbf98
lr   = 0xb6a0360d, pc   = 0xb673f6f2
cpsr = 0x200e0030

Memory Information
MemTotal:   987012 KB
MemFree:    177608 KB
Buffers:     43724 KB
Cached:     317924 KB
VmPeak:     127784 KB
VmSize:     127780 KB
VmLck:           0 KB
VmPin:           0 KB
VmHWM:       29168 KB
VmRSS:       25852 KB
VmData:      44876 KB
VmStk:         136 KB
VmExe:          20 KB
VmLib:       35600 KB
VmPTE:          94 KB
VmSwap:          0 KB

Threads Information
Threads: 5
PID = 4748 TID = 4748
4748 4749 5049 5050 5059 

Maps Information
afe2e000 b062d000 rwxp [stack:5059]
b1523000 b152b000 r-xp /usr/lib/ecore_evas/engines/extn/v-1.13/module.so
b153d000 b1d3c000 rwxp [stack:5050]
b1d3c000 b1d3d000 r-xp /usr/lib/edje/modules/feedback/v-1.13/module.so
b1d4d000 b1d61000 r-xp /usr/lib/edje/modules/elm/v-1.13/module.so
b1d75000 b1d76000 r-xp /usr/lib/libX11-xcb.so.1.0.0
b1d86000 b1d89000 r-xp /usr/lib/libxcb-sync.so.1.0.0
b1d9a000 b1d9b000 r-xp /usr/lib/libxshmfence.so.1.0.0
b1dab000 b1dad000 r-xp /usr/lib/libxcb-present.so.0.0.0
b1dbd000 b1dbf000 r-xp /usr/lib/libxcb-dri3.so.0.0.0
b1dcf000 b1ddf000 r-xp /usr/lib/evas/modules/engines/software_x11/v-1.13/module.so
b1def000 b1dfb000 r-xp /usr/lib/ecore_evas/engines/x/v-1.13/module.so
b1e0d000 b260c000 rwxp [stack:5049]
b280c000 b2813000 r-xp /usr/lib/libefl-extension.so.0.1.0
b2826000 b282c000 r-xp /usr/lib/bufmgr/libtbm_sprd7727.so.0.0.0
b283c000 b2841000 r-xp /opt/usr/apps/org.example.uibuildersingleview/bin/uibuildersingleview
b2992000 b2a75000 r-xp /usr/lib/libscim-1.0.so.8.2.3
b2aac000 b2ad4000 r-xp /usr/lib/ecore_imf/modules/isf/v-1.13/module.so
b2ae6000 b32e5000 rwxp [stack:4749]
b32e5000 b32e7000 r-xp /usr/lib/ecore/system/systemd/v-1.13/module.so
b32f7000 b3301000 r-xp /lib/libnss_files-2.20-2014.11.so
b3312000 b331b000 r-xp /lib/libnss_nis-2.20-2014.11.so
b332c000 b333d000 r-xp /lib/libnsl-2.20-2014.11.so
b3350000 b3356000 r-xp /lib/libnss_compat-2.20-2014.11.so
b3367000 b3368000 r-xp /usr/lib/osp/libappinfo.so.1.2.2.1
b3390000 b3397000 r-xp /usr/lib/libminizip.so.1.0.0
b33a7000 b33ac000 r-xp /usr/lib/libstorage.so.0.1
b33bc000 b341b000 r-xp /usr/lib/libmmfcamcorder.so.0.0.0
b3431000 b3445000 r-xp /usr/lib/libcapi-media-camera.so.0.1.90
b3455000 b3499000 r-xp /usr/lib/libgstbase-1.0.so.0.405.0
b34a9000 b34b1000 r-xp /usr/lib/libgstapp-1.0.so.0.405.0
b34c1000 b34f1000 r-xp /usr/lib/libgstvideo-1.0.so.0.405.0
b3504000 b35bd000 r-xp /usr/lib/libgstreamer-1.0.so.0.405.0
b35d1000 b3624000 r-xp /usr/lib/libmmfplayer.so.0.0.0
b3635000 b3650000 r-xp /usr/lib/libcapi-media-player.so.0.2.16
b3660000 b3721000 r-xp /usr/lib/libprotobuf.so.9.0.1
b3734000 b3744000 r-xp /usr/lib/libefl-assist.so.0.1.0
b3754000 b3761000 r-xp /usr/lib/libmdm-common.so.1.0.98
b3772000 b3779000 r-xp /usr/lib/libcapi-media-tool.so.0.2.2
b3789000 b37ca000 r-xp /usr/lib/libmdm.so.1.2.12
b37da000 b37e2000 r-xp /usr/lib/lib_DNSe_NRSS_ver225.so
b37f1000 b3801000 r-xp /usr/lib/lib_SamsungRec_TizenV04014.so
b3822000 b3882000 r-xp /usr/lib/libasound.so.2.0.0
b3894000 b3897000 r-xp /usr/lib/libpulse-simple.so.0.0.4
b38a7000 b38aa000 r-xp /usr/lib/libsyspopup_caller.so.0.1.0
b38ba000 b38bf000 r-xp /usr/lib/libmmfsoundcommon.so.0.0.0
b38cf000 b38d0000 r-xp /usr/lib/libgthread-2.0.so.0.4301.0
b38e0000 b38eb000 r-xp /usr/lib/libaudio-session-mgr.so.0.0.1
b38ff000 b3906000 r-xp /usr/lib/libmmutil_imgp.so.0.0.0
b3916000 b391c000 r-xp /usr/lib/libmmutil_jpeg.so.0.0.0
b392c000 b3931000 r-xp /usr/lib/libmmfsession.so.0.0.1
b3941000 b395c000 r-xp /usr/lib/libmmfsound.so.0.1.0
b396c000 b3973000 r-xp /usr/lib/libmmfcommon.so.0.0.0
b3983000 b3986000 r-xp /usr/lib/libcapi-media-image-util.so.0.1.10
b3997000 b39c5000 r-xp /usr/lib/libidn.so.11.5.44
b39d5000 b39eb000 r-xp /usr/lib/libnghttp2.so.5.4.0
b39fc000 b3a06000 r-xp /usr/lib/libcares.so.2.1.0
b3a16000 b3a20000 r-xp /usr/lib/libcapi-media-sound-manager.so.0.3.32
b3a30000 b3a32000 r-xp /usr/lib/libcapi-media-wav-player.so.0.1.18
b3a42000 b3a43000 r-xp /usr/lib/libmmfkeysound.so.0.0.0
b3a53000 b3a57000 r-xp /usr/lib/libecore_ipc.so.1.13.0
b3a68000 b3a90000 r-xp /usr/lib/libui-extension.so.0.1.0
b3aa1000 b3aca000 r-xp /usr/lib/libturbojpeg.so
b3aea000 b3af0000 r-xp /usr/lib/libgif.so.4.1.6
b3b00000 b3b46000 r-xp /usr/lib/libcurl.so.4.3.0
b3b57000 b3b78000 r-xp /usr/lib/libexif.so.12.3.3
b3b93000 b3ba8000 r-xp /usr/lib/libtts.so
b3bb9000 b3bc1000 r-xp /usr/lib/libfeedback.so.0.1.4
b3bd1000 b3c97000 r-xp /usr/lib/libdali-core.so.0.0.0
b3cb7000 b3daf000 r-xp /usr/lib/libdali-adaptor.so.0.0.0
b3dce000 b3e9c000 r-xp /usr/lib/libdali-toolkit.so.0.0.0
b3eb3000 b3eb5000 r-xp /usr/lib/libboost_system.so.1.51.0
b3ec5000 b3ecb000 r-xp /usr/lib/libboost_chrono.so.1.51.0
b3edb000 b3efe000 r-xp /usr/lib/libboost_thread.so.1.51.0
b3f0f000 b3f11000 r-xp /usr/lib/libappsvc.so.0.1.0
b3f21000 b3f23000 r-xp /usr/lib/libcapi-appfw-app-common.so.0.3.1.0
b3f34000 b3f39000 r-xp /usr/lib/libcapi-appfw-app-control.so.0.3.1.0
b3f50000 b3f52000 r-xp /usr/lib/libosp-env-config.so.1.2.2.1
b3f62000 b3f69000 r-xp /usr/lib/libsensord-share.so
b3f79000 b3f91000 r-xp /usr/lib/libsensor.so.1.1.0
b3fa2000 b3fa5000 r-xp /usr/lib/libXv.so.1.0.0
b3fb5000 b3fba000 r-xp /usr/lib/libutilX.so.1.1.0
b3fca000 b3fcf000 r-xp /usr/lib/libappcore-common.so.1.1
b3fdf000 b3fe6000 r-xp /usr/lib/libcapi-ui-efl-util.so.0.2.11
b3ff9000 b3ffd000 r-xp /usr/lib/libcapi-appfw-application.so.0.3.1.0
b400e000 b40ec000 r-xp /usr/lib/libCOREGL.so.4.0
b410c000 b410f000 r-xp /usr/lib/libuuid.so.1.3.0
b411f000 b4136000 r-xp /usr/lib/libblkid.so.1.1.0
b4147000 b4149000 r-xp /usr/lib/libXau.so.6.0.0
b4159000 b41a0000 r-xp /usr/lib/pulseaudio/libpulsecommon-4.0.so
b41b2000 b41b8000 r-xp /usr/lib/libjson-c.so.2.0.1
b41c9000 b41cd000 r-xp /usr/lib/libogg.so.0.7.1
b41dd000 b41ff000 r-xp /usr/lib/libvorbis.so.0.4.3
b420f000 b42f3000 r-xp /usr/lib/libvorbisenc.so.2.0.6
b430f000 b4312000 r-xp /usr/lib/libEGL.so.1.4
b4323000 b4329000 r-xp /usr/lib/libxcb-render.so.0.0.0
b4339000 b433b000 r-xp /usr/lib/libxcb-shm.so.0.0.0
b434b000 b4358000 r-xp /usr/lib/libGLESv2.so.2.0
b4369000 b43cb000 r-xp /usr/lib/libpixman-1.so.0.28.2
b43e0000 b43f8000 r-xp /usr/lib/libmount.so.1.1.0
b440a000 b441e000 r-xp /usr/lib/libxcb.so.1.1.0
b442e000 b4435000 r-xp /lib/libcrypt-2.20-2014.11.so
b446d000 b446f000 r-xp /usr/lib/libiri.so
b447f000 b448a000 r-xp /usr/lib/libgpg-error.so.0.15.0
b449b000 b44d1000 r-xp /usr/lib/libpulse.so.0.16.2
b44e2000 b4525000 r-xp /usr/lib/libsndfile.so.1.0.25
b453a000 b454f000 r-xp /lib/libexpat.so.1.5.2
b4561000 b461f000 r-xp /usr/lib/libcairo.so.2.11200.14
b4633000 b463b000 r-xp /usr/lib/libdrm.so.2.4.0
b464b000 b464e000 r-xp /usr/lib/libdri2.so.0.0.0
b465e000 b46ac000 r-xp /usr/lib/libssl.so.1.0.0
b46c1000 b46cd000 r-xp /usr/lib/libeeze.so.1.13.0
b46de000 b46e7000 r-xp /usr/lib/libethumb.so.1.13.0
b46f7000 b46fa000 r-xp /usr/lib/libecore_input_evas.so.1.13.0
b470a000 b48c1000 r-xp /usr/lib/libcrypto.so.1.0.0
b56ac000 b56b5000 r-xp /usr/lib/libXi.so.6.1.0
b56c5000 b56c7000 r-xp /usr/lib/libXgesture.so.7.0.0
b56d7000 b56db000 r-xp /usr/lib/libXtst.so.6.1.0
b56eb000 b56f1000 r-xp /usr/lib/libXrender.so.1.3.0
b5701000 b5707000 r-xp /usr/lib/libXrandr.so.2.2.0
b5717000 b5719000 r-xp /usr/lib/libXinerama.so.1.0.0
b572a000 b572d000 r-xp /usr/lib/libXfixes.so.3.1.0
b573d000 b5748000 r-xp /usr/lib/libXext.so.6.4.0
b5758000 b575a000 r-xp /usr/lib/libXdamage.so.1.1.0
b576a000 b576c000 r-xp /usr/lib/libXcomposite.so.1.0.0
b577c000 b585e000 r-xp /usr/lib/libX11.so.6.3.0
b5872000 b5879000 r-xp /usr/lib/libXcursor.so.1.0.2
b5889000 b58a1000 r-xp /usr/lib/libudev.so.1.6.0
b58a3000 b58a6000 r-xp /lib/libattr.so.1.1.0
b58b6000 b58d6000 r-xp /usr/lib/libsecurity-server-commons.so.1.0.0
b58d7000 b58dc000 r-xp /usr/lib/libffi.so.6.0.2
b58ed000 b5905000 r-xp /lib/libz.so.1.2.8
b5915000 b5917000 r-xp /usr/lib/libgmodule-2.0.so.0.4301.0
b5927000 b59fc000 r-xp /usr/lib/libxml2.so.2.9.2
b5a11000 b5aac000 r-xp /usr/lib/libstdc++.so.6.0.20
b5ac8000 b5acb000 r-xp /usr/lib/libSLP-db-util.so.0.1.0
b5adb000 b5af4000 r-xp /usr/lib/libpkgmgr_parser.so.0.1.0
b5b05000 b5b16000 r-xp /lib/libresolv-2.20-2014.11.so
b5b2a000 b5ba4000 r-xp /usr/lib/libgcrypt.so.20.0.3
b5bb9000 b5bbb000 r-xp /usr/lib/libecore_imf_evas.so.1.13.0
b5bcb000 b5bd2000 r-xp /usr/lib/libembryo.so.1.13.0
b5be2000 b5bec000 r-xp /usr/lib/libecore_audio.so.1.13.0
b5bfd000 b5c15000 r-xp /usr/lib/libpng12.so.0.50.0
b5c26000 b5c49000 r-xp /usr/lib/libjpeg.so.8.0.2
b5c6a000 b5c7e000 r-xp /usr/lib/libector.so.1.13.0
b5c8f000 b5ca7000 r-xp /usr/lib/liblua-5.1.so
b5cb8000 b5d0f000 r-xp /usr/lib/libfreetype.so.6.11.3
b5d23000 b5d4b000 r-xp /usr/lib/libfontconfig.so.1.8.0
b5d5c000 b5d6f000 r-xp /usr/lib/libfribidi.so.0.3.1
b5d80000 b5dba000 r-xp /usr/lib/libharfbuzz.so.0.940.0
b5dcb000 b5dd9000 r-xp /usr/lib/libgraphics-extension.so.0.1.0
b5de9000 b5df1000 r-xp /usr/lib/libtbm.so.1.0.0
b5e01000 b5e0e000 r-xp /usr/lib/libeio.so.1.13.0
b5e1e000 b5e20000 r-xp /usr/lib/libefreet_trash.so.1.13.0
b5e30000 b5e35000 r-xp /usr/lib/libefreet_mime.so.1.13.0
b5e45000 b5e5c000 r-xp /usr/lib/libefreet.so.1.13.0
b5e6e000 b5e8e000 r-xp /usr/lib/libeldbus.so.1.13.0
b5e9e000 b5ebe000 r-xp /usr/lib/libecore_con.so.1.13.0
b5ec0000 b5ec6000 r-xp /usr/lib/libecore_imf.so.1.13.0
b5ed6000 b5ee7000 r-xp /usr/lib/libemotion.so.1.13.0
b5ef8000 b5eff000 r-xp /usr/lib/libethumb_client.so.1.13.0
b5f0f000 b5f1e000 r-xp /usr/lib/libeo.so.1.13.0
b5f2f000 b5f41000 r-xp /usr/lib/libecore_input.so.1.13.0
b5f52000 b5f57000 r-xp /usr/lib/libecore_file.so.1.13.0
b5f67000 b5f80000 r-xp /usr/lib/libecore_evas.so.1.13.0
b5f90000 b5fad000 r-xp /usr/lib/libeet.so.1.13.0
b5fc6000 b600e000 r-xp /usr/lib/libeina.so.1.13.0
b601f000 b602f000 r-xp /usr/lib/libefl.so.1.13.0
b6040000 b6125000 r-xp /usr/lib/libicuuc.so.51.1
b6142000 b6282000 r-xp /usr/lib/libicui18n.so.51.1
b6299000 b62d1000 r-xp /usr/lib/libecore_x.so.1.13.0
b62e3000 b62e6000 r-xp /lib/libcap.so.2.21
b62f6000 b631f000 r-xp /usr/lib/libsecurity-server-client.so.1.0.1
b6330000 b6337000 r-xp /usr/lib/libcapi-base-common.so.0.2.2
b6349000 b6380000 r-xp /usr/lib/libgobject-2.0.so.0.4301.0
b6391000 b647c000 r-xp /usr/lib/libgio-2.0.so.0.4301.0
b648f000 b6508000 r-xp /usr/lib/libsqlite3.so.0.8.6
b651a000 b651f000 r-xp /usr/lib/libcapi-system-info.so.0.2.1
b652f000 b6539000 r-xp /usr/lib/libvconf.so.0.2.45
b6549000 b654b000 r-xp /usr/lib/libvasum.so.0.3.1
b655b000 b655d000 r-xp /usr/lib/libttrace.so.1.1
b656d000 b6570000 r-xp /usr/lib/libiniparser.so.0
b6580000 b65a6000 r-xp /usr/lib/libpkgmgr-info.so.0.0.17
b65b6000 b65bb000 r-xp /usr/lib/libxdgmime.so.1.1.0
b65cc000 b65e3000 r-xp /usr/lib/libdbus-glib-1.so.2.2.2
b65f4000 b665f000 r-xp /lib/libm-2.20-2014.11.so
b6670000 b6676000 r-xp /lib/librt-2.20-2014.11.so
b6687000 b6694000 r-xp /usr/lib/libunwind.so.8.0.1
b66ca000 b67ee000 r-xp /lib/libc-2.20-2014.11.so
b6803000 b681c000 r-xp /lib/libgcc_s-4.9.so.1
b682c000 b690e000 r-xp /usr/lib/libglib-2.0.so.0.4301.0
b691f000 b6949000 r-xp /usr/lib/libdbus-1.so.3.8.12
b695a000 b6996000 r-xp /usr/lib/libsystemd.so.0.4.0
b6998000 b6a1b000 r-xp /usr/lib/libedje.so.1.13.0
b6a2e000 b6a4c000 r-xp /usr/lib/libecore.so.1.13.0
b6a6c000 b6bf3000 r-xp /usr/lib/libevas.so.1.13.0
b6c28000 b6c3c000 r-xp /lib/libpthread-2.20-2014.11.so
b6c50000 b6e84000 r-xp /usr/lib/libelementary.so.1.13.0
b6eb3000 b6eb7000 r-xp /usr/lib/libsmack.so.1.0.0
b6ec7000 b6ece000 r-xp /usr/lib/libprivilege-control.so.0.0.2
b6ede000 b6ee0000 r-xp /usr/lib/libdlog.so.0.0.0
b6ef0000 b6ef3000 r-xp /usr/lib/libbundle.so.0.1.22
b6f03000 b6f05000 r-xp /lib/libdl-2.20-2014.11.so
b6f16000 b6f2e000 r-xp /usr/lib/libaul.so.0.1.0
b6f42000 b6f47000 r-xp /usr/lib/libappcore-efl.so.1.1
b6f58000 b6f65000 r-xp /usr/lib/liblptcp.so
b6f77000 b6f7b000 r-xp /usr/lib/libsys-assert.so
b6f8c000 b6fac000 r-xp /lib/ld-2.20-2014.11.so
b6fbd000 b6fc2000 r-xp /usr/bin/launchpad-loader
b79e9000 b7d0e000 rw-p [heap]
be9ac000 be9cd000 rwxp [stack]
b79e9000 b7d0e000 rw-p [heap]
be9ac000 be9cd000 rwxp [stack]
End of Maps Information

Callstack Information (PID:4748)
Call Stack Count: 1
 0: strcmp + 0x1 (0xb673f6f2) [/lib/libc.so.6] + 0x756f2
End of Call Stack

Package Information
Package Name: org.example.uibuildersingleview
Package ID : org.example.uibuildersingleview
Version: 1.0.0
Package Type: tpk
App Name: uibuildersingleview
App ID: org.example.uibuildersingleview
Type: capp
Categories: 

Latest Debug Message Information
--------- beginning of /dev/log_main
ib/ecore_x/xlib/ecore_x_events.c:603 _ecore_x_event_handle_button_press() ButtonEvent:press time=8845198
12-07 20:24:52.807+0530 E/EFL     ( 4165): ecore_x<4165> lib/ecore_x/xlib/ecore_x_events.c:756 _ecore_x_event_handle_button_release() ButtonEvent:release time=8845286
12-07 20:24:52.947+0530 E/EFL     (  330): eo<330> lib/eo/eo.c:676 _eo_call_resolve() in lib/edje/edje_object.eo.c:316: func 'edje_obj_signal_emit' (415) could not be resolved for class 'Evas_Object_Smart'.
12-07 20:24:52.957+0530 W/AUL_AMD (  664): amd_launch.c: __e17_status_handler(3328) > pid(895) status(3)
12-07 20:24:52.957+0530 W/AUL_AMD (  664): amd_key.c: _key_ungrab(269) > fail(-1) to ungrab key(XF86Back)
12-07 20:24:52.957+0530 W/AUL_AMD (  664): amd_launch.c: __e17_status_handler(3349) > back key ungrab error
12-07 20:24:52.957+0530 W/AUL     (  664): amd_app_group.c: __set_fg_flag(180) > send_signal FG com.samsung.homescreen(895)
12-07 20:24:52.957+0530 W/AUL     (  664): app_signal.c: aul_send_app_status_change_signal(581) > send_app_status_change_signal, pid: 895, appid: com.samsung.homescreen, status: fg
12-07 20:24:52.957+0530 I/AUL_PAD ( 1521): sigchild.h: __launchpad_process_sigchld(162) > dead_pid = 4165 pgid = 4165
12-07 20:24:52.957+0530 I/AUL_PAD ( 1521): sigchild.h: __sigchild_action(143) > dead_pid(4165)
12-07 20:24:52.987+0530 I/AUL_PAD ( 1521): sigchild.h: __sigchild_action(149) > __send_app_dead_signal(0)
12-07 20:24:52.987+0530 I/AUL_PAD ( 1521): sigchild.h: __launchpad_process_sigchld(170) > after __sigchild_action
12-07 20:24:52.987+0530 E/AUL_PAD ( 1521): launchpad.c: main(698) > error reading sigchld info
12-07 20:24:52.987+0530 I/ESD     (  941): esd_main.c: __esd_app_dead_handler(1773) > pid: 4165
12-07 20:24:52.987+0530 I/Tizen::App( 1290): (243) > App[org.example.uibuildersingleview] pid[4165] terminate event is forwarded
12-07 20:24:52.987+0530 I/Tizen::System( 1290): (256) > osp.accessorymanager.service provider is found.
12-07 20:24:52.987+0530 I/Tizen::System( 1290): (196) > Accessory Owner is removed [org.example.uibuildersingleview, 4165, ]
12-07 20:24:52.987+0530 I/Tizen::System( 1290): (256) > osp.system.service provider is found.
12-07 20:24:52.987+0530 I/Tizen::App( 1290): (506) > TerminatedApp(org.example.uibuildersingleview)
12-07 20:24:52.987+0530 I/Tizen::App( 1290): (512) > Not registered pid(4165)
12-07 20:24:52.987+0530 I/Tizen::System( 1290): (246) > Terminated app [org.example.uibuildersingleview]
12-07 20:24:52.987+0530 I/Tizen::Io( 1290): (729) > Entry not found
12-07 20:24:52.997+0530 W/AUL_AMD (  664): amd_main.c: __app_dead_handler(327) > __app_dead_handler, pid: 4165
12-07 20:24:52.997+0530 W/AUL_AMD (  664): amd_main.c: __app_dead_handler(333) > app_group_leader_app, pid: 4165
12-07 20:24:52.997+0530 E/RESOURCED(  779): resourced-dbus.c: resourced_dbus_system_hash_drop_busname(324) > Does not exist in busname hash: :1.397
12-07 20:24:53.007+0530 W/AUL_AMD (  664): amd_launch.c: __e17_status_handler(3328) > pid(895) status(0)
12-07 20:24:53.017+0530 I/Tizen::System( 1290): (157) > change brightness system value.
12-07 20:24:53.017+0530 I/Tizen::App( 1290): (782) > Finished invoking application event listener for org.example.uibuildersingleview, 4165.
12-07 20:24:53.027+0530 I/APP_CORE(  895): appcore-efl.c: __do_app(514) > [APP 895] Event: RESUME State: PAUSED
12-07 20:24:53.027+0530 I/CAPI_APPFW_APPLICATION(  895): app_main.c: _ui_app_appcore_resume(706) > app_appcore_resume
12-07 20:24:53.027+0530 E/cluster-home(  895): homescreen.cpp: OnResume(233) >  app resume
12-07 20:24:53.037+0530 E/EFL     (  330): eo<330> lib/eo/eo.c:676 _eo_call_resolve() in lib/edje/edje_object.eo.c:316: func 'edje_obj_signal_emit' (415) could not be resolved for class 'Evas_Object_Smart'.
12-07 20:24:53.067+0530 I/Tizen::System( 1290): (259) > Active app [com.samsun], current [org.exampl] 
12-07 20:24:53.067+0530 I/Tizen::Io( 1290): (729) > Entry not found
12-07 20:24:53.087+0530 I/Tizen::System( 1290): (157) > change brightness system value.
12-07 20:24:53.097+0530 I/TIZEN_N_SOUND_MANAGER(  961): sound_manager.c: sound_manager_get_volume(84) > returns : type=0, volume=8, ret=0x0
12-07 20:24:53.097+0530 E/TIZEN_N_SOUND_MANAGER(  961): sound_manager_private.c: __convert_sound_manager_error_code(142) > [sound_manager_get_volume] ERROR_NONE(0x00000000) : core frameworks error code(0x00000000)
12-07 20:24:53.097+0530 W/CRASH_MANAGER( 4756): worker.c: worker_job(1199) > 11041657569621481122492
12-07 20:24:53.107+0530 I/TIZEN_N_SOUND_MANAGER(  961): sound_manager.c: sound_manager_get_volume(84) > returns : type=4, volume=0, ret=0x0
12-07 20:24:53.107+0530 E/TIZEN_N_SOUND_MANAGER(  961): sound_manager_private.c: __convert_sound_manager_error_code(142) > [sound_manager_get_volume] ERROR_NONE(0x00000000) : core frameworks error code(0x00000000)
12-07 20:24:53.117+0530 W/AUL_AMD (  664): amd_request.c: __request_handler(906) > __request_handler: 15
12-07 20:24:54.409+0530 I/CAPI_APPFW_APPLICATION(  895): app_main.c: _ui_app_appcore_rotation_event(485) > _ui_app_appcore_rotation_event
12-07 20:24:55.009+0530 I/CAPI_APPFW_APPLICATION(  895): app_main.c: _ui_app_appcore_rotation_event(485) > _ui_app_appcore_rotation_event
12-07 20:24:55.550+0530 I/CAPI_APPFW_APPLICATION(  895): app_main.c: _ui_app_appcore_rotation_event(485) > _ui_app_appcore_rotation_event
12-07 20:24:57.802+0530 E/INDICATOR(  776): list.c: list_try_to_find_icon_to_show(299) > (!icon) -> list_try_to_find_icon_to_show() return
12-07 20:24:57.802+0530 E/INDICATOR(  776): list.c: list_try_to_find_icon_to_show(299) > (!icon) -> list_try_to_find_icon_to_show() return
12-07 20:24:57.802+0530 E/INDICATOR(  776): list.c: list_try_to_find_icon_to_show(299) > (!icon) -> list_try_to_find_icon_to_show() return
12-07 20:24:57.802+0530 E/INDICATOR(  776): list.c: list_try_to_find_icon_to_show(299) > (!icon) -> list_try_to_find_icon_to_show() return
12-07 20:24:57.802+0530 E/INDICATOR(  776): box.c: _update_icon(232) > (!list) -> _update_icon() return
12-07 20:24:57.802+0530 E/INDICATOR(  776): box.c: _update_icon(232) > (!list) -> _update_icon() return
12-07 20:24:57.802+0530 E/INDICATOR(  776): list.c: list_try_to_find_icon_to_remove(378) > (!icon) -> list_try_to_find_icon_to_remove() return
12-07 20:24:57.802+0530 E/INDICATOR(  776): list.c: list_try_to_find_icon_to_show(299) > (!icon) -> list_try_to_find_icon_to_show() return
12-07 20:24:57.802+0530 E/INDICATOR(  776): list.c: list_try_to_find_icon_to_show(299) > (!icon) -> list_try_to_find_icon_to_show() return
12-07 20:24:57.802+0530 E/INDICATOR(  776): list.c: list_try_to_find_icon_to_show(299) > (!icon) -> list_try_to_find_icon_to_show() return
12-07 20:24:57.812+0530 E/INDICATOR(  776): box.c: _update_icon(232) > (!list) -> _update_icon() return
12-07 20:24:57.812+0530 E/INDICATOR(  776): box.c: _update_icon(232) > (!list) -> _update_icon() return
12-07 20:25:00.575+0530 I/INDICATOR(  776): clock.c: indicator_clock_changed_cb(195) > ""
12-07 20:25:00.575+0530 E/UXT     (  776): uxt_util.c: uxt_util_get_date_time_text_by_locale(389) > text : 8:25 p.m.
12-07 20:25:00.575+0530 I/INDICATOR(  776): clock.c: getTimeFormatted(176) > "time format : 8:25 p.m."
12-07 20:25:00.575+0530 I/INDICATOR(  776): clock.c: indicator_clock_changed_cb(244) > "time format : 8:25 p.m."
12-07 20:25:00.575+0530 W/INDICATOR(  776): clock.c: indicator_clock_changed_cb(272) > 
12-07 20:25:00.575+0530 I/INDICATOR(  776): clock.c: indicator_clock_changed_cb(317) > "[CLOCK MODULE] Timer Status : -2146587096 Time: <font_size=31> </font_size> <font_size=31> 8:25 p.m.</font_size></font>"
12-07 20:25:13.157+0530 E/INDICATOR(  776): list.c: list_try_to_find_icon_to_show(299) > (!icon) -> list_try_to_find_icon_to_show() return
12-07 20:25:13.157+0530 E/INDICATOR(  776): list.c: list_try_to_find_icon_to_show(299) > (!icon) -> list_try_to_find_icon_to_show() return
12-07 20:25:13.157+0530 E/INDICATOR(  776): list.c: list_try_to_find_icon_to_show(299) > (!icon) -> list_try_to_find_icon_to_show() return
12-07 20:25:13.157+0530 E/INDICATOR(  776): list.c: list_try_to_find_icon_to_show(299) > (!icon) -> list_try_to_find_icon_to_show() return
12-07 20:25:13.157+0530 E/INDICATOR(  776): box.c: _update_icon(232) > (!list) -> _update_icon() return
12-07 20:25:13.157+0530 E/INDICATOR(  776): box.c: _update_icon(232) > (!list) -> _update_icon() return
12-07 20:25:13.167+0530 E/INDICATOR(  776): list.c: list_try_to_find_icon_to_remove(378) > (!icon) -> list_try_to_find_icon_to_remove() return
12-07 20:25:13.167+0530 E/INDICATOR(  776): list.c: list_try_to_find_icon_to_show(299) > (!icon) -> list_try_to_find_icon_to_show() return
12-07 20:25:13.167+0530 E/INDICATOR(  776): list.c: list_try_to_find_icon_to_show(299) > (!icon) -> list_try_to_find_icon_to_show() return
12-07 20:25:13.167+0530 E/INDICATOR(  776): list.c: list_try_to_find_icon_to_show(299) > (!icon) -> list_try_to_find_icon_to_show() return
12-07 20:25:13.167+0530 E/INDICATOR(  776): box.c: _update_icon(232) > (!list) -> _update_icon() return
12-07 20:25:13.167+0530 E/INDICATOR(  776): box.c: _update_icon(232) > (!list) -> _update_icon() return
12-07 20:25:52.726+0530 E/EFL     (  895): ecore_x<895> lib/ecore_x/xlib/ecore_x_events.c:603 _ecore_x_event_handle_button_press() ButtonEvent:press time=8905208
12-07 20:25:52.966+0530 W/cluster-home(  895): cluster-data-provider.cpp: OnMainScrollStarted(4737) >  Main Scroll Started
12-07 20:25:52.966+0530 W/cluster-home(  895): cluster-data-provider.cpp: OnMainScrollStarted(4739) >  Main Scroll Started Done
12-07 20:25:53.386+0530 I/CAPI_APPFW_APPLICATION(  895): app_main.c: _ui_app_appcore_rotation_event(485) > _ui_app_appcore_rotation_event
12-07 20:25:53.566+0530 E/EFL     (  895): ecore_x<895> lib/ecore_x/xlib/ecore_x_events.c:756 _ecore_x_event_handle_button_release() ButtonEvent:release time=8906056
12-07 20:25:53.807+0530 W/cluster-home(  895): cluster-data-provider.cpp: OnMainScrollCompleted(4747) >  Main Scroll Completed
12-07 20:25:53.807+0530 W/cluster-home(  895): cluster-data-provider.cpp: OnMainScrollCompleted(4750) >  Main Scroll Completed Done
12-07 20:25:53.807+0530 W/cluster-home(  895): cluster-data-provider.cpp: OnFocusedViewChanged(4344) >  view type is not chaged,same view[0]
12-07 20:25:55.248+0530 I/CAPI_APPFW_APPLICATION(  895): app_main.c: _ui_app_appcore_rotation_event(485) > _ui_app_appcore_rotation_event
12-07 20:25:59.092+0530 I/CAPI_APPFW_APPLICATION(  895): app_main.c: _ui_app_appcore_rotation_event(485) > _ui_app_appcore_rotation_event
12-07 20:25:59.152+0530 I/CAPI_APPFW_APPLICATION(  895): app_main.c: _ui_app_appcore_rotation_event(485) > _ui_app_appcore_rotation_event
12-07 20:25:59.332+0530 I/CAPI_APPFW_APPLICATION(  895): app_main.c: _ui_app_appcore_rotation_event(485) > _ui_app_appcore_rotation_event
12-07 20:25:59.452+0530 I/CAPI_APPFW_APPLICATION(  895): app_main.c: _ui_app_appcore_rotation_event(485) > _ui_app_appcore_rotation_event
12-07 20:25:59.853+0530 E/PKGMGR_SERVER( 4839): pkgmgr-server.c: main(2414) > package manager server start
12-07 20:25:59.873+0530 I/CAPI_APPFW_APPLICATION(  895): app_main.c: _ui_app_appcore_rotation_event(485) > _ui_app_appcore_rotation_event
12-07 20:25:59.903+0530 E/PKGMGR_SERVER( 4839): pkgmgr-server.c: __set_recovery_mode(234) > rev_file[/opt/share/packages/.recovery/pkgmgr/org.example.uibuildersingleview] is null
12-07 20:25:59.913+0530 E/PKGMGR  ( 4837): pkgmgr.c: __check_sync_process(883) > file is can not remove[/tmp/org.example.uibuildersingleview, -1]
12-07 20:25:59.943+0530 W/AUL_AMD (  664): amd_request.c: __request_handler(906) > __request_handler: 14
12-07 20:25:59.953+0530 W/AUL_AMD (  664): amd_request.c: __send_result_to_client(158) > __send_result_to_client, pid: -1
12-07 20:25:59.963+0530 E/PKGMGR_SERVER( 4839): pkgmgr-server.c: __unset_recovery_mode(282) > remove recovery_file[/opt/share/packages/.recovery/pkgmgr/org.example.uibuildersingleview] fail
12-07 20:25:59.963+0530 E/PKGMGR_SERVER( 4839): pkgmgr-server.c: sighandler(417) > child NORMAL exit [4842]
12-07 20:26:00.523+0530 I/CAPI_APPFW_APPLICATION(  895): app_main.c: _ui_app_appcore_rotation_event(485) > _ui_app_appcore_rotation_event
12-07 20:26:00.573+0530 I/INDICATOR(  776): clock.c: indicator_clock_changed_cb(195) > ""
12-07 20:26:00.573+0530 E/UXT     (  776): uxt_util.c: uxt_util_get_date_time_text_by_locale(389) > text : 8:26 p.m.
12-07 20:26:00.573+0530 I/INDICATOR(  776): clock.c: getTimeFormatted(176) > "time format : 8:26 p.m."
12-07 20:26:00.573+0530 I/INDICATOR(  776): clock.c: indicator_clock_changed_cb(244) > "time format : 8:26 p.m."
12-07 20:26:00.573+0530 W/INDICATOR(  776): clock.c: indicator_clock_changed_cb(272) > 
12-07 20:26:00.573+0530 I/INDICATOR(  776): clock.c: indicator_clock_changed_cb(317) > "[CLOCK MODULE] Timer Status : -2146547082 Time: <font_size=31> </font_size> <font_size=31> 8:26 p.m.</font_size></font>"
12-07 20:26:02.515+0530 E/PKGMGR_SERVER( 4839): pkgmgr-server.c: exit_server(1381) > exit_server Start [backend_status=1, queue_status=1, drm_status=1] 
12-07 20:26:02.515+0530 E/PKGMGR_SERVER( 4839): pkgmgr-server.c: main(2471) > package manager server terminated.
12-07 20:26:06.539+0530 E/PKGMGR  ( 4928): pkgmgr.c: pkgmgr_client_reinstall(2020) > reinstall pkg start.
12-07 20:26:06.619+0530 E/PKGMGR_SERVER( 4930): pkgmgr-server.c: main(2414) > package manager server start
12-07 20:26:06.669+0530 E/PKGMGR_SERVER( 4930): pkgmgr-server-internal.c: _zone_set_type_and_backend(180) > tep_filepath [org.example.uibuildersingleview]
12-07 20:26:06.679+0530 E/PKGMGR_SERVER( 4930): pm-mdm.c: _get_package_info_from_file(116) > [0;31m[_get_package_info_from_file(): 116](ret < 0) access() failed. path: org.example.uibuildersingleview errno: 2 (No such file or directory)[0;m
12-07 20:26:06.679+0530 E/PKGMGR_SERVER( 4930): pkgmgr-server.c: __set_recovery_mode(234) > rev_file[/opt/share/packages/.recovery/pkgmgr/org.example.uibuildersingleview] is null
12-07 20:26:06.679+0530 E/PKGMGR  ( 4928): pkgmgr.c: pkgmgr_client_reinstall(2133) > reinstall pkg finish, ret=[49280002]
12-07 20:26:06.709+0530 I/CAPI_APPFW_APPLICATION(  895): app_main.c: _ui_app_appcore_rotation_event(485) > _ui_app_appcore_rotation_event
12-07 20:26:06.769+0530 I/CAPI_APPFW_APPLICATION(  895): app_main.c: _ui_app_appcore_rotation_event(485) > _ui_app_appcore_rotation_event
12-07 20:26:06.829+0530 E/PKGMGR  ( 1495): pkgmgr.c: __find_op_cbinfo(312) > tmp is NULL
12-07 20:26:06.829+0530 I/Tizen::App( 1290): (1894) > PackageEventHandler - req: 12900002, pkg_type: tpk, pkg_name: org.example.uibuildersingleview, key: start, val: update
12-07 20:26:06.829+0530 I/Tizen::App( 1290): (1584) > Package = [org.example.uibuildersingleview], Key = [start], Value = [update], install = [1]
12-07 20:26:06.829+0530 E/PKGMGR  ( 1495): pkgmgr.c: __find_op_cbinfo(312) > tmp is NULL
12-07 20:26:06.839+0530 W/AUL_AMD (  664): amd_appinfo.c: __amd_pkgmgrinfo_status_cb(1068) > __amd_pkgmgrinfo_start_handler
12-07 20:26:06.839+0530 E/PKGMGR  ( 1495): pkgmgr.c: __find_op_cbinfo(312) > tmp is NULL
12-07 20:26:06.839+0530 I/Tizen::App( 1290): (1894) > PackageEventHandler - req: 12900002, pkg_type: tpk, pkg_name: org.example.uibuildersingleview, key: install_percent, val: 30
12-07 20:26:06.839+0530 I/Tizen::App( 1290): (119) > InstallationInProgress [30]
12-07 20:26:06.839+0530 I/Tizen::App( 1290): (1584) > Package = [org.example.uibuildersingleview], Key = [install_percent], Value = [30], install = [96]
12-07 20:26:06.839+0530 E/PKGMGR  ( 1495): pkgmgr.c: __find_op_cbinfo(312) > tmp is NULL
12-07 20:26:06.959+0530 W/CERT_SVC_VCORE( 4933): DUID.cpp: CheckCertifiedDevice(668) > No device id on distributor certificate
12-07 20:26:06.989+0530 E/rpm-installer( 4933): coretpk-parser.c: _coretpk_parser_get_manifest_info(1183) > (doc == NULL) xmlParseFile() failed.
12-07 20:26:06.989+0530 E/rpm-installer( 4933): coretpk-installer.c: _coretpk_installer_verify_privilege_list(1594) > (pkg_file_info == NULL) pkg_file_info is NULL.
12-07 20:26:07.050+0530 E/PKGMGR_PARSER( 4933): pkgmgr_parser.c: __check_theme(154) > theme for uninstallation.
12-07 20:26:07.070+0530 E/PKGMGR_CERT( 4933): pkgmgrinfo_certinfo.c: pkgmgrinfo_delete_certinfo(617) > Transaction Begin
12-07 20:26:07.070+0530 E/PKGMGR_CERT( 4933): pkgmgrinfo_certinfo.c: pkgmgrinfo_delete_certinfo(622) > Certificate Deletion Success
12-07 20:26:07.070+0530 E/PKGMGR_CERT( 4933): pkgmgrinfo_certinfo.c: pkgmgrinfo_delete_certinfo(635) > Transaction Commit and End
12-07 20:26:07.110+0530 E/rpm-installer( 4933): coretpk-installer.c: _coretpk_installer_package_reinstall(6347) > _coretpk_installer_package_reinstall(org.example.uibuildersingleview) failed.
12-07 20:26:07.120+0530 E/PKGMGR  ( 1495): pkgmgr.c: __find_op_cbinfo(312) > tmp is NULL
12-07 20:26:07.120+0530 I/Tizen::App( 1290): (1894) > PackageEventHandler - req: 12900002, pkg_type: tpk, pkg_name: org.example.uibuildersingleview, key: end, val: fail
12-07 20:26:07.120+0530 I/Tizen::App( 1290): (1584) > Package = [org.example.uibuildersingleview], Key = [end], Value = [fail], install = [96]
12-07 20:26:07.120+0530 E/PKGMGR  ( 1495): pkgmgr.c: __find_op_cbinfo(312) > tmp is NULL
12-07 20:26:07.120+0530 E/ESD     (  941): esd_main.c: __esd_pkgmgr_event_callback(1757) > pkg_event(3) falied
12-07 20:26:07.130+0530 W/AUL_AMD (  664): amd_appinfo.c: __amd_pkgmgrinfo_status_cb(1111) > __amd_pkgmgrinfo_fail_handler
12-07 20:26:07.130+0530 W/ISF_PANEL_EFL(  823): isf_panel_efl.cpp: _isf_insert_ime_info_by_pkgid(1168) > pkgmgrinfo_pkginfo_get_pkginfo("org.example.uibuildersingleview",~) returned -1
12-07 20:26:07.140+0530 W/ISF_PANEL_EFL(  823): isf_panel_efl.cpp: _package_manager_event_cb(1381) > isf_db_select_appids_by_pkgid returned 0.
12-07 20:26:07.370+0530 I/CAPI_APPFW_APPLICATION(  895): app_main.c: _ui_app_appcore_rotation_event(485) > _ui_app_appcore_rotation_event
12-07 20:26:07.430+0530 I/CAPI_APPFW_APPLICATION(  895): app_main.c: _ui_app_appcore_rotation_event(485) > _ui_app_appcore_rotation_event
12-07 20:26:07.850+0530 I/CAPI_APPFW_APPLICATION(  895): app_main.c: _ui_app_appcore_rotation_event(485) > _ui_app_appcore_rotation_event
12-07 20:26:07.910+0530 I/CAPI_APPFW_APPLICATION(  895): app_main.c: _ui_app_appcore_rotation_event(485) > _ui_app_appcore_rotation_event
12-07 20:26:08.521+0530 E/PKGMGR_SERVER( 4930): pkgmgr-server.c: exit_server(1381) > exit_server Start [backend_status=0, queue_status=1, drm_status=1] 
12-07 20:26:08.751+0530 I/CAPI_APPFW_APPLICATION(  895): app_main.c: _ui_app_appcore_rotation_event(485) > _ui_app_appcore_rotation_event
12-07 20:26:08.851+0530 E/PKGMGR_SERVER( 4930): pkgmgr-server.c: __unset_recovery_mode(282) > remove recovery_file[/opt/share/packages/.recovery/pkgmgr/org.example.uibuildersingleview] fail
12-07 20:26:08.851+0530 E/PKGMGR_SERVER( 4930): pkgmgr-server.c: sighandler(417) > child NORMAL exit [4933]
12-07 20:26:10.523+0530 E/PKGMGR_SERVER( 4930): pkgmgr-server.c: exit_server(1381) > exit_server Start [backend_status=1, queue_status=1, drm_status=1] 
12-07 20:26:10.523+0530 E/PKGMGR_SERVER( 4930): pkgmgr-server.c: main(2471) > package manager server terminated.
12-07 20:26:11.964+0530 E/PKGMGR  ( 4983): pkgmgr.c: pkgmgr_client_install(1605) > install pkg start.
12-07 20:26:12.044+0530 E/PKGMGR_SERVER( 4985): pkgmgr-server.c: main(2414) > package manager server start
12-07 20:26:12.094+0530 E/PKGMGR_SERVER( 4985): pkgmgr-server-internal.c: _zone_set_type_and_backend(180) > tep_filepath [/opt/usr/apps/tmp/org.example.uibuildersingleview-1.0.0-arm.tpk]
12-07 20:26:12.104+0530 E/PKGMGR_INFO( 4985): pkgmgrinfo_pkginfo.c: pkgmgrinfo_pkginfo_get_unmounted_pkginfo(778) > (exist == 0) pkgid[/opt/usr/apps/tmp/org.example.uibuildersingleview-1.0.0-arm.tpk] not found in DB
12-07 20:26:12.114+0530 E/rpm-installer( 4985): librpm.c: __installer_util_delete_dir(171) > opendir(/tmp/coretpk-unzip) failed. [2][No such file or directory]
12-07 20:26:12.125+0530 E/PKGMGR_SERVER( 4985): pm-mdm.c: _pm_check_mdm_policy(75) > [0;31m[_pm_check_mdm_policy(): 75](ret != MDM_RESULT_SUCCESS) can not connect mdm server[0;m
12-07 20:26:12.125+0530 E/PKGMGR_SERVER( 4985): pkgmgr-server.c: __set_recovery_mode(234) > rev_file[/opt/share/packages/.recovery/pkgmgr/org.example.uibuildersingleview-1.0.0-arm.tpk] is null
12-07 20:26:12.125+0530 E/PKGMGR  ( 4983): pkgmgr.c: pkgmgr_client_install(1723) > install pkg finish, ret=[49830002]
12-07 20:26:12.245+0530 E/PKGMGR_INSTALLER( 4988): pkgmgr_installer.c: pkgmgr_installer_receive_request(225) > option is [i]
12-07 20:26:12.245+0530 E/rpm-installer( 4988): rpm-appcore-intf.c: main(186) > [/opt/usr/apps/tmp/org.example.uibuildersingleview-1.0.0-arm.tpk] is tpk package.
12-07 20:26:12.255+0530 E/rpm-installer( 4988): coretpk-parser.c: _coretpk_parser_is_svc_app(1031) > (ret == 1) NodeSet is empty. (//*[name() = 'manifest']/*[name()='service-application'])
12-07 20:26:12.255+0530 E/rpm-installer( 4988): coretpk-parser.c: __coretpk_parser_get_value(1147) > (result_value == NULL) [install-location] is empty.
12-07 20:26:12.255+0530 E/rpm-installer( 4988): coretpk-parser.c: __coretpk_parser_get_value_list(1104) > (ret == 1) [//*[name() ='privileges']/*[name()='privilege']] is empty.
12-07 20:26:12.255+0530 E/rpm-installer( 4988): coretpk-parser.c: __coretpk_parser_is_theme(31) > (ret == 1) metadata(watchface) is empty.
12-07 20:26:12.255+0530 E/rpm-installer( 4988): coretpk-parser.c: _coretpk_parser_is_widget(997) > (ret == 1) NodeSet is empty. (//*[name() = 'manifest']/*[name()='widget-application'])
12-07 20:26:12.255+0530 E/rpm-installer( 4988): installer-util.c: _installer_util_get_configuration_value(565) > [signature]=[on]
12-07 20:26:12.315+0530 W/CERT_SVC_VCORE( 4988): DUID.cpp: CheckCertifiedDevice(668) > No device id on distributor certificate
12-07 20:26:12.365+0530 I/Tizen::App( 1290): (1894) > PackageEventHandler - req: 12900002, pkg_type: tpk, pkg_name: org.example.uibuildersingleview, key: start, val: install
12-07 20:26:12.365+0530 I/Tizen::App( 1290): (1584) > Package = [org.example.uibuildersingleview], Key = [start], Value = [install], install = [96]
12-07 20:26:12.365+0530 E/PKGMGR  ( 1495): pkgmgr.c: __find_op_cbinfo(312) > tmp is NULL
12-07 20:26:12.375+0530 E/PKGMGR  ( 1495): pkgmgr.c: __find_op_cbinfo(312) > tmp is NULL
12-07 20:26:12.375+0530 I/Tizen::App( 1290): (1894) > PackageEventHandler - req: 12900002, pkg_type: tpk, pkg_name: org.example.uibuildersingleview, key: install_percent, val: 30
12-07 20:26:12.375+0530 I/Tizen::App( 1290): (119) > InstallationInProgress [30]
12-07 20:26:12.375+0530 I/Tizen::App( 1290): (1584) > Package = [org.example.uibuildersingleview], Key = [install_percent], Value = [30], install = [96]
12-07 20:26:12.385+0530 E/PKGMGR  ( 1495): pkgmgr.c: __find_op_cbinfo(312) > tmp is NULL
12-07 20:26:12.385+0530 E/PKGMGR  ( 1495): pkgmgr.c: __find_op_cbinfo(312) > tmp is NULL
12-07 20:26:12.485+0530 E/rpm-installer( 4988): coretpk-parser.c: __coretpk_parser_check_vip_tag(396) > (ret == 1) metadata(watchface) is empty.
12-07 20:26:12.485+0530 E/rpm-installer( 4988): coretpk-parser.c: __coretpk_parser_get_value(1147) > (result_value == NULL) [ui-gadget] is empty.
12-07 20:26:12.485+0530 E/rpm-installer( 4988): coretpk-parser.c: __coretpk_parser_append_path(335) > (ret == 1) NodeSet is empty. (//@portrait-effectimage)
12-07 20:26:12.485+0530 E/rpm-installer( 4988): coretpk-parser.c: __coretpk_parser_is_theme(31) > (ret == 1) metadata(watchface) is empty.
12-07 20:26:12.495+0530 E/PKGMGR_PARSER( 4988): pkgmgr_parser.c: pkgmgr_parser_check_manifest_validation(2678) > Manifest is Valid
12-07 20:26:12.495+0530 E/PKGMGR_PARSER( 4988): pkgmgr_parser_signature.c: __ps_check_mdm_policy(979) > (ret != MDM_RESULT_SUCCESS) can not connect mdm server
12-07 20:26:12.735+0530 E/PKGMGR_PARSER( 4988): pkgmgr_parser.c: __check_theme(142) > theme for installation.
12-07 20:26:12.755+0530 E/PKGMGR_CERT( 4988): pkgmgrinfo_certinfo.c: pkgmgrinfo_save_certinfo(427) > Transaction Begin
12-07 20:26:12.755+0530 E/PKGMGR_CERT( 4988): pkgmgrinfo_certinfo.c: pkgmgrinfo_save_certinfo(496) > Id:Count = 1 112
12-07 20:26:12.755+0530 E/PKGMGR_CERT( 4988): pkgmgrinfo_certinfo.c: pkgmgrinfo_save_certinfo(496) > Id:Count = 2 112
12-07 20:26:12.755+0530 E/PKGMGR_CERT( 4988): pkgmgrinfo_certinfo.c: pkgmgrinfo_save_certinfo(496) > Id:Count = 31 7
12-07 20:26:12.755+0530 E/PKGMGR_CERT( 4988): pkgmgrinfo_certinfo.c: pkgmgrinfo_save_certinfo(496) > Id:Count = 32 7
12-07 20:26:12.755+0530 E/PKGMGR_CERT( 4988): pkgmgrinfo_certinfo.c: pkgmgrinfo_save_certinfo(496) > Id:Count = 33 7
12-07 20:26:12.755+0530 E/PKGMGR_CERT( 4988): pkgmgrinfo_certinfo.c: pkgmgrinfo_save_certinfo(496) > Id:Count = 34 7
12-07 20:26:12.765+0530 E/PKGMGR_CERT( 4988): pkgmgrinfo_certinfo.c: pkgmgrinfo_save_certinfo(576) > Transaction Commit and End
12-07 20:26:12.765+0530 E/PKGMGR  ( 1495): pkgmgr.c: __find_op_cbinfo(312) > tmp is NULL
12-07 20:26:12.765+0530 I/Tizen::App( 1290): (1894) > PackageEventHandler - req: 12900002, pkg_type: tpk, pkg_name: org.example.uibuildersingleview, key: install_percent, val: 60
12-07 20:26:12.765+0530 I/Tizen::App( 1290): (119) > InstallationInProgress [60]
12-07 20:26:12.765+0530 I/Tizen::App( 1290): (1584) > Package = [org.example.uibuildersingleview], Key = [install_percent], Value = [60], install = [96]
12-07 20:26:12.765+0530 E/PKGMGR  ( 1495): pkgmgr.c: __find_op_cbinfo(312) > tmp is NULL
12-07 20:26:12.785+0530 E/rpm-installer( 4988): installer-util.c: _installer_util_get_configuration_value(565) > [signature]=[on]
12-07 20:26:12.805+0530 E/rpm-installer( 4988): coretpk-installer.c: __post_install_for_mmc(652) > (handle == NULL) handle is NULL.
12-07 20:26:12.805+0530 E/PKGMGR  ( 1495): pkgmgr.c: __find_op_cbinfo(312) > tmp is NULL
12-07 20:26:12.805+0530 I/Tizen::App( 1290): (1894) > PackageEventHandler - req: 12900002, pkg_type: tpk, pkg_name: org.example.uibuildersingleview, key: install_percent, val: 100
12-07 20:26:12.805+0530 I/Tizen::App( 1290): (119) > InstallationInProgress [100]
12-07 20:26:12.805+0530 I/Tizen::App( 1290): (1584) > Package = [org.example.uibuildersingleview], Key = [install_percent], Value = [100], install = [96]
12-07 20:26:12.805+0530 E/PKGMGR  ( 1495): pkgmgr.c: __find_op_cbinfo(312) > tmp is NULL
12-07 20:26:14.517+0530 E/PKGMGR_SERVER( 4985): pkgmgr-server.c: exit_server(1381) > exit_server Start [backend_status=0, queue_status=1, drm_status=1] 
12-07 20:26:14.807+0530 I/Tizen::App( 1290): (1894) > PackageEventHandler - req: 12900002, pkg_type: tpk, pkg_name: org.example.uibuildersingleview, key: end, val: ok
12-07 20:26:14.807+0530 I/Tizen::App( 1290): (78) > Installation is Completed. [Package = org.example.uibuildersingleview]
12-07 20:26:14.807+0530 I/Tizen::App( 1290): (671) > Enter. package(org.example.uibuildersingleview), installationResult(0)
12-07 20:26:14.807+0530 E/cluster-home(  895): mainmenu-package-manager.cpp: OnClientListenCb(571) >  #Step 1
12-07 20:26:14.807+0530 E/cluster-home(  895): mainmenu-package-manager.cpp: OnClientListenCb(575) >  #Step 2
12-07 20:26:14.807+0530 E/cluster-home(  895): mainmenu-package-manager.cpp: _GetAppIds(350) >  BEGIN
12-07 20:26:14.827+0530 W/ISF_PANEL_EFL(  823): isf_panel_efl.cpp: _package_manager_event_cb(1513) > _isf_insert_ime_info_by_pkgid returned 0.
12-07 20:26:14.837+0530 I/Tizen::App( 1290): (1360) > package(org.example.uibuildersingleview), version(1.0.0), type(tpk), displayName(uibuildersingleview), uninstallable(1), downloaded(1), updated(0), preloaded(0)movable(1), externalStorage(0), mainApp(org.example.uibuildersingleview), storeClient(), appRootPath(/opt/usr/apps/org.example.uibuildersingleview)
12-07 20:26:14.857+0530 E/PKGMGR  ( 1495): pkgmgr.c: __find_op_cbinfo(312) > tmp is NULL
12-07 20:26:14.857+0530 E/PKGMGR  ( 1495): pkgmgr.c: __find_op_cbinfo(312) > tmp is NULL
12-07 20:26:14.857+0530 E/cluster-home(  895): mainmenu-package-manager.cpp: _PkgMgrAppInfoGetListCb(253) >  ##### [org.example.uibuildersingleview]
12-07 20:26:14.857+0530 E/cluster-home(  895): mainmenu-package-manager.cpp: _GetAppIds(375) >  ##### [org.example.uibuildersingleview]
12-07 20:26:14.857+0530 E/cluster-home(  895): mainmenu-package-manager.cpp: _GetAppIds(379) >  END
12-07 20:26:14.857+0530 E/cluster-home(  895): mainmenu-package-manager.cpp: _DoPkgJob(458) >  #Step 3 size[1]
12-07 20:26:14.857+0530 E/cluster-home(  895): mainmenu-package-manager.cpp: _DoPkgJob(462) >  appId[org.example.uibuildersingleview]
12-07 20:26:14.867+0530 E/cluster-home(  895): mainmenu-package-manager.cpp: _GetAppInfo(990) >  Name[uibuildersingleview] enable[1] system[0]
12-07 20:26:14.877+0530 E/HOME_APPS(  895): mainmenu-package-manager.cpp: _DoPkgJob(484) >  appId[org.example.uibuildersingleview] mdm is not enabled
12-07 20:26:14.877+0530 E/cluster-home(  895): mainmenu-package-manager.cpp: GetAppInfo(630) >  Find a App Info Name[uibuildersingleview] enable[1] system[0]
12-07 20:26:14.877+0530 W/HOME_APPS(  895): mainmenu-box-impl.cpp: UpdateBoxData(1326) >  update box data!!!!! old icon path[/opt/usr/apps/org.example.uibuildersingleview/shared/res/uibuildersingleview.png], New icon path[/opt/usr/apps/org.example.uibuildersingleview/shared/res/uibuildersingleview.png]!!!!!
12-07 20:26:14.907+0530 E/util-view(  895): util-text-image-provider.cpp: GetPixelsFromText(240) >  DEFAULT='font=SamsungOneUI:style=Regular style=shadow,bottom shadow_color=# font_size=28 align=center valign=top color=#FFFFFF color_class=ATO001 text_class=ATO001 ellipsis=1 wrap=mixed linegap=-3'
12-07 20:26:14.907+0530 I/Tizen::App( 1290): (483) > pkgmgrinfo_appinfo_get_appid(): app = [org.example.uibuildersingleview]
12-07 20:26:14.907+0530 E/PKGMGR_SERVER( 4985): pkgmgr-server.c: __unset_recovery_mode(282) > remove recovery_file[/opt/share/packages/.recovery/pkgmgr/org.example.uibuildersingleview-1.0.0-arm.tpk] fail
12-07 20:26:14.907+0530 E/PKGMGR_SERVER( 4985): pkgmgr-server.c: sighandler(417) > child NORMAL exit [4988]
12-07 20:26:14.927+0530 I/Tizen::App( 1290): (416) > appName = [uibuildersingleview]
12-07 20:26:14.927+0530 I/Tizen::App( 1290): (509) > exe = [/opt/usr/apps/org.example.uibuildersingleview/bin/uibuildersingleview], displayName = [uibuildersingleview], mainApp = [1], menuIconVisible = [0], serviceApp = [0]
12-07 20:26:14.927+0530 E/PKGMGR_INFO( 1290): pkgmgrinfo_appinfo.c: pkgmgrinfo_appinfo_get_list(887) > (component == PMINFO_SVC_APP) PMINFO_SVC_APP is done
12-07 20:26:14.927+0530 I/Tizen::App( 1290): (683) > Application count(1) in this package
12-07 20:26:14.927+0530 I/Tizen::App( 1290): (840) > Enter.
12-07 20:26:14.927+0530 I/Tizen::App( 1290): (703) > Exit.
12-07 20:26:14.927+0530 I/Tizen::App( 1290): (1584) > Package = [org.example.uibuildersingleview], Key = [end], Value = [ok], install = [96]
12-07 20:26:14.937+0530 W/HOME_APPS(  895): mainmenu-box-impl.cpp: _OnImageLoadFinishedHandler(2152) >  finished path[/opt/usr/apps/org.example.uibuildersingleview/shared/res/uibuildersingleview.png], Loading state:[1]
12-07 20:26:14.947+0530 I/Tizen::App( 1290): (416) > appName = [uibuildersingleview]
12-07 20:26:14.947+0530 I/Tizen::App( 1290): (509) > exe = [/opt/usr/apps/org.example.uibuildersingleview/bin/uibuildersingleview], displayName = [uibuildersingleview], mainApp = [1], menuIconVisible = [0], serviceApp = [0]
12-07 20:26:14.947+0530 I/Tizen::App( 1290): (2343) > info file is not existed. [/opt/usr/apps/org.exampl/info/org.example.uibuildersingleview.info]
12-07 20:26:14.947+0530 I/Tizen::App( 1290): (131) > Enter
12-07 20:26:14.947+0530 I/Tizen::App( 1290): (137) > org.example.uibuildersingleview does not have launch condition
12-07 20:26:14.947+0530 I/Tizen::App( 1290): (883) > Exit.
12-07 20:26:16.519+0530 E/PKGMGR_SERVER( 4985): pkgmgr-server.c: exit_server(1381) > exit_server Start [backend_status=1, queue_status=1, drm_status=1] 
12-07 20:26:16.519+0530 E/PKGMGR_SERVER( 4985): pkgmgr-server.c: main(2471) > package manager server terminated.
12-07 20:26:20.573+0530 W/AUL     ( 5042): launch.c: app_request_to_launchpad(396) > request cmd(0) to(org.example.uibuildersingleview)
12-07 20:26:20.573+0530 W/AUL_AMD (  664): amd_request.c: __request_handler(906) > __request_handler: 0
12-07 20:26:20.583+0530 I/AUL     (  664): menu_db_util.h: _get_app_info_from_db_by_apppath(242) > path : /usr/bin/launch_app, ret : 0
12-07 20:26:20.593+0530 I/AUL     (  664): menu_db_util.h: _get_app_info_from_db_by_apppath(242) > path : /bin/bash, ret : 0
12-07 20:26:20.593+0530 E/AUL_AMD (  664): amd_launch.c: _start_app(2499) > no caller appid info, ret: -1
12-07 20:26:20.593+0530 W/AUL_AMD (  664): amd_launch.c: _start_app(2508) > caller pid : 5042
12-07 20:26:20.593+0530 E/AUL_AMD (  664): amd_appinfo.c: appinfo_get_value(1296) > appinfo get value: Invalid argument, 19
12-07 20:26:20.603+0530 E/RESOURCED(  779): block.c: block_prelaunch_state(134) > insert data org.example.uibuildersingleview, table num : 4
12-07 20:26:20.603+0530 E/RESOURCED(  779): heart-memory.c: heart_memory_get_data(601) > hashtable heart_memory_app_list is NULL
12-07 20:26:20.603+0530 W/AUL_AMD (  664): amd_launch.c: _start_app(3052) > pad pid(-5)
12-07 20:26:20.603+0530 W/AUL_PAD ( 1521): launchpad.c: __launchpad_main_loop(520) > Launch on type-based process-pool
12-07 20:26:20.603+0530 W/AUL_PAD ( 1521): launchpad.c: __send_result_to_caller(267) > Check app launching
12-07 20:26:20.613+0530 I/CAPI_APPFW_APPLICATION( 4748): app_main.c: ui_app_main(789) > app_efl_main
12-07 20:26:20.613+0530 I/CAPI_APPFW_APPLICATION( 4748): app_main.c: _ui_app_appcore_create(641) > app_appcore_create
12-07 20:26:20.663+0530 E/TBM     ( 4748): tbm_bufmgr.c: _tbm_bufmgr_init_state(691) > USE TGL LOCK!
12-07 20:26:20.693+0530 W/AUL     (  664): app_signal.c: aul_send_app_launch_request_signal(423) > send_app_launch_signal, pid: 4748, appid: org.example.uibuildersingleview
12-07 20:26:20.693+0530 E/AUL     (  664): amd_app_group.c: app_group_start_app(1037) > app_group_start_app
12-07 20:26:20.703+0530 W/AUL     ( 5042): launch.c: app_request_to_launchpad(425) > request cmd(0) result(4748)
12-07 20:26:20.873+0530 I/APP_CORE( 4748): appcore-efl.c: __do_app(514) > [APP 4748] Event: RESET State: CREATED
12-07 20:26:20.873+0530 I/CAPI_APPFW_APPLICATION( 4748): app_main.c: _ui_app_appcore_reset(723) > app_appcore_reset
12-07 20:26:20.883+0530 E/EFL     ( 4748): edje<4748> lib/edje/edje_embryo.c:4134 _edje_embryo_test_run() You are running Embryo->EDC->Embryo with script program '_p26';
12-07 20:26:20.883+0530 E/EFL     ( 4748): By the power of Grayskull, your previous Embryo stack is now broken!
12-07 20:26:20.893+0530 E/EFL     ( 4748): edje<4748> lib/edje/edje_embryo.c:4134 _edje_embryo_test_run() You are running Embryo->EDC->Embryo with script program '_p26';
12-07 20:26:20.893+0530 E/EFL     ( 4748): By the power of Grayskull, your previous Embryo stack is now broken!
12-07 20:26:20.893+0530 E/EFL     ( 4748): edje<4748> lib/edje/edje_embryo.c:4134 _edje_embryo_test_run() You are running Embryo->EDC->Embryo with script program '_p26';
12-07 20:26:20.893+0530 E/EFL     ( 4748): By the power of Grayskull, your previous Embryo stack is now broken!
12-07 20:26:20.903+0530 W/APP_CORE( 4748): appcore-efl.c: __show_cb(920) > [EVENT_TEST][EVENT] GET SHOW EVENT!!!. WIN:5400002
12-07 20:26:20.903+0530 W/AUL_AMD (  664): amd_request.c: __request_handler(906) > __request_handler: 35
12-07 20:26:20.903+0530 E/EFL     (  330): eo<330> lib/eo/eo.c:676 _eo_call_resolve() in lib/edje/edje_object.eo.c:316: func 'edje_obj_signal_emit' (415) could not be resolved for class 'Evas_Object_Smart'.
12-07 20:26:20.903+0530 E/EFL     (  330): eo<330> lib/eo/eo.c:676 _eo_call_resolve() in lib/edje/edje_object.eo.c:316: func 'edje_obj_signal_emit' (415) could not be resolved for class 'Evas_Object_Smart'.
12-07 20:26:20.923+0530 I/Tizen::System( 1290): (259) > Active app [org.exampl], current [com.samsun] 
12-07 20:26:20.923+0530 I/Tizen::Io( 1290): (729) > Entry not found
12-07 20:26:20.933+0530 I/Tizen::System( 1290): (157) > change brightness system value.
12-07 20:26:20.933+0530 W/AUL_AMD (  664): amd_request.c: __request_handler(906) > __request_handler: 15
12-07 20:26:20.983+0530 I/APP_CORE(  895): appcore-efl.c: __do_app(514) > [APP 895] Event: PAUSE State: RUNNING
12-07 20:26:20.983+0530 I/CAPI_APPFW_APPLICATION(  895): app_main.c: _ui_app_appcore_pause(689) > app_appcore_pause
12-07 20:26:20.983+0530 E/cluster-home(  895): homescreen.cpp: OnPause(260) >  app pause
12-07 20:26:20.983+0530 W/AUL_AMD (  664): amd_launch.c: __e17_status_handler(3328) > pid(895) status(4)
12-07 20:26:20.983+0530 W/AUL     (  664): amd_app_group.c: __set_fg_flag(190) > send_signal BG com.samsung.homescreen(895)
12-07 20:26:20.983+0530 W/AUL     (  664): app_signal.c: aul_send_app_status_change_signal(581) > send_app_status_change_signal, pid: 895, appid: com.samsung.homescreen, status: bg
12-07 20:26:20.993+0530 W/AUL_AMD (  664): amd_launch.c: __e17_status_handler(3328) > pid(4748) status(3)
12-07 20:26:20.993+0530 W/AUL_AMD (  664): amd_key.c: _key_ungrab(269) > fail(-1) to ungrab key(XF86Back)
12-07 20:26:20.993+0530 W/AUL_AMD (  664): amd_launch.c: __e17_status_handler(3349) > back key ungrab error
12-07 20:26:20.993+0530 W/AUL     (  664): amd_app_group.c: __set_fg_flag(180) > send_signal FG org.example.uibuildersingleview(4748)
12-07 20:26:20.993+0530 W/AUL     (  664): app_signal.c: aul_send_app_status_change_signal(581) > send_app_status_change_signal, pid: 4748, appid: org.example.uibuildersingleview, status: fg
12-07 20:26:20.993+0530 I/APP_CORE( 4748): appcore-efl.c: __do_app(514) > [APP 4748] Event: RESUME State: CREATED
12-07 20:26:21.013+0530 I/APP_CORE( 4748): appcore-efl.c: __do_app(623) > Legacy lifecycle: 0
12-07 20:26:21.013+0530 I/APP_CORE( 4748): appcore-efl.c: __do_app(625) > [APP 4748] Initial Launching, call the resume_cb
12-07 20:26:21.013+0530 I/CAPI_APPFW_APPLICATION( 4748): app_main.c: _ui_app_appcore_resume(706) > app_appcore_resume
12-07 20:26:21.364+0530 W/AUL_AMD (  664): amd_launch.c: __e17_status_handler(3328) > pid(4748) status(0)
12-07 20:26:21.744+0530 I/Tizen::App( 1290): (499) > LaunchedApp(org.example.uibuildersingleview)
12-07 20:26:21.744+0530 E/RESOURCED(  779): resourced-dbus.c: resourced_dbus_system_hash_drop_busname(324) > Does not exist in busname hash: :1.410
12-07 20:26:21.754+0530 I/Tizen::App( 1290): (733) > Finished invoking application event listener for org.example.uibuildersingleview, 4748.
12-07 20:26:22.485+0530 I/UXT     ( 5056): Uxt_ObjectManager.cpp: OnInitialized(737) > Initialized.
12-07 20:26:22.935+0530 E/EFL     ( 4748): ecore_x<4748> lib/ecore_x/xlib/ecore_x_events.c:603 _ecore_x_event_handle_button_press() ButtonEvent:press time=8935419
12-07 20:26:23.035+0530 E/EFL     ( 4748): ecore_x<4748> lib/ecore_x/xlib/ecore_x_events.c:756 _ecore_x_event_handle_button_release() ButtonEvent:release time=8935519
12-07 20:26:23.035+0530 E/VCONF   ( 4748): vconf.c: _vconf_check_retry_err(1368) > db/ise/keysound : check retry err (-21/13).
12-07 20:26:23.035+0530 E/VCONF   ( 4748): vconf.c: _vconf_get_key_filesys(2371) > _vconf_get_key_filesys(db/ise/keysound) step(-21) failed(13 / Permission denied) retry(0) 
12-07 20:26:23.035+0530 E/VCONF   ( 4748): vconf.c: _vconf_get_key(2407) > _vconf_get_key(db/ise/keysound) step(-21) failed(13 / Permission denied)
12-07 20:26:23.035+0530 E/VCONF   ( 4748): vconf.c: vconf_get_bool(2729) > vconf_get_bool(4748) : db/ise/keysound error
12-07 20:26:23.035+0530 E/VCONF   ( 4748): vconf-kdb.c: _vconf_kdb_add_notify(318) > _vconf_kdb_add_notify : add noti(Permission denied)
12-07 20:26:23.035+0530 E/VCONF   ( 4748): vconf.c: vconf_notify_key_changed(3168) > vconf_notify_key_changed : key(db/ise/keysound) add notify fail
12-07 20:26:23.826+0530 E/EFL     ( 4748): ecore_x<4748> lib/ecore_x/xlib/ecore_x_events.c:603 _ecore_x_event_handle_button_press() ButtonEvent:press time=8936305
12-07 20:26:23.886+0530 E/EFL     ( 4748): ecore_x<4748> lib/ecore_x/xlib/ecore_x_events.c:756 _ecore_x_event_handle_button_release() ButtonEvent:release time=8936371
12-07 20:26:24.326+0530 E/EFL     ( 4748): ecore_x<4748> lib/ecore_x/xlib/ecore_x_events.c:603 _ecore_x_event_handle_button_press() ButtonEvent:press time=8936814
12-07 20:26:24.376+0530 E/EFL     ( 4748): ecore_x<4748> lib/ecore_x/xlib/ecore_x_events.c:756 _ecore_x_event_handle_button_release() ButtonEvent:release time=8936868
12-07 20:26:25.998+0530 I/APP_CORE(  895): appcore-efl.c: __do_app(514) > [APP 895] Event: MEM_FLUSH State: PAUSED
12-07 20:26:26.028+0530 I/CAPI_APPFW_APPLICATION(  895): app_main.c: _ui_app_appcore_rotation_event(485) > _ui_app_appcore_rotation_event
12-07 20:26:26.088+0530 I/CAPI_APPFW_APPLICATION( 4748): app_main.c: _ui_app_appcore_rotation_event(485) > _ui_app_appcore_rotation_event
12-07 20:26:26.088+0530 I/CAPI_APPFW_APPLICATION(  895): app_main.c: _ui_app_appcore_rotation_event(485) > _ui_app_appcore_rotation_event
12-07 20:26:42.644+0530 I/CAPI_APPFW_APPLICATION( 4748): app_main.c: _ui_app_appcore_rotation_event(485) > _ui_app_appcore_rotation_event
12-07 20:26:42.644+0530 I/CAPI_APPFW_APPLICATION(  895): app_main.c: _ui_app_appcore_rotation_event(485) > _ui_app_appcore_rotation_event
12-07 20:26:42.885+0530 I/CAPI_APPFW_APPLICATION( 4748): app_main.c: _ui_app_appcore_rotation_event(485) > _ui_app_appcore_rotation_event
12-07 20:26:42.885+0530 I/CAPI_APPFW_APPLICATION(  895): app_main.c: _ui_app_appcore_rotation_event(485) > _ui_app_appcore_rotation_event
12-07 20:26:42.945+0530 I/CAPI_APPFW_APPLICATION( 4748): app_main.c: _ui_app_appcore_rotation_event(485) > _ui_app_appcore_rotation_event
12-07 20:26:42.945+0530 I/CAPI_APPFW_APPLICATION(  895): app_main.c: _ui_app_appcore_rotation_event(485) > _ui_app_appcore_rotation_event
12-07 20:26:47.929+0530 I/CAPI_APPFW_APPLICATION( 4748): app_main.c: _ui_app_appcore_rotation_event(485) > _ui_app_appcore_rotation_event
12-07 20:26:47.929+0530 I/CAPI_APPFW_APPLICATION(  895): app_main.c: _ui_app_appcore_rotation_event(485) > _ui_app_appcore_rotation_event
12-07 20:26:48.020+0530 E/EFL     ( 4748): edje<4748> lib/edje/edje_embryo.c:4134 _edje_embryo_test_run() You are running Embryo->EDC->Embryo with script program '_p26';
12-07 20:26:48.020+0530 E/EFL     ( 4748): By the power of Grayskull, your previous Embryo stack is now broken!
12-07 20:26:48.110+0530 I/CAPI_APPFW_APPLICATION(  895): app_main.c: _ui_app_appcore_rotation_event(485) > _ui_app_appcore_rotation_event
12-07 20:26:48.200+0530 E/EFL     (  894): eo<894> lib/eo/eo.c:676 _eo_call_resolve() in lib/edje/edje_object.eo.c:164: func 'edje_obj_part_geometry_get' (762) could not be resolved for class 'Elm_Layout'.
12-07 20:26:48.340+0530 I/CAPI_APPFW_APPLICATION( 4748): app_main.c: _ui_app_appcore_rotation_event(485) > _ui_app_appcore_rotation_event
12-07 20:26:48.400+0530 E/EFL     (  330): elementary<330> elm_transit.c:648 elm_transit_del_cb_set() Elm_Transit transit has already been deleted!
12-07 20:26:52.904+0530 I/CAPI_APPFW_APPLICATION( 4748): app_main.c: _ui_app_appcore_rotation_event(485) > _ui_app_appcore_rotation_event
12-07 20:26:52.904+0530 I/CAPI_APPFW_APPLICATION(  895): app_main.c: _ui_app_appcore_rotation_event(485) > _ui_app_appcore_rotation_event
12-07 20:26:52.984+0530 E/EFL     ( 4748): edje<4748> lib/edje/edje_embryo.c:4134 _edje_embryo_test_run() You are running Embryo->EDC->Embryo with script program '_p26';
12-07 20:26:52.984+0530 E/EFL     ( 4748): By the power of Grayskull, your previous Embryo stack is now broken!
12-07 20:26:53.054+0530 E/EFL     (  894): eo<894> lib/eo/eo.c:676 _eo_call_resolve() in lib/edje/edje_object.eo.c:164: func 'edje_obj_part_geometry_get' (762) could not be resolved for class 'Elm_Layout'.
12-07 20:26:53.255+0530 E/EFL     (  330): elementary<330> elm_transit.c:648 elm_transit_del_cb_set() Elm_Transit transit has already been deleted!
12-07 20:26:53.565+0530 I/CAPI_APPFW_APPLICATION( 4748): app_main.c: _ui_app_appcore_rotation_event(485) > _ui_app_appcore_rotation_event
12-07 20:26:53.565+0530 I/CAPI_APPFW_APPLICATION(  895): app_main.c: _ui_app_appcore_rotation_event(485) > _ui_app_appcore_rotation_event
12-07 20:26:59.220+0530 E/EFL     ( 4748): ecore_x<4748> lib/ecore_x/xlib/ecore_x_events.c:603 _ecore_x_event_handle_button_press() ButtonEvent:press time=8971702
12-07 20:26:59.311+0530 E/EFL     ( 4748): ecore_x<4748> lib/ecore_x/xlib/ecore_x_events.c:756 _ecore_x_event_handle_button_release() ButtonEvent:release time=8971790
12-07 20:26:59.431+0530 I/AUL_PAD ( 1521): sigchild.h: __launchpad_process_sigchld(162) > dead_pid = 4748 pgid = 4748
12-07 20:26:59.431+0530 I/AUL_PAD ( 1521): sigchild.h: __sigchild_action(143) > dead_pid(4748)
12-07 20:26:59.441+0530 E/EFL     (  330): eo<330> lib/eo/eo.c:676 _eo_call_resolve() in lib/edje/edje_object.eo.c:316: func 'edje_obj_signal_emit' (415) could not be resolved for class 'Evas_Object_Smart'.
12-07 20:26:59.471+0530 W/AUL_AMD (  664): amd_launch.c: __e17_status_handler(3328) > pid(895) status(3)
12-07 20:26:59.481+0530 W/AUL_AMD (  664): amd_key.c: _key_ungrab(269) > fail(-1) to ungrab key(XF86Back)
12-07 20:26:59.481+0530 W/AUL_AMD (  664): amd_launch.c: __e17_status_handler(3349) > back key ungrab error
12-07 20:26:59.481+0530 W/AUL     (  664): amd_app_group.c: __set_fg_flag(180) > send_signal FG com.samsung.homescreen(895)
12-07 20:26:59.481+0530 W/AUL     (  664): app_signal.c: aul_send_app_status_change_signal(581) > send_app_status_change_signal, pid: 895, appid: com.samsung.homescreen, status: fg
12-07 20:26:59.511+0530 I/AUL_PAD ( 1521): sigchild.h: __sigchild_action(149) > __send_app_dead_signal(0)
12-07 20:26:59.511+0530 I/AUL_PAD ( 1521): sigchild.h: __launchpad_process_sigchld(170) > after __sigchild_action
12-07 20:26:59.511+0530 E/AUL_PAD ( 1521): launchpad.c: main(698) > error reading sigchld info
12-07 20:26:59.511+0530 I/ESD     (  941): esd_main.c: __esd_app_dead_handler(1773) > pid: 4748
12-07 20:26:59.511+0530 I/Tizen::App( 1290): (243) > App[org.example.uibuildersingleview] pid[4748] terminate event is forwarded
12-07 20:26:59.511+0530 I/Tizen::System( 1290): (256) > osp.accessorymanager.service provider is found.
12-07 20:26:59.511+0530 I/Tizen::System( 1290): (196) > Accessory Owner is removed [org.example.uibuildersingleview, 4748, ]
12-07 20:26:59.511+0530 I/Tizen::System( 1290): (256) > osp.system.service provider is found.
12-07 20:26:59.511+0530 I/Tizen::App( 1290): (506) > TerminatedApp(org.example.uibuildersingleview)
12-07 20:26:59.511+0530 I/Tizen::App( 1290): (512) > Not registered pid(4748)
12-07 20:26:59.511+0530 I/Tizen::System( 1290): (246) > Terminated app [org.example.uibuildersingleview]
12-07 20:26:59.511+0530 I/Tizen::Io( 1290): (729) > Entry not found
12-07 20:26:59.511+0530 W/AUL_AMD (  664): amd_main.c: __app_dead_handler(327) > __app_dead_handler, pid: 4748
12-07 20:26:59.511+0530 W/AUL_AMD (  664): amd_main.c: __app_dead_handler(333) > app_group_leader_app, pid: 4748
12-07 20:26:59.511+0530 E/RESOURCED(  779): resourced-dbus.c: resourced_dbus_system_hash_drop_busname(324) > Does not exist in busname hash: :1.414
12-07 20:26:59.521+0530 W/AUL_AMD (  664): amd_launch.c: __e17_status_handler(3328) > pid(895) status(0)
12-07 20:26:59.531+0530 I/APP_CORE(  895): appcore-efl.c: __do_app(514) > [APP 895] Event: RESUME State: PAUSED
12-07 20:26:59.531+0530 I/CAPI_APPFW_APPLICATION(  895): app_main.c: _ui_app_appcore_resume(706) > app_appcore_resume
12-07 20:26:59.531+0530 E/cluster-home(  895): homescreen.cpp: OnResume(233) >  app resume
12-07 20:26:59.531+0530 E/EFL     (  330): eo<330> lib/eo/eo.c:676 _eo_call_resolve() in lib/edje/edje_object.eo.c:316: func 'edje_obj_signal_emit' (415) could not be resolved for class 'Evas_Object_Smart'.
12-07 20:26:59.531+0530 I/Tizen::System( 1290): (157) > change brightness system value.
12-07 20:26:59.531+0530 I/Tizen::App( 1290): (782) > Finished invoking application event listener for org.example.uibuildersingleview, 4748.
12-07 20:26:59.571+0530 I/Tizen::System( 1290): (259) > Active app [com.samsun], current [org.exampl] 
12-07 20:26:59.571+0530 I/Tizen::Io( 1290): (729) > Entry not found
12-07 20:26:59.581+0530 I/TIZEN_N_SOUND_MANAGER(  961): sound_manager.c: sound_manager_get_volume(84) > returns : type=0, volume=8, ret=0x0
12-07 20:26:59.581+0530 E/TIZEN_N_SOUND_MANAGER(  961): sound_manager_private.c: __convert_sound_manager_error_code(142) > [sound_manager_get_volume] ERROR_NONE(0x00000000) : core frameworks error code(0x00000000)
12-07 20:26:59.581+0530 I/TIZEN_N_SOUND_MANAGER(  961): sound_manager.c: sound_manager_get_volume(84) > returns : type=4, volume=0, ret=0x0
12-07 20:26:59.581+0530 E/TIZEN_N_SOUND_MANAGER(  961): sound_manager_private.c: __convert_sound_manager_error_code(142) > [sound_manager_get_volume] ERROR_NONE(0x00000000) : core frameworks error code(0x00000000)
12-07 20:26:59.591+0530 W/AUL_AMD (  664): amd_request.c: __request_handler(906) > __request_handler: 15
12-07 20:26:59.611+0530 I/Tizen::System( 1290): (157) > change brightness system value.
12-07 20:26:59.611+0530 W/CRASH_MANAGER( 5064): worker.c: worker_job(1199) > 1104748756962148112261
