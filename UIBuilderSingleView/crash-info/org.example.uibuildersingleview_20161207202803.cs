S/W Version Information
Model: SM-Z300H
Tizen-Version: 2.4.0.3
Build-Number: Z300HDDU0BPI2
Build-Date: 2016.09.30 15:37:12

Crash Information
Process Name: uibuildersingleview
PID: 5056
Date: 2016-12-07 20:28:03+0530
Executable File Path: /opt/usr/apps/org.example.uibuildersingleview/bin/uibuildersingleview
Signal: 11
      (SIGSEGV)
      si_code: -6
      signal sent by tkill (sent by pid 5056, uid 5000)

Register Information
r0   = 0xb866d080, r1   = 0x0000000c
r2   = 0x0000003c, r3   = 0x0000000c
r4   = 0xb85575c8, r5   = 0xb85c7e68
r6   = 0x80012493, r7   = 0xb6e1b488
r8   = 0xb85ca2f8, r9   = 0xb866d080
r10  = 0x80012493, fp   = 0xb85bf180
ip   = 0xb6a2d4d4, sp   = 0xbed1df98
lr   = 0xb6a0460d, pc   = 0xb67406f2
cpsr = 0x200e0030

Memory Information
MemTotal:   987012 KB
MemFree:    177064 KB
Buffers:     44196 KB
Cached:     318028 KB
VmPeak:     127552 KB
VmSize:     127548 KB
VmLck:           0 KB
VmPin:           0 KB
VmHWM:       25952 KB
VmRSS:       25952 KB
VmData:      44876 KB
VmStk:         136 KB
VmExe:          20 KB
VmLib:       35600 KB
VmPTE:          94 KB
VmSwap:          0 KB

Threads Information
Threads: 5
PID = 5056 TID = 5056
5056 5057 5361 5362 5371 

Maps Information
af6ee000 afeed000 rwxp [stack:5371]
b1524000 b152c000 r-xp /usr/lib/ecore_evas/engines/extn/v-1.13/module.so
b153e000 b1d3d000 rwxp [stack:5362]
b1d3d000 b1d3e000 r-xp /usr/lib/edje/modules/feedback/v-1.13/module.so
b1d4e000 b1d62000 r-xp /usr/lib/edje/modules/elm/v-1.13/module.so
b1d76000 b1d77000 r-xp /usr/lib/libX11-xcb.so.1.0.0
b1d87000 b1d8a000 r-xp /usr/lib/libxcb-sync.so.1.0.0
b1d9b000 b1d9c000 r-xp /usr/lib/libxshmfence.so.1.0.0
b1dac000 b1dae000 r-xp /usr/lib/libxcb-present.so.0.0.0
b1dbe000 b1dc0000 r-xp /usr/lib/libxcb-dri3.so.0.0.0
b1dd0000 b1de0000 r-xp /usr/lib/evas/modules/engines/software_x11/v-1.13/module.so
b1df0000 b1dfc000 r-xp /usr/lib/ecore_evas/engines/x/v-1.13/module.so
b1e0e000 b260d000 rwxp [stack:5361]
b280d000 b2814000 r-xp /usr/lib/libefl-extension.so.0.1.0
b2827000 b282d000 r-xp /usr/lib/bufmgr/libtbm_sprd7727.so.0.0.0
b283d000 b2842000 r-xp /opt/usr/apps/org.example.uibuildersingleview/bin/uibuildersingleview
b2993000 b2a76000 r-xp /usr/lib/libscim-1.0.so.8.2.3
b2aad000 b2ad5000 r-xp /usr/lib/ecore_imf/modules/isf/v-1.13/module.so
b2ae7000 b32e6000 rwxp [stack:5057]
b32e6000 b32e8000 r-xp /usr/lib/ecore/system/systemd/v-1.13/module.so
b32f8000 b3302000 r-xp /lib/libnss_files-2.20-2014.11.so
b3313000 b331c000 r-xp /lib/libnss_nis-2.20-2014.11.so
b332d000 b333e000 r-xp /lib/libnsl-2.20-2014.11.so
b3351000 b3357000 r-xp /lib/libnss_compat-2.20-2014.11.so
b3368000 b3369000 r-xp /usr/lib/osp/libappinfo.so.1.2.2.1
b3391000 b3398000 r-xp /usr/lib/libminizip.so.1.0.0
b33a8000 b33ad000 r-xp /usr/lib/libstorage.so.0.1
b33bd000 b341c000 r-xp /usr/lib/libmmfcamcorder.so.0.0.0
b3432000 b3446000 r-xp /usr/lib/libcapi-media-camera.so.0.1.90
b3456000 b349a000 r-xp /usr/lib/libgstbase-1.0.so.0.405.0
b34aa000 b34b2000 r-xp /usr/lib/libgstapp-1.0.so.0.405.0
b34c2000 b34f2000 r-xp /usr/lib/libgstvideo-1.0.so.0.405.0
b3505000 b35be000 r-xp /usr/lib/libgstreamer-1.0.so.0.405.0
b35d2000 b3625000 r-xp /usr/lib/libmmfplayer.so.0.0.0
b3636000 b3651000 r-xp /usr/lib/libcapi-media-player.so.0.2.16
b3661000 b3722000 r-xp /usr/lib/libprotobuf.so.9.0.1
b3735000 b3745000 r-xp /usr/lib/libefl-assist.so.0.1.0
b3755000 b3762000 r-xp /usr/lib/libmdm-common.so.1.0.98
b3773000 b377a000 r-xp /usr/lib/libcapi-media-tool.so.0.2.2
b378a000 b37cb000 r-xp /usr/lib/libmdm.so.1.2.12
b37db000 b37e3000 r-xp /usr/lib/lib_DNSe_NRSS_ver225.so
b37f2000 b3802000 r-xp /usr/lib/lib_SamsungRec_TizenV04014.so
b3823000 b3883000 r-xp /usr/lib/libasound.so.2.0.0
b3895000 b3898000 r-xp /usr/lib/libpulse-simple.so.0.0.4
b38a8000 b38ab000 r-xp /usr/lib/libsyspopup_caller.so.0.1.0
b38bb000 b38c0000 r-xp /usr/lib/libmmfsoundcommon.so.0.0.0
b38d0000 b38d1000 r-xp /usr/lib/libgthread-2.0.so.0.4301.0
b38e1000 b38ec000 r-xp /usr/lib/libaudio-session-mgr.so.0.0.1
b3900000 b3907000 r-xp /usr/lib/libmmutil_imgp.so.0.0.0
b3917000 b391d000 r-xp /usr/lib/libmmutil_jpeg.so.0.0.0
b392d000 b3932000 r-xp /usr/lib/libmmfsession.so.0.0.1
b3942000 b395d000 r-xp /usr/lib/libmmfsound.so.0.1.0
b396d000 b3974000 r-xp /usr/lib/libmmfcommon.so.0.0.0
b3984000 b3987000 r-xp /usr/lib/libcapi-media-image-util.so.0.1.10
b3998000 b39c6000 r-xp /usr/lib/libidn.so.11.5.44
b39d6000 b39ec000 r-xp /usr/lib/libnghttp2.so.5.4.0
b39fd000 b3a07000 r-xp /usr/lib/libcares.so.2.1.0
b3a17000 b3a21000 r-xp /usr/lib/libcapi-media-sound-manager.so.0.3.32
b3a31000 b3a33000 r-xp /usr/lib/libcapi-media-wav-player.so.0.1.18
b3a43000 b3a44000 r-xp /usr/lib/libmmfkeysound.so.0.0.0
b3a54000 b3a58000 r-xp /usr/lib/libecore_ipc.so.1.13.0
b3a69000 b3a91000 r-xp /usr/lib/libui-extension.so.0.1.0
b3aa2000 b3acb000 r-xp /usr/lib/libturbojpeg.so
b3aeb000 b3af1000 r-xp /usr/lib/libgif.so.4.1.6
b3b01000 b3b47000 r-xp /usr/lib/libcurl.so.4.3.0
b3b58000 b3b79000 r-xp /usr/lib/libexif.so.12.3.3
b3b94000 b3ba9000 r-xp /usr/lib/libtts.so
b3bba000 b3bc2000 r-xp /usr/lib/libfeedback.so.0.1.4
b3bd2000 b3c98000 r-xp /usr/lib/libdali-core.so.0.0.0
b3cb8000 b3db0000 r-xp /usr/lib/libdali-adaptor.so.0.0.0
b3dcf000 b3e9d000 r-xp /usr/lib/libdali-toolkit.so.0.0.0
b3eb4000 b3eb6000 r-xp /usr/lib/libboost_system.so.1.51.0
b3ec6000 b3ecc000 r-xp /usr/lib/libboost_chrono.so.1.51.0
b3edc000 b3eff000 r-xp /usr/lib/libboost_thread.so.1.51.0
b3f10000 b3f12000 r-xp /usr/lib/libappsvc.so.0.1.0
b3f22000 b3f24000 r-xp /usr/lib/libcapi-appfw-app-common.so.0.3.1.0
b3f35000 b3f3a000 r-xp /usr/lib/libcapi-appfw-app-control.so.0.3.1.0
b3f51000 b3f53000 r-xp /usr/lib/libosp-env-config.so.1.2.2.1
b3f63000 b3f6a000 r-xp /usr/lib/libsensord-share.so
b3f7a000 b3f92000 r-xp /usr/lib/libsensor.so.1.1.0
b3fa3000 b3fa6000 r-xp /usr/lib/libXv.so.1.0.0
b3fb6000 b3fbb000 r-xp /usr/lib/libutilX.so.1.1.0
b3fcb000 b3fd0000 r-xp /usr/lib/libappcore-common.so.1.1
b3fe0000 b3fe7000 r-xp /usr/lib/libcapi-ui-efl-util.so.0.2.11
b3ffa000 b3ffe000 r-xp /usr/lib/libcapi-appfw-application.so.0.3.1.0
b400f000 b40ed000 r-xp /usr/lib/libCOREGL.so.4.0
b410d000 b4110000 r-xp /usr/lib/libuuid.so.1.3.0
b4120000 b4137000 r-xp /usr/lib/libblkid.so.1.1.0
b4148000 b414a000 r-xp /usr/lib/libXau.so.6.0.0
b415a000 b41a1000 r-xp /usr/lib/pulseaudio/libpulsecommon-4.0.so
b41b3000 b41b9000 r-xp /usr/lib/libjson-c.so.2.0.1
b41ca000 b41ce000 r-xp /usr/lib/libogg.so.0.7.1
b41de000 b4200000 r-xp /usr/lib/libvorbis.so.0.4.3
b4210000 b42f4000 r-xp /usr/lib/libvorbisenc.so.2.0.6
b4310000 b4313000 r-xp /usr/lib/libEGL.so.1.4
b4324000 b432a000 r-xp /usr/lib/libxcb-render.so.0.0.0
b433a000 b433c000 r-xp /usr/lib/libxcb-shm.so.0.0.0
b434c000 b4359000 r-xp /usr/lib/libGLESv2.so.2.0
b436a000 b43cc000 r-xp /usr/lib/libpixman-1.so.0.28.2
b43e1000 b43f9000 r-xp /usr/lib/libmount.so.1.1.0
b440b000 b441f000 r-xp /usr/lib/libxcb.so.1.1.0
b442f000 b4436000 r-xp /lib/libcrypt-2.20-2014.11.so
b446e000 b4470000 r-xp /usr/lib/libiri.so
b4480000 b448b000 r-xp /usr/lib/libgpg-error.so.0.15.0
b449c000 b44d2000 r-xp /usr/lib/libpulse.so.0.16.2
b44e3000 b4526000 r-xp /usr/lib/libsndfile.so.1.0.25
b453b000 b4550000 r-xp /lib/libexpat.so.1.5.2
b4562000 b4620000 r-xp /usr/lib/libcairo.so.2.11200.14
b4634000 b463c000 r-xp /usr/lib/libdrm.so.2.4.0
b464c000 b464f000 r-xp /usr/lib/libdri2.so.0.0.0
b465f000 b46ad000 r-xp /usr/lib/libssl.so.1.0.0
b46c2000 b46ce000 r-xp /usr/lib/libeeze.so.1.13.0
b46df000 b46e8000 r-xp /usr/lib/libethumb.so.1.13.0
b46f8000 b46fb000 r-xp /usr/lib/libecore_input_evas.so.1.13.0
b470b000 b48c2000 r-xp /usr/lib/libcrypto.so.1.0.0
b56ad000 b56b6000 r-xp /usr/lib/libXi.so.6.1.0
b56c6000 b56c8000 r-xp /usr/lib/libXgesture.so.7.0.0
b56d8000 b56dc000 r-xp /usr/lib/libXtst.so.6.1.0
b56ec000 b56f2000 r-xp /usr/lib/libXrender.so.1.3.0
b5702000 b5708000 r-xp /usr/lib/libXrandr.so.2.2.0
b5718000 b571a000 r-xp /usr/lib/libXinerama.so.1.0.0
b572b000 b572e000 r-xp /usr/lib/libXfixes.so.3.1.0
b573e000 b5749000 r-xp /usr/lib/libXext.so.6.4.0
b5759000 b575b000 r-xp /usr/lib/libXdamage.so.1.1.0
b576b000 b576d000 r-xp /usr/lib/libXcomposite.so.1.0.0
b577d000 b585f000 r-xp /usr/lib/libX11.so.6.3.0
b5873000 b587a000 r-xp /usr/lib/libXcursor.so.1.0.2
b588a000 b58a2000 r-xp /usr/lib/libudev.so.1.6.0
b58a4000 b58a7000 r-xp /lib/libattr.so.1.1.0
b58b7000 b58d7000 r-xp /usr/lib/libsecurity-server-commons.so.1.0.0
b58d8000 b58dd000 r-xp /usr/lib/libffi.so.6.0.2
b58ee000 b5906000 r-xp /lib/libz.so.1.2.8
b5916000 b5918000 r-xp /usr/lib/libgmodule-2.0.so.0.4301.0
b5928000 b59fd000 r-xp /usr/lib/libxml2.so.2.9.2
b5a12000 b5aad000 r-xp /usr/lib/libstdc++.so.6.0.20
b5ac9000 b5acc000 r-xp /usr/lib/libSLP-db-util.so.0.1.0
b5adc000 b5af5000 r-xp /usr/lib/libpkgmgr_parser.so.0.1.0
b5b06000 b5b17000 r-xp /lib/libresolv-2.20-2014.11.so
b5b2b000 b5ba5000 r-xp /usr/lib/libgcrypt.so.20.0.3
b5bba000 b5bbc000 r-xp /usr/lib/libecore_imf_evas.so.1.13.0
b5bcc000 b5bd3000 r-xp /usr/lib/libembryo.so.1.13.0
b5be3000 b5bed000 r-xp /usr/lib/libecore_audio.so.1.13.0
b5bfe000 b5c16000 r-xp /usr/lib/libpng12.so.0.50.0
b5c27000 b5c4a000 r-xp /usr/lib/libjpeg.so.8.0.2
b5c6b000 b5c7f000 r-xp /usr/lib/libector.so.1.13.0
b5c90000 b5ca8000 r-xp /usr/lib/liblua-5.1.so
b5cb9000 b5d10000 r-xp /usr/lib/libfreetype.so.6.11.3
b5d24000 b5d4c000 r-xp /usr/lib/libfontconfig.so.1.8.0
b5d5d000 b5d70000 r-xp /usr/lib/libfribidi.so.0.3.1
b5d81000 b5dbb000 r-xp /usr/lib/libharfbuzz.so.0.940.0
b5dcc000 b5dda000 r-xp /usr/lib/libgraphics-extension.so.0.1.0
b5dea000 b5df2000 r-xp /usr/lib/libtbm.so.1.0.0
b5e02000 b5e0f000 r-xp /usr/lib/libeio.so.1.13.0
b5e1f000 b5e21000 r-xp /usr/lib/libefreet_trash.so.1.13.0
b5e31000 b5e36000 r-xp /usr/lib/libefreet_mime.so.1.13.0
b5e46000 b5e5d000 r-xp /usr/lib/libefreet.so.1.13.0
b5e6f000 b5e8f000 r-xp /usr/lib/libeldbus.so.1.13.0
b5e9f000 b5ebf000 r-xp /usr/lib/libecore_con.so.1.13.0
b5ec1000 b5ec7000 r-xp /usr/lib/libecore_imf.so.1.13.0
b5ed7000 b5ee8000 r-xp /usr/lib/libemotion.so.1.13.0
b5ef9000 b5f00000 r-xp /usr/lib/libethumb_client.so.1.13.0
b5f10000 b5f1f000 r-xp /usr/lib/libeo.so.1.13.0
b5f30000 b5f42000 r-xp /usr/lib/libecore_input.so.1.13.0
b5f53000 b5f58000 r-xp /usr/lib/libecore_file.so.1.13.0
b5f68000 b5f81000 r-xp /usr/lib/libecore_evas.so.1.13.0
b5f91000 b5fae000 r-xp /usr/lib/libeet.so.1.13.0
b5fc7000 b600f000 r-xp /usr/lib/libeina.so.1.13.0
b6020000 b6030000 r-xp /usr/lib/libefl.so.1.13.0
b6041000 b6126000 r-xp /usr/lib/libicuuc.so.51.1
b6143000 b6283000 r-xp /usr/lib/libicui18n.so.51.1
b629a000 b62d2000 r-xp /usr/lib/libecore_x.so.1.13.0
b62e4000 b62e7000 r-xp /lib/libcap.so.2.21
b62f7000 b6320000 r-xp /usr/lib/libsecurity-server-client.so.1.0.1
b6331000 b6338000 r-xp /usr/lib/libcapi-base-common.so.0.2.2
b634a000 b6381000 r-xp /usr/lib/libgobject-2.0.so.0.4301.0
b6392000 b647d000 r-xp /usr/lib/libgio-2.0.so.0.4301.0
b6490000 b6509000 r-xp /usr/lib/libsqlite3.so.0.8.6
b651b000 b6520000 r-xp /usr/lib/libcapi-system-info.so.0.2.1
b6530000 b653a000 r-xp /usr/lib/libvconf.so.0.2.45
b654a000 b654c000 r-xp /usr/lib/libvasum.so.0.3.1
b655c000 b655e000 r-xp /usr/lib/libttrace.so.1.1
b656e000 b6571000 r-xp /usr/lib/libiniparser.so.0
b6581000 b65a7000 r-xp /usr/lib/libpkgmgr-info.so.0.0.17
b65b7000 b65bc000 r-xp /usr/lib/libxdgmime.so.1.1.0
b65cd000 b65e4000 r-xp /usr/lib/libdbus-glib-1.so.2.2.2
b65f5000 b6660000 r-xp /lib/libm-2.20-2014.11.so
b6671000 b6677000 r-xp /lib/librt-2.20-2014.11.so
b6688000 b6695000 r-xp /usr/lib/libunwind.so.8.0.1
b66cb000 b67ef000 r-xp /lib/libc-2.20-2014.11.so
b6804000 b681d000 r-xp /lib/libgcc_s-4.9.so.1
b682d000 b690f000 r-xp /usr/lib/libglib-2.0.so.0.4301.0
b6920000 b694a000 r-xp /usr/lib/libdbus-1.so.3.8.12
b695b000 b6997000 r-xp /usr/lib/libsystemd.so.0.4.0
b6999000 b6a1c000 r-xp /usr/lib/libedje.so.1.13.0
b6a2f000 b6a4d000 r-xp /usr/lib/libecore.so.1.13.0
b6a6d000 b6bf4000 r-xp /usr/lib/libevas.so.1.13.0
b6c29000 b6c3d000 r-xp /lib/libpthread-2.20-2014.11.so
b6c51000 b6e85000 r-xp /usr/lib/libelementary.so.1.13.0
b6eb4000 b6eb8000 r-xp /usr/lib/libsmack.so.1.0.0
b6ec8000 b6ecf000 r-xp /usr/lib/libprivilege-control.so.0.0.2
b6edf000 b6ee1000 r-xp /usr/lib/libdlog.so.0.0.0
b6ef1000 b6ef4000 r-xp /usr/lib/libbundle.so.0.1.22
b6f04000 b6f06000 r-xp /lib/libdl-2.20-2014.11.so
b6f17000 b6f2f000 r-xp /usr/lib/libaul.so.0.1.0
b6f43000 b6f48000 r-xp /usr/lib/libappcore-efl.so.1.1
b6f59000 b6f66000 r-xp /usr/lib/liblptcp.so
b6f78000 b6f7c000 r-xp /usr/lib/libsys-assert.so
b6f8d000 b6fad000 r-xp /lib/ld-2.20-2014.11.so
b6fbe000 b6fc3000 r-xp /usr/bin/launchpad-loader
b8374000 b8699000 rw-p [heap]
becfe000 bed1f000 rwxp [stack]
b8374000 b8699000 rw-p [heap]
becfe000 bed1f000 rwxp [stack]
End of Maps Information

Callstack Information (PID:5056)
Call Stack Count: 1
 0: strcmp + 0x1 (0xb67406f2) [/lib/libc.so.6] + 0x756f2
End of Call Stack

Package Information
Package Name: org.example.uibuildersingleview
Package ID : org.example.uibuildersingleview
Version: 1.0.0
Package Type: tpk
App Name: uibuildersingleview
App ID: org.example.uibuildersingleview
Type: capp
Categories: 

Latest Debug Message Information
--------- beginning of /dev/log_main
ew, table num : 4
12-07 20:26:20.603+0530 E/RESOURCED(  779): heart-memory.c: heart_memory_get_data(601) > hashtable heart_memory_app_list is NULL
12-07 20:26:20.603+0530 W/AUL_AMD (  664): amd_launch.c: _start_app(3052) > pad pid(-5)
12-07 20:26:20.603+0530 W/AUL_PAD ( 1521): launchpad.c: __launchpad_main_loop(520) > Launch on type-based process-pool
12-07 20:26:20.603+0530 W/AUL_PAD ( 1521): launchpad.c: __send_result_to_caller(267) > Check app launching
12-07 20:26:20.613+0530 I/CAPI_APPFW_APPLICATION( 4748): app_main.c: ui_app_main(789) > app_efl_main
12-07 20:26:20.613+0530 I/CAPI_APPFW_APPLICATION( 4748): app_main.c: _ui_app_appcore_create(641) > app_appcore_create
12-07 20:26:20.663+0530 E/TBM     ( 4748): tbm_bufmgr.c: _tbm_bufmgr_init_state(691) > USE TGL LOCK!
12-07 20:26:20.693+0530 W/AUL     (  664): app_signal.c: aul_send_app_launch_request_signal(423) > send_app_launch_signal, pid: 4748, appid: org.example.uibuildersingleview
12-07 20:26:20.693+0530 E/AUL     (  664): amd_app_group.c: app_group_start_app(1037) > app_group_start_app
12-07 20:26:20.703+0530 W/AUL     ( 5042): launch.c: app_request_to_launchpad(425) > request cmd(0) result(4748)
12-07 20:26:20.873+0530 I/APP_CORE( 4748): appcore-efl.c: __do_app(514) > [APP 4748] Event: RESET State: CREATED
12-07 20:26:20.873+0530 I/CAPI_APPFW_APPLICATION( 4748): app_main.c: _ui_app_appcore_reset(723) > app_appcore_reset
12-07 20:26:20.883+0530 E/EFL     ( 4748): edje<4748> lib/edje/edje_embryo.c:4134 _edje_embryo_test_run() You are running Embryo->EDC->Embryo with script program '_p26';
12-07 20:26:20.883+0530 E/EFL     ( 4748): By the power of Grayskull, your previous Embryo stack is now broken!
12-07 20:26:20.893+0530 E/EFL     ( 4748): edje<4748> lib/edje/edje_embryo.c:4134 _edje_embryo_test_run() You are running Embryo->EDC->Embryo with script program '_p26';
12-07 20:26:20.893+0530 E/EFL     ( 4748): By the power of Grayskull, your previous Embryo stack is now broken!
12-07 20:26:20.893+0530 E/EFL     ( 4748): edje<4748> lib/edje/edje_embryo.c:4134 _edje_embryo_test_run() You are running Embryo->EDC->Embryo with script program '_p26';
12-07 20:26:20.893+0530 E/EFL     ( 4748): By the power of Grayskull, your previous Embryo stack is now broken!
12-07 20:26:20.903+0530 W/APP_CORE( 4748): appcore-efl.c: __show_cb(920) > [EVENT_TEST][EVENT] GET SHOW EVENT!!!. WIN:5400002
12-07 20:26:20.903+0530 W/AUL_AMD (  664): amd_request.c: __request_handler(906) > __request_handler: 35
12-07 20:26:20.903+0530 E/EFL     (  330): eo<330> lib/eo/eo.c:676 _eo_call_resolve() in lib/edje/edje_object.eo.c:316: func 'edje_obj_signal_emit' (415) could not be resolved for class 'Evas_Object_Smart'.
12-07 20:26:20.903+0530 E/EFL     (  330): eo<330> lib/eo/eo.c:676 _eo_call_resolve() in lib/edje/edje_object.eo.c:316: func 'edje_obj_signal_emit' (415) could not be resolved for class 'Evas_Object_Smart'.
12-07 20:26:20.923+0530 I/Tizen::System( 1290): (259) > Active app [org.exampl], current [com.samsun] 
12-07 20:26:20.923+0530 I/Tizen::Io( 1290): (729) > Entry not found
12-07 20:26:20.933+0530 I/Tizen::System( 1290): (157) > change brightness system value.
12-07 20:26:20.933+0530 W/AUL_AMD (  664): amd_request.c: __request_handler(906) > __request_handler: 15
12-07 20:26:20.983+0530 I/APP_CORE(  895): appcore-efl.c: __do_app(514) > [APP 895] Event: PAUSE State: RUNNING
12-07 20:26:20.983+0530 I/CAPI_APPFW_APPLICATION(  895): app_main.c: _ui_app_appcore_pause(689) > app_appcore_pause
12-07 20:26:20.983+0530 E/cluster-home(  895): homescreen.cpp: OnPause(260) >  app pause
12-07 20:26:20.983+0530 W/AUL_AMD (  664): amd_launch.c: __e17_status_handler(3328) > pid(895) status(4)
12-07 20:26:20.983+0530 W/AUL     (  664): amd_app_group.c: __set_fg_flag(190) > send_signal BG com.samsung.homescreen(895)
12-07 20:26:20.983+0530 W/AUL     (  664): app_signal.c: aul_send_app_status_change_signal(581) > send_app_status_change_signal, pid: 895, appid: com.samsung.homescreen, status: bg
12-07 20:26:20.993+0530 W/AUL_AMD (  664): amd_launch.c: __e17_status_handler(3328) > pid(4748) status(3)
12-07 20:26:20.993+0530 W/AUL_AMD (  664): amd_key.c: _key_ungrab(269) > fail(-1) to ungrab key(XF86Back)
12-07 20:26:20.993+0530 W/AUL_AMD (  664): amd_launch.c: __e17_status_handler(3349) > back key ungrab error
12-07 20:26:20.993+0530 W/AUL     (  664): amd_app_group.c: __set_fg_flag(180) > send_signal FG org.example.uibuildersingleview(4748)
12-07 20:26:20.993+0530 W/AUL     (  664): app_signal.c: aul_send_app_status_change_signal(581) > send_app_status_change_signal, pid: 4748, appid: org.example.uibuildersingleview, status: fg
12-07 20:26:20.993+0530 I/APP_CORE( 4748): appcore-efl.c: __do_app(514) > [APP 4748] Event: RESUME State: CREATED
12-07 20:26:21.013+0530 I/APP_CORE( 4748): appcore-efl.c: __do_app(623) > Legacy lifecycle: 0
12-07 20:26:21.013+0530 I/APP_CORE( 4748): appcore-efl.c: __do_app(625) > [APP 4748] Initial Launching, call the resume_cb
12-07 20:26:21.013+0530 I/CAPI_APPFW_APPLICATION( 4748): app_main.c: _ui_app_appcore_resume(706) > app_appcore_resume
12-07 20:26:21.364+0530 W/AUL_AMD (  664): amd_launch.c: __e17_status_handler(3328) > pid(4748) status(0)
12-07 20:26:21.744+0530 I/Tizen::App( 1290): (499) > LaunchedApp(org.example.uibuildersingleview)
12-07 20:26:21.744+0530 E/RESOURCED(  779): resourced-dbus.c: resourced_dbus_system_hash_drop_busname(324) > Does not exist in busname hash: :1.410
12-07 20:26:21.754+0530 I/Tizen::App( 1290): (733) > Finished invoking application event listener for org.example.uibuildersingleview, 4748.
12-07 20:26:22.485+0530 I/UXT     ( 5056): Uxt_ObjectManager.cpp: OnInitialized(737) > Initialized.
12-07 20:26:22.935+0530 E/EFL     ( 4748): ecore_x<4748> lib/ecore_x/xlib/ecore_x_events.c:603 _ecore_x_event_handle_button_press() ButtonEvent:press time=8935419
12-07 20:26:23.035+0530 E/EFL     ( 4748): ecore_x<4748> lib/ecore_x/xlib/ecore_x_events.c:756 _ecore_x_event_handle_button_release() ButtonEvent:release time=8935519
12-07 20:26:23.035+0530 E/VCONF   ( 4748): vconf.c: _vconf_check_retry_err(1368) > db/ise/keysound : check retry err (-21/13).
12-07 20:26:23.035+0530 E/VCONF   ( 4748): vconf.c: _vconf_get_key_filesys(2371) > _vconf_get_key_filesys(db/ise/keysound) step(-21) failed(13 / Permission denied) retry(0) 
12-07 20:26:23.035+0530 E/VCONF   ( 4748): vconf.c: _vconf_get_key(2407) > _vconf_get_key(db/ise/keysound) step(-21) failed(13 / Permission denied)
12-07 20:26:23.035+0530 E/VCONF   ( 4748): vconf.c: vconf_get_bool(2729) > vconf_get_bool(4748) : db/ise/keysound error
12-07 20:26:23.035+0530 E/VCONF   ( 4748): vconf-kdb.c: _vconf_kdb_add_notify(318) > _vconf_kdb_add_notify : add noti(Permission denied)
12-07 20:26:23.035+0530 E/VCONF   ( 4748): vconf.c: vconf_notify_key_changed(3168) > vconf_notify_key_changed : key(db/ise/keysound) add notify fail
12-07 20:26:23.826+0530 E/EFL     ( 4748): ecore_x<4748> lib/ecore_x/xlib/ecore_x_events.c:603 _ecore_x_event_handle_button_press() ButtonEvent:press time=8936305
12-07 20:26:23.886+0530 E/EFL     ( 4748): ecore_x<4748> lib/ecore_x/xlib/ecore_x_events.c:756 _ecore_x_event_handle_button_release() ButtonEvent:release time=8936371
12-07 20:26:24.326+0530 E/EFL     ( 4748): ecore_x<4748> lib/ecore_x/xlib/ecore_x_events.c:603 _ecore_x_event_handle_button_press() ButtonEvent:press time=8936814
12-07 20:26:24.376+0530 E/EFL     ( 4748): ecore_x<4748> lib/ecore_x/xlib/ecore_x_events.c:756 _ecore_x_event_handle_button_release() ButtonEvent:release time=8936868
12-07 20:26:25.998+0530 I/APP_CORE(  895): appcore-efl.c: __do_app(514) > [APP 895] Event: MEM_FLUSH State: PAUSED
12-07 20:26:26.028+0530 I/CAPI_APPFW_APPLICATION(  895): app_main.c: _ui_app_appcore_rotation_event(485) > _ui_app_appcore_rotation_event
12-07 20:26:26.088+0530 I/CAPI_APPFW_APPLICATION( 4748): app_main.c: _ui_app_appcore_rotation_event(485) > _ui_app_appcore_rotation_event
12-07 20:26:26.088+0530 I/CAPI_APPFW_APPLICATION(  895): app_main.c: _ui_app_appcore_rotation_event(485) > _ui_app_appcore_rotation_event
12-07 20:26:42.644+0530 I/CAPI_APPFW_APPLICATION( 4748): app_main.c: _ui_app_appcore_rotation_event(485) > _ui_app_appcore_rotation_event
12-07 20:26:42.644+0530 I/CAPI_APPFW_APPLICATION(  895): app_main.c: _ui_app_appcore_rotation_event(485) > _ui_app_appcore_rotation_event
12-07 20:26:42.885+0530 I/CAPI_APPFW_APPLICATION( 4748): app_main.c: _ui_app_appcore_rotation_event(485) > _ui_app_appcore_rotation_event
12-07 20:26:42.885+0530 I/CAPI_APPFW_APPLICATION(  895): app_main.c: _ui_app_appcore_rotation_event(485) > _ui_app_appcore_rotation_event
12-07 20:26:42.945+0530 I/CAPI_APPFW_APPLICATION( 4748): app_main.c: _ui_app_appcore_rotation_event(485) > _ui_app_appcore_rotation_event
12-07 20:26:42.945+0530 I/CAPI_APPFW_APPLICATION(  895): app_main.c: _ui_app_appcore_rotation_event(485) > _ui_app_appcore_rotation_event
12-07 20:26:47.929+0530 I/CAPI_APPFW_APPLICATION( 4748): app_main.c: _ui_app_appcore_rotation_event(485) > _ui_app_appcore_rotation_event
12-07 20:26:47.929+0530 I/CAPI_APPFW_APPLICATION(  895): app_main.c: _ui_app_appcore_rotation_event(485) > _ui_app_appcore_rotation_event
12-07 20:26:48.020+0530 E/EFL     ( 4748): edje<4748> lib/edje/edje_embryo.c:4134 _edje_embryo_test_run() You are running Embryo->EDC->Embryo with script program '_p26';
12-07 20:26:48.020+0530 E/EFL     ( 4748): By the power of Grayskull, your previous Embryo stack is now broken!
12-07 20:26:48.110+0530 I/CAPI_APPFW_APPLICATION(  895): app_main.c: _ui_app_appcore_rotation_event(485) > _ui_app_appcore_rotation_event
12-07 20:26:48.200+0530 E/EFL     (  894): eo<894> lib/eo/eo.c:676 _eo_call_resolve() in lib/edje/edje_object.eo.c:164: func 'edje_obj_part_geometry_get' (762) could not be resolved for class 'Elm_Layout'.
12-07 20:26:48.340+0530 I/CAPI_APPFW_APPLICATION( 4748): app_main.c: _ui_app_appcore_rotation_event(485) > _ui_app_appcore_rotation_event
12-07 20:26:48.400+0530 E/EFL     (  330): elementary<330> elm_transit.c:648 elm_transit_del_cb_set() Elm_Transit transit has already been deleted!
12-07 20:26:52.904+0530 I/CAPI_APPFW_APPLICATION( 4748): app_main.c: _ui_app_appcore_rotation_event(485) > _ui_app_appcore_rotation_event
12-07 20:26:52.904+0530 I/CAPI_APPFW_APPLICATION(  895): app_main.c: _ui_app_appcore_rotation_event(485) > _ui_app_appcore_rotation_event
12-07 20:26:52.984+0530 E/EFL     ( 4748): edje<4748> lib/edje/edje_embryo.c:4134 _edje_embryo_test_run() You are running Embryo->EDC->Embryo with script program '_p26';
12-07 20:26:52.984+0530 E/EFL     ( 4748): By the power of Grayskull, your previous Embryo stack is now broken!
12-07 20:26:53.054+0530 E/EFL     (  894): eo<894> lib/eo/eo.c:676 _eo_call_resolve() in lib/edje/edje_object.eo.c:164: func 'edje_obj_part_geometry_get' (762) could not be resolved for class 'Elm_Layout'.
12-07 20:26:53.255+0530 E/EFL     (  330): elementary<330> elm_transit.c:648 elm_transit_del_cb_set() Elm_Transit transit has already been deleted!
12-07 20:26:53.565+0530 I/CAPI_APPFW_APPLICATION( 4748): app_main.c: _ui_app_appcore_rotation_event(485) > _ui_app_appcore_rotation_event
12-07 20:26:53.565+0530 I/CAPI_APPFW_APPLICATION(  895): app_main.c: _ui_app_appcore_rotation_event(485) > _ui_app_appcore_rotation_event
12-07 20:26:59.220+0530 E/EFL     ( 4748): ecore_x<4748> lib/ecore_x/xlib/ecore_x_events.c:603 _ecore_x_event_handle_button_press() ButtonEvent:press time=8971702
12-07 20:26:59.311+0530 E/EFL     ( 4748): ecore_x<4748> lib/ecore_x/xlib/ecore_x_events.c:756 _ecore_x_event_handle_button_release() ButtonEvent:release time=8971790
12-07 20:26:59.431+0530 I/AUL_PAD ( 1521): sigchild.h: __launchpad_process_sigchld(162) > dead_pid = 4748 pgid = 4748
12-07 20:26:59.431+0530 I/AUL_PAD ( 1521): sigchild.h: __sigchild_action(143) > dead_pid(4748)
12-07 20:26:59.441+0530 E/EFL     (  330): eo<330> lib/eo/eo.c:676 _eo_call_resolve() in lib/edje/edje_object.eo.c:316: func 'edje_obj_signal_emit' (415) could not be resolved for class 'Evas_Object_Smart'.
12-07 20:26:59.471+0530 W/AUL_AMD (  664): amd_launch.c: __e17_status_handler(3328) > pid(895) status(3)
12-07 20:26:59.481+0530 W/AUL_AMD (  664): amd_key.c: _key_ungrab(269) > fail(-1) to ungrab key(XF86Back)
12-07 20:26:59.481+0530 W/AUL_AMD (  664): amd_launch.c: __e17_status_handler(3349) > back key ungrab error
12-07 20:26:59.481+0530 W/AUL     (  664): amd_app_group.c: __set_fg_flag(180) > send_signal FG com.samsung.homescreen(895)
12-07 20:26:59.481+0530 W/AUL     (  664): app_signal.c: aul_send_app_status_change_signal(581) > send_app_status_change_signal, pid: 895, appid: com.samsung.homescreen, status: fg
12-07 20:26:59.511+0530 I/AUL_PAD ( 1521): sigchild.h: __sigchild_action(149) > __send_app_dead_signal(0)
12-07 20:26:59.511+0530 I/AUL_PAD ( 1521): sigchild.h: __launchpad_process_sigchld(170) > after __sigchild_action
12-07 20:26:59.511+0530 E/AUL_PAD ( 1521): launchpad.c: main(698) > error reading sigchld info
12-07 20:26:59.511+0530 I/ESD     (  941): esd_main.c: __esd_app_dead_handler(1773) > pid: 4748
12-07 20:26:59.511+0530 I/Tizen::App( 1290): (243) > App[org.example.uibuildersingleview] pid[4748] terminate event is forwarded
12-07 20:26:59.511+0530 I/Tizen::System( 1290): (256) > osp.accessorymanager.service provider is found.
12-07 20:26:59.511+0530 I/Tizen::System( 1290): (196) > Accessory Owner is removed [org.example.uibuildersingleview, 4748, ]
12-07 20:26:59.511+0530 I/Tizen::System( 1290): (256) > osp.system.service provider is found.
12-07 20:26:59.511+0530 I/Tizen::App( 1290): (506) > TerminatedApp(org.example.uibuildersingleview)
12-07 20:26:59.511+0530 I/Tizen::App( 1290): (512) > Not registered pid(4748)
12-07 20:26:59.511+0530 I/Tizen::System( 1290): (246) > Terminated app [org.example.uibuildersingleview]
12-07 20:26:59.511+0530 I/Tizen::Io( 1290): (729) > Entry not found
12-07 20:26:59.511+0530 W/AUL_AMD (  664): amd_main.c: __app_dead_handler(327) > __app_dead_handler, pid: 4748
12-07 20:26:59.511+0530 W/AUL_AMD (  664): amd_main.c: __app_dead_handler(333) > app_group_leader_app, pid: 4748
12-07 20:26:59.511+0530 E/RESOURCED(  779): resourced-dbus.c: resourced_dbus_system_hash_drop_busname(324) > Does not exist in busname hash: :1.414
12-07 20:26:59.521+0530 W/AUL_AMD (  664): amd_launch.c: __e17_status_handler(3328) > pid(895) status(0)
12-07 20:26:59.531+0530 I/APP_CORE(  895): appcore-efl.c: __do_app(514) > [APP 895] Event: RESUME State: PAUSED
12-07 20:26:59.531+0530 I/CAPI_APPFW_APPLICATION(  895): app_main.c: _ui_app_appcore_resume(706) > app_appcore_resume
12-07 20:26:59.531+0530 E/cluster-home(  895): homescreen.cpp: OnResume(233) >  app resume
12-07 20:26:59.531+0530 E/EFL     (  330): eo<330> lib/eo/eo.c:676 _eo_call_resolve() in lib/edje/edje_object.eo.c:316: func 'edje_obj_signal_emit' (415) could not be resolved for class 'Evas_Object_Smart'.
12-07 20:26:59.531+0530 I/Tizen::System( 1290): (157) > change brightness system value.
12-07 20:26:59.531+0530 I/Tizen::App( 1290): (782) > Finished invoking application event listener for org.example.uibuildersingleview, 4748.
12-07 20:26:59.571+0530 I/Tizen::System( 1290): (259) > Active app [com.samsun], current [org.exampl] 
12-07 20:26:59.571+0530 I/Tizen::Io( 1290): (729) > Entry not found
12-07 20:26:59.581+0530 I/TIZEN_N_SOUND_MANAGER(  961): sound_manager.c: sound_manager_get_volume(84) > returns : type=0, volume=8, ret=0x0
12-07 20:26:59.581+0530 E/TIZEN_N_SOUND_MANAGER(  961): sound_manager_private.c: __convert_sound_manager_error_code(142) > [sound_manager_get_volume] ERROR_NONE(0x00000000) : core frameworks error code(0x00000000)
12-07 20:26:59.581+0530 I/TIZEN_N_SOUND_MANAGER(  961): sound_manager.c: sound_manager_get_volume(84) > returns : type=4, volume=0, ret=0x0
12-07 20:26:59.581+0530 E/TIZEN_N_SOUND_MANAGER(  961): sound_manager_private.c: __convert_sound_manager_error_code(142) > [sound_manager_get_volume] ERROR_NONE(0x00000000) : core frameworks error code(0x00000000)
12-07 20:26:59.591+0530 W/AUL_AMD (  664): amd_request.c: __request_handler(906) > __request_handler: 15
12-07 20:26:59.611+0530 I/Tizen::System( 1290): (157) > change brightness system value.
12-07 20:26:59.611+0530 W/CRASH_MANAGER( 5064): worker.c: worker_job(1199) > 11047487569621481122619
12-07 20:27:00.582+0530 I/INDICATOR(  776): clock.c: indicator_clock_changed_cb(195) > ""
12-07 20:27:00.582+0530 E/UXT     (  776): uxt_util.c: uxt_util_get_date_time_text_by_locale(389) > text : 8:27 p.m.
12-07 20:27:00.582+0530 I/INDICATOR(  776): clock.c: getTimeFormatted(176) > "time format : 8:27 p.m."
12-07 20:27:00.582+0530 I/INDICATOR(  776): clock.c: indicator_clock_changed_cb(244) > "time format : 8:27 p.m."
12-07 20:27:00.582+0530 W/INDICATOR(  776): clock.c: indicator_clock_changed_cb(272) > 
12-07 20:27:00.582+0530 I/INDICATOR(  776): clock.c: indicator_clock_changed_cb(317) > "[CLOCK MODULE] Timer Status : -2146520406 Time: <font_size=31> </font_size> <font_size=31> 8:27 p.m.</font_size></font>"
12-07 20:27:39.840+0530 E/PKGMGR_SERVER( 5149): pkgmgr-server.c: main(2414) > package manager server start
12-07 20:27:39.880+0530 E/PKGMGR_SERVER( 5149): pkgmgr-server.c: __set_recovery_mode(234) > rev_file[/opt/share/packages/.recovery/pkgmgr/org.example.uibuildersingleview] is null
12-07 20:27:39.880+0530 E/PKGMGR  ( 5147): pkgmgr.c: __check_sync_process(883) > file is can not remove[/tmp/org.example.uibuildersingleview, -1]
12-07 20:27:39.920+0530 W/AUL_AMD (  664): amd_request.c: __request_handler(906) > __request_handler: 14
12-07 20:27:39.930+0530 W/AUL_AMD (  664): amd_request.c: __send_result_to_client(158) > __send_result_to_client, pid: -1
12-07 20:27:39.930+0530 E/PKGMGR_SERVER( 5149): pkgmgr-server.c: __unset_recovery_mode(282) > remove recovery_file[/opt/share/packages/.recovery/pkgmgr/org.example.uibuildersingleview] fail
12-07 20:27:39.930+0530 E/PKGMGR_SERVER( 5149): pkgmgr-server.c: sighandler(417) > child NORMAL exit [5152]
12-07 20:27:42.523+0530 E/PKGMGR_SERVER( 5149): pkgmgr-server.c: exit_server(1381) > exit_server Start [backend_status=1, queue_status=1, drm_status=1] 
12-07 20:27:42.523+0530 E/PKGMGR_SERVER( 5149): pkgmgr-server.c: main(2471) > package manager server terminated.
12-07 20:27:46.497+0530 E/PKGMGR  ( 5239): pkgmgr.c: pkgmgr_client_reinstall(2020) > reinstall pkg start.
12-07 20:27:46.587+0530 E/PKGMGR_SERVER( 5241): pkgmgr-server.c: main(2414) > package manager server start
12-07 20:27:46.637+0530 E/PKGMGR_SERVER( 5241): pkgmgr-server-internal.c: _zone_set_type_and_backend(180) > tep_filepath [org.example.uibuildersingleview]
12-07 20:27:46.647+0530 E/PKGMGR_SERVER( 5241): pm-mdm.c: _get_package_info_from_file(116) > [0;31m[_get_package_info_from_file(): 116](ret < 0) access() failed. path: org.example.uibuildersingleview errno: 2 (No such file or directory)[0;m
12-07 20:27:46.647+0530 E/PKGMGR_SERVER( 5241): pkgmgr-server.c: __set_recovery_mode(234) > rev_file[/opt/share/packages/.recovery/pkgmgr/org.example.uibuildersingleview] is null
12-07 20:27:46.647+0530 E/PKGMGR  ( 5239): pkgmgr.c: pkgmgr_client_reinstall(2133) > reinstall pkg finish, ret=[52390002]
12-07 20:27:46.807+0530 I/Tizen::App( 1290): (1894) > PackageEventHandler - req: 12900002, pkg_type: tpk, pkg_name: org.example.uibuildersingleview, key: start, val: update
12-07 20:27:46.807+0530 I/Tizen::App( 1290): (1584) > Package = [org.example.uibuildersingleview], Key = [start], Value = [update], install = [1]
12-07 20:27:46.807+0530 W/AUL_AMD (  664): amd_appinfo.c: __amd_pkgmgrinfo_status_cb(1068) > __amd_pkgmgrinfo_start_handler
12-07 20:27:46.807+0530 I/Tizen::App( 1290): (1894) > PackageEventHandler - req: 12900002, pkg_type: tpk, pkg_name: org.example.uibuildersingleview, key: install_percent, val: 30
12-07 20:27:46.807+0530 I/Tizen::App( 1290): (119) > InstallationInProgress [30]
12-07 20:27:46.807+0530 I/Tizen::App( 1290): (1584) > Package = [org.example.uibuildersingleview], Key = [install_percent], Value = [30], install = [96]
12-07 20:27:46.817+0530 E/PKGMGR  ( 1495): pkgmgr.c: __find_op_cbinfo(312) > tmp is NULL
12-07 20:27:46.817+0530 E/PKGMGR  ( 1495): pkgmgr.c: __find_op_cbinfo(312) > tmp is NULL
12-07 20:27:46.817+0530 E/PKGMGR  ( 1495): pkgmgr.c: __find_op_cbinfo(312) > tmp is NULL
12-07 20:27:46.817+0530 E/PKGMGR  ( 1495): pkgmgr.c: __find_op_cbinfo(312) > tmp is NULL
12-07 20:27:46.917+0530 W/CERT_SVC_VCORE( 5244): DUID.cpp: CheckCertifiedDevice(668) > No device id on distributor certificate
12-07 20:27:46.957+0530 E/rpm-installer( 5244): coretpk-parser.c: _coretpk_parser_get_manifest_info(1183) > (doc == NULL) xmlParseFile() failed.
12-07 20:27:46.957+0530 E/rpm-installer( 5244): coretpk-installer.c: _coretpk_installer_verify_privilege_list(1594) > (pkg_file_info == NULL) pkg_file_info is NULL.
12-07 20:27:47.017+0530 E/PKGMGR_PARSER( 5244): pkgmgr_parser.c: __check_theme(154) > theme for uninstallation.
12-07 20:27:47.027+0530 E/PKGMGR_CERT( 5244): pkgmgrinfo_certinfo.c: pkgmgrinfo_delete_certinfo(617) > Transaction Begin
12-07 20:27:47.037+0530 E/PKGMGR_CERT( 5244): pkgmgrinfo_certinfo.c: pkgmgrinfo_delete_certinfo(622) > Certificate Deletion Success
12-07 20:27:47.037+0530 E/PKGMGR_CERT( 5244): pkgmgrinfo_certinfo.c: pkgmgrinfo_delete_certinfo(635) > Transaction Commit and End
12-07 20:27:47.077+0530 E/rpm-installer( 5244): coretpk-installer.c: _coretpk_installer_package_reinstall(6347) > _coretpk_installer_package_reinstall(org.example.uibuildersingleview) failed.
12-07 20:27:47.077+0530 E/PKGMGR  ( 1495): pkgmgr.c: __find_op_cbinfo(312) > tmp is NULL
12-07 20:27:47.077+0530 I/Tizen::App( 1290): (1894) > PackageEventHandler - req: 12900002, pkg_type: tpk, pkg_name: org.example.uibuildersingleview, key: end, val: fail
12-07 20:27:47.077+0530 I/Tizen::App( 1290): (1584) > Package = [org.example.uibuildersingleview], Key = [end], Value = [fail], install = [96]
12-07 20:27:47.087+0530 E/PKGMGR  ( 1495): pkgmgr.c: __find_op_cbinfo(312) > tmp is NULL
12-07 20:27:47.087+0530 E/ESD     (  941): esd_main.c: __esd_pkgmgr_event_callback(1757) > pkg_event(3) falied
12-07 20:27:47.097+0530 W/AUL_AMD (  664): amd_appinfo.c: __amd_pkgmgrinfo_status_cb(1111) > __amd_pkgmgrinfo_fail_handler
12-07 20:27:47.097+0530 W/ISF_PANEL_EFL(  823): isf_panel_efl.cpp: _isf_insert_ime_info_by_pkgid(1168) > pkgmgrinfo_pkginfo_get_pkginfo("org.example.uibuildersingleview",~) returned -1
12-07 20:27:47.097+0530 W/ISF_PANEL_EFL(  823): isf_panel_efl.cpp: _package_manager_event_cb(1381) > isf_db_select_appids_by_pkgid returned 0.
12-07 20:27:48.519+0530 E/PKGMGR_SERVER( 5241): pkgmgr-server.c: exit_server(1381) > exit_server Start [backend_status=0, queue_status=1, drm_status=1] 
12-07 20:27:48.829+0530 E/PKGMGR_SERVER( 5241): pkgmgr-server.c: __unset_recovery_mode(282) > remove recovery_file[/opt/share/packages/.recovery/pkgmgr/org.example.uibuildersingleview] fail
12-07 20:27:48.829+0530 E/PKGMGR_SERVER( 5241): pkgmgr-server.c: sighandler(417) > child NORMAL exit [5244]
12-07 20:27:50.520+0530 E/PKGMGR_SERVER( 5241): pkgmgr-server.c: exit_server(1381) > exit_server Start [backend_status=1, queue_status=1, drm_status=1] 
12-07 20:27:50.520+0530 E/PKGMGR_SERVER( 5241): pkgmgr-server.c: main(2471) > package manager server terminated.
12-07 20:27:51.301+0530 E/PKGMGR  ( 5293): pkgmgr.c: pkgmgr_client_install(1605) > install pkg start.
12-07 20:27:51.381+0530 E/PKGMGR_SERVER( 5295): pkgmgr-server.c: main(2414) > package manager server start
12-07 20:27:51.431+0530 E/PKGMGR_SERVER( 5295): pkgmgr-server-internal.c: _zone_set_type_and_backend(180) > tep_filepath [/opt/usr/apps/tmp/org.example.uibuildersingleview-1.0.0-arm.tpk]
12-07 20:27:51.441+0530 E/PKGMGR_INFO( 5295): pkgmgrinfo_pkginfo.c: pkgmgrinfo_pkginfo_get_unmounted_pkginfo(778) > (exist == 0) pkgid[/opt/usr/apps/tmp/org.example.uibuildersingleview-1.0.0-arm.tpk] not found in DB
12-07 20:27:51.451+0530 E/rpm-installer( 5295): librpm.c: __installer_util_delete_dir(171) > opendir(/tmp/coretpk-unzip) failed. [2][No such file or directory]
12-07 20:27:51.461+0530 E/PKGMGR_SERVER( 5295): pm-mdm.c: _pm_check_mdm_policy(75) > [0;31m[_pm_check_mdm_policy(): 75](ret != MDM_RESULT_SUCCESS) can not connect mdm server[0;m
12-07 20:27:51.461+0530 E/PKGMGR_SERVER( 5295): pkgmgr-server.c: __set_recovery_mode(234) > rev_file[/opt/share/packages/.recovery/pkgmgr/org.example.uibuildersingleview-1.0.0-arm.tpk] is null
12-07 20:27:51.461+0530 E/PKGMGR  ( 5293): pkgmgr.c: pkgmgr_client_install(1723) > install pkg finish, ret=[52930002]
12-07 20:27:51.582+0530 E/PKGMGR_INSTALLER( 5298): pkgmgr_installer.c: pkgmgr_installer_receive_request(225) > option is [i]
12-07 20:27:51.582+0530 E/rpm-installer( 5298): rpm-appcore-intf.c: main(186) > [/opt/usr/apps/tmp/org.example.uibuildersingleview-1.0.0-arm.tpk] is tpk package.
12-07 20:27:51.592+0530 E/rpm-installer( 5298): coretpk-parser.c: _coretpk_parser_is_svc_app(1031) > (ret == 1) NodeSet is empty. (//*[name() = 'manifest']/*[name()='service-application'])
12-07 20:27:51.592+0530 E/rpm-installer( 5298): coretpk-parser.c: __coretpk_parser_get_value(1147) > (result_value == NULL) [install-location] is empty.
12-07 20:27:51.592+0530 E/rpm-installer( 5298): coretpk-parser.c: __coretpk_parser_get_value_list(1104) > (ret == 1) [//*[name() ='privileges']/*[name()='privilege']] is empty.
12-07 20:27:51.592+0530 E/rpm-installer( 5298): coretpk-parser.c: __coretpk_parser_is_theme(31) > (ret == 1) metadata(watchface) is empty.
12-07 20:27:51.592+0530 E/rpm-installer( 5298): coretpk-parser.c: _coretpk_parser_is_widget(997) > (ret == 1) NodeSet is empty. (//*[name() = 'manifest']/*[name()='widget-application'])
12-07 20:27:51.592+0530 E/rpm-installer( 5298): installer-util.c: _installer_util_get_configuration_value(565) > [signature]=[on]
12-07 20:27:51.642+0530 W/CERT_SVC_VCORE( 5298): DUID.cpp: CheckCertifiedDevice(668) > No device id on distributor certificate
12-07 20:27:51.692+0530 I/Tizen::App( 1290): (1894) > PackageEventHandler - req: 12900002, pkg_type: tpk, pkg_name: org.example.uibuildersingleview, key: start, val: install
12-07 20:27:51.702+0530 I/Tizen::App( 1290): (1584) > Package = [org.example.uibuildersingleview], Key = [start], Value = [install], install = [96]
12-07 20:27:51.702+0530 E/PKGMGR  ( 1495): pkgmgr.c: __find_op_cbinfo(312) > tmp is NULL
12-07 20:27:51.702+0530 E/PKGMGR  ( 1495): pkgmgr.c: __find_op_cbinfo(312) > tmp is NULL
12-07 20:27:51.712+0530 I/Tizen::App( 1290): (1894) > PackageEventHandler - req: 12900002, pkg_type: tpk, pkg_name: org.example.uibuildersingleview, key: install_percent, val: 30
12-07 20:27:51.712+0530 I/Tizen::App( 1290): (119) > InstallationInProgress [30]
12-07 20:27:51.712+0530 I/Tizen::App( 1290): (1584) > Package = [org.example.uibuildersingleview], Key = [install_percent], Value = [30], install = [96]
12-07 20:27:51.712+0530 E/PKGMGR  ( 1495): pkgmgr.c: __find_op_cbinfo(312) > tmp is NULL
12-07 20:27:51.712+0530 E/PKGMGR  ( 1495): pkgmgr.c: __find_op_cbinfo(312) > tmp is NULL
12-07 20:27:51.812+0530 E/rpm-installer( 5298): coretpk-parser.c: __coretpk_parser_check_vip_tag(396) > (ret == 1) metadata(watchface) is empty.
12-07 20:27:51.812+0530 E/rpm-installer( 5298): coretpk-parser.c: __coretpk_parser_get_value(1147) > (result_value == NULL) [ui-gadget] is empty.
12-07 20:27:51.812+0530 E/rpm-installer( 5298): coretpk-parser.c: __coretpk_parser_append_path(335) > (ret == 1) NodeSet is empty. (//@portrait-effectimage)
12-07 20:27:51.812+0530 E/rpm-installer( 5298): coretpk-parser.c: __coretpk_parser_is_theme(31) > (ret == 1) metadata(watchface) is empty.
12-07 20:27:51.822+0530 E/PKGMGR_PARSER( 5298): pkgmgr_parser.c: pkgmgr_parser_check_manifest_validation(2678) > Manifest is Valid
12-07 20:27:51.822+0530 E/PKGMGR_PARSER( 5298): pkgmgr_parser_signature.c: __ps_check_mdm_policy(979) > (ret != MDM_RESULT_SUCCESS) can not connect mdm server
12-07 20:27:52.062+0530 E/PKGMGR_PARSER( 5298): pkgmgr_parser.c: __check_theme(142) > theme for installation.
12-07 20:27:52.082+0530 E/PKGMGR_CERT( 5298): pkgmgrinfo_certinfo.c: pkgmgrinfo_save_certinfo(427) > Transaction Begin
12-07 20:27:52.082+0530 E/PKGMGR_CERT( 5298): pkgmgrinfo_certinfo.c: pkgmgrinfo_save_certinfo(496) > Id:Count = 1 112
12-07 20:27:52.082+0530 E/PKGMGR_CERT( 5298): pkgmgrinfo_certinfo.c: pkgmgrinfo_save_certinfo(496) > Id:Count = 2 112
12-07 20:27:52.082+0530 E/PKGMGR_CERT( 5298): pkgmgrinfo_certinfo.c: pkgmgrinfo_save_certinfo(496) > Id:Count = 31 7
12-07 20:27:52.082+0530 E/PKGMGR_CERT( 5298): pkgmgrinfo_certinfo.c: pkgmgrinfo_save_certinfo(496) > Id:Count = 32 7
12-07 20:27:52.082+0530 E/PKGMGR_CERT( 5298): pkgmgrinfo_certinfo.c: pkgmgrinfo_save_certinfo(496) > Id:Count = 33 7
12-07 20:27:52.082+0530 E/PKGMGR_CERT( 5298): pkgmgrinfo_certinfo.c: pkgmgrinfo_save_certinfo(496) > Id:Count = 34 7
12-07 20:27:52.092+0530 E/PKGMGR_CERT( 5298): pkgmgrinfo_certinfo.c: pkgmgrinfo_save_certinfo(576) > Transaction Commit and End
12-07 20:27:52.092+0530 I/Tizen::App( 1290): (1894) > PackageEventHandler - req: 12900002, pkg_type: tpk, pkg_name: org.example.uibuildersingleview, key: install_percent, val: 60
12-07 20:27:52.092+0530 I/Tizen::App( 1290): (119) > InstallationInProgress [60]
12-07 20:27:52.092+0530 I/Tizen::App( 1290): (1584) > Package = [org.example.uibuildersingleview], Key = [install_percent], Value = [60], install = [96]
12-07 20:27:52.092+0530 E/PKGMGR  ( 1495): pkgmgr.c: __find_op_cbinfo(312) > tmp is NULL
12-07 20:27:52.092+0530 E/PKGMGR  ( 1495): pkgmgr.c: __find_op_cbinfo(312) > tmp is NULL
12-07 20:27:52.112+0530 E/rpm-installer( 5298): installer-util.c: _installer_util_get_configuration_value(565) > [signature]=[on]
12-07 20:27:52.132+0530 E/rpm-installer( 5298): coretpk-installer.c: __post_install_for_mmc(652) > (handle == NULL) handle is NULL.
12-07 20:27:52.132+0530 E/PKGMGR  ( 1495): pkgmgr.c: __find_op_cbinfo(312) > tmp is NULL
12-07 20:27:52.132+0530 I/Tizen::App( 1290): (1894) > PackageEventHandler - req: 12900002, pkg_type: tpk, pkg_name: org.example.uibuildersingleview, key: install_percent, val: 100
12-07 20:27:52.132+0530 I/Tizen::App( 1290): (119) > InstallationInProgress [100]
12-07 20:27:52.132+0530 I/Tizen::App( 1290): (1584) > Package = [org.example.uibuildersingleview], Key = [install_percent], Value = [100], install = [96]
12-07 20:27:52.142+0530 E/PKGMGR  ( 1495): pkgmgr.c: __find_op_cbinfo(312) > tmp is NULL
12-07 20:27:53.513+0530 E/PKGMGR_SERVER( 5295): pkgmgr-server.c: exit_server(1381) > exit_server Start [backend_status=0, queue_status=1, drm_status=1] 
12-07 20:27:54.114+0530 I/Tizen::App( 1290): (1894) > PackageEventHandler - req: 12900002, pkg_type: tpk, pkg_name: org.example.uibuildersingleview, key: end, val: ok
12-07 20:27:54.114+0530 I/Tizen::App( 1290): (78) > Installation is Completed. [Package = org.example.uibuildersingleview]
12-07 20:27:54.114+0530 I/Tizen::App( 1290): (671) > Enter. package(org.example.uibuildersingleview), installationResult(0)
12-07 20:27:54.124+0530 E/PKGMGR  ( 1495): pkgmgr.c: __find_op_cbinfo(312) > tmp is NULL
12-07 20:27:54.124+0530 E/PKGMGR  ( 1495): pkgmgr.c: __find_op_cbinfo(312) > tmp is NULL
12-07 20:27:54.124+0530 E/cluster-home(  895): mainmenu-package-manager.cpp: OnClientListenCb(571) >  #Step 1
12-07 20:27:54.124+0530 E/cluster-home(  895): mainmenu-package-manager.cpp: OnClientListenCb(575) >  #Step 2
12-07 20:27:54.124+0530 E/cluster-home(  895): mainmenu-package-manager.cpp: _GetAppIds(350) >  BEGIN
12-07 20:27:54.154+0530 E/PKGMGR_SERVER( 5295): pkgmgr-server.c: __unset_recovery_mode(282) > remove recovery_file[/opt/share/packages/.recovery/pkgmgr/org.example.uibuildersingleview-1.0.0-arm.tpk] fail
12-07 20:27:54.154+0530 E/PKGMGR_SERVER( 5295): pkgmgr-server.c: sighandler(417) > child NORMAL exit [5298]
12-07 20:27:54.184+0530 W/ISF_PANEL_EFL(  823): isf_panel_efl.cpp: _package_manager_event_cb(1513) > _isf_insert_ime_info_by_pkgid returned 0.
12-07 20:27:54.204+0530 I/Tizen::App( 1290): (1360) > package(org.example.uibuildersingleview), version(1.0.0), type(tpk), displayName(uibuildersingleview), uninstallable(1), downloaded(1), updated(0), preloaded(0)movable(1), externalStorage(0), mainApp(org.example.uibuildersingleview), storeClient(), appRootPath(/opt/usr/apps/org.example.uibuildersingleview)
12-07 20:27:54.214+0530 I/Tizen::App( 1290): (483) > pkgmgrinfo_appinfo_get_appid(): app = [org.example.uibuildersingleview]
12-07 20:27:54.224+0530 E/cluster-home(  895): mainmenu-package-manager.cpp: _PkgMgrAppInfoGetListCb(253) >  ##### [org.example.uibuildersingleview]
12-07 20:27:54.224+0530 E/cluster-home(  895): mainmenu-package-manager.cpp: _GetAppIds(375) >  ##### [org.example.uibuildersingleview]
12-07 20:27:54.224+0530 E/cluster-home(  895): mainmenu-package-manager.cpp: _GetAppIds(379) >  END
12-07 20:27:54.224+0530 E/cluster-home(  895): mainmenu-package-manager.cpp: _DoPkgJob(458) >  #Step 3 size[1]
12-07 20:27:54.224+0530 E/cluster-home(  895): mainmenu-package-manager.cpp: _DoPkgJob(462) >  appId[org.example.uibuildersingleview]
12-07 20:27:54.224+0530 I/Tizen::App( 1290): (416) > appName = [uibuildersingleview]
12-07 20:27:54.224+0530 I/Tizen::App( 1290): (509) > exe = [/opt/usr/apps/org.example.uibuildersingleview/bin/uibuildersingleview], displayName = [uibuildersingleview], mainApp = [1], menuIconVisible = [0], serviceApp = [0]
12-07 20:27:54.224+0530 E/PKGMGR_INFO( 1290): pkgmgrinfo_appinfo.c: pkgmgrinfo_appinfo_get_list(887) > (component == PMINFO_SVC_APP) PMINFO_SVC_APP is done
12-07 20:27:54.224+0530 I/Tizen::App( 1290): (683) > Application count(1) in this package
12-07 20:27:54.234+0530 I/Tizen::App( 1290): (840) > Enter.
12-07 20:27:54.234+0530 I/Tizen::App( 1290): (703) > Exit.
12-07 20:27:54.234+0530 I/Tizen::App( 1290): (1584) > Package = [org.example.uibuildersingleview], Key = [end], Value = [ok], install = [96]
12-07 20:27:54.244+0530 E/cluster-home(  895): mainmenu-package-manager.cpp: _GetAppInfo(990) >  Name[uibuildersingleview] enable[1] system[0]
12-07 20:27:54.244+0530 E/HOME_APPS(  895): mainmenu-package-manager.cpp: _DoPkgJob(484) >  appId[org.example.uibuildersingleview] mdm is not enabled
12-07 20:27:54.244+0530 E/cluster-home(  895): mainmenu-package-manager.cpp: GetAppInfo(630) >  Find a App Info Name[uibuildersingleview] enable[1] system[0]
12-07 20:27:54.244+0530 W/HOME_APPS(  895): mainmenu-box-impl.cpp: UpdateBoxData(1326) >  update box data!!!!! old icon path[/opt/usr/apps/org.example.uibuildersingleview/shared/res/uibuildersingleview.png], New icon path[/opt/usr/apps/org.example.uibuildersingleview/shared/res/uibuildersingleview.png]!!!!!
12-07 20:27:54.254+0530 I/Tizen::App( 1290): (416) > appName = [uibuildersingleview]
12-07 20:27:54.254+0530 I/Tizen::App( 1290): (509) > exe = [/opt/usr/apps/org.example.uibuildersingleview/bin/uibuildersingleview], displayName = [uibuildersingleview], mainApp = [1], menuIconVisible = [0], serviceApp = [0]
12-07 20:27:54.254+0530 I/Tizen::App( 1290): (2343) > info file is not existed. [/opt/usr/apps/org.exampl/info/org.example.uibuildersingleview.info]
12-07 20:27:54.254+0530 I/Tizen::App( 1290): (131) > Enter
12-07 20:27:54.254+0530 I/Tizen::App( 1290): (137) > org.example.uibuildersingleview does not have launch condition
12-07 20:27:54.254+0530 I/Tizen::App( 1290): (883) > Exit.
12-07 20:27:54.264+0530 E/util-view(  895): util-text-image-provider.cpp: GetPixelsFromText(240) >  DEFAULT='font=SamsungOneUI:style=Regular style=shadow,bottom shadow_color=# font_size=28 align=center valign=top color=#FFFFFF color_class=ATO001 text_class=ATO001 ellipsis=1 wrap=mixed linegap=-3'
12-07 20:27:54.274+0530 W/HOME_APPS(  895): mainmenu-box-impl.cpp: _OnImageLoadFinishedHandler(2152) >  finished path[/opt/usr/apps/org.example.uibuildersingleview/shared/res/uibuildersingleview.png], Loading state:[1]
12-07 20:27:55.515+0530 E/PKGMGR_SERVER( 5295): pkgmgr-server.c: exit_server(1381) > exit_server Start [backend_status=1, queue_status=1, drm_status=1] 
12-07 20:27:55.515+0530 E/PKGMGR_SERVER( 5295): pkgmgr-server.c: main(2471) > package manager server terminated.
12-07 20:27:59.830+0530 W/AUL     ( 5354): launch.c: app_request_to_launchpad(396) > request cmd(0) to(org.example.uibuildersingleview)
12-07 20:27:59.830+0530 W/AUL_AMD (  664): amd_request.c: __request_handler(906) > __request_handler: 0
12-07 20:27:59.840+0530 I/AUL     (  664): menu_db_util.h: _get_app_info_from_db_by_apppath(242) > path : /usr/bin/launch_app, ret : 0
12-07 20:27:59.850+0530 I/AUL     (  664): menu_db_util.h: _get_app_info_from_db_by_apppath(242) > path : /bin/bash, ret : 0
12-07 20:27:59.850+0530 E/AUL_AMD (  664): amd_launch.c: _start_app(2499) > no caller appid info, ret: -1
12-07 20:27:59.850+0530 W/AUL_AMD (  664): amd_launch.c: _start_app(2508) > caller pid : 5354
12-07 20:27:59.850+0530 E/AUL_AMD (  664): amd_appinfo.c: appinfo_get_value(1296) > appinfo get value: Invalid argument, 19
12-07 20:27:59.860+0530 E/RESOURCED(  779): block.c: block_prelaunch_state(134) > insert data org.example.uibuildersingleview, table num : 4
12-07 20:27:59.860+0530 E/RESOURCED(  779): heart-memory.c: heart_memory_get_data(601) > hashtable heart_memory_app_list is NULL
12-07 20:27:59.860+0530 W/AUL_AMD (  664): amd_launch.c: _start_app(3052) > pad pid(-5)
12-07 20:27:59.860+0530 W/AUL_PAD ( 1521): launchpad.c: __launchpad_main_loop(520) > Launch on type-based process-pool
12-07 20:27:59.860+0530 W/AUL_PAD ( 1521): launchpad.c: __send_result_to_caller(267) > Check app launching
12-07 20:27:59.870+0530 I/CAPI_APPFW_APPLICATION( 5056): app_main.c: ui_app_main(789) > app_efl_main
12-07 20:27:59.880+0530 I/CAPI_APPFW_APPLICATION( 5056): app_main.c: _ui_app_appcore_create(641) > app_appcore_create
12-07 20:27:59.920+0530 E/TBM     ( 5056): tbm_bufmgr.c: _tbm_bufmgr_init_state(691) > USE TGL LOCK!
12-07 20:27:59.960+0530 W/AUL     (  664): app_signal.c: aul_send_app_launch_request_signal(423) > send_app_launch_signal, pid: 5056, appid: org.example.uibuildersingleview
12-07 20:27:59.960+0530 E/AUL     (  664): amd_app_group.c: app_group_start_app(1037) > app_group_start_app
12-07 20:27:59.970+0530 W/AUL     ( 5354): launch.c: app_request_to_launchpad(425) > request cmd(0) result(5056)
12-07 20:28:00.150+0530 I/APP_CORE( 5056): appcore-efl.c: __do_app(514) > [APP 5056] Event: RESET State: CREATED
12-07 20:28:00.150+0530 I/CAPI_APPFW_APPLICATION( 5056): app_main.c: _ui_app_appcore_reset(723) > app_appcore_reset
12-07 20:28:00.150+0530 E/EFL     ( 5056): edje<5056> lib/edje/edje_embryo.c:4134 _edje_embryo_test_run() You are running Embryo->EDC->Embryo with script program '_p26';
12-07 20:28:00.150+0530 E/EFL     ( 5056): By the power of Grayskull, your previous Embryo stack is now broken!
12-07 20:28:00.160+0530 E/EFL     ( 5056): edje<5056> lib/edje/edje_embryo.c:4134 _edje_embryo_test_run() You are running Embryo->EDC->Embryo with script program '_p26';
12-07 20:28:00.160+0530 E/EFL     ( 5056): By the power of Grayskull, your previous Embryo stack is now broken!
12-07 20:28:00.160+0530 E/EFL     ( 5056): edje<5056> lib/edje/edje_embryo.c:4134 _edje_embryo_test_run() You are running Embryo->EDC->Embryo with script program '_p26';
12-07 20:28:00.160+0530 E/EFL     ( 5056): By the power of Grayskull, your previous Embryo stack is now broken!
12-07 20:28:00.180+0530 W/APP_CORE( 5056): appcore-efl.c: __show_cb(920) > [EVENT_TEST][EVENT] GET SHOW EVENT!!!. WIN:5600002
12-07 20:28:00.180+0530 W/AUL_AMD (  664): amd_request.c: __request_handler(906) > __request_handler: 35
12-07 20:28:00.180+0530 E/EFL     (  330): eo<330> lib/eo/eo.c:676 _eo_call_resolve() in lib/edje/edje_object.eo.c:316: func 'edje_obj_signal_emit' (415) could not be resolved for class 'Evas_Object_Smart'.
12-07 20:28:00.180+0530 E/EFL     (  330): eo<330> lib/eo/eo.c:676 _eo_call_resolve() in lib/edje/edje_object.eo.c:316: func 'edje_obj_signal_emit' (415) could not be resolved for class 'Evas_Object_Smart'.
12-07 20:28:00.200+0530 I/Tizen::System( 1290): (259) > Active app [org.exampl], current [com.samsun] 
12-07 20:28:00.200+0530 I/Tizen::Io( 1290): (729) > Entry not found
12-07 20:28:00.200+0530 I/Tizen::System( 1290): (157) > change brightness system value.
12-07 20:28:00.210+0530 W/AUL_AMD (  664): amd_request.c: __request_handler(906) > __request_handler: 15
12-07 20:28:00.260+0530 I/APP_CORE(  895): appcore-efl.c: __do_app(514) > [APP 895] Event: PAUSE State: RUNNING
12-07 20:28:00.260+0530 I/CAPI_APPFW_APPLICATION(  895): app_main.c: _ui_app_appcore_pause(689) > app_appcore_pause
12-07 20:28:00.260+0530 I/APP_CORE( 5056): appcore-efl.c: __do_app(514) > [APP 5056] Event: RESUME State: CREATED
12-07 20:28:00.260+0530 E/cluster-home(  895): homescreen.cpp: OnPause(260) >  app pause
12-07 20:28:00.260+0530 W/AUL_AMD (  664): amd_launch.c: __e17_status_handler(3328) > pid(895) status(4)
12-07 20:28:00.260+0530 W/AUL     (  664): amd_app_group.c: __set_fg_flag(190) > send_signal BG com.samsung.homescreen(895)
12-07 20:28:00.260+0530 W/AUL     (  664): app_signal.c: aul_send_app_status_change_signal(581) > send_app_status_change_signal, pid: 895, appid: com.samsung.homescreen, status: bg
12-07 20:28:00.260+0530 W/AUL_AMD (  664): amd_launch.c: __e17_status_handler(3328) > pid(5056) status(3)
12-07 20:28:00.270+0530 W/AUL_AMD (  664): amd_key.c: _key_ungrab(269) > fail(-1) to ungrab key(XF86Back)
12-07 20:28:00.270+0530 W/AUL_AMD (  664): amd_launch.c: __e17_status_handler(3349) > back key ungrab error
12-07 20:28:00.270+0530 W/AUL     (  664): amd_app_group.c: __set_fg_flag(180) > send_signal FG org.example.uibuildersingleview(5056)
12-07 20:28:00.270+0530 W/AUL     (  664): app_signal.c: aul_send_app_status_change_signal(581) > send_app_status_change_signal, pid: 5056, appid: org.example.uibuildersingleview, status: fg
12-07 20:28:00.270+0530 I/APP_CORE( 5056): appcore-efl.c: __do_app(623) > Legacy lifecycle: 0
12-07 20:28:00.270+0530 I/APP_CORE( 5056): appcore-efl.c: __do_app(625) > [APP 5056] Initial Launching, call the resume_cb
12-07 20:28:00.270+0530 I/CAPI_APPFW_APPLICATION( 5056): app_main.c: _ui_app_appcore_resume(706) > app_appcore_resume
12-07 20:28:00.580+0530 I/INDICATOR(  776): clock.c: indicator_clock_changed_cb(195) > ""
12-07 20:28:00.590+0530 E/UXT     (  776): uxt_util.c: uxt_util_get_date_time_text_by_locale(389) > text : 8:28 p.m.
12-07 20:28:00.590+0530 I/INDICATOR(  776): clock.c: getTimeFormatted(176) > "time format : 8:28 p.m."
12-07 20:28:00.590+0530 I/INDICATOR(  776): clock.c: indicator_clock_changed_cb(244) > "time format : 8:28 p.m."
12-07 20:28:00.590+0530 W/INDICATOR(  776): clock.c: indicator_clock_changed_cb(272) > 
12-07 20:28:00.590+0530 I/INDICATOR(  776): clock.c: indicator_clock_changed_cb(317) > "[CLOCK MODULE] Timer Status : -2146500912 Time: <font_size=31> </font_size> <font_size=31> 8:28 p.m.</font_size></font>"
12-07 20:28:00.630+0530 W/AUL_AMD (  664): amd_launch.c: __e17_status_handler(3328) > pid(5056) status(0)
12-07 20:28:01.011+0530 E/RESOURCED(  779): resourced-dbus.c: resourced_dbus_system_hash_drop_busname(324) > Does not exist in busname hash: :1.427
12-07 20:28:01.021+0530 I/Tizen::App( 1290): (499) > LaunchedApp(org.example.uibuildersingleview)
12-07 20:28:01.031+0530 I/Tizen::App( 1290): (733) > Finished invoking application event listener for org.example.uibuildersingleview, 5056.
12-07 20:28:01.761+0530 I/UXT     ( 5367): Uxt_ObjectManager.cpp: OnInitialized(737) > Initialized.
12-07 20:28:02.112+0530 E/EFL     ( 5056): ecore_x<5056> lib/ecore_x/xlib/ecore_x_events.c:603 _ecore_x_event_handle_button_press() ButtonEvent:press time=9034594
12-07 20:28:02.232+0530 E/EFL     ( 5056): ecore_x<5056> lib/ecore_x/xlib/ecore_x_events.c:756 _ecore_x_event_handle_button_release() ButtonEvent:release time=9034714
12-07 20:28:02.242+0530 E/VCONF   ( 5056): vconf.c: _vconf_check_retry_err(1368) > db/ise/keysound : check retry err (-21/13).
12-07 20:28:02.242+0530 E/VCONF   ( 5056): vconf.c: _vconf_get_key_filesys(2371) > _vconf_get_key_filesys(db/ise/keysound) step(-21) failed(13 / Permission denied) retry(0) 
12-07 20:28:02.242+0530 E/VCONF   ( 5056): vconf.c: _vconf_get_key(2407) > _vconf_get_key(db/ise/keysound) step(-21) failed(13 / Permission denied)
12-07 20:28:02.242+0530 E/VCONF   ( 5056): vconf.c: vconf_get_bool(2729) > vconf_get_bool(5056) : db/ise/keysound error
12-07 20:28:02.242+0530 E/VCONF   ( 5056): vconf-kdb.c: _vconf_kdb_add_notify(318) > _vconf_kdb_add_notify : add noti(Permission denied)
12-07 20:28:02.242+0530 E/VCONF   ( 5056): vconf.c: vconf_notify_key_changed(3168) > vconf_notify_key_changed : key(db/ise/keysound) add notify fail
12-07 20:28:02.452+0530 E/EFL     ( 5056): ecore_x<5056> lib/ecore_x/xlib/ecore_x_events.c:603 _ecore_x_event_handle_button_press() ButtonEvent:press time=9034936
12-07 20:28:02.592+0530 E/EFL     ( 5056): ecore_x<5056> lib/ecore_x/xlib/ecore_x_events.c:756 _ecore_x_event_handle_button_release() ButtonEvent:release time=9035067
12-07 20:28:02.883+0530 E/EFL     ( 5056): ecore_x<5056> lib/ecore_x/xlib/ecore_x_events.c:603 _ecore_x_event_handle_button_press() ButtonEvent:press time=9035359
12-07 20:28:03.013+0530 E/EFL     ( 5056): ecore_x<5056> lib/ecore_x/xlib/ecore_x_events.c:756 _ecore_x_event_handle_button_release() ButtonEvent:release time=9035491
12-07 20:28:03.363+0530 E/EFL     ( 5056): ecore_x<5056> lib/ecore_x/xlib/ecore_x_events.c:603 _ecore_x_event_handle_button_press() ButtonEvent:press time=9035858
12-07 20:28:03.463+0530 E/EFL     ( 5056): ecore_x<5056> lib/ecore_x/xlib/ecore_x_events.c:756 _ecore_x_event_handle_button_release() ButtonEvent:release time=9035957
12-07 20:28:03.613+0530 I/AUL_PAD ( 1521): sigchild.h: __launchpad_process_sigchld(162) > dead_pid = 5056 pgid = 5056
12-07 20:28:03.613+0530 I/AUL_PAD ( 1521): sigchild.h: __sigchild_action(143) > dead_pid(5056)
12-07 20:28:03.613+0530 E/EFL     (  330): eo<330> lib/eo/eo.c:676 _eo_call_resolve() in lib/edje/edje_object.eo.c:316: func 'edje_obj_signal_emit' (415) could not be resolved for class 'Evas_Object_Smart'.
12-07 20:28:03.633+0530 W/AUL_AMD (  664): amd_launch.c: __e17_status_handler(3328) > pid(895) status(3)
12-07 20:28:03.633+0530 W/AUL_AMD (  664): amd_key.c: _key_ungrab(269) > fail(-1) to ungrab key(XF86Back)
12-07 20:28:03.633+0530 W/AUL_AMD (  664): amd_launch.c: __e17_status_handler(3349) > back key ungrab error
12-07 20:28:03.633+0530 W/AUL     (  664): amd_app_group.c: __set_fg_flag(180) > send_signal FG com.samsung.homescreen(895)
12-07 20:28:03.633+0530 W/AUL     (  664): app_signal.c: aul_send_app_status_change_signal(581) > send_app_status_change_signal, pid: 895, appid: com.samsung.homescreen, status: fg
12-07 20:28:03.673+0530 I/AUL_PAD ( 1521): sigchild.h: __sigchild_action(149) > __send_app_dead_signal(0)
12-07 20:28:03.673+0530 I/AUL_PAD ( 1521): sigchild.h: __launchpad_process_sigchld(170) > after __sigchild_action
12-07 20:28:03.673+0530 E/AUL_PAD ( 1521): launchpad.c: main(698) > error reading sigchld info
12-07 20:28:03.673+0530 W/AUL_AMD (  664): amd_launch.c: __e17_status_handler(3328) > pid(895) status(0)
12-07 20:28:03.673+0530 I/ESD     (  941): esd_main.c: __esd_app_dead_handler(1773) > pid: 5056
12-07 20:28:03.673+0530 I/Tizen::App( 1290): (243) > App[org.example.uibuildersingleview] pid[5056] terminate event is forwarded
12-07 20:28:03.673+0530 I/Tizen::System( 1290): (256) > osp.accessorymanager.service provider is found.
12-07 20:28:03.673+0530 I/Tizen::System( 1290): (196) > Accessory Owner is removed [org.example.uibuildersingleview, 5056, ]
12-07 20:28:03.673+0530 I/Tizen::System( 1290): (256) > osp.system.service provider is found.
12-07 20:28:03.673+0530 I/Tizen::App( 1290): (506) > TerminatedApp(org.example.uibuildersingleview)
12-07 20:28:03.673+0530 I/Tizen::App( 1290): (512) > Not registered pid(5056)
12-07 20:28:03.673+0530 I/Tizen::System( 1290): (246) > Terminated app [org.example.uibuildersingleview]
12-07 20:28:03.673+0530 I/Tizen::Io( 1290): (729) > Entry not found
12-07 20:28:03.673+0530 W/AUL_AMD (  664): amd_main.c: __app_dead_handler(327) > __app_dead_handler, pid: 5056
12-07 20:28:03.673+0530 W/AUL_AMD (  664): amd_main.c: __app_dead_handler(333) > app_group_leader_app, pid: 5056
12-07 20:28:03.693+0530 I/Tizen::System( 1290): (157) > change brightness system value.
12-07 20:28:03.693+0530 I/Tizen::App( 1290): (782) > Finished invoking application event listener for org.example.uibuildersingleview, 5056.
12-07 20:28:03.703+0530 I/APP_CORE(  895): appcore-efl.c: __do_app(514) > [APP 895] Event: RESUME State: PAUSED
12-07 20:28:03.703+0530 I/CAPI_APPFW_APPLICATION(  895): app_main.c: _ui_app_appcore_resume(706) > app_appcore_resume
12-07 20:28:03.703+0530 E/cluster-home(  895): homescreen.cpp: OnResume(233) >  app resume
12-07 20:28:03.703+0530 E/EFL     (  330): eo<330> lib/eo/eo.c:676 _eo_call_resolve() in lib/edje/edje_object.eo.c:316: func 'edje_obj_signal_emit' (415) could not be resolved for class 'Evas_Object_Smart'.
12-07 20:28:03.723+0530 I/Tizen::System( 1290): (259) > Active app [com.samsun], current [org.exampl] 
12-07 20:28:03.723+0530 I/Tizen::Io( 1290): (729) > Entry not found
12-07 20:28:03.753+0530 W/CRASH_MANAGER( 5374): worker.c: worker_job(1199) > 1105056756962148112268
